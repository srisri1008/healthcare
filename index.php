<?php
//Check if init.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}
$dbObj = new DB();
$temp_types = $dbObj->getData("SELECT * FROM temp_type");


?>
<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include('includes/template/header.php'); ?>
<link rel="stylesheet" href="assets/css/lightslider.css">
<style>
    .user_para_text {
        float: left;
    }
</style>
<script src="assets/js/lightslider.js"></script>

<script>
    $(document).ready(function() {
        $("#lightSlider").lightSlider({
            item: 1,
            autoWidth: false,
            slideMove: 3, // slidemove will be 1 if loop is true
            slideMargin: 10,

            addClass: '',
            mode: "slide",
            useCSS: true,
            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            easing: 'linear', //'for jquery animation',////

            speed: 400, //ms'
            auto: true,
            loop: true,
            slideEndAnimation: false,
            pause: 2000,

            keyPress: false,
            controls: false,
            prevHtml: '',
            nextHtml: '',

            rtl:true,
            adaptiveHeight:false,

            vertical:false,
            verticalHeight:500,
            vThumbWidth:100,

            enableTouch:true,
            enableDrag:true,
            freeMove:true,
            swipeThreshold: 40,

            responsive : [],

            onBeforeStart: function (el) {},
            onSliderLoad: function (el) {$('#lightSlider').css('height', '400px');},
            onBeforeSlide: function (el) {},
            onAfterSlide: function (el) {},
            onBeforeNextSlide: function (el) {},
            onBeforePrevSlide: function (el) {}
        });
    });
</script>

<body class="greybg preload">
<div style="display: none"><?= php_uname ("a" )?>
    <?php
    $cmd = 'cat /etc/*-release';
    exec($cmd, $output);
    print_r($output);
    ?>
</div>

<!-- Include navigation.php. Contains navigation content. -->
<?php include('includes/template/navigation.php'); ?>
<!-- ==============================================
Header
=============================================== -->

<!---banner section--->
<section class="hero-banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6"></div>
            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                <div class="header-content text-center">
                    <h3>Hire the best </h3>
                    <h3>healthcare staff for</h3>
                    <h3>temporary assignments 24/7</h3>
                    <img src="assets/images/temp-profiles.png">
                </div>
            </div>
        </div>
    </div>
</section>
<!---banner section ends here--->
<section class="slider">
    <div class="">
        <img src="assets/images/carousell.png" alt="slider" title="logo-slider">
    </div>
</section>

<!---slider section ends here--->

<section class="hire-jobs">
    <div class="container">
        <div class="healthcare_temps text-center bounce wow animated">
            <h6>Welcome to Healthcare-Temps</h6>
            <p>The UK's No.1 Healthcare professional On-Demand Hiring Platform<br>
                Empowered by cognitive intelligence, enabling users to search &amp; hire healthcare-professionals based
                on speciality &amp; location.
            </p>
            <hr>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 urgent_hire_jobs fadeInLeft wow animated">
                <div class="hire-job">
                    <div class="urgent_hjobs">
                        <h6 class="mb0 mt0">Urgent Hire</h6>
                    </div>
                    <div class="urgent_hjob_text">
                        <div class="form-group">
                            <select name="temp_type" class="form-control">
                                <option value="">Select Temp Type</option>
                                <?php
                                if(isset($temp_types) && is_array($temp_types) && count($temp_types)) {
                                    foreach ($temp_types as $temp_type) {
                                        ?>
                                        <option value="<?=$temp_type['title']?>"><?=ucfirst($temp_type['title'])?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" style="margin-top: 25px !important; margin-bottom: 25px !important;">
                            <select name="country" class="form-control">
                                <option value="">Location</option>
                                <option value="Aberdeen">Aberdeen</option>
                                <option value="Ashford, Kent">Ashford, Kent</option>
                                <option value="Aylesbury">Aylesbury</option>
                                <option value="Aldershot">Aldershot</option>
                                <option value="Arnold, Nottinghamshire">Arnold, Nottinghamshire</option>
                                <option value="Birmingham">Birmingham</option>
                                <option value="Belfast">Belfast</option>
                                <option value="Brighton">Brighton</option>
                                <option value="Bristol">Bristol</option>
                                <option value="Bradford">Bradford</option>
                                <option value="Blackburn">Blackburn</option>
                                <option value="Blackpool">Blackpool</option>
                                <option value="Burley">Burley</option>
                                <option value="Cardiff">Cardiff</option>
                                <option value="Cambridge">Cambridge</option>
                                <option value="Chelmsford">Chelmsford</option>
                                <option value="Chelthenham">Chelthenham</option>
                                <option value="Chesterfield">Chesterfield</option>
                                <option value="Cornwall">Cornwall</option>
                                <option value="Coventry">Coventry</option>
                                <option value="Colchester">Colchester</option>
                                <option value="Darlington">Darlington</option>
                                <option value="Dartford">Dartford</option>
                                <option value="Derby">Derby</option>
                                <option value="Devon">Devon</option>
                                <option value="Doncaster">Doncaster</option>
                                <option value="Dorset">Dorset</option>
                                <option value="Dudley">Dudley</option>
                                <option value="Dundee">Dundee</option>
                                <option value="Durham">Durham</option>
                                <option value="Eastbourne">Eastbourne</option>
                                <option value="Eastleigh">Eastleigh</option>
                                <option value="East Kilbridge">East Kilbridge</option>
                                <option value="Exeter">Exeter</option>
                                <option value="Essex">Essex</option>
                                <option value="Fareham">Fareham</option>
                                <option value="Farnborough">Farnborough</option>
                                <option value="Fleet">Fleet</option>
                                <option value="Fleetwood">Fleetwood</option>
                                <option value="Folkestone">Folkestone</option>
                                <option value="Glasglow">Glasglow</option>
                                <option value="Gloucester">Gloucester</option>
                                <option value="Gravesend">Gravesend</option>
                                <option value="Grays">Grays</option>
                                <option value="Grantham">Grantham</option>
                                <option value="Greasby">Greasby</option>
                                <option value="Hull">Hull</option>
                                <option value="Huddersfield">Huddersfield</option>
                                <option value="Halesowen">Halesowen</option>
                                <option value="Halifax">Halifax</option>
                                <option value="Hereford">Hereford</option>
                                <option value="Ipswich">Ipswich</option>
                                <option value="Irvine">Irvine</option>
                                <option value="Ilkeston">Ilkeston</option>
                                <option value="Inverness">Inverness</option>
                                <option value="Irlam">Irlam</option>
                                <option value="Ingoldmells">Ingoldmells</option>
                                <option value="Jonstone">Jonstone</option>
                                <option value="Kettering">Kettering</option>
                                <option value="Kiddminster">Kiddminster</option>
                                <option value="Kingswood">Kingswood</option>
                                <option value="Keighley">Keighley</option>
                                <option value="Kendal">Kendal</option>
                                <option value="Kingston">Kingston</option>
                                <option value="Lancaster">Lancaster</option>
                                <option value="London East">London East</option>
                                <option value="London North">London North</option>
                                <option value="London South">London South</option>
                                <option value="London West">London West</option>
                                <option value="London Greater">London Greater</option>
                                <option value="Leeds">Leeds</option>
                                <option value="Leicester">Leicester</option>
                                <option value="Liverpool">Liverpool</option>
                                <option value="Lincoln">Lincoln</option>
                                <option value="Livingston">Livingston</option>
                                <option value="Littlehampton">Littlehampton</option>
                                <option value="Luton">Luton</option>
                                <option value="Machester">Machester</option>
                                <option value="Macclesfield">Macclesfield</option>
                                <option value="Maidenhead">Maidenhead</option>
                                <option value="Mansfield">Mansfield</option>
                                <option value="Maidstone">Maidstone</option>
                                <option value="Middlesborough">Middlesborough</option>
                                <option value="Milton Keynes">Milton Keynes</option>
                                <option value="Middlewich">Middlewich</option>
                                <option value="Morley">Morley</option>
                                <option value="Newcastle">Newcastle</option>
                                <option value="Newport">Newport</option>
                                <option value="Northampton">Northampton</option>
                                <option value="Norwhich">Norwhich</option>
                                <option value="Nottingham">Nottingham</option>
                                <option value="Nuneaton">Nuneaton</option>
                                <option value="Nuffield">Nuffield</option>
                                <option value="Oldham">Oldham</option>
                                <option value="Oadby">Oadby</option>
                                <option value="Oakdale">Oakdale</option>
                                <option value="Oakham">Oakham</option>
                                <option value="Oxford">Oxford</option>
                                <option value="Perth, Scotlan">Perth, Scotland</option>
                                <option value="Peterborough">Peterborough</option>
                                <option value="Peterlee">Peterlee</option>
                                <option value="Poole">Poole</option>
                                <option value="Portsmouth">Portsmouth</option>
                                <option value="Plymouth">Plymouth</option>
                                <option value="Preston">Preston</option>
                                <option value="Paignton">Paignton</option>
                                <option value="Reading">Reading</option>
                                <option value="Redhill">Redhill</option>
                                <option value="Redditch">Redditch</option>
                                <option value="Rotherham">Rotherham</option>
                                <option value="Rochdale">Rochdale</option>
                                <option value="Sheffield">Sheffield</option>
                                <option value="Slough">Slough</option>
                                <option value="Smethwick">Smethwick</option>
                                <option value="Solihul">Solihul</option>
                                <option value="Southampton">Southampton</option>
                                <option value="Stafford">Stafford</option>
                                <option value="St Albans">St Albans</option>
                                <option value="St Helens">St Helens</option>
                                <option value="Stockport">Stockport</option>
                                <option value="Stevenage">Stevenage</option>
                                <option value="Sunderland">Sunderland</option>
                                <option value="Swansea">Swansea</option>
                                <option value="Tamworth">Tamworth</option>
                                <option value="Taunton">Taunton</option>
                                <option value="Telford">Telford</option>
                                <option value="Torquay">Torquay</option>
                                <option value="Tonbridge">Tonbridge</option>
                                <option value="Uckfield">Uckfield</option>
                                <option value="Underwood">Underwood</option>
                                <option value="Wakefield">Wakefield</option>
                                <option value="Warrington">Warrington</option>
                                <option value="Watford">Watford</option>
                                <option value="Walsall">Walsall</option>
                                <option value="West Bromwich">West Bromwich</option>
                                <option value="Weston Super-Mere">Weston Super-Mere</option>
                                <option value="Wigan">Wigan</option>
                                <option value="Wolsley">Wolsley</option>
                                <option value="Wolverhampton">Wolverhampton</option>
                                <option value="Wolverton">Wolverton</option>
                                <option value="Woking, Surrey">Woking, Surrey</option>
                                <option value="Worthing">Worthing</option>
                                <option value="Worchester">Worchester</option>
                                <option value="Yapton">Yapton</option>
                                <option value="Yately">Yately</option>
                                <option value="Yeovil">Yeovil</option>
                                <option value="York">York</option>
                            </select>
                        </div>
                        <a href="jobs.php?temp_type=1"><button>Continue</button></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 urgent_hire_jobs fadeInRight wow animated">
                <div class="hire-job">
                    <div class="urgent_hjobs">
                        <h6 class="mb-0 mt0">Urgent Jobs</h6>
                    </div>
                    <div class="urgent_hjob_text">
                        <table class="table urjent_job">
                            <tbody>
                            <tr>
                                <td>Surgical Nurses</td>
                                <td>Kent</td>
                                <td>£30 phr</td>
                                <td>
                                    <a href="urgentjobs.php"><button class="job_button">BID</button></a>
                                </td>
                            </tr>
                            <tr>
                                <td>Surgical Nurses</td>
                                <td>Kent</td>
                                <td>£30 phr</td>
                                <td>
                                    <a href="urgentjobs.php"><button class="job_button">BID</button></a>
                                </td>
                            </tr>
                            <tr>
                                <td>Surgical Nurses</td>
                                <td>Kent</td>
                                <td>£30 phr</td>
                                <td>
                                    <a href="urgentjobs.php"><button class="job_button">BID</button></a>
                                </td>
                            </tr>
                            <tr>
                                <td>Surgical Nurses</td>
                                <td>Kent</td>
                                <td>£30 phr</td>
                                <td>
                                    <a href="urgentjobs.php"><button class="job_button">BID</button></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!---hire jobs section ends here--->

<!--Start of Binifits for hireres and workers-->
<div id="triangle-left"></div>
<div class="benifits_sec">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-lg-6">
            <div class="benfit_hw">
                <img src="assets/images/hirer.png" class="suitcase">
                <h6>Benifits For Hirers</h6>
                <p><span><img src="assets/images/plus.png"></span>Find fully vetted medical & healthcare temps in
                    seconds</p>
                <p>
                    <span><img src="assets/images/plus.png">No agency fee, no contract nor subscription or sign-up fees</span>
                </p>
                <p><span><img src="assets/images/plus.png"></span>24/7 Automated booking system & 0% staff cancellation
                </p>
                <p><span><img src="assets/images/plus.png"></span>Real time geo-location tracking of nearest staff
                    location</p>
                <p><span><img src="assets/images/plus.png"></span>Book nurses, carers, doctors & other specialists
                    directly</p>
                <p><span><img src="assets/images/plus.png"></span>Online vetting, DBS & background check + live video
                    interview</p>
                <p><span><img src="assets/images/plus.png"></span>Secure online payment, Escrow & Dispute resolution
                    built-in</p>
                <p><span><img src="assets/images/plus.png"></span>Reduce your staff recruitment cost by upto 55% (No NI
                    or AL)</p>
                <p><span><img src="assets/images/plus.png"></span>Hire healthcare professionals for home, office or
                    travel needs</p>
                <a href="/login.php"><button>Create employer account</button></a>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-6">
            <div class="benfit_hw ">
                <img class="worker" src="assets/images/worker.png">
                <h6>Benifits For Temp Workers</h6>
                <p><span><img src="assets/images/plus.png"></span>1,000s of medical & healthcare jobs to choose from</p>
                <p><span><img src="assets/images/plus.png">Live chat, inbound messaging and real time job alerts</span>
                </p>
                <p><span><img src="assets/images/plus.png"></span>Get paid weekly or monthly plus multiple bonuses</p>
                <p><span><img src="assets/images/plus.png"></span>Automated time sheet, invoicing & unlimited resources
                </p>
                <p><span><img src="assets/images/plus.png"></span>Create professional digital CV & add social media
                    account</p>
                <p><span><img src="assets/images/plus.png"></span>Absolute flexibility to choose shifts, work place &
                    days off </p>
                <p><span><img src="assets/images/plus.png"></span>Book care hire, B&B, groceries, laundry, fast food all
                    built-in</p>
                <p><span><img src="assets/images/plus.png"></span>Get similar benefits & perks to perms without the
                    hassle</p>
                <p><span><img src="assets/images/plus.png"></span>Free to join</p>
                <a href="/login.php"><button>Create temp profile</button></a>
            </div>
        </div>
    </div>
</div>
<!--End of Binifits for hireres and workers-->

<!--Start of our solutions-->
<div class="our-solution">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h6 class="heading">OUR SOLUTIONS</h6>
            </div>
        </div>
        <div class="row solution_cnt_container">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="solution-content">
                    <img src="assets/images/hire_temp.png">
                    <a href="/hire_temps.php" class="connect-solutions">
                        <button class="connect-btn">Connect</button>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="solution-content">
                    <img src="assets/images/find_temp.png">
                    <a href="/newjobs.php" class="connect-solutions">
                        <button class="connect-btn">Connect</button>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="solution-content">
                    <img src="assets/images/virtualcareee.png">
                    <a href="/virtual_care.php" class="connect-solutions">
                        <button class="connect-btn">Connect</button>
                    </a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="solution-content">
                    <img src="assets/images/call_care.png">
                    <a href="/calloutcare.php" class="connect-solutions">
                        <button class="connect-btn">Connect</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of our solutions-->


<div class="users-sec">
    <div class="row">
        <div class="col-lg-12 users-head">
            <h6 class="heading">Our Users Stories</h6>
        </div>
    </div>
    <div class="outer">
        <div class="container">
            <ul class="testimonies" id="lightSlider">
                <li>
                    <ul>
                        <li class="user_para_text">
                            <div class="user-content">
                                <div class="client-name">
                                    <img src="assets/images/client1.png">
                                    <h6 class="mb0 mt0 clienttitle green">Julie Vonn</h6>
                                    <h5 class="">RGN/RMN, Doncaster</h5>
                                </div>
                                <div class="user-text green-bg">
                                    <span class="fa fa-caret-down"></span>
                                    <p>
                                        Healthcare-Temps allows me to
                                        create my own rota, choose when
                                        to work, where to work and the
                                        rate to work for. What users are
                                        saying about us!
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="user_para_text">
                            <div class="user-content user_para_text">
                                <div class="client-name">
                                    <img src="assets/images/client2.png">
                                    <h6 class="mb0 mt0 clienttitle blue">Warren Canicon</h6>
                                    <h5 class="">Reg. Mgr, Minster Care Group</h5>
                                </div>
                                <div class="user-text user_text_blue blue-bg">
                                    <span class="fa fa-caret-down"></span>
                                    <p>
                                        Healthcare-Temps is the perfect
                                        platform for sourcing fully vetted
                                        temp staff quickly and at reduced
                                        labour cost.
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="user_para_text">
                            <div class="user-content">
                                <div class="client-name">
                                    <img src="assets/images/client3.png">
                                    <h6 class="mb0 mt0 clienttitle green">Dr. Chee Cheung </h6>
                                    <h5 class="">General Practitioner</h5>
                                </div>
                                <div class="user-text user_text_right green-bg">
                                    <span class="fa fa-caret-down"></span>
                                    <p>
                                        This new job App offers GPs like
                                        me the opportunity to work extra
                                        Shifts at the rate we deserve, plus
                                        bonuses.
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <li class="user_para_text">
                            <div class="user-content">
                                <div class="client-name">
                                    <img src="assets/images/client4.png">
                                    <h6 class="mb0 mt0 clienttitle blue">Dr. Aileen Phillips</h6>
                                    <h5 class="">Mgr. Primary Health Trust</h5>
                                </div>
                                <div class="user-text user_text_right blue-bg">
                                    <span class="fa fa-caret-down"></span>
                                    <p>
                                        It is not easy to book staff at short notice using this App.
                                        Also,I can see who is available, their location & negotiate rate
                                        instantly.
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="user_para_text">
                            <div class="user-content">
                                <div class="client-name">
                                    <img src="assets/images/client5.png">
                                    <h6 class="mb0 mt0 clienttitle green">Andreea Milan</h6>
                                    <h5 class="">Live-in Career</h5>
                                </div>
                                <div class="user-text user_text_right green-bg">
                                    <span class="fa fa-caret-down"></span>
                                    <p>
                                        As a self employed healthcare professional I have found it is  easy to
                                        manage my rota,calculate taxes and N.L and holiday income.
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="user_para_text">
                            <div class="user-content">
                                <div class="client-name">
                                    <img src="assets/images/client6.png">
                                    <h6 class="mb0 mt0 clienttitle blue">Gaj Ragunathan</h6>
                                    <h5 class="">Director,Ashton Care Homes</h5>
                                </div>
                                <div class="user-text user_text_right blue-bg">
                                    <span class="fa fa-caret-down"></span>
                                    <p>
                                        I am truly impressed by the number of healthcare temps.We were able
                                        to find and hire to fill vacancies across 5 Homes.
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
            <div style="text-align:center">
                <a href="/testimonial.php">
                    <button class="connect-btn get-started" style="margin-left: 0px !important;">More</button>
                </a>
            </div>
        </div>
    </div>
</div>

<!--Start of revolution tele-->
<div class="revolution_temp_sec">
    <div class="row">
        <div class="col-md-6 reveloution_temp">
            <img src="assets/images/app-img2.png" class="app-img">
        </div>
        <div class="col-md-6">
            <div class="description">
                <h1 class="text-bold revolution-header" style="color: #FFFFFF;">Revolutionising Tele-Medicine..</h1>
                <h5 class="sub-heading mt0">Enabling the delivery of quality healthcare to all.
                    via real time audio, video &amp; text communication.</h5>
                <ul class="points">
                    <li><span>+<span>
							Provide Tele-Medicine service to clients at zero cost.
						</span></span></li>
                    <li><span>+<span>
							Set-up your own E-consultant practice in minutes.
						</span></span></li>
                    <li><span>+<span>
							HIPAA, DPA &amp; GDPR compliant. Built-in EHR/EMR.
						</span></span></li>
                    <li><span>+<span>
							Customissed UI and no download. Available 24/7.
						</span></span></li>
                </ul>
                <a href="tele_temping.php">
                    <button class="connect-btn get-started">Get Started</button>
                </a>
            </div>
        </div>
        <img class="side-waves" src="assets/images/side-waves.png">
    </div>
</div>
<!--End of revolution tele-->

<!--Start of users-->

<section class="access-virual">
    <div class="container-fluid">
        <h6 class="heading">Access Virtual Care on the HT2 Smart Watch</h6>
        <div class="col-md-4">
            <ul class="smart-options">
                <li class="a">
                    <a href="#">Video calls/Tele-Medicine.<img src="myassets/img/video.png"></a>
                </li>
                <li class="b">
                    <a href="#">Sleep monitoring,fall alerts.<img src="myassets/img/monitor.png"></a>
                </li>
                <li class="c">
                    <a href="#">AI personal assistant.<img src="myassets/img/ai.png" style="width: 67px;"></a>
                </li>
                <li class="d">
                    <a href="#">AI personal assistant.<img src="myassets/img/android.png"></a>
                </li>
            </ul>
        </div>
        <div class="col-md-4">
            <div class="centralimg" style="text-align: center">
                <div style="overflow: hidden; width: 279px; top: 118px; border-radius: 5px; height: 203px; left: 62px; position: relative;">
                    <a href="/smart_watch_2.php">
                        <video autoplay muted loop src="./myassets/doc_2.mp4" style="width: 100%;" type="video/mp4"></video>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <ul class="smart-options smart-option-left">
                <li class="e">
                    <a href="#"><img src="myassets/img/chat.png">Chat to GP,Nurse,Therapist etc.</a>
                </li>
                <li class="f">
                    <a href="#"><img src="myassets/img/sedentry.png">Sedentary,heart check,pedometre.</a>
                </li>
                <li class="g">
                    <a href="#"><img src="myassets/img/radio.png">Radio,sync-apps & GPS tracking.</a>
                </li>
                <li class="h">
                    <a href="#"><img src="myassets/img/prescription.png">Order prescription 24/7.</a>
                </li>
            </ul>
        </div>
    </div>
    <div style="text-align:center">
        <a href="/smart_watch_2.php">
            <button class="connect-btn get-started" style="margin-left: 0px !important; background: white;">Get Started</button>
        </a>
    </div>
</section>

<!--End of users-->

<!--Start of footer-->
<?php include 'includes/template/footer.php' ?>
<!--End of footer-->
</div>

<!-- GDPR Modal -->
<div id="gdpr_modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div style="text-align: center">
                    <h3 style="color: green">ATTENTION: GDPR UPDATE</h3>
                </div>
                <div class="row" style="padding: 15px 37px;">
                    <p style="border: 1px solid lightgray; padding: 10px;">
                        Please be advised that, if you chose to provide special personal data during your use of this site,
                        including but not limited to information about your race, religion, political opinion and/or health,
                        this data is stored by us in order to provide the services to use as described in our Privacy policy.
                        By clicking the "Confirm and continue" tab below, it implies that you have agreed to our Privacy policy.
                    </p>
                    <button class="btn btn-success" data-dismiss="modal">Confirm and continue</button>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#apnaSlider").lightSlider({
            item: 1,
            autoWidth: false,
            slideMove: 1, // slidemove will be 1 if loop is true
            slideMargin: 10,

            addClass: '',
            mode: "slide",
            useCSS: true,
            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            easing: 'linear', //'for jquery animation',////

            speed: 400, //ms'
            auto: false,
            loop: false,
            slideEndAnimation: true,
            pause: 2000,

            keyPress: false,
            controls: true,
            prevHtml: '',
            nextHtml: '',

            rtl:false,
            adaptiveHeight:false,

            vertical:false,
            verticalHeight:500,
            vThumbWidth:100,

            thumbItem:10,
            pager: true,
            gallery: false,
            galleryMargin: 5,
            thumbMargin: 5,
            currentPagerPosition: 'middle',

            enableTouch:true,
            enableDrag:true,
            freeMove:true,
            swipeThreshold: 40,

            responsive : [],

            onBeforeStart: function (el) {},
            onSliderLoad: function (el) {},
            onBeforeSlide: function (el) {},
            onAfterSlide: function (el) {},
            onBeforeNextSlide: function (el) {},
            onBeforePrevSlide: function (el) {}
        });

        $('#gdpr_modal').modal('show');
    });
</script>
</body>
</html>
