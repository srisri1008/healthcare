<?php
//Check if frontinit.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

if ($_GET['id'] == '') {
    Redirect::to('register.php');
}


//Get Site Settings Data
$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
    foreach ($query->results() as $row) {
        $title = $row->title;
        $use_icon = $row->use_icon;
        $site_icon = $row->site_icon;
        $tagline = $row->tagline;
        $description = $row->description;
        $keywords = $row->keywords;
        $author = $row->author;
        $bgimage = $row->bgimage;
    }
}

//Get Payments Settings Data
$q1 = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($q1->count()) {
    foreach ($q1->results() as $r1) {
        $currency = $r1->currency;
        $membershipid = $r1->membershipid;
    }
}

//Getting Payement Id from Database
$query = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
if ($query->count() === 1) {
    $q1 = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
} else {
    $q1 = DB::getInstance()->get("membership_agency", "*", ["membershipid" => $membershipid]);
}
if ($q1->count() === 1) {
    foreach ($q1->results() as $r1) {
        $bids = $r1->bids;
    }
}

if (!function_exists('base_url')) {
    function base_url($atRoot = FALSE, $atCore = FALSE, $parse = FALSE)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            $core = $core[0];

            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf($tmplt, $http, $hostname, $end);
        } else $base_url = 'http://localhost/';

        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }

        return $base_url;
    }
}
$conn = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
$conn->exec("SET CHARACTER SET utf8");
$sql = "SELECT * FROM freelancer WHERE freelancerid=" . $_GET['id'];
$result = $conn->query($sql);
$row = $result->fetch(PDO::FETCH_NUM);
$msg = '';
if (isset($row[0]) && $row[0] != '') {
    if(isset($_POST['_method'])) {
        foreach ($_POST as $file) {
            if(empty($_POST['nino_no']) || empty($_POST['utr_no']) || empty($_POST['company_no'])) {
                Redirect::to('compliance_upload_document.php?id='. $_GET['id']);
                exit;
            }
        }
        $appendTo = "";
        $sql = "SELECT * FROM freelancer_info WHERE freelancerid = ". $row[0];
        $res = $conn->query($sql)->fetch(PDO::FETCH_ASSOC);
        if(count($res) > 1) {
            foreach ($_POST as $k => $v) {
                if($k != '_method' && $k != 'data') {
                    $appendTo .= $k .'= "'. $v . '",';
                }
            }
            $appendTo = rtrim($appendTo, ',');
            $sql = "UPDATE freelancer_info SET ". $appendTo . " WHERE freelancerid = ". $row[0];
            $conn->query($sql);
        } else {
            $keys = "freelancerid,";
            $values = "$row[0],";
            foreach ($_POST as $k => $v) {
                if($k != '_method' && $k != 'data' && !empty($v)) {
                    $keys .= "$k,";
                    $values .= "'$v',";
                }
            }
            $keys = rtrim($keys, ',');
            $values = rtrim($values, ',');

            $sql = "INSERT INTO freelancer_info (" .$keys. ") VALUES (" .$values. ")";
            $conn->query($sql);
        }
        Redirect::to('send_mail.php?id='. $_GET['id']);
    }
} else {
    Redirect::to('register.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    #pages-bonus {
        margin-top: 104px;
    }
    .grid-div {
        display: grid;
        grid-template-columns: repeat(3, 300px);
        grid-gap: 25px;
        grid-auto-rows: minmax(100px, auto);
        grid-auto-columns: minmax(10px, auto);
        align-content: center;
        justify-content: center;
        margin-top: 70px;
        margin-bottom: 40px;
    }
    .grid-div > div {
        border: 1px solid lightgrey;
        padding: 11px;
        box-shadow: 0px 0px 7px;
    }
    .single-div > h4 {
        cursor: pointer;
        color: #1aa933;
    }
    #header {
        z-index: 1;
    }
    #agreement2 {
        overflow-y: auto;
    }
    #agreement3 {
        overflow-y: auto;
    }
    .img-thumbnail inner_img img {
        width: 50px;
        height: 50px;
    }

    .img-thumbnail.inner_img {
        margin-bottom: 33px;
    }

    .compliance-content .img-thumbnail inner_img {
        display: block;
        max-width: 100%;
        height: auto;
        padding: 4px;
        line-height: 1.42857143;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
        margin: 2px auto;
    }

    .wrapper {
        display: grid;
        grid-gap: 10px;
        color: #444;
        margin-bottom: 23px;
    }

    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    .smbox {
        border: 2px solid lightgrey;
        padding: 8px;
    }
    .page::before {
        content: none;
    }

</style>
<div id="pages-bonus" class="content user">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
            </ul>
        </div>

        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container">
                    <link rel="stylesheet" type="text/css" href="/css/new_css/style.css"><script type="text/javascript" src="/js/new_js/jquery.min.js"></script><style>
                        .register-options .radio label{
                            /*margin-right: 0px !important;*/
                        }
                    </style>
                    <head>
                        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

                    </head>
                    <section class="ver-space ver-smspace sec_wrpr">
                        <div class="page">
                            <div class="" style="text-align: center;">
                                <h2>Tax compliance</h2>
                                <span style="color: #00b9ff;line-height: 25px">Become Tax Compliant</span>
                                <p style="line-height: 25px">
                                    Becoming a healthcare temp worker is an important financial responsibility. Joining temps-Direct uk requires you to get compliant with U.K. tax laws.<br /> This can be done by registereing as a self-employed worker. so you become up-to-date on tax payment and class 2 National Insurance contribution.
                                </p>
                            </div>
                            <div class="container">
                                <div class="form_div_reg">
                                    <form action="" class="form-horizontal register-form top-space js-geo-info js-register-form clearfix" id="UserRegister/manual/temp:3/543Form" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                        <div style="display:none;">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="hidden" name="data[_Token][key]" value="488b480792b4c44985bacb8f16be026a870f6bf4" id="Token1874834625">
                                        </div>
                                        <div class="temp3 form-no-1">
                                            <div class="three-box-f">
                                                <div class="col-md-4">
                                                    <div class="input text" style="text-align: center;">
                                                        <input name="nino_no" minlength="9" maxlength="9" required="true" type="text" id="UserNino">
                                                        <h5 class="text-center"style="color: #00b9ff;">Your NiNo</h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="input text" style="text-align: center;">
                                                        <input name="utr_no" minlength="10" maxlength="10" required="true" type="text" id="UserUtrCode">
                                                        <h5 class="text-center"style="color: #00b9ff;">Your UTR code</h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="input text" style="text-align: center;">
                                                        <input name="company_no" required="true" type="text" id="UserCompanyNo">
                                                        <h5 class="text-center" style="color: #00b9ff;">Your Company<br /> Registration No.</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <h5 class="" style="line-height: 70px; text-align: center;">If you not registered for any of the above, apply now!</h5>
                                            <div class="row">
                                                <div class="col-xs-4 col-md-3" style="text-align: center;">
                                                    <p style="line-height: 40px">1</p>
                                                    <a class="smbox" href="https://www.gov.uk/log-in-file-self-assessment-tax-return/register-if-youre-self-employed">Apply for UTR/ Self-assesment</a>
                                                </div>
                                                <div class="col-xs-4 col-md-3" style="text-align: center;">
                                                    <p style="line-height: 40px">2</p>
                                                    <a class="smbox" href="https://www.gov.uk/apply-national-insurance-number">Apply for NINO</a>
                                                </div>
                                                <div class="col-xs-4 col-md-3" style="text-align: center;">
                                                    <p style="line-height: 40px">3</p>
                                                    <a class="smbox" href="healthcare-temps.co.uk/page/ltd_company">Apply for a ltd Company </a>
                                                </div>
                                                <div class="col-xs-4 col-md-3" style="text-align: center;color: #00b9ff;">
                                                    <p style="line-height: 40px"><br /></p>
                                                    <a class="smbox" href="javascript:;" onclick="$('#agreement1').modal('show');">Apply for CRB</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="row">
                                        <div class="col-xs-12 col-md-offset-1" style="margin-top: 90px;">
                                            <style>
                                                .circle-bar .circles{
                                                    border-radius: 50%;
                                                    width: 40px;
                                                    height: 40px;
                                                    display: inline-block;
                                                    background-color: #fff;
                                                    margin-top: -17px;
                                                }
                                                .blankcircles{
                                                    border-color: green;
                                                    border: 1px solid green;
                                                    border-radius: 50%;
                                                    width: 40px;
                                                    height: 40px;
                                                    display: inline-block;
                                                    background-color: #fff;
                                                    margin-top: -17px;
                                                }
                                                .lgcircles{
                                                    border-radius: 50%;
                                                    width: 60px;
                                                    height: 60px;
                                                    border-color: green;
                                                    border: 1px solid green;
                                                    display: inline-block;
                                                    background-color: #fff;
                                                    margin-top: -23px;
                                                }
                                            </style>
                                            <div class="circle-bar" style=" overflow: visible;position: relative;width: 80%;margin: 0 auto; height: 5px;background-color: green;">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <div class="circles">
                                                            <i style="font-size: 37px; height: 40px;width: 40px; border-radius: 50%;color: white;background-color: green; " class="fa fa-check" aria-hidden="true"></i>
                                                        </div>
                                                        <p>Compliance<br /> Upload Documents</p>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div class=" lgcircles">
                                                            <i style="font-size: 56px; height: 40px;width: 40px; border-radius: 50%;color: green; " class="fa fa-check" aria-hidden="true"></i>
                                                        </div>
                                                        <p>Tax Compliance <br /> & Self-Employment</p>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div class="blankcircles"></div>
                                                        <p>Create CV <br/> Profile</p>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div style="width: 100%;background-color: #fff;height: 5px;"></div>
                                                        <div class="blankcircles">
                                                        </div>
                                                        <p> Delivered</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="submit" style="float: right;">
                                        <input class="" style="background-color: #00b9ff; margin-top: 50px; color: white;    padding: 10px 21px;font-size: 19px;}" type="submit" value="Next" onclick="validateData()">
                                        <a href="https://healthcare-temps.co.uk/temp_upload_3.php?id=<?=$_GET['id']?>" id="gotonext"></a>
                                    </div>
                                </div>
                            </div>


                            <div style="display:none;"><input type="hidden" name="data[_Token][fields]" value="f78a90e4ace4295cc9a7d0a1f777c92ea9f3f0e0%3A" id="TokenFields245310378"><input type="hidden" name="data[_Token][unlocked]" value="City.auto_detected_city%7CCity.id%7CCity.name%7CState.auto_detected_state%7CState.id%7CState.name%7CUser.browser_info%7CUser.country_id%7CUser.country_iso_code%7CUser.geobyte_info%7CUser.invite_hash%7CUser.is_agree_terms_conditions%7CUser.latitude%7CUser.longitude%7CUser.maxmind_info%7CUser.ne_latitude%7CUser.ne_longitude%7CUser.payment_gateway_id%7CUser.referred_by_user_id%7CUser.send_to_user_id%7CUser.sw_latitude%7CUser.sw_longitude%7CUserProfile.about_me%7CUserProfile.city_id%7CUserProfile.country_id%7CUserProfile.first_name%7CUserProfile.last_name%7CUserProfile.phone%7CUserProfile.state_id%7CUserProfile.zip_code%7Cadcopy_challenge%7Cadcopy_response%7Cnino%7Cutc_code%7Cvat" id="TokenUnlocked914982880"></div>
                        </div>
                </div></section>
                <div class="clearfix"></div>
                <style type="text/css">
                    .btn-file {
                        position: relative;
                        overflow: hidden;
                    }
                    .btn-file input[type=file] {
                        position: absolute;
                        top: 0;
                        right: 0;
                        min-width: 100%;
                        min-height: 100%;
                        font-size: 100px;
                        text-align: right;
                        filter: alpha(opacity=0);
                        opacity: 0;
                        outline: none;
                        background: white;
                        cursor: inherit;
                        display: block;
                    }
                </style>
                <script type="text/javascript">

                    function validateData() {
                        var nino = $('#UserNino').val();
                        var utr = $('#UserUtrCode').val();
                        var companyno = $('#UserCompanyNo').val();
                        if(nino && utr && companyno) {
                            window.location.href = $('#gotonext').attr('href');
                        }
                        return false;
                    }

                    $(document).ready(function(){
                        var loc = window.location;
                        var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf(':') + 0);

                        if (window.location.href.indexOf("/temp") > -1) {
                            $('#UserRoleId3').attr('checked', true);
                        }
                        if (window.location.href.indexOf("/emp") > -1) {

                            $('#UserRoleId2').attr('checked', true);
                        }
                        $('.radio .js-user-type').click(function(){
                            if(($(this).val()=='3')&&(pathName!='/users/register/manual/temp')){

                                window.location.replace("https://healthcare-temps.co.uk/users/register/manual/temp:1");
                            }
                            if(($(this).val()=='2')&&(pathName!='/users/register/manual/emp')){
                                window.location.replace("https://healthcare-temps.co.uk/users/register/manual/emp:1");
                            }
                        });
                    });
                </script>
            </div>
        </div>
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

</body>
</html>
