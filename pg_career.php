<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    .recruit-table td {
        color: lightgreen;
    }
</style>
<div class="container-fluid" style="text-align: center; margin-top: 80px;">
    <h2 style="color: rgb(90, 190, 90); text-decoration: underline">Join a team that's pioneering the healthcare revolution!</h2>
    <img src="./myassets/img/pg-career.png" alt="pg career">
    <h2 style="color: rgb(90, 190, 90); text-decoration: underline; margin-top: 48px; margin-bottom: 34px;">Yes, we are recruiting!</h2>
    <div class="table-responsive">
        <table class="table table-striped recruit-table" style="width: 77%; margin-left: 150px">
            <tbody>
            <tr>
                <th>ENGINEERING/IT</th>
                <td>IT Support Specialist<br />
                    Lead/Staff Software Engineer - Search<br />
                    Senior Engineering Manager - Team Collaboration</td>
            </tr>
            <tr>
                <th>FINANCE & ACCOUNTING</th>
                <td>Enterprise Contract & Billing Analyst<br />
                    Payroll Specialist</td>
            </tr>
            <tr>
                <th>MARKETING</th>
                <td>Manager of Marketing<br />
                    Account Executive</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<!-- Include footer.php. Contains footer content. -->
<?php //include ('includes/template/footer.php'); ?>

</body>
</html>
