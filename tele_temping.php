<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');
    exit;
}else{
 require_once 'core/frontinit.php';
}
if(isset($_POST['submit'])) {
    $dbObj = new DB();
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $company_name = $_POST['company_name'];
    $job_title = $_POST['job_title'];
    $qualification = $_POST['qualification'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $created_at = $_POST['created_at'];
    $sql = "INSERT INTO tele_temping (first_name, last_name, company_name, jobtitle, qualification, phone, email, created_at) VALUES 
            ('$first_name', '$last_name', '$company_name', '$job_title', '$qualification', '$phone', '$email', '$created_at')";
    $dbObj->query($sql);
    header('Location: '. $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

    <!-- Include header.php. Contains header content. -->
    <?php include ('includes/template/header.php'); ?>

<body class="greybg">

     <!-- Include navigation.php. Contains navigation content. -->
 	 <?php include ('includes/template/navigation.php'); ?>

      <!-- ==============================================
	 Header
	 =============================================== -->
     <div id="pages-tele-temping" class="content user">
         <div class="wrapper">
             <div class="social_icn">
                 <ul class="unstyled clearfix pull-right media-icons">
                     <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                     <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                     <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                     <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                     <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                     <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
                 </ul>
             </div>
             <div id="js-head-menu" class="">
                 <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                     <div class="modal-dialog">

                         <!-- Modal content-->
                         <div class="modal-content">
                             <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">×</button>
                                 <a class="show hor-space" title="Temps-direct.uk" href="/">
                                     <img alt="" src="/img/logo.png">
                                 </a>
                             </div>
                             <div class="cantent">
                                 <p>
                                     Your log-out has been successful.</p>
                                 <p>
                                     Thank you for using Temps-Direct temp recruiting platform.</p>
                                 <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                                 <input name="feedback" type="commant">
                                 <p>Thank you!</p>
                             </div>
                             <div class="modal-footer hide">
                                 <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default">  Close</a>
                             </div>
                         </div>

                     </div>
                 </div>
                 <header id="header">
                     <div class="navbar no-mar pr z-top">
                         <div class="navigation-wrapper">
                             <div class="navbar-inner no-pad no-round">
                                 <div class="container clearfix">
                                     <h1 class="brand top-smspace"> <a href="/" title="healthcare-temps.co.uk" class="show hor-space"><img src="/img/logo.png" alt=""></a></h1>
                                     <a class="btn btn-navbar mspace js-no-pjax" data-toggle="collapse" data-target=".nav-collapse"> <i class="icon-align-justify icon-24 whitec"></i></a>
                                     <div class="nav-collapse">
                                         <ul class="nav dc">
                                             <!--li class=" divider-left divider-right"> <a href="/page/employer" class="js-no-pjax bootstro" data-bootstro-step="1" data-bootstro-title="Employer?" data-bootstro-placement="bottom"> <i class="icon-briefcase text-20 no-pad"></i> <span class="show top-mspace">Employer?</span></a>
                             </li>
                                             <li class=" divider-left divider-right">
                               <a href="/page/freelancer" class="bootstro" data-bootstro-step="2" data-bootstro-title="Freelancer?" data-bootstro-placement="bottom" title="Freelancer?"><i class="icon-male text-20 no-pad"></i> <span class="show top-mspace">Freelancer?</span></a>				</li-->

                                         </ul>
                                         <div class="pull-right mob-clr tab-clr dc">
                                             <ul class="nav nav-right dc">
                                                 <li class="divider-left divider-right tab-sep-none "><a href="/users/register/manual" title="Register">Register</a></li>

                                                 <li class="divider-left tab-sep-none divider-right "><a href="/users/login" title="Login">Login</a></li>
                                                 <li class=" divider-left tab-sep-none divider-right">
                                                     <a href="/page/how-it-works" title="How it Works">How it Works</a>
                                                 </li>
                                                 <li class="divider-right tab-sep-none">
                                                     <form class="form-search bot-mspace mob-searchform">
                                                         <div class="input select">
                                                             <select id="search_type_id" name="search_type_id">
                                                                 <option value="0">Portfolios</option>
                                                                 <option value="1">Jobs</option>
                                                                 <option value="2">Freelance Projects</option>
                                                                 <option value="3">Freelancers</option>
                                                                 <option value="4">Services</option>
                                                             </select>
                                                         </div>
                                                         <div class="input text">
                                                             <label class="hide" for="search">Search</label>
                                                             <input id="search" placeholder="Search" name="q" class="span18" type="search">
                                                             <label class="hide" for="Search">Search</label>
                                                             <input value="Search" title="Search" class="btn" id="Search" type="submit">
                                                         </div>
                                                     </form>
                                                     <a id="trigger-search" href="#" class="js-no-pjax" title="Search"><i class="icon-search text-20"></i></a>
                                                     <!--?php// } ?-->
                                                 </li>
                                                 <li class="divider-left tab-sep-none ">
                                                     <a href="/" title="Home">Home</a>
                                                 </li>
                                                 <li class="divider-left tab-sep-none ">
                                                     <a href="#"><span style="display: block;position: absolute;margin-left: 22px;">0800-368-8769</span><i class="fa fa-phone" aria-hidden="true" style="font-size: 30px;"></i></a>
                                                 </li>

                                             </ul>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div id="nav-search">
                             <div class="search-input-container">
                                 <form action="/users" class="form-search blue-btn-bg bot-mspace home-search-form clearfix {&quot;user_url&quot;:&quot;/users&quot;}" id="UserViewForm" method="get" accept-charset="utf-8"><div style="display:none;"><input name="_Token" value="eda337656a7143b49c645268edb40849e750bc2b" id="Token117520410" type="hidden"></div><div class="input select"><select name="search_type_id" id="search_type_id">
                                             <option value="3">Temps</option>
                                             <option value="2">Temp Jobs</option>
                                         </select></div><div class="input text right-space"><label for="UserQ">Location</label><input name="q" id="UserQ" type="text"></div><div class="submit"><input value="Go" type="submit"></div><div style="display:none;"><input name="_Token" value="2b7195b6dfe8c13a4d606cf90df5de63477dad34%3A" id="TokenFields1653482436" type="hidden"><input name="_Token" value="Page.Add%7CPage.Preview%7CPage.Update%7CPage.content%7CPage.description_meta_tag%7CPage.id%7CPage.meta_keywords%7CPage.parent_id%7CPage.slug%7CPage.status_option_id%7CPage.title%7C_wysihtml5_mode" id="TokenUnlocked1290645018" type="hidden"></div></form>	  </div>
                         </div>
                     </div>
                 </header>
                 <script type="text/javascript">
                     //<![CDATA[
                     function getCookie (c_name) {var c_value = document.cookie;var c_start = c_value.indexOf(" " + c_name + "=");if (c_start == -1) {c_start = c_value.indexOf(c_name + "=");}if (c_start == -1) {c_value = null;} else {c_start = c_value.indexOf("=", c_start) + 1;var c_end = c_value.indexOf(";", c_start);if (c_end == -1) {c_end = c_value.length;}c_value = unescape(c_value.substring(c_start,c_end));}return c_value;}if (getCookie('il')) {(function() {var js = document.createElement('script'); js.type = 'text/javascript'; js.async = true;js.src = "https://healthcare-temps.co.uk/users/show_header.js?u=";var s = document.getElementById('js-head-menu'); s.parentNode.insertBefore(js, s);})();} else {document.getElementById('js-head-menu').className = '';}
                     //]]>
                 </script>
             </div>



             <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
             <div id="pjax-body">
                 <div id="main" class="main">
                     <div class="container">
                         <style>
                             .page:before {
                                 content: "";
                                 width: 100%;
                                 height: 100%;
                                 display: table-column !important;
                                 border-bottom: 4px solid #e0e0e0;
                                 padding-bottom: 10px;
                             }
                             .med-specialist {
                                 background-image: url(myassets/img/medical-specialist.jpg);
                                 background-repeat: no-repeat;
                                 background-size: cover;
                                 height: 520px;
                             }
                         </style>
                         <section class="row ver-space ver-smspace">
                             <div class="page">
                                 <div class="tab-head bot-mspace text-13 clearfix green-head-bg" style="margin-left: 15px;">
                                     <h2 class="text-16 no-mar" style="text-align: center">Tele Temping</h2>
                                 </div>
                                 <div class="frontier-sec">
                                     <div class="container">
                                         <div class="row">
                                             <div class="col-lg-12">
                                                 <img src="myassets/img/Tele-Temping.jpg">
                                             </div>
                                         </div>
                                         <div class="row">
                                             <div class="col-lg-12">
                                                 <h5 style="text-align: center;font-size: 32px;font-weight: bold;color: #0cb9fd;margin-top: 25px;">Boldly going beyond frontiers</h5>
                                                 <p style="text-align: center;font-size: 23px;color: #0CB9FF; margin-bottom: 25px;">Sign up to become a tele-temp &amp; earn extra money! </p>
                                                 <hr style="background-color: #dadada;height: 2px;">
                                             </div>
                                         </div>
                                         <div class="row" style="margin-top: 21px;">
                                             <div class="col-lg-6">
                                                 <p style="font-size: 16px;color: #888888;">
                                                     The demand for affordable quality healthcare is growing at an <br>
                                                     unprecedented rate, significantly outstripping supply four fold. <br>
                                                     Millions of healthcare users are unable to receive proper <br>
                                                     healthcare in a timely and convenient manner.
                                                 </p>

                                                 <p style="font-size: 16px;color: #888888;">
                                                     Tele-Temps are playing a vital role in alleviating these <br>
                                                     deficiencies, particularly, via real time video communication<br>
                                                     on PC, Mobile and Tablet.
                                                 </p>
                                                 <h5 style="font-size: 22px;color: #0CB9FF;">Tele-Temps high in Demand. </h5>
                                                 <p style="font-size: 16px;color: #888888;">
                                                     Tele-Temps high in Demand. <br>
                                                     Pre-Surgery Consultation<br>
                                                     Ophthalmology <br>
                                                     Ultrasound Scan <br>
                                                     Gynaecology <br>
                                                     Tele-stroke <br>
                                                     Radiology <br>
                                                     Psychiatry <br>
                                                     Dental-Care <br>
                                                     Cancer-Care <br>
                                                     Audiology<br>
                                                     Dermatology<br>
                                                     Cardiology <br>
                                                     Oncology Obstetics <br>
                                                     Dieticians
                                                 </p>
                                             </div>
                                             <div class="col-lg-6">
                                                 <form action="" method="post" style="width: 92%;">
                                                     <div class="form-group" style="display: inline-block;width: 49%;">
                                                         <input class="form-control" placeholder="First Name" name="first_name" style="height: 47px;" type="text" required>
                                                     </div>
                                                     <div class="form-group " style="display: inline-block;width: 49%;">
                                                         <input class="form-control" placeholder="Last Name" name="last_name" style="height: 47px;" type="text" required>
                                                     </div>
                                                     <div class="form-group">
                                                         <input class="form-control" placeholder="Company Name " name="company_name" style="height: 47px;" type="text" required>
                                                     </div>
                                                     <div class="form-group">
                                                         <input class="form-control" placeholder="Jobtitle" name="job_title" style="height: 47px;" type="text" required>
                                                     </div>
                                                     <div class="form-group">
                                                         <input class="form-control" placeholder="Qualification" name="qualification" style="height: 47px;" type="text" required>
                                                     </div>
                                                     <div class="form-group">
                                                         <input class="form-control" placeholder="Phone" name="phone" style="height: 47px;" type="tel" required>
                                                     </div>
                                                     <div class="form-group">
                                                         <input class="form-control" placeholder="Email" name="email" style="height: 47px;" type="email" required>
                                                     </div>
                                                     <div class="form-row" style="margin-top: 12px; margin-bottom: 12px">
                                                         <div class="form-controls">
                                                             <div class="g-recaptcha" name="g-recaptcha-response" data-sitekey="6Lf23VMUAAAAAJAc70Ch797luTFLM1dmmknWEj65"></div>
                                                         </div><!-- /.form-controls -->
                                                     </div><!-- /.form-row -->
                                                     <button type="submit" name="submit" class="btn btn-default" style="background-color: skyblue;margin-left: 122px;font-size: 21px;">Submit</button>
                                                 </form>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </section>

                     </div>
                 </div>
             </div>
             <!-- for modal -->
             <div class="modal hide fade" id="js-ajax-modal">
                 <div class="modal-body"></div>
                 <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a> </div>
             </div>
             <!-- for modal -->
             <!-- for modal -->
             <div class="modal hide fade" id="js-ajax-modal-child">
                 <div class="modal-body"></div>
                 <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a> </div>
             </div>
             <!-- for modal -->

             <div class="clearfix"></div>
             <!-- for modal
             <div class="footer-push"></div> -->
         </div>
         <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
     </div>

     <!-- Include footer.php. Contains footer content. -->
 	 <?php include ('includes/template/footer.php'); ?>

</body>
</html>
