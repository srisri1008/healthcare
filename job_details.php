<?php
//unset($_SESSION['job_url']);
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php';	
}

//Start new Freelancer object
$freelancer = new Freelancer();
$obj = new DB();
$jobtitle = Input::get('title');
//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
  $_SESSION['job_url'] = "job_details.php?title=$jobtitle";
  Redirect::to('../login.php');	
}


$hostname = $_SERVER['HTTP_HOST'];

//echo '<pre>';print_r($_SERVER);echo '</pre>';
$obj = new DB();
//Getting Job Data
$jobID = Input::get('title');
$query = DB::getInstance()->get("job", "*", ["jobid" => $jobID, "LIMIT" => 1]);
if ($query->count() === 1) {
 foreach($query->results() as $row) {
  $jobid = $row->jobid;
  $title_job = $row->title;
  $clientid = $row->clientid;
  $catid = $row->catid;
//  $budget = $row->budget;
  $job_type = $row->job_type;
  $start_date = $row->start_date;
  $end_date = $row->end_date;
  $description_job = $row->description;
  $location = $row->country;
  $skills = $row->skills;
  $arr=explode(',',$skills);
  $date_added = ago(strtotime($row->date_added));
  $completed = $row->completed;
  $accepted = $row->accepted;
  $temp_type = $obj->getData("SELECT * FROM temp_type WHERE id = ". $row->temp_type_id . " LIMIT 0,1")[0];
  $temp_category = $obj->getData("SELECT * FROM temp_category WHERE id = ". $temp_type['category_id'] . " LIMIT 0,1")[0];
  $temp_shift = $obj->getData("SELECT * FROM temp_shift WHERE id = ". $row->temp_shift_id . " LIMIT 0,1")[0];
  $temp_rate = $obj->getData("SELECT * FROM temp_rate WHERE id = ". $row->temp_rate_id . " LIMIT 0,1")[0];
     $name1 = $temp_category['title'];
     $budget = $temp_rate['title'] .': '. $temp_rate['rangemin'] . ($temp_rate['rangemax'] ? '-'. $temp_rate['rangemax'] : '+');

     
 }
} else {
  Redirect::to('jobs.php');
}
$cat_name = $temp_category['title'];
//Getting Category Name
//$query = DB::getInstance()->get("category", "*", ["catid" => $catid, "LIMIT" => 1]);
//if ($query->count() === 1) {
// foreach($query->results() as $row) {
//  $cat_name = $row->name;
// }
//}else {
//  $cat_name = "Undefined";
//}

//Getting Client
$q1 = DB::getInstance()->get("client", "*", ["clientid" => $clientid]);
if ($q1->count()) {
	 foreach ($q1->results() as $r1) {
	  $name1 = $r1->name;	
	  $username1 = $r1->username;	
	  $imagelocation = $r1->imagelocation;	
	  $bgimage = $r1->bgimage;	
     }
}

//Getting Proposals
$q2 = DB::getInstance()->get("proposal", "*", ["jobid" => $jobid]);
 if ($q2->count() === 0) {
  $job_proposals = 0;	
 } else {
  $job_proposals = $q2->count();
 }

 //echo '<pre>';print_r($_POST);echo '</pre>';
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('includes/template/header.php'); ?>
<style>
    body.greybg {
        background-color: white;
    }

    .modal-content {
    	box-shadow: none;
		border: none;
	}
	.footer-popup .term-con a:hover{
		color: #1d95eb !important;
	}

	a:hover{
		color: #1d95eb !important;
	}
</style>

<body class="greybg">
	
     <!-- Include navigation.php. Contains navigation content. -->
 	 <?php include ('includes/template/navigation.php'); ?> 	 
	 
     <!-- ==============================================
	 Header
	 =============================================== -->
     <!-- ==============================================
	 Job Post Section
	 =============================================== -->
     <section class="jobpost" style="margin-top: 175px;">
	  <div class="container">
		<div class="row">
	  <div class="rows">
			<div class="box1">
			<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 titleJob">
				<h4 style="color: #00a2e8 !important;">Job Description</h4>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				<ul class="listItem">
					<li class="">
						<span class="">Bids</span><br>
						<span class="Mgreen"><?php echo $job_proposals;?></span>						
					</li>
					<li class="b-r"></li>
					
					<li >
						<span class="">Posted</span><br>
						<span class="Mgreen"><?php echo $date_added; ?></span>						
					</li>
				</ul>
			</div>
	</div>
			
		</div>

	   <div class="row">
	    <div class="col-lg-9 white">
		
		 <div class="row">
		  <div class="col-lg-12">
		   <h6><a href="jobs.html" class="anlist" style="color: #00a2e8 !important;"><?php echo $lang['categories']; ?> / <?php echo $cat_name; ?></a></h6>
           <h4><?php echo $title_job; ?></h4>
           <hr class="small-hr">
		  </div>		
		 </div> 
		 
		 <div class="row post-top-sec">
		  <div class="col-lg-3">
		   <h5> <?php echo $lang['posted']; ?> </h5>
		   <p><?php echo $date_added; ?></p>
		  </div><!-- /.col-lg-3 -->
		  <div class="col-lg-3">
		   <h5> <?php echo $lang['location']; ?> </h5>
		   <p><i class="fa fa-map-marker"></i> <?=$location?></p>
		  </div><!-- /.col-lg-3 -->
		  <div class="col-lg-3">
		   <h5><?php echo $lang['budget']; ?> </h5>
		   <p><?php echo $budget; ?></p>
		  </div><!-- /.col-lg-3 -->
		  <div class="col-lg-3">
		   <h5> <?php echo $lang['applicants']; ?> </h5>
		   <p><?php echo $job_proposals; ?></p>
		  </div><!-- /.col-lg-3 -->
		  
		  <div class="col-lg-12">
           <hr class="small-hr">
		  </div> <!-- /.col-lg-12 -->
		 </div><!-- /.row -->
		 
		 <div class="row post-top-sec">
		  <div class="col-lg-3">
		   <h5> <?php echo $lang['date']; ?> <?php echo $lang['to']; ?> <?php echo $lang['start']; ?> </h5>
		   <p><?php echo $start_date; ?></p>
		  </div><!-- /.col-lg-3 -->
		  <div class="col-lg-3">
		   <h5> <?php echo $lang['date']; ?> <?php echo $lang['to']; ?> <?php echo $lang['end']; ?> </h5>
		   <p><?php echo $end_date; ?></p>
		  </div><!-- /.col-lg-3 -->
		  <div class="col-lg-3">
		   <h5> <?php echo $lang['job']; ?> <?php echo $lang['category']; ?></h5>
		   <p><?php //echo $job_type; 
		   echo $temp_category['title']; ?></p>
		  </div><!-- /.col-lg-3 -->
		  <div class="col-lg-3">
		   <h5> <?php echo $lang['job']; ?> <?php echo $lang['status']; ?></h5>
		   <p><?php 
		         if($accepted === 1):
					  if($completed === '1'):	 
					   echo $lang['completed'];	
					  else:
					   echo $lang['on']; echo $lang['progress'];	  
					  endif;	   
				 else:
				 echo $lang['opened'];	 
				 endif;	  
		    ?></p>
		  </div><!-- /.col-lg-3 -->
		  
		  <div class="col-lg-12">
           <hr class="small-hr">
		  </div> <!-- /.col-lg-12 -->
		 </div><!-- /.row -->		 
		  
		 <div class="post-bottom-sec">
		  <h4 style="color: #00a2e8;float: left;width: 100%"><?php echo $lang['job']; ?> <?php echo $lang['description']; ?></h4>
          <?php echo $description_job; ?>
		
		  <h4 style="color: #00a2e8;"><?php echo $lang['skills']; ?></h4>
           <?php
            foreach ($arr as $key => $value) {
              echo '<label class="label" style="background-color: #00a2e8">'. $value .'</label> &nbsp;';
            }
		   ?>	
		
		 </div><!-- /.post-bottom-sec --> <br>
		 <div class="post-bottom-sec">

		
		  <!-- <div class="post-row-section"> <label class="label" style="background-color: #00a2e8"><?php //echo $lang['bid_receiver']; ?></label> <span style="text-align: center;"><?php //echo $job_proposals;?></span>

		   </div> -->

		    <div class="post-row-section job-details-popup"> <button class="label" id="myBtn" href="#" style="background-color: #00a2e8; color: white"> <?php echo 'Bid Now'; ?></button>
			<div id="myModal" class="modal">

				<!-- Modal content -->
				<div class="modal-content" id="subform">
				<form name="apply" method="post" id="apply_form">
					<span class="close">&times;</span>
					<p>Do you want to bid for this job?</p>

					<p id="errors" style="width:100% ; color: red !important;"></p>
					<p id="success" style="width:100% ; color: green !important;"></p>

					<div class="hourly-rate-section">
					  <label>Your Hourly Rate?:</label>
                        <span><i><?php echo $currency_symbol; ?></i></span><input onkeypress="return isNumbera(event)" type="text" id="rate" name="rate" value="" maxlength="4"/>
					    <select name="hour_type" id="hour_type">
						<option value="Hour">Per Hour</option>
						<option value="Day">Per Day</option>
                                            </select>
					</div>

					<div class="hourly-rate-section">
					 
                        <span>Hrs</span><input onkeypress="return isNumbera(event)" type="text" name="rate_two" id="rate_two" value="" maxlength="2"/>
					    <select name="hours_rate" id="hours_rate">
	                        <option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
                       </select>

						<div class="tmpry-name">Your temp Name:</div>
					</div>
                   
				   <div class="private-quote">
                     <label>Send a private quote to the Hirer:</label>
					<textarea name="quote" id="quote" rows="3" cols="50" value=''>
					
					</textarea>
				   </div>

				   <div class="your-name-input">
                     <label>Your name:</label>
					 <input type="text" oninput="return validateAlpha();" 
					 name="firstname" id="firstname" maxlength="50" value="">
				   </div>

				   <div class="your-name-input">
                     <label>CRB/DBS Number</label>
                     <input type="text" name="crb_num" id="crb_num" maxlength="12" onkeypress="return isNumbera(event)" value="">
				   </div>

				  <div class="Bid-Now-btn"> 
				  	<button type="button" id="applyjob" class="btn" style="background-color: #00a2e8">Bid Now</button>
				  	<!--<input id="applyjob" type="submit" class="btn" value="Bid Now">-->
				  	</div>

				   <div class="footer-popup">
                     <input type="checkbox" name="terms" id="terms" value="1">By clicking you are indicating that you have read & agree to the <a href="#" class="term-con">terms and condition</a> and <a href="#">Privacy Policy</a>. 
				   </div>

				</form>

				</div>
				<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

setInterval(showModal, 300000);

function showModal()
{
  $('#myModal').modal('show');
}

</script>


          </div>



		 </div>

		   <!--<div class="post-row-section">  <label class="label" style="background-color: #00a2e8"><?php //echo $lang['budget']; ?></label> <span><?php //echo    $currency_symbol . $temp_rate['rangemin'] . ($temp_rate['rangemax'] ? '-'.  $currency_symbol .$temp_rate['rangemax'] : '+') ; ?></span>
		   </div> -->

		   <div class="post-row-section"> <a class="label" href="tempworker/inbox.php" style="background-color: #00a2e8;"> <?php echo $lang['email_hirer']; ?></a>
		   </div>

		   <div class="post-row-section"> <a class="label" href="tempworker/job.php?a=overview&id=<?php echo $jobID; ?>" style="background-color: #00a2e8;"> <?php echo 'Overview'; ?></a>
		   </div>



		<?php if($job_proposals > 0){ ?>	   		 
		 <h4 style="color: #0ebf1f;float: left;width: 100%;"><?php echo $lang['proposals']; ?></h4>
		 	
		 <div id="comments-list">	
          <?php echo getFeaturedProposals($jobid); ?>
          <?php echo getProposals(null, $jobid, $proposal_limit) ?>
		 </div>
		<?php }?>
		 
		</div><!-- /.col-lg-8 -->
            </div>
	    <div class="col-lg-3">
		
		 <div class="panel user-client revealOnScroll" data-animation="slideInUp" data-timeout="200">
		  <div class="row text-center">
		   <a href="hirer.php?a=overview&id=<?php echo $clientid; ?>">
		   
            
                       <div class="col-xs-12 user-avatar avtar-profile" style="height: 100%;">
                           <img src="hirer/<?php echo $imagelocation; ?>" alt="Image" class="img-thumbnail img-responsive" style="top: 0px;">
             <h4 style="color: #0ebf1f;top: 0px;"><?php echo $name1; ?></h4>
             <p class="name-profile" style="font-weight: bold;top: 0px;background-color: #00a2e8;"><?php echo $name1; ?>:Hirer</p>
            </div><!-- /.col-xs-12 -->
		   </a>
          </div><!-- /.row -->
		  
		  <div class="list-group">
           <div class="list-group-item">&nbsp;&nbsp;&nbsp;<?php echo $lang['jobs']; ?> <?php echo $lang['posted']; ?>
            <span class="badge">
            <?php	
             $query = DB::getInstance()->get("job", "*", ["AND" => ["clientid" => $clientid, "invite" => 0]]);
             echo $query->count();
            ?>
	        </span>
		   </div><!-- /.list-group-item -->
           <div class="list-group-item">&nbsp;&nbsp;&nbsp;<?php echo $lang['jobs']; ?> <?php echo $lang['invites']; ?>
            <span class="badge">
            <?php	
             $query = DB::getInstance()->get("job", "*", ["AND" => ["clientid" => $clientid, "invite" => 1]]);
             echo $query->count();
            ?>
	        </span>
		   </div><!-- /.list-group-item -->
           <div class="list-group-item">&nbsp;&nbsp;&nbsp;<?php echo $lang['jobs']; ?> <?php echo $lang['completed']; ?>
            <span class="badge">
            <?php	
             $query = DB::getInstance()->get("job", "*", ["AND" => ["clientid" => $clientid, "completed" => 1]]);
             echo $query->count();
            ?>
	        </span>
		   </div><!-- /.list-group-item -->
           <div class="list-group-item">&nbsp;&nbsp;&nbsp;<?php echo $lang['job']; ?> <?php echo $lang['payments']; ?>
            <span class="badge">
        	<?php
		         echo $currency_symbol.'&nbsp;';
                    $query = DB::getInstance()->sum("transactions", "payment", ["AND" =>["freelancerid" => $clientid, "transaction_type" => 4]]);
					foreach($query->results()[0] as $row) {
						echo $row;
					}	?>
						</span>
		   </div><!-- /.list-group-item -->
           <div class="list-group-item">&nbsp;&nbsp;&nbsp;<?php echo $lang['ratings']; ?> 
	            (<?php	
	             $query = DB::getInstance()->get("ratings_client", "*", ["AND" => ["clientid" => $clientid]]);
	             $count = $query->count();
				 echo $re = $count/7;
	            ?>)
                    <span class="badge" style="font-size:9px">
		     <i class="fa fa-star"></i>
			 <i class="fa fa-star"></i>
			 <i class="fa fa-star"></i>
			 <i class="fa fa-star"></i>
			 <i class="fa fa-star"></i>
		    </span>
		   </div><!-- /.list-group-item -->
		  </div><!-- /.list-group -->
		 
		 </div><!-- /.list-group-item -->
	   <?php 
		 $ShareUrl = urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
		 $Media = 'http://2.bp.blogspot.com/-nr1K0W-Zqi0/U_4lUoyvvVI/AAAAAAAABJE/F_C7i48sI58/s1600/new2.png';
		?>
			
		 <div class="list">
		  <div class="list-group">
           <span class="list-group-item active cat-top" style="background-color: #00a2e8 !important;">
            <em class="fa fa-fw fa-coffee"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['share']; ?> <?php echo $lang['this']; ?> <?php echo $lang['job']; ?>
		   </span>
			<a class="list-group-item cat-list" onclick="shareinsocialmedia('https://www.facebook.com/sharer/sharer.php?u=<?php echo $ShareUrl;?>&title=<?php echo $title_job;?>')" href="">
			<em class="fa fa-fw fa-facebook"></em>&nbsp;&nbsp;&nbsp;Facebook
			</a>
			<a class="list-group-item cat-list" onclick="shareinsocialmedia('http://twitter.com/home?status=<?php echo $title_job; ?>+<?php echo $ShareUrl; ?>')" href="">
			<em class="fa fa-fw fa-twitter"></em>&nbsp;&nbsp;&nbsp;Twitter
			</a>
			<a class="list-group-item cat-list" onclick="shareinsocialmedia('http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $ShareUrl; ?>&title=<?php echo $title_job; ?>')" href="">
			<em class="fa fa-fw fa-linkedin"></em>&nbsp;&nbsp;&nbsp;LinkedIn
			</a>						
          </div><!-- /.list-group -->
		 </div><!-- /.list --> 
		 
		</div><!-- /.col-lg-4 -->
		
	   </div><!-- /.row-->
	  </div><!-- /.container -->  	 
	 </section><!-- /section --> 	 
	  
      <!-- Include footer.php. Contains footer content. -->	
	  <?php include 'includes/template/footer1.php'; ?>	
	 
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	 
     <!-- jQuery 2.1.4 -->
     <script src="assets/js/jQuery-2.1.4.min.js"></script>
     <!-- Bootstrap 3.3.6 JS -->
     <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
     <!-- Waypoints JS -->
     <script src="assets/js/waypoints.min.js" type="text/javascript"></script>
     <!-- Kafe JS -->
  
    <script type="text/javascript">
        function isNumbera(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode >= 65 && charCode <= 90 ) || (charCode >= 97 && charCode <= 122 )) {
        return false;
    }
    return true;
}
function validateAlpha(){
    var textInput = document.getElementById("firstName").value;
    textInput = textInput.replace(/[^A-Za-z ]/g, "");
    document.getElementById("firstName").value = textInput;
}


	function loadProposals(id, jobid, limit) {
		$('#more_comments_'+id).html('<div class="preloader-retina preloader-center"></div>');
		$.ajax({
			type: "POST",
			url: "includes/template/requests/load_proposals.php",
			data: "id="+id+"&jobid="+jobid+"&limit="+limit, 
			cache: false,
			success: function(html) {
				// Remove the loader animation
				$('#more_comments_'+id).remove();
				
				// Append the new comment to the div id
				$('#comments-list').append(html);
			
			}
		});
	}	


	$(document).ready(function()
	{	
		//alert('fdgdfgh');
			$("#applyjob").click(function(){

	         var rate = $("#rate").val();
	         var hour_type = $("#hour_type").val();
	         var rate_two = $("#rate_two").val();
	         var hours_rate = $("#hours_rate").val();
	         var quote = $("#quote").val();
	         var firstname = $("#firstname").val();
	         var crb_num = $("#crb_num").val();
	         var terms = $("#terms").val();
	         var clientid = <?php echo $clientid; ?>;
	         var job = <?php echo $jobID; ?>;
	         var hostname = "<?php echo 'https://'.$hostname; ?>";
	         

	            
	            $.ajax({
	              type: "POST",
	              url: hostname+"/tempworker/save_applyjob.php",
	              dataType: "html",
	              data: "rate="+rate+"&hour_type="+hour_type+"&rate_two="+rate_two+"&hours_rate="+hours_rate+"&quote="+quote+"&firstname="+firstname+"&crb_num="+crb_num+"&terms="+terms+"&clientid="+clientid,
	              success: function(response)
	              {
	                  //alert(response);
	                  if(response == 1){
	                  	$("#success").html('Apply Sussessfully.');
	                // window.location = "http://172.10.1.5:8021/job_details.php?title=834303551489";
	    window.location.href = hostname+"/job_details.php?title=" + job;
	                  }else{
	                  	$("#errors").html(response);
	                  }
	                  
	                  //$("#ajax_list").html($(response).find("#ajax_list").html());
	                    
	              }
	            });
	            return false;

	        });
    });
	</script>  



	
   <script src="assets/js/kafe.js" type="text/javascript"></script>
	<script type="text/javascript" async >
	    function shareinsocialmedia(url){
	    window.open(url,'sharein','toolbar=0,status=0,width=648,height=395');
	    return true;
	    }
	</script>
     <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
     <script>
         (adsbygoogle = window.adsbygoogle || []).push({
             google_ad_client: "ca-pub-2199989077469674",
             enable_page_level_ads: true
         });
     </script>

</body>
</html>
