 <?php
$basename = basename($_SERVER["REQUEST_URI"], ".php");
$editname = basename($_SERVER["REQUEST_URI"]);
$test = $_SERVER["REQUEST_URI"];
?>
     <!-- ==============================================
     Navigation Section
     =============================================== -->
 <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #2d3d56; max-width: 100% !important;width:100%;margin: 0px 0px !important;">
     <div class="container-fluid">
         <div class="navbar-header">
             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                 <span class="sr-only">Toggle navigation</span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
             </button>
             <a class="navbar-brand" href="/">
                 <img src="../../myassets/img/logo.png" class="img-responsive" alt="Healthcare" title="healthcare">
             </a>
         </div>
         <div id="navbar" class="collapse navbar-collapse" style="margin-left: 30%;">
             <ul class="nav navbar-nav" style="margin-left: 550px;">
                 <li class=""><a href="/">Home</a></li>
                 <li><a href="/features.php">features</a></li>
                 <li><a href="/how.php">how it works</a></li>
                 <li><a href="/login.php">Log-in</a></li>
                 <li class="active"><a href="/register.php">Sign-up</a></li>
                 <li><a href="javascript:">Languages</a></li>
             </ul>
         </div><!--/.nav-collapse -->
     </div>
 </nav>
 <style>
     /* Social Icons */
     #social_side_links {
         position: fixed;
         top: 36%;
         left: 95%;
         padding: 0;
         list-style: none;
         z-index: 99;
     }

     #social_side_links li a {display: block;}

     #social_side_links li a img {
         display: block;
         max-width:40px;
         padding: 10px;
         -webkit-transition:  background .2s ease-in-out;
         -moz-transition:  background .2s ease-in-out;
         -o-transition:  background .2s ease-in-out;
         transition:  background .2s ease-in-out;
     }

     #social_side_links li a:hover img {
         background: rgba(0, 0, 0, .2);
     }
     @media screen and (max-width: 500px) {
         .navbar-nav > li:last-child {
             margin: none;
             margin-bottom: 5px;}
         .navbar .nav > li {
             float: left;
             display: block;
             width: 100%;
             margin-top: 5px;}
     }
 </style>
 <!-- Social Icons -->
 <ul id="social_side_links">
     <li><a style="background-color: #3c5a96;" href="https://facebook.com" target="_blank"><img src="../../myassets/images/facebook-icon.png" alt="" /></a></li>
     <li><a style="background-color: #1178b3;" href="https://www.linkedin.com/in/healthcare-temps-b92620113?trk=org-employees_mini-profile_cta" target="_blank"><img src="../../myassets/images/linkedin-icon.png" alt="" /></a></li>
     <li><a style="background-color: #1dadeb;" href="https://twitter.com" target="_blank"><img src="../../myassets/images/twitter-icon.png" alt="" /></a></li>
     <li><a style="background-color: rgb(237, 28, 36);" href="https://google.com" target="_blank"><img src="../../myassets/images/google-plus.png" alt="" /></a></li>
     <li><a style="background-color: rgb(255, 163, 26);" href="https://healthcare-temps.blogspot.co.uk/" target="_blank"><img src="../../myassets/images/blogger_icon.png" alt="" /></a></li>
     <li><a style="background-color: rgb(207, 34, 0);" href="https://www.youtube.com/channel/UCqQw7gCVKdPpktRVpnhj0ZQ" target="_blank"><img src="../../myassets/images/youtube_icon.png" alt="" /></a></li>
 </ul>
 <script>
 $(document).ready(function() {  

// change color of header on scrolling
   var scroll_start = 0;
   var startchange = $('nav +  section');
   var offset = startchange.offset();
    if (startchange.length){
   $(document).scroll(function() { 
      scroll_start = $(this).scrollTop();
      if(scroll_start > offset.top) {
          $('.navbar-inverse').css('background-color', '#0ed41f');
       } else {
         $('.navbar-inverse').css('background-color', '#017eff');
       }
   });
  }
});

</script>
