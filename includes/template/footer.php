<!----footer--------->
<footer>
    <section class="">
		<?php
			$hide = "";
			if($_SERVER['SCRIPT_NAME'] !== '/index.php'){
				$hide = "hidden";
			}
		?>
        <div class="container <?=$hide ?>">
            <div class="row links">
                <div class="col-xs-12 col-md-3">
                    <div class="footer-head">
                        <h6>Company</h6>
                        <ul class="footer_links">
                            <li><a href="/about.php">About us</a></li>
                            <li><a href="https://healthcare-temps.blogspot.co.uk/">Blog</a></li>
                            <li><a href="/pg_career.php">Careers</a></li>
                            <li><a href="javascript:;">Charity</a></li>
                            <li><a href="/pg_partners.php">Partners</a></li>
                            <li><a href="/service.php">Service</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="footer-head">
                        <h6>Hirers</h6>
                        <ul class="footer_links">
                            <li><a href="/advertise_us.php">Advertise</a></li>
                            <li><a href="/hirer_bonus.php">Bonuses</a></li>
                            <li><a href="/pg_byot.php">BYOT</a></li>
                            <li><a href="/pg_recruiter.php">Recruiter 24/7</a></li>
                            <li><a href="/faq_page.php">FAQs</a></li>
                            <li><a href="javascript:;">User Guide</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="footer-head">
                        <h6>Temps</h6>
                        <ul class="footer_links">
                            <li><a href="/bonus.php">Bonuses</a></li>
                            <li><a href="/pg_compliance.php">Compliance</a></li>
                            <li><a href="/faq_page.php">FAQs</a></li>
                            <li><a href="/features.php ">Features</a></li>
                            <li><a href="/tele_temping.php">Tele-Temping</a></li>
                            <li><a href="javascript:;">Temp Guide</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="footer-head">
                        <h6 class="footer_links">Tools</h6>
                        <ul class="footer_links">
                            <li><a href="javascript:;">E-Shop</a></li>
                            <li><a href="/packages_content.php">Packages</a></li>
                            <li><a href="https://salarybot.co.uk/" target="_blank">Salary Calculator</a></li>
                            <li><a href="http://shocas.co.uk/" target="_blank">Smart Suites</a></li>
                            <li><a href="/tech_support.php">Tech-Support</a></li>
                            <li><a href="javascript:;">What's new</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom_links">
            <div class="row">

                <ul class="bottom_links_ul">
                    <li><a href="/terms_of_use.php">Terms of Use</a></li>
                    <li><a href="/privacy_policy.php">Privacy Policy</a></li>
                    <li><a href="/user_agreement.php">User Agreement</a></li>
                    <li><a href="/faq_page.php">FAQ</a></li>
                    <li><a href="/contact.php">Contact Us</a></li>
                    <li><a href="/modern_slavery_policy.php">Modern Slavery Stmt</a></li>
                    <li><a href="/gdpr_policy.php">GDPR</a></li>
                </ul>

                <div class="right-coll">
                    <h6 class="copyright">
                        &copy;2017-2018 Healthcare-Temps. All rights reserved.
                    </h6>
                </div>
            </div>
        </div>
    </section>
</footer>
