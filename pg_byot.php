<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    h1,h3,p,i {
        color: #FFFFFF;
    }
    p {
        font-size: 16px;
    }
    .single_story > h3 {
        display: inline-block;
    }
    .single_story > i {
        font-size: 30px;
    }
</style>
<div class="container-fluid" style="text-align: center; background-color: #5F8CC3; margin-top: 80px;">
    <h1>Build Your Own Team</h1>
    <p>Need to put together a team of Healthcare Professionals for professional assignments.<br />Source vetted temps from across the U.K.</p>
    <div class="row" style="margin-top: 27px;">
        <div class="col-lg-4 col-lg-offset-1">
            <div class="single_story" style="text-align: right">
                <h3>Talent Pooling</h3> <i class="fa fa-users"></i>
                <p>Log in, Search and create a pool of<br />
                    Talented Healthcare professionals for a<br />
                    temporary assignments.<br />
                    Multi-disciplinary teams improve<br />
                    patient outcome.
                </p>
            </div>
            <div class="single_story" style="text-align: right">
                <h3>Schedule Appointments</h3> <i class="fa fa-calendar"></i>
                <p>Schedule appointments with patient<br />
                    and team, integrate knowledge and<br />
                    innovation to maximize value Achieve<br />
                    quicker results via out-of-hours<br />
                    co-working.
                </p>
            </div>
        </div>
        <div class="col-lg-2">
            <img src="/myassets/img/byot-phone.png" alt="phone">
        </div>
        <div class="col-lg-4" style="padding-left: 36px;">
            <div class="single_story" style="text-align: left">
                <i class="fa fa-comments"></i> <h3>Secure Video Chat</h3>
                <p>Invite healthcare experts world-wide<br />
                    to be part of your team via real time<br />
                    video communication. HIPPA & DPA<br />
                    compliant. Co-ordinate<br />
                    communcation easier and faster.
                </p>
            </div>
            <div class="single_story" style="text-align: left">
                <i class="fa fa-puzzle-piece"></i> <h3>Real Solutions</h3>
                <p>Hire Healthcare experts on-demands<br />
                    Eliminate surgery delays due to staff<br />
                    shortage.
                </p>
            </div>
        </div>
    </div>
</div>


<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>
<!----footer--------->


</body>
</html>
