<style>
    .modal-backdrop.in{ z-index: 1; }

</style>
<?php
//Check if frontinit.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

if ($_GET['id'] == '') {
    Redirect::to('register.php');
}


//Get Site Settings Data
$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
    foreach ($query->results() as $row) {
        $title = $row->title;
        $use_icon = $row->use_icon;
        $site_icon = $row->site_icon;
        $tagline = $row->tagline;
        $description = $row->description;
        $keywords = $row->keywords;
        $author = $row->author;
        $bgimage = $row->bgimage;
    }
}

//Get Payments Settings Data
$q1 = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($q1->count()) {
    foreach ($q1->results() as $r1) {
        $currency = $r1->currency;
        $membershipid = $r1->membershipid;
    }
}

//Getting Payement Id from Database
$query = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
if ($query->count() === 1) {
    $q1 = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
} else {
    $q1 = DB::getInstance()->get("membership_agency", "*", ["membershipid" => $membershipid]);
}
if ($q1->count() === 1) {
    foreach ($q1->results() as $r1) {
        $bids = $r1->bids;
    }
}

if (!function_exists('base_url')) {
    function base_url($atRoot = FALSE, $atCore = FALSE, $parse = FALSE)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            $core = $core[0];

            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf($tmplt, $http, $hostname, $end);
        } else $base_url = 'http://localhost/';

        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }

        return $base_url;
    }
}


function getUploadedCount() {
    $numDocumentUploaded = 0;
    $attachmentFiles = [];
    $conn = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
    $conn->exec("SET CHARACTER SET utf8");
    $sql = "SELECT * FROM freelancer WHERE freelancerid=" . $_GET['id'];
    $result = $conn->query($sql);
    $row = $result->fetch(PDO::FETCH_ASSOC);

    if(count($row)) {
        $sql = "SELECT cbr_dbs,proof,certificates,proofaddress,hepatitis,prof,registration,utr,NHSSmart,wp,ref1,ref2 FROM documentupload WHERE user_id=". $row['id'];
        $result = $conn->query($sql);
        $record = $result->fetch(PDO::FETCH_ASSOC);
        foreach ($record as $k => $v) {
            if(strlen($v)) {
                $numDocumentUploaded++;
            }
        }
        if($numDocumentUploaded > 5) {
            foreach($record as $k => $v) {
                $attachmentFiles[$k] = $v;
            }
        }
    }
    return array('count' => $numDocumentUploaded, 'files' => $attachmentFiles);
}
//Get user
$conn = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
$conn->exec("SET CHARACTER SET utf8");
$sql = "SELECT * FROM freelancer WHERE freelancerid=" . $_GET['id'];
$result = $conn->query($sql);
$row = $result->fetch(PDO::FETCH_ASSOC);
$msg = '';
$userData = [];
if (isset($row['id']) && $row['id'] != '') {
    /* file uploads*/
    $sql = "SELECT * FROM freelancer LEFT JOIN documentupload ON freelancer.id = documentupload.user_id WHERE freelancer.freelancerid = ". $_GET['id'];
    $userData = $conn->query($sql)->fetch(PDO::FETCH_ASSOC);
    $path = "uploads/";
    $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "pdf", "doc", "docx");
    if (isset($_POST) && $_SERVER['REQUEST_METHOD'] == "POST") {
        $count = 0;
        foreach ($_FILES as $file) {
            if($file['error'] == 0) {
                $countUpload++;
            }
        }

        if($countUpload > 5) {
            foreach ($_FILES as $fileKey => $file) {
                $errorsCount = 0;
                $name = $file['name'];
                $size = $file['size'];
                if (strlen($name)) {
                    list($txt, $ext) = explode(".", $name);
                    if (in_array($ext, $valid_formats)) {
                        if ($size < (1024 * 1024)) // Image size max 1 MB
                        {
                            $actual_image_name = time() . 'health' . $_GET['id'] . "." . $ext;
                            $tmp = $file['tmp_name'];

                            if (move_uploaded_file($tmp, $path . $actual_image_name)) {
                                try {
                                    $sql = "SELECT id FROM documentupload WHERE user_id=" . $row['id'];
                                    $result = $conn->query($sql);
                                    $record = $result->fetch(PDO::FETCH_NUM);

                                    if (isset($record) && $record[0] != '') {
                                        $sql = "UPDATE documentupload SET " . $fileKey . "='" . $actual_image_name . "' WHERE id=" . $record[0];
                                        $conn->exec($sql);
//                                        echo 'File uploaded successfully';

                                    } else {
                                        $sql = "INSERT INTO documentupload (user_id," . $fileKey . ") VALUES ('" . $row['id'] . "','" . $actual_image_name . "')";
                                        $conn->exec($sql);
//                                        echo 'File uploaded successfully';
                                    }

                                } catch (PDOException $e) {
                                    //echo $sql . "<br>" . $e->getMessage();
                                }
                            }
                        } else {
//                            echo 'Image file size max 1 MB';

                        }
                    } else {

//                        echo 'Invalid file format.. and format must be jpg, png, gif, bmp,jpeg,pdf, doc, docx';

                    }
                } else {
//                    echo 'Please select image..!';
                }
            }
            $numberOfUploads = getUploadedCount();
            if($numberOfUploads['count'] > 5) {
//                echo '<pre>';print_r($numberOfUploads);die;
                //Sending mail to admin with attachments
                $email = $row['email'];
                $adminMail = 'newtemps@healthcare-temps.co.uk';
                $username = $row['username'];
                $title = "Document Verification";
                $subject = "Verify User Documents";
                $message = '<!DOCTYPE html><head></head><body><div style="text-align: center;"><h2>'.$username.' as just uploaded the doocuments with email ID '.$email.'. <br />Please find the attachments to verify user documents.</h2></div></body></html>';

                sendMail('rajnish.virtuelogics@gmail.com', $message, $subject, $title,$numberOfUploads['files']);
                sendMail($adminMail, $message, $subject, $title,$numberOfUploads['files']);
                sendMail('healthcaretempsltd@gmail.com', $message, $subject, $title,$numberOfUploads['files']);
                ////////////////////////////////////////
                Redirect::to('compliance_upload_document.php?id='. $_GET['id']);
            }

        } else {
            $_SESSION['first_step_error'] = 'Upload at least 6 files to proceed to the next step';
        }

    }

} else {
    Redirect::to('register.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    #pages-bonus {
        margin-top: 104px;
    }
    .grid-div {
        display: grid;
        grid-template-columns: repeat(3, 300px);
        grid-gap: 25px;
        grid-auto-rows: minmax(100px, auto);
        grid-auto-columns: minmax(10px, auto);
        align-content: center;
        justify-content: center;
        margin-top: 70px;
        margin-bottom: 40px;
    }
    .grid-div > div {
        border: 1px solid lightgrey;
        padding: 11px;
        box-shadow: 0px 0px 7px;
    }
    .single-div > h4 {
        cursor: pointer;
        color: #1aa933;
    }
    #header {
        z-index: 1;
    }
    #agreement2 {
        overflow-y: auto;
    }
    #agreement3 {
        overflow-y: auto;
    }
    .img-thumbnail inner_img img {
        width: 50px;
        height: 50px;
    }

    .img-thumbnail.inner_img {
        margin-bottom: 33px;
    }

    .compliance-content .img-thumbnail inner_img {
        display: block;
        max-width: 100%;
        height: auto;
        padding: 4px;
        line-height: 1.42857143;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
        margin: 2px auto;
    }

    .wrapper {
        display: grid;
        grid-gap: 10px;
        color: #444;
        margin-bottom: 23px;
    }

    img-thumbnail.inner_img {
        margin-bottom: 33px;
        display: block;
    }

    .img-responsive01 {
        width: 50%;
    }

    .img-thumbnail.inner_img.bottom_img {
        padding: 15px 46px;
    }

    .img-responsive01.bottom_img_01 {
        width: 89%;
    }

    .responsive01.bottom_img_01.bottom_img_02 {
        width: 75%;
    }

    #nxt_btn {
        position: absolute;
        right: -130px;
        top: -7px;
        padding: 7px 24px;
    }
</style>
<div id="pages-bonus" class="content user">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
            </ul>
        </div>

        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container">
                    <div class="row">
                        <!-- ==============================================
                Banner Login Section
                =============================================== -->
                        <section class="banner-login">
                            <?php if (isset($error)) {
                                echo $error;
                            }
                            ?>
                            <div class="compliance-content wrapper" style="display: inline-block;min-height:auto !important;height: auto !important">
                                <?php
                                if(isset($_SESSION['first_step_error'])) {
                                    echo '<span style="color: red; font-size: 16px;">'. $_SESSION['first_step_error']. '</span>';
                                    unset($_SESSION['first_step_error']);
                                }
                                ?>
                                <span style="color:black;">Uploading your registration and complaince documents makes it easier for you to bid for temp jobs and have you bids accepted. Plus, these documents will be used to prove your eligibility to work in the U.K.'s health and medical industry. Please upload a minimum of six documents for your registration to be successful. We aim to approve and activate your account within 24-48 hours.</span>
                            </div>

                            <div class="file_upload">
                                <style>
                                    #imageform label{
                                        white-space: nowrap;
                                    }
                                    #imageform .img-thumbnail{
                                        width: 100%;
                                        text-align: center;
                                    }
                                    #imageform .img-responsive01{
                                        width: auto !important;
                                        height: 100px;
                                    }
                                </style>
                                <input name="u_id" id="userid" value="<?php echo $_GET['id']; ?>" type="hidden">
                                <form id="imageform" method="post" enctype="multipart/form-data" action=''>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img">
                                                <label for="UserCbr/dbs">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/IconDBS.jpg" alt="UploadCRB/DBS" class="img-responsive01" style="margin-bottom:-5px;">
                                                    <p style="margin-bottom: -10px;">DBS/CRB</p>
                                                    <input name="cbr_dbs" style="display: none" id="UserCbr/dbs" value="<?=isset($userData['cbr_dbs']) ? $userData['cbr_dbs'] : ''?>" type="file" class="photoimg">
                                                    
                                                </label>
                                                <small style="cursor: pointer; color: #e33b00" onclick="$('#agreement1').modal('show');">No CRB? Apply now!</small>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img">
                                                <label for="UserProof">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/iconid.png" alt="Upload Proof of Identification" class="img-responsive01">
                                                    <p>Photo I.D.</p>
                                                    <input name="proof" style="display: none" id="UserProof" value="<?=isset($userData['proof']) ? $userData['proof'] : ''?>"  type="file" class="photoimg">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img">
                                                <label for="UserCertificates">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/icontraining.gif" alt="Upload Certificates of Completion Diploma/NVQ" class="img-responsive01">
                                                    <p>Training Certificates</p>
                                                    <input name="certificates" style="display: none" id="UserCertificates" value="<?=isset($userData['certificates']) ? $userData['certificates'] : ''?>" type="file" class="photoimg">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img">
                                                <label for="UserProofaddress">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/iconproof.jpg" alt="Upload Proof of Address" class="img-responsive01">
                                                    <p>Proof of Address</p>
                                                    <input name="proofaddress" value="<?=isset($userData['proofaddress']) ? $userData['proofaddress'] : ''?>" style="display: none" id="UserProofaddress" type="file" class="photoimg">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img">
                                                <label for="UserHepatitis">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/iconhepB.png" alt="Upload Hepatitis B Certificate" class="img-responsive01">
                                                    <p>Flu jab help B/Medical</p>
                                                    <input name="hepatitis" style="display: none" id="UserHepatitis" type="file" value="<?=isset($userData['hepatitis']) ? $userData['hepatitis'] : ''?>" class="photoimg">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img">
                                                <label for="UserProf">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/black-umbrella-png-3.jpg" alt="Upload Hepatitis B Certificate" class="img-responsive01">
                                                    <p>Professional Indemnity</p>
                                                    <input name="prof" style="display: none" id="UserProf" value="<?=isset($userData['prof']) ? $userData['prof'] : ''?>" type="file" class="photoimg">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img">
                                                <label for="UserRegistration">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/reg-number.png" alt="Upload Registration  NMC/GMC" class="img-responsive01">
                                                    <p>Registration Numbers</p>
                                                    <input name="registration" style="display: none" id="UserRegistration" type="file" value="<?=isset($userData['registration']) ? $userData['registration'] : ''?>" class="photoimg">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img">
                                                <label for="UserUTR">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/NINO.png" alt="Upload UTR, NINO &amp; Com Registration" class="img-responsive01">
                                                    <p>NINO</p>
                                                    <input name="utr" style="display: none" id="UserUTR" type="file" value="<?=isset($userData['utr']) ? $userData['utr'] : ''?>" class="photoimg">
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img bottom_img">
                                                <label for="UserNHSSmart">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/NHS.png" alt="Upload UTR, NINO &amp; Com Registration" class="img-responsive01 bottom_img_01  bottom_img_02">
                                                    <p>NHS Smart Card</p>
                                                    <input name="NHSSmart" style="display: none" id="UserNHSSmart" type="file" value="<?=isset($userData['NHSSmart']) ? $userData['NHSSmart'] : ''?>" class="photoimg">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img bottom_img">
                                                <label for="UserWp">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/WP.png" alt="Upload Employment References WorkPermit" class="img-responsive01 bottom_img_01 bottom_img_02">
                                                    <p>Work Permit</p>
                                                    <input name="wp" style="display: none" id="UserWp" type="file" value="<?=isset($userData['wp']) ? $userData['wp'] : ''?>" class="photoimg">
                                                </label>
                                            </div>

                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img bottom_img">
                                                <!-- <span class="btn btn-default btn-file">-->
                                                <label for="UserRef1">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/reference.png" alt="Upload Employment References x1" class="img-responsive01 bottom_img_01">
                                                    <p>Reference X1</p>
                                                    <input name="ref1" style="display: none" id="UserRef1" type="file" value="<?=isset($userData['ref1']) ? $userData['ref1'] : ''?>" class="photoimg">
                                                </label>
                                            </div>

                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="img-thumbnail inner_img bottom_img">
                                                <label for="UserRef2">
                                                    <img src="<?php echo base_url(TRUE); ?>assets/img/reference.png" alt="Upload Employment References x2" class="img-responsive01 bottom_img_01">
                                                    <p>Reference X2</p>
                                                    <input name="ref2" style="display: none" id="UserRef2" type="file" value="<?=isset($userData['ref2']) ? $userData['ref2'] : ''?>" class="photoimg">
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 col-md-offset-1" style="margin-top: 90px;">
                                            <style>
                                                .circle-bar .circles{
                                                    border-radius: 50%;
                                                    width: 40px;
                                                    height: 40px;
                                                    display: inline-block;
                                                    background-color: #fff;
                                                    margin-top: -17px;
                                                }
                                                .blankcircles{
                                                    border-color: green;
                                                    border: 1px solid green;
                                                    border-radius: 50%;
                                                    width: 40px;
                                                    height: 40px;
                                                    display: inline-block;
                                                    background-color: #fff;
                                                    margin-top: -17px;
                                                }
                                                .lgcircles{
                                                    border-radius: 50%;
                                                    width: 60px;
                                                    height: 60px;
                                                    border-color: green;
                                                    border: 1px solid green;
                                                    display: inline-block;
                                                    background-color: #fff;
                                                    margin-top: -23px;
                                                }
                                            </style>
                                            <div class="circle-bar" style=" overflow: visible;position: relative;width: 80%;margin: 0 auto; height: 5px;background-color: green;">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <div class=" lgcircles">
                                                            <i style="font-size: 56px; height: 40px;width: 40px; border-radius: 50%;color: green; " class="fa fa-check" aria-hidden="true"></i>
                                                        </div>
                                                        <p>Compliance<br> Upload Documents</p>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div class="blankcircles">
                                                        </div>
                                                        <p>Tax Compliance <br> &amp; Self-Employment</p>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div class="blankcircles"></div>
                                                        <p>Create CV <br> Profile</p>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <div style="width: 100%;background-color: #fff;height: 5px;"></div>
                                                        <div class="blankcircles">
                                                        </div>
                                                        <p> Delivered</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2 hidden-xs"></div>
                                        <div class="col-md-8">
                                            <div class="text-center">
                                                <p style="color:white;">If you do not have all your registration documents now,
                                                    that's ok. You can upload later. However, you will not be able to start work or
                                                    get paid until these documents are provided, in particular, your CRB/DBS.</p>
                                                <!--                                    --><?php //if($numberOfUploads > 5) { ?>
                                                <button class="btn btn-info" id="nxt_btn" type="submit">Next</button>
                                                <!--                                    --><?php //} ?>
                                            </div>
                                        </div>
                                        <div class="col-md-2 hidden-xs"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>

                        </section><!-- /section -->


                        <!-- Agreement Modals -->
                        <!-- Modal 1 -->
                        <div id="agreement1" class="modal fade" role="dialog">
                            <div class="modal-dialog" style="width: 78%; z-index: 99999;">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div style="padding-top: 40px; ">
                                            <div class="row col-lg-offset-3">
                                                <p>
                                                <h3 style="color:#6b5c93;"> Applicant consent</h3>
                                                <p>
                                                    By clicking confirm below, it infer that you are bound by our tem#rms and<br />
                                                    conditions and you have given us consent to carry our a DBS / CRB check on<br />
                                                    you. Our terms and condition can be found on the footer of the home page.
                                                </p>
                                                <p>
                                                <hr style="border: 1px solid #000000;width: 70px;float: left;"><br />
                                                </p>
                                                <p class="text-muted">
                                                    By clicking "I Confirm" you are agreeing the following :<br />
                                                    That on this date you the applicant:
                                                </p>
                                                <ul class="text-muted">
                                                    <li>
                                                        Gave consent for the applicant<br />
                                                    </li>
                                                    <li>
                                                        Represented his/her identity to be true<br />
                                                    </li>
                                                    <li>
                                                        Corrobrated his/her identity to be true via documentation<br />
                                                    </li>
                                                    <li>
                                                        Gave a declaration about the information he/she provided
                                                    </li>
                                                </ul>
                                                <p class="text-muted" style="margin-top:10px;">
                                                    A record of consent will be recorded electronically and retained for a period of 12<br />months,<br />
                                                    All information requested is used solely for the purpose of producing a Dislosure<br />
                                                    and Barring Service of Disclosure Scotland certificate and is collected, stored and<br />
                                                    processed by UK CRBS ltd and the Disclosure<br />
                                                    Scotland in accordance with the Data protection Act 1998. All personal information
                                                </p>
                                                <p style="border: 1px solid #cecbcb; width:80%"><hr></p>
                                                <p>
                                                    in order to complete this application form you will copies of:
                                                </p>
                                                <p>
                                                    1. Current valid Passport(if applicable)<br />
                                                    2. Current Driving Licence UK, isie of man, Channel island and EU (Photo card style only)<br />
                                                    3. Your national insurance number (if application)<br />
                                                    4. Address details for the last 5 years including the dates you moved in and out of each property<br />
                                                    5. Details of any previous name you have been known by including the dates that you changed them
                                                </p>
                                                <div class="row row col-lg-offset-5">
                                                    <button type="button" data-dismiss="modal" style="background-color: #f2480d;color: white;padding: 6px 30px;">I Decline</button>
                                                    <button type="button" data-dismiss="modal" style="background-color: #58226d;color: white;padding: 6px 30px;" onclick="setTimeout($('#agreement2').modal('show'), 100);">I Confirm</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Modal 2 -->
                        <div id="agreement2" class="modal fade" role="dialog">
                            <div class="modal-dialog" style="width: 78%; z-index: 99999; maring-top: 64px;">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="" method="post" id="agreement2form" onsubmit="return false">
                                        <div class="modal-body">
                                            <div class="container-fluid col-lg-offset-1">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <h2 style="color: #3c9a66;">
                                                            <b>Disclosure $ Barring Services / CRB</b>
                                                        </h2>
                                                        <p style="color: #3c9a66;">
                                                            it is mandatory to have a valid DBS / CBR - Criminal Record Check to work in the U.K's healthcare industry.<br />
                                                            Please fill in your details in the fields below in order to apply for a DBS/CRB
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-lg-0">
                                                        <nav style="color: #3c9a66;border: 10px;">
                                                            <div class="container-fluid">
                                                                <div class="navbar-header">
                                                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                                                        <span class="icon-bar"></span>
                                                                        <span class="icon-bar"></span>
                                                                        <span class="icon-bar"></span>
                                                                    </button>
                                                                </div>
                                                                <div class="collapse navbar-collapse" id="myNavbar" style="margin-left: -46px;">
                                                                    <ul class="nav navbar-nav" style="border: 1px solid #dbdbdb;margin-bottom: 10px;">
                                                                        <li><a href="#" style="color: #3c9a66"><b>Personal Details</b></a></li>
                                                                        <li><a href="#" style="color: #3c9a66"><b>Address details</b></a></li>
                                                                        <li><a href="#" style="color: #3c9a66"><b>Verifu I.D.</b></a></li>
                                                                        <li><a href="#" style="color: #3c9a66"><b>Declaration</b></a></li>
                                                                        <li><a href="#" style="color: #3c9a66"><b>I.D Confirmation</b></a></li>
                                                                        <li><a href="#" style="color: #3c9a66"><b>Payment</b></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </nav>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-lg-8" style="border: 1px solid #c9c1c1; padding: 10px;">
                                                        <h3>
                                                            Applicant Personal Details
                                                        </h3>
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td>Title:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td>
                                                                    <input type="text" name="title" class="agree_field">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Gender:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td>
                                                                    <label class="checkbox-inline">
                                                                        <label><input type="radio" name="gender" value="Male" checked>Male</label>
                                                                        <label><input type="radio" name="gender" value="Female">Female</label>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>First Name:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td><input type="text" name="first_name" class="agree_field"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Middle Name(s)</td>
                                                                <td><input type="text" placeholder="Middle Name:" name="middle_name"></td>
                                                                <td><input type="text" placeholder="Middle Name:" name="middle_name"></td>
                                                                <td><input type="text" placeholder="Middle Name:" name="middle_name"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Last Name / Family<br /> Name:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td><input type="text" name="last_name" class="agree_field"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Date of Birth:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td>
                                                                    <input type="date" name="dob" class="agree_field">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Telephone Number:</td>
                                                                <td><input type="tel" name="phone" class="agree_field"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Email:</td>
                                                                <td><input type="email" name="email" class="agree_field"></td>
                                                            </tr>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <h3>Place of Birth</h3>
                                                        <p style="border: 1px solid #dddada; width: 65%;">
                                                        <hr>
                                                        </p>
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td>Town/City:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td><input type="text" name="city" class="agree_field"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Country:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td>
                                                                    <select name="country">
                                                                        <option value="United Kingdom" class="agree_field">United Kingdom</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Nationality at Birth<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td><input type="text" name="birth_nation"  class="agree_field"></td>
                                                            </tr>
                                                            <h5>Previous Names</h5>
                                                            <p>Have you/the application ever been known by any other name?<span>*</span></p>
                                                            <td>
                                                                <label class="checkbox-inline">
                                                                    <label><input type="radio" name="other_name" value="yes" checked>Yes</label>
                                                                    <label><input type="radio" name="other_name" value="no">No</label>
                                                                </label>
                                                            </td>
                                                            </tbody>
                                                        </table>
                                                        <h3>Current Address</h3>
                                                        <p style="border: 1px solid #dddada; width: 65%;">
                                                        <hr>
                                                        </p>
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td>Address 1:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td><input type="text" name="address1" class="agree_field"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td>address 2:</td>
                                                                <td><input type="text" name="address2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Town/City:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td><input type="text" name="address3"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Country:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td>
                                                                    <select name="current_country">
                                                                        <option value="United Kingdom" selected>United Kingdom</option>
                                                                    </select>
                                                            </tr>
                                                            <tr>
                                                                <td>Postcode:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td><input type="text" name="postal_code" class="agree_field"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>At Address<br /> Since:<span style="color: red;font-size: 25px;">*</span></td>
                                                                <td>
                                                                    <input type="date" name="since_date" class="agree_field">
                                                                </td>
                                                            </tr>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="row " style="float: right;margin-right: 15px; ">
                                                            <button type="button" style="background-color: #53235d; color:white;" data-dismiss="modal" onclick="setTimeout($('#agreement3').modal('show'), 100);" class="btn btn-md btn-lg" >Continue To Next Step <i class="fas fa-arrow-right"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="freelancer_id" value="<?=$_GET['id']?>">
                                    </form>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Modal 3 -->
                        <div id="agreement3" class="modal fade" role="dialog">
                            <div class="modal-dialog" style="width: 78%;">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form id="agreement3form" action="" method="post" onsubmit="return false;">
                                        <div class="modal-body">
                                            <div class="container-fluid col-lg-offset-1">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <h2 style="color: #3c9a66;">
                                                            <b>Disclosure $ Barring Services / CRB</b>
                                                        </h2>
                                                        <p style="color: #3c9a66;">
                                                            it is mandatory to have a valid DBS / CBR - Criminal Record Check to work in the U.K's healthcare industry.<br />
                                                            Please fill in your details in the fields below in order to apply for a DBS/CRB
                                                        </p>
                                                    </div>
                                                </div>
                                                <!--------------------navBar------------------------------>
                                                <div class="row">
                                                    <div class="col-xs-12 col-lg-0">
                                                        <nav style="color: #3c9a66;border: 10px;">
                                                            <div class="container-fluid">
                                                                <div class="navbar-header">
                                                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                                                        <span class="icon-bar"></span>
                                                                        <span class="icon-bar"></span>
                                                                        <span class="icon-bar"></span>
                                                                    </button>
                                                                </div>
                                                                <div class="collapse navbar-collapse" id="myNavbar" style="margin-left: -46px;">
                                                                    <ul class="nav navbar-nav" style="border: 1px solid #dbdbdb;margin-bottom: 10px;">
                                                                        <li><a href="#" style="color: #3c9a66"><b>Personal Details</b></a></li>
                                                                        <li><a href="#" style="color: #3c9a66"><b>Address details</b></a></li>
                                                                        <li><a href="#" style="color: #3c9a66"><b>Verifu I.D.</b></a></li>
                                                                        <li><a href="#" style="color: #3c9a66"><b>Declaration</b></a></li>
                                                                        <li><a href="#" style="color: #3c9a66"><b>I.D Confirmation</b></a></li>
                                                                        <li><a href="#" style="color: #3c9a66"><b>Payment</b></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </nav>
                                                    </div>
                                                </div>
                                                <!--------------------identity---------------------------->
                                                <div class="row" >
                                                    <div style="border: 1px solid #dbdbdb;padding: 10px 15px;width: 78%;">
                                                        <h5 style="display: inline-block;">Identity</h5>
                                                        <input type="text" name="identity" class="agree_field" style="border:none;width:62%;margin-left: 50px;">
                                                        <p>
                                                        <hr style="border: 1px solid #dbdbdb;width: 97%;"><br />
                                                        </p>
                                                        <h5 style="display: inline-block;">NI Information</h5>
                                                        <input type="text" name="infomation" class="agree_field" style="border:none;width:62%;margin-left: 50px;">
                                                        <p>
                                                        <hr style="border: 1px solid #dbdbdb;width: 97%;"><br />
                                                        </p>
                                                        <div class="form-group">
                                                            <div class="">
                                                                <label for="number">NI Number<span style="color: red;font-size: 25px;">*</span></label>
                                                                <input style="width: 10%;" type="text" class="agree_field" name="ni1">
                                                                <input style="width: 10%;" type="text" class="agree_field" name="ni2">
                                                                <input style="width: 10%;" type="text" class="agree_field" name="ni3">
                                                                <input style="width: 10%;" type="text" class="agree_field" name="ni4">
                                                                <input style="width: 10%;" type="text" class="agree_field" name="ni5">

                                                                <i class="fas fa-info-circle"></i>
                                                            </div>
                                                            <h2  style="display: inline-block;">Driving Licence Information</h2>
                                                            <input type="text" name="driving" style="border:none;width:58%;margin-left: 50px;">
                                                            <p>
                                                            <hr style="border: 1px solid #dbdbdb;width: 97%;"><br />
                                                            </p>
                                                            <div>
                                                                <table>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>Are you able to<br />provide the driving<br />licence information?<span style="color: red; font-size: 25px;">*</span> </td>
                                                                        <td>
                                                                            <label class="checkbox-inline">
                                                                                <label><input type="radio" name="able" value="yes" checked>Yes</label>
                                                                                <label><input type="radio" name="able" value="no">No</label>
                                                                            </label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Date Of Birth:<span style="color: red; font-size: 25px;">*</span></td>
                                                                        <td><input type="date" name="driver_dob" class="agree_field"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Licence Valid From:<span style="color: red;font-size: 25px;">*</span></td>
                                                                        <td>
                                                                            <input type="date" name="license_valid_date" class="agree_field">
                                                                        </td>
                                                                    </tr>
                                                                    <td>
                                                                        Licence Type:<span style="color: red;font-size: 25px;">*</span>
                                                                    </td>
                                                                    <td>
                                                                        <label class="checkbox-inline">
                                                                            <label><input type="radio" name="paper" value="paper" checked>Paper</label>
                                                                            <label><input type="radio" name="paper" value="photocard">Photocard</label>
                                                                        </label>
                                                                    </td>
                                                                    <tr>
                                                                        <td>
                                                                            Country Of Issue:<span>*</span>
                                                                        </td>
                                                                        <td>
                                                                            <label class="checkbox-inline">
                                                                                <label><input type="radio" name="united" value="united" checked>United Kingdom</label>
                                                                                <label><input type="radio" name="united" value="Northern Ireland">Photocard</label>
                                                                            </label>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>Town/City:<span style="color: red;font-size: 25px;">*</span></td>
                                                                        <td><input type="text" name="driver_city" class="agree_field"> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Country:<span style="color: red;font-size: 25px;">*</span></td>
                                                                        <td>
                                                                            <select name="driver_location">
                                                                                <option>United Kingdom</option>
                                                                            </select>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Postcode:<span style="color: red;font-size: 25px;">*</span></td>
                                                                        <td><input type="text" name="driver_postcode" class="agree_field"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>At Address Since:<span style="color: red;font-size: 25px;">*</span></td>
                                                                        <td>
                                                                            <input type="date" name="address_since" class="agree_field">
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!---------------------------declaration------------------>
                                                    <div class="row" style="border: 1px solid #dbdbdb;padding: 10px 15px; width: 78%; margin: 10px 0px;">
                                                        <h3  style="display: inline-block;">Declaration</h3>
                                                        <input type="text" name="declaration" class="agree_field" style="border:none;width:62%;margin-left: 50px;">
                                                        <p>
                                                        <hr style="border: 1px solid #dbdbdb;width: 97%;"><br />
                                                        </p>
                                                        <p>
                                                            <span style="color: red;font-size: 25px;">*</span> Does the applicant have any convictions, cautions,reprimands or final warnings which would not be filtered in line with current guidance?
                                                        </p>
                                                        <label class="checkbox-inline">
                                                            <label><input type="radio" name="convict" value="yes" checked>Yes</label>
                                                            <label><input type="radio" name="convict" value="no">No</label>
                                                        </label>
                                                        <p>
                                                        <hr style="border: 1px solid #dbdbdb;width: 97%;"><br />
                                                        </p>
                                                    </div>
                                                    <!----------------------------position-------------------------->
                                                    <div style="border: 1px solid #dbdbdb;padding: 10px 15px; width: 78%;">
                                                        <h3  style="display: inline-block;">Position Applied For</h3>
                                                        <input type="text" class="agree_field" style="border:none;width:62%;margin-left: 50px;">
                                                        <p>
                                                        <hr style="border: 1px solid #dbdbdb;width: 97%;"><br />
                                                        </p>
                                                        <p>This applicant well be working with:<span style="color: red;font-size: 25px;">*</span></p>
                                                        <select>
                                                            <option>Please choose...</option>
                                                        </select>
                                                        <h3>
                                                            Sign Off
                                                        </h3>
                                                        <div class="" style="background-color: #fef7e4; margin-bottom: 10px; padding-left:10px; ">
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" checked style="margin-left: -25px;" value="">Please confirm the information you have provided is complete and true information.knowing a false statement is a criminal offence
                                                            </label>
                                                        </div>
                                                        <div style="background-color: #daecf8; padding-left:10px;">
                                                            <label class="checkbox-inline">
                                                                <input type="checkbox" checked style="margin-left: -25px;" value="">I understand that any errors with the application's personal details may produce an inaccuate disclosure certificate and therefore require<br />
                                                                another application to be submitted. if errors are found after submission to the DBS the disclosure will still be charged at the full cost.
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div style="border: 1px solid #dbdbdb;padding: 10px 15px; width: 78%; margin-top: 10px; margin-bottom: 30px;">
                                                        <div class="row" style="padding-left: 10px;">
                                                            <button name="previous" style="background-color: #e0e1dc; color: #5b1f67; padding: 8px 12px;"><i style="margin-right: 5px;" class="fas fa-arrow-left"></i>Return To Previous Step</button>

                                                            <div class="" style="float: right; margin-right: 20px;">
                                                                <button name="save" style="background-color: #5b1f67; color: white; padding: 8px 12px;" onclick="submitAgreements()"> save and close<i style="margin-left:5px;" class="fas fa-share-square"></i></button>
                                                                <button name="save" style="background-color: #5b1f67; color: white; padding: 8px 12px;"> submit <i style="margin-left: 5px;" class="fas fa-share-square"></i> </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <script>
                            function submitAgreements() {
                                var status = 1;
                                $('.agree_field').each(function() {
                                    if($.trim($(this).val()) == "") {
                                        status = 0;
                                    }
                                });
                                if(status == 0) {
                                    alert('Please fill all the details.');
                                    return false;
                                }
                                $.ajax({
                                    type: 'POST',
                                    url: 'agreement_submit.php',
                                    data: $('#agreement2form').serialize() + $('#agreement3form').serialize(),
                                    success: function (data) {
                                        alert('Success');
                                        $('#agreement3').modal('hide');
                                    }
                                });
                            }
                        </script>
                        <script type="text/javascript">
                            $(document).ready(function () {
//        $('.photoimg').on('change', function () {
//            $("#imageform").ajaxForm({
//                    success: processJson,
//                }).submit();
//        });
                            });

                            function processJson(data) {
                                alert(data);
                                window.location.reload();
                            }

                            console.log(<?=$numberOfUploads?>);
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

</body>
</html>

