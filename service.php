<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    .container > div.row {
        padding: 10px;
    }
    .info_head > span.fa {
        font-size: 30px;
        color: #FFFFFF;
        padding-top: 22px;
    }
    .info_head > h3 {
        margin: 0px;
        color: #FFFFFF;
        padding: 25px 0px;
    }
    .info_body {
        background-color: #FFFFFF;
        padding: 20px;
    }
    .info_footer {
        background-color: lightgrey;
        padding: 11px;
    }
    .info_footer > button {
        color: steelblue;
        font-weight: bold;
    }
    .info_body > ul >li {
        margin-left: -18px;
    }
</style>
<div class="container-fluid" style="text-align: center; margin-top: 80px;">
    <div class="row" style="background-color: rgb(0, 127, 255)">
        <h1 style="font-weight: bold; color: #FFFFFF">Hello Hirer</h1>
        <h3 style="color: #FFFFFF">Healthcare Temps offers four premium services to help enhance your<br />
            hiring experience on our website.</h3>
    </div>
    <div class="row" style="background-color: rgb(255, 255, 255)">
        <h3 style="color: limegreen">Choose a plan that is tailored to your individual needs.</h3>
    </div>
    <div class="row" style="background-color: rgb(231, 235, 236)">
        <div class="col-lg-3">
            <div class="info_head" style="background-color: rgb(47, 198, 31)">
                <span class="fa fa-suitcase"></span>
                <h3>Urgent job placement</h3>
            </div>
            <div class="info_body">
                <ul>
                    <li>Stand out from the crowd and bid faster</li>
                    <li>Your job requirements appears on<br />our home page or premium listing<br />page.</li>
                    <li>Get 4X time more interest from<br />targeted job alerts.</li>
                </ul>
            </div>
            <div class="info_footer">
                <button class="btn">Select Plan</button>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="info_head" style="background-color: rgb(65, 162, 217)">
                <span class="fa fa-user"></span>
                <h3>Temp pro 24/7</h3>
            </div>
            <div class="info_body">
                <ul>
                    <li>Eliminiate staff shortage to near<br />0.1%</li>
                    <li>Get a dedicated recruiter to search<br />& fill your vacancy 24/7</li>
                    <li>Save time & money by using a<br />recruiter  to supply fully vetted<br />temps</li>
                </ul>
            </div>
            <div class="info_footer">
                <button class="btn">Select Plan</button>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="info_head" style="background-color: rgb(218, 137, 58)">
                <span class="fa fa-users"></span>
                <h3>BYOT</h3>
            </div>
            <div class="info_body">
                <ul>
                    <li>Build a team for special short term assignments</li>
                    <li>Handpick the talent you want &<br />when</li>
                    <li>Talent pool from group of<br />Surgeons, Consultants, Nurses etc<br />for operation, research or one-off<br />project</li>
                </ul>
            </div>
            <div class="info_footer">
                <button class="btn">Select Plan</button>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="info_head" style="background-color: rgb(134, 115, 170)">
                <span class="fa fa-handshake-o"></span>
                <h3>Premium Advertising</h3>
            </div>
            <div class="info_body">
                <ul>
                    <li>Promote your company's brand<br />infront of 1000's of healthcare<br />temps across the UK</li>
                    <li>Your company's message is sent direct to temps mobile</li>
                    <li>Build trusted relationships with<br />propective temps</li>
                </ul>
            </div>
            <div class="info_footer">
                <button class="btn">Select Plan</button>
            </div>
        </div>
    </div>
    <div class="row" style="padding: 0px; margin-top: -11px;">
        <h4 style="padding: 20px 10px; background-color: #E7EBEC; color: limegreen">We also offer technology enabled tele-health solutions<br />
            healthcare providers & instituions at zero cose.<br />
            Thus, no need to invest in your own tele-health software.</h4>
    </div>
    <div class="row" style="background-color: rgb(255, 255, 255);margin-top: -10px;padding-top: 19px;">
        <div class="col-lg-3">
            <span class="fa fa-desktop" style="font-size: 47px;color: #499DB7"></span>
            <h3 style="color: limegreen">Provider access<br />Virtual care</h3>
        </div>
        <div class="col-lg-3">
            <span class="fa fa-users" style="font-size: 47px;color: #499DB7"></span>
            <h3 style="color: limegreen">Support groups<br />Tele-medicine</h3></div>
        <div class="col-lg-3">
            <span class="fa fa-user" style="font-size: 47px;color: #499DB7"></span>
            <h3 style="color: limegreen">Specialist to Specialist<br />Video Consultation</h3></div>
        <div class="col-lg-3">
            <span class="fa fa-heartbeat" style="font-size: 47px;color: #499DB7"></span>
            <h3 style="color: limegreen">Tele-temping</h3></div>
    </div>
</div>

<!-- Include footer.php. Contains footer content. -->
<?php ///include ('includes/template/footer.php'); ?>

</body>
</html>
