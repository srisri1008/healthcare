<?php
//Check if frontinit.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

if ($_GET['id'] == '') {
    Redirect::to('register.php');
}

function sendMailTemps($email,$message,$subject,$title,$attachments = [], $smail = 'developer@healthcare-temps.co.uk',$smailpass = 'I6kk0$m0')
{
    require_once 'includes/vendor/autoload.php';

    $mail = new PHPMailer;
//		$mail->SMTPDebug = 3;                               // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'auth.smtp.1and1.co.uk';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = $smail;                 // SMTP username
    $mail->Password = $smailpass;                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to
//    $mail->SMTPDebug = 2;
    $mail->setFrom($smail, $title);
    $mail->addAddress($email);     // Add a recipient
    $mail->addReplyTo($smail, $title);
//		$mail->addCC('cc@example.com');
//		$mail->addBCC('bcc@example.com');

    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body    = $message;
    $mail->AltBody = $message;
    if(count($attachments)) {
        foreach($attachments as $k => $v) {
            if(strlen($v)) {
                $mail->addAttachment( $_SERVER['DOCUMENT_ROOT'].'/uploads/'. $v, $k);
            }
        }
    }
    
    try {
      $mail->Send();
//      echo "Message Sent OK\n";
    } catch (phpmailerException $e) {
      echo $e->errorMessage(); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
      echo $e->getMessage(); //Boring error messages from anything else!
    }
     
}
 

//Get Site Settings Data
$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
    foreach ($query->results() as $row) {
        $title = $row->title;
        $use_icon = $row->use_icon;
        $site_icon = $row->site_icon;
        $tagline = $row->tagline;
        $description = $row->description;
        $keywords = $row->keywords;
        $author = $row->author;
        $bgimage = $row->bgimage;
    }
}

//Get Payments Settings Data
$q1 = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($q1->count()) {
    foreach ($q1->results() as $r1) {
        $currency = $r1->currency;
        $membershipid = $r1->membershipid;
    }
}

//Getting Payement Id from Database
$query = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
if ($query->count() === 1) {
    $q1 = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
} else {
    $q1 = DB::getInstance()->get("membership_agency", "*", ["membershipid" => $membershipid]);
}
if ($q1->count() === 1) {
    foreach ($q1->results() as $r1) {
        $bids = $r1->bids;
    }
}

if (!function_exists('base_url')) {
    function base_url($atRoot = FALSE, $atCore = FALSE, $parse = FALSE)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            $core = $core[0];

            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf($tmplt, $http, $hostname, $end);
        } else $base_url = 'http://localhost/';

        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }

        return $base_url;
    }
}
$conn = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
$conn->exec("SET CHARACTER SET utf8");
$sql = "SELECT * FROM freelancer WHERE freelancerid=" . $_GET['id'];
$result = $conn->query($sql);
$row = $result->fetch(PDO::FETCH_NUM);
$msg = '';

if (isset($row[0]) && $row[0] != '') {
    $isActive = $row[12];
    if(!$isActive) {
        $temp_code = md5(mt_rand(18828383331882838, 98823338381882838)); //Generating random token number
        $conn->query("UPDATE freelancer SET temp_code = '". $temp_code. "' WHERE id = ". $row[0]);
        $email = $row[6];
//        $email = 'rajnish.virtuelogics@gmail.com';
        $title = "Welcome To Health Care";
        $subject = "Verify Your Health Care Account";
        $message = '<!DOCTYPE html>
                    <html lang="en"><head>
                        <title>Demo</title>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <!--Bootstrap Css-->
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                        <!--Jquery-->
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                        <!--Javascript-->
                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                        <!--Font awesome-->
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                        <style>
                            .mylogocontainer {
                                display: table;
                                padding: 60px 62px;
                                margin: 0 auto;
                            }
                            .mylogo {
                                width: 350px;
                                margin:  0 auto;
                                margin-bottom: 40px;
                                display:  block;
                            }
                            .welcometext {
                                color: #1DB2CB;
                                font-size: 31px;
                                text-align:  center;
                            }
                            .largestmedical {
                                text-align:  center;
                                font-size: 22px;
                            }
                            .clickbelow {
                                margin-top: 32px;
                                font-size: 20px;
                                text-align:  center;
                                color: #EE9692;
                            }
                            .getstarted {
                                background-color: #53A2D7;
                                border: 1px solid #484848;
                                color: white;
                                padding: 4px 13px;
                                font-size: 20px;
                                border-radius: 4px;
                            }
                            
                        </style>
                    </head>
                    <body>
                    <div class="mylogocontainer">
                        <table>
                            <tbody>
                                <tr>
                                    <td><img class="mylogo" src="'.$_SERVER['HTTP_HOST'].'/assets/img/HT_2.png"></td>
                                </tr>
                                <tr>
                                    <td class="welcometext">Hi '.ucfirst($row[2]).', welcome to Healthcare-Temps</td>
                                </tr>
                                <tr>
                                    <td class="largestmedical">The U.K. Largest medical &amp; healthcare temp recruitment platform.</td>
                                </tr>
                                <tr>
                                    <td class="largestmedical">Hospital jobs, private clinics, care homes, day centres, the choice is yours.</td>
                                </tr>
                                <tr>
                                    <td><p class="clickbelow">Firstly, let\'s help you create a digital Temp CV</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align:  center;"><a href="confirm_user.php?code='.$temp_code.'" style="cursor: pointer;"><button class="getstarted">Let\'s Get Started</button></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </body></html>';

        sendMailTemps($email, $message, $subject, $title);
        ?>
        <html lang="en"><head>
            <title>Demo</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!--Bootstrap Css-->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <!--Jquery-->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <!--Javascript-->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <!--Font awesome-->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <!-- Custom Css-->
        </head>
        <body>
            <div class="container" style="border: 2px solid lightgray; text-align: center; margin: 154px 465px; width:438px;">
                <div class="row">
                    <img src="myassets/images/bgirl.jpg" style="width:150px; height:150px; border-radius: 200px; margin-top: -80px;">
                </div>
                <div class="row">
                    <img src="myassets/images/chota-tick.png" style="margin-top:10px">
                </div>
                <h3 style="color: green; margin-top:10px;">Congratulations</h3>
                <p style="padding: 10px 0px;">
                    You have successfully signed up with healthcare-temps.<br>
                    You are now one step away from earning money on oursite.<br>
                    But first, check your email account for our welcome message.
                </p>
                <a href="/bonus.php"><button class="btn btn-success" style="padding: 10px 0px 10px 0px;margin-bottom: 26px;width: 381px;">Finish</button></a>
            </div>
        </body>
        </html>
        <?php
    }
    else {
        Redirect::to('login.php');
    }

} else {
    Redirect::to('register.php');
}
