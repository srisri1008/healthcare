<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    .colorblue{
        color: #553;
    }
    .sourcil{
        border: 1px solid #6E3A9F;
        border-radius: 49px;
        padding: 3px 10px;
        color: white;
        background-color: #6E3A9F;
        font-size: 15px;
    }
    .colorgreen{
        color: #1FB34D
    }
    .hsize{
        display: inline-block;
        margin-left: 20px;
        color: #6E3A9F;
    }
    .psize{
        margin-left: 51px;
    }
    .recruiter{
        background-color: #6E3A9F;
        color: white;
        border-radius: 5px;
        position: relative;
        top: -50px;
        right: -162px;
    }
</style>
<div class="container-fluid" style="margin-bottom: 20px;margin-top:80px">
    <div class="row" style="width: 100%; height:20px; background-color: #1FB34D"></div>
    <div class="row col-lg-offset-4">
        <div class="col-lg-5">
            <h4 style="font-family: initial;">Say goodbye to staff shortage<br />We are open 24/7/365</h4>
            <h3 class="colorgreen" style="font-size: 28px;">A tailor-made solution<br />guaranteed to help you<br />find a healthcare temp</h3>
        </div>
        <div class="col-lg-4">
            <img src="/myassets/img/hr-girl.png" style="height: 235px;">
        </div>
    </div>
    <div class="col-lg-offset-4">
        <h4 class="colorgreen" style="border: 1px solid black; padding: 5px 10px; display: inline-block;">We are quick - We're successful-We guarantee placement</h4>
    </div>
    <div class="row" style="width: 90%;margin-top: 20px;">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5 col-lg-offset-1">
            <form  action="" method="post" class="form" style="border: 1px solid #ccc6c6; padding: 20px; margin-top: 20px;">
                <span class="recruiter row" >RECRUITER</span>
                <p>What type of healthcare temp do you need ?</p>
                <div class="form-group">
                    <input type="text" class="form-control" name="need" placeholder="What do you need?" >
                </div>
                <p>Describe what you want done*</p>
                <div class="form-group">
                    <textarea   class="form-control" placeholder="Describe what you want done"></textarea>
                </div>
                <p>What skills are required?*</p>
                <div class="form-group">
                    <input type="tetx" class="form-control" name="skills">
                </div>
                <p>Shift required covering?</p>
                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <input type="date" class="form-control" id="myDate" placeholder="Date" >
                        </div>
                    </div>
                    <div class="col-lg-6 col-lg-offset-1 form-group">
                        <select name="ShiftTime" class="form-control" style=" margin-left: 10px; ">
                            <option value="1">Volvo XC90</option>
                            <option value="2">Saab 95</option>
                            <option value="3">Mercedes</option>
                            <option value="4">Audi TT</option>
                        </select>
                    </div>
                </div>
                <p>What budget do you have in mind?*</p>
                <div class="row">
                    <div class="col-lg-5 ">
                        <div class="form-group">
                            <input type="text" id="pounds" class="form-control" value="Pounds" style="display: inline-block;">
                        </div>
                    </div>
                    <div class="col-lg-6 col-lg-offset-1 form-group">
                        <div class="form-group">
                            <select name="ShiftTime" class="form-control" style=" margin-left: 10px; ">
                                <option value="1">Volvo XC90</option>
                                <option value="2">Saab 95</option>
                                <option value="3">Mercedes</option>
                                <option value="4">Audi TT</option>
                            </select>
                        </div>
                    </div>
                </div>
                <p>Posting a temp recruiter job incuers a fee of &pound;99.00</p>
                <div class="row" style="text-align: center;">
                    <button class="postjob" style="padding: 10px; background-color: #436cdb; color: white; text-align: center; border: none;" >Post job to us</button>
                </div>

            </form>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5" style="margin-left: 93px;">
            <h4 class="colorgreen">How does"Temp Recruiter 24/7"works?</h4>
            <span class="colorblue sourcil">1</span><h5 class="colorblue hsize">Getting started:</h5><p class="psize">Log in to your Hirer's account</p>
            <span class="colorblue sourcil">2</span><h5 class="colorblue hsize" >Posting Jobs:</h5><p class="psize">Complete the recruiter 24/7 from on the right</p>
            <span class="colorblue sourcil">3</span><h5 class="colorblue hsize">Make payment:</h5><p class="psize">Once payment is made, a member of our Temp Recruiter 24/7<br />team will contact you with confirmation of staff availability:</p>
            <span class="colorblue sourcil">4</span><h5 class="colorblue hsize">Receive profiles & Hire:</h5><p class="psize">After confirmation, a number of staff profiles will be sent to you<br /> so you can choose and hire.</p>
        </div>

    </div>
</div>
<!-- Include footer.php. Contains footer content. -->
<?php //include ('includes/template/footer.php'); ?>

</body>
</html>
