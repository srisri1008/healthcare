<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}

//echo '<pre>';print_r($_SERVER);echo '</pre>';

$hostname = $_SERVER['HTTP_HOST'];

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg">

<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->

<div class="container">
    <div class="content">
        <div class="row">

            <?php
            //Start new Admin object
            $admin = new Admin();
            //Start new Client object
            $client = new Client();
            //Start new Freelancer object
            $freelancer = new Freelancer();

            if ($admin->isLoggedIn()) { ?>
            <?php } elseif($freelancer->isLoggedIn()) { ?>
            <?php } elseif($client->isLoggedIn()) { ?>


<!--                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">-->
<!--                    <a href="hirer/addjob.php" class="kafe-btn kafe-btn-mint full-width revealOnScroll" data-animation="bounceIn" data-timeout="400">-->
<!--                        <i class="fa fa-tags"></i>-->
<!--                        --><?php //echo $lang['post']; ?><!-- --><?php //echo $lang['a']; ?><!-- --><?php //echo $lang['job']; ?><!--, --><?php //echo $lang['it\'s']; ?><!-- --><?php //echo $lang['free']; ?><!-- !-->
<!--                    </a>-->
<!--                </div><!-- /.col-lg-3 -->-->
<!--            --><?php //} else { ?>
<!--                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">-->
<!--                    <a href="login.php" class="kafe-btn kafe-btn-mint full-width revealOnScroll" data-animation="bounceIn" data-timeout="400">-->
<!--                        <i class="fa fa-tags"></i>-->
<!--                        --><?php //echo $lang['post']; ?>
<!--                        --><?php //echo $lang['a']; ?>
<!--                        --><?php //echo $lang['job']; ?><!--,-->
<!--                        --><?php //echo $lang['it\'s']; ?>
<!--                        --><?php //echo $lang['free']; ?><!-- !-->
<!--                    </a>-->
<!--                </div><!-- /.col-lg-3 -->-->

            <?php } ?>

        </div> <!-- /.row -->
    </div> <!-- /.content -->


</div><!-- /.container -->


<!-- ==============================================
Jobs Section
=============================================== -->
<style>
    div.search-jobs-urgents input,
    div.search-jobs-urgents button,
    div.search-jobs-urgents select
    {
        margin-top: 7px;
    }

    div.job
    {
        /*box-shadow: 12px 12px 2px 1px rgba(0, 0, 255, .2);*/
    }


    .btn-primary:hover{
    background-color: #286090 !important;
    border-color: #204d74 !important;
    }
    .btn-primary{
        background: none;
        transition: all .3s;
        background-color: #337ab7 !important;
        border-color: #2e6da4 !important;
    }
</style>
<section class="jobslist" style="margin-top: 27px;">

    <div class="container">

        <div class="row">
        
        <form name="jobserach" id="jobsearch" method="post">

            <div  style="height:60px;margin-top:10%;background-color:#195C9D" class="search-jobs-urgents col-lg-12">
              <select class="w20" id="job_type" name="job_type">
                <option value="all" selected="selected">All Jobs</option>
                <option value="active">Active Jobs</option>
                <option value="new">New Jobs</option>
                <option value="urgent">Urgent Jobs</option>
                <option value="featured">Featured Jobs</option>
                
              </select>
                <!--<input placeholder="Care Assisstant" style="height: 38px;width:20%" type="text" class="form-control">-->

                <?php

               

                    $TempTypeQuery =  DB::getInstance()->get("temp_type", "*");

                     if ($TempTypeQuery->count()) {

                        $temp_type = $TempTypeQuery->results();
                           ?>
                           
                           <select name="temp_type_id" id="temp_type_id" style="width:20%;display: inline-block" class="form-control">
                           <option value="">Temp Type:</option>
                           <?php
                           foreach($temp_type AS $ttype) {
                               ?>
                               <option value="<?=$ttype->id?>" <?=($ttype->id == $type_id ? 'selected' : '')?>><?=$ttype->title?></option>
                               <?php
                           }
                        }
                           ?>
                       </select>


               
                <?php                            

                           $locationQuery =  DB::getInstance()->get("locations", "*",["ORDER" => "location ASC",  "AND" => ["active" => 1]]);

                     if ($locationQuery->count()) {

                        $locations = $locationQuery->results();
                           ?>
                           <select style="width:20%;display: inline-block" class="form-control" id="location" name="country" required>
                           <option value="">Location</option>
                           <?php
                           foreach($locations AS $location) {
                               ?>
                               <option value="<?=$location->location?>" ><?=$location->location?></option>
                               <?php
                           }
                        }
                           ?>
                       </select>
                <style>
                    .ourstar {
                        font-size: 28px;
                        color: white;
                        position: relative;
                        top: 8px;
                        margin: auto 12px;
                    }
                </style>
               <!-- <select style="width:20%;display: inline-block" class="form-control">
                    <option value="10 miles">10 miles</option>
                </select>-->
                <!--<button class="btn btn-warning" type="button" style="margin-top:1%;background-color:#E25600;!important;color:#FFE199">Search</button>-->
                <input type="hidden" id="rowcount" name="rowcount" value="<?php echo $_GET["rowcount"]; ?>" />
                <input type="submit" name="search" id="search" value="Search" class="submit-btn btn btn-warning" style="margin-top:1%;background-color:#1d95eb;!important;color:#FFE199; margin-right: 30px;"/>
                <i class="fa fa-star ourstar"></i><input type="submit" name="search" id="search" value="Create A Temp CV" class="submit-btn btn btn-warning" style="margin-top:1%;background-color:#1d95eb;!important;color:#FFE199"/><i class="fa fa-star ourstar"></i>
               <!-- <input type="reset" name="search" value="Reset" id="resetSearch" class="btn btn-warning" style="margin-top:1%;background-color:#E25600;!important;color:#FFE199"/>-->
            </div>
            </form>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 white padding0" >
             <div id="ajax_list">
                <?php

                $settingsq =  DB::getInstance()->get("settings", "*");
                if ($settingsq->count()) {

                  $settings = $settingsq->results();
                  $urgentHours = $settings[0]->urgent_job_hours;
                  $activeHours = $settings[0]->active_job_hours ;
                  $newHours = $settings[0]->new_job_hours;
                  $feturedHours = $settings[0]->featured_job_hours ;
                  //echo '<pre>';print_r($settings);echo '</pre>';
                }

                $conditions = '';
                $jobType_cond = '';

                if(isset($_POST['search']) && $_POST['search'] == 'Search'){

                    if((isset($_POST['temp_type_id']) && !empty($_POST['temp_type_id'])) && (isset($_POST['country']) && !empty($_POST['country']))){

                        $conditions = ' AND temp_type_id = "'.$_POST['temp_type_id'].'" AND `country` = "'.$_POST['country'].'" ';

                    }elseif(empty($_POST['temp_type_id']) && !empty($_POST['country'])){

                        $conditions = ' AND `country` = "'.$_POST['country'].'"';

                    }elseif(!empty($_POST['temp_type_id']) && empty($_POST['country'])){

                        $conditions = ' AND temp_type_id = "'.$_POST['temp_type_id'].'"';
                    }else{
                        $conditions = '';
                    }

                    

                }


                $jobType_cond = " AND ((`date_added` >= (NOW() - INTERVAL ".$newHours." DAY) AND `date_added`  < NOW()) OR (`date_added` >= (NOW() - INTERVAL ".$urgentHours." DAY) AND `date_added`  < NOW() AND `chk_urgent` = '1') OR (`date_added` >= (NOW() - INTERVAL ".$feturedHours." DAY) AND `date_added`  < NOW() AND `featured` = '1') OR (`date_added` >= (NOW() - INTERVAL ".$activeHours." DAY) AND `date_added`  < NOW()) )";

               // $query = DB::getInstance()->get("job", "*", ["ORDER" => "date_added DESC",  "AND" => ["active" => 1, "delete_remove" => 0, "chk_urgent" => 1]]);
                // Very important to set the page number first.
                if (!(isset($_GET['page']))) { 
                  $page = 1; 
                } else {
                  $page = intval($_GET['page']);    
                }
                 
                //Number of results displayed per page  by default its 10.
                $limit =  ($_GET["show"] <> "" && is_numeric($_GET["show"]) ) ? intval($_GET["show"]) : 5;
                 
                $q1 = DB::getInstance()->query("SELECT  *  FROM `job` where `active` = '1' AND `delete_remove` = '0' ".$conditions." ".$jobType_cond." ");

                 $total = $q1->count();

                //Calculate the last page based on total number of rows and rows per page. 
                $last = ceil($total/$limit); 
                $startpoint = ($page - 1) * $limit;

              /*$query = DB::getInstance()->query("SELECT  *  FROM `job` where `active` = '1' AND 
                    `delete_remove` = '0' AND `date_added` >= (NOW() - INTERVAL 2 DAY) AND `date_added`  < NOW() ".$conditions."  ORDER BY date_added DESC LIMIT ".$startpoint.",".$limit."");*/

                    $query = DB::getInstance()->query("SELECT  *  FROM `job` where `active` = '1' AND `delete_remove` = '0' ".$conditions." ".$jobType_cond." ORDER BY date_added DESC LIMIT ".$startpoint.",".$limit."");

              

              

               // $total = $query->count();

                  if($query->count() > 0) {

                    $jresults = $query->results();

                    //echo '<pre>';print_r($jresults);echo '</pre>';
                    $serviceList = '';
                    $x = 1;

                foreach($query->results() as $row) 
                {
                    $tt =  date('Y-m-d',strtotime($row->date_added));

                    $current_date = date('Y-m-d');

                     $days_ago1 = date('Y-m-d', strtotime('-2 days', strtotime($current_date)));

                    //$days_ago2 = date('Y-m-d', strtotime('-2 days', strtotime($row->date_added)));



                   $serviceList = '';
                   $date_added = ago(strtotime($row->date_added));


                    $blurb = truncateHtml($row->description, 400);

                    
                    $skills_each =  '<label class="label label-success"><span>'. $row->skills .'</span></label> &nbsp;';

                   $clientquery =  DB::getInstance()->get("client", "*", ["clientid" => $row->clientid]);

                     if ($clientquery->count()) {

                        $clientdetails = $clientquery->results();

                            foreach ($clientquery->results() as $clientrow) {
                                $clientName = $clientrow->name;
                                $clientusername = $clientrow->username;
                                $clientimagelocation = $clientrow->imagelocation;
                            }

                        }


                        $profilequery =  DB::getInstance()->get("profile", "*", ["userid" => $row->clientid]);

                     if ($profilequery->count()) {

                        $profiledetails = $profilequery->results();

                        //echo '<pre>';print_r($profiledetail);echo '</pre>';exit;

                            foreach ($profilequery->results() as $profilerow) {
                                $company_name = $profilerow->company_name;
                                $care_settings = $profilerow->care_settings;
                                $no_patients = $profilerow->no_patients;
                                $about = $profilerow->about;
                            }

                        }


                        $TempTypeQuery =  DB::getInstance()->get("temp_type", "*", ["id" => $row->temp_type_id]);

                     if ($TempTypeQuery->count()) {

                        $tempTypedetails = $TempTypeQuery->results();
                            foreach ($TempTypeQuery->results() as $r2) {
                                $TempTypeName = $r2->title;
                                
                            }
                        }


                         $jobCategoryquery =  DB::getInstance()->get("category", "*", ["id" => $row->temp_category_id]);

                     if ($jobCategoryquery->count()) {

                        $jobCatdetails = $jobCategoryquery->results();
                            foreach ($jobCategoryquery->results() as $r3) {
                                $categoryName = $r3->name;
                                
                            }
                        }

                        

                    $tempRateId =  DB::getInstance()->get("temp_rate", "*", ["id" => $row->temp_rate_id]);

                     if ($tempRateId->count()) {

                        $tempRatedetails = $tempRateId->results();
                            foreach ($tempRateId->results() as $r4) {
                                $rangemin = $r4->rangemin;
                                $rangemax = $r4->rangemax;
                                $title = $r4->title;
                                $temp_rateId = $r4->id;
                                
                            }
                        }


                        $tempshift =  DB::getInstance()->get("temp_shift", "*", ["id" => $row->temp_shift_id]);

                     if ($tempshift->count()) {

                        $tempShiftdetails = $tempshift->results();
                            foreach ($tempshift->results() as $r5) {
                                
                                $shiftTitle = $r5->title;
                                
                                
                            }
                        }


                        //Start new Admin object
                        $admin = new Admin();
                        //Start new Client object
                        $client = new Client();
                        //Start new Freelancer object
                        $freelancer = new Freelancer();
                        
                        $tab1 = '';
                        $tab2 = '';
                        $tab3 = '';

                        if($row->featured == '1'){

                          $tab1 = '<button type="button" class="btn btn-warning blue">FEATURED</button>';

                          $tab1 = '<button type="button" class="btn btn-primary">FEATURED</button>';

                        }else{
                          $tab1=  '';
                        }

                        if($row->chk_urgent == '1'){
                          $tab2 = '<button type="button" class="btn btn-danger">URGENT</button>';
                        }else{
                          $tab2=  '';
                        }

                        if($row->chk_recruit == '1'){
                          $tab3 = '<button type="button" class="btn btn-warning btn-recruiter">RECRUITER</button>';
                        }else{
                          $tab3=  '';
                        }

                        
                     ?>
        <?php //if($days_ago1 == $tt){
          ?>
        

    <div class="job job-section" id="newslist">  
      <div class="row top-sec">
        <div class="col-lg-12">
            <div class="col-lg-2 col-xs-12">
                <div class="profile-icon" style="width:115px;border-right:1px solid black;border-bottom:1px solid black" class="temp-img">
                   <a href="#">
                    <img style="width:70px; height: 115px;" class="img-responsive" src="hirer/<?php echo escape($clientimagelocation); ?>"  alt="">
                    
                   </a>
                </div>
            </div><!-- /.col-lg-2 -->

            <div class="col-lg-7 col-xs-12 company-profile">
              <h4><a style="color:#498dcc;font-size: 20px;" href="#"><i class="fa fa-user-md" aria-hidden="true"></i> <?php echo $TempTypeName; ?></a></h4>
              <p style="color:#498dcc;font-size: 12px;"><i class="fa fa-map-marker"></i> <?php echo $row->country; ?></p>
              <p style="color:#498dcc;font-size: 12px;"><i style="font-size: 19px;"><?php echo $currency_symbol; ?></i>&nbsp;&nbsp;<?php echo $rangemin .($rangemax ? ' - '. $rangemax : '+') ; ?></p>
              <p style="color:#498dcc;font-size: 12px;"><i class="fa fa-building" aria-hidden="true"></i> <?php echo $company_name; ?></p>
              <p style="color:#498dcc;font-size: 12px;"><i class="fa fa-calendar" aria-hidden="true"></i> &nbsp;<?php echo $shiftTitle; ?> </p>
            </div><!-- /.col-lg-10 -->
            <div class="col-lg-3">
                <span style="font-size: 14px; margin-right: 13px; color: #642891"><i class="fa fa-heart-o"></i> Save</span>
                <span style="font-size: 14px; margin-right: 13px; color: #642891"><i class="fa fa-envelope-o"></i> Email</span>
                <a href="job_details.php?title=<?php echo $row->jobid; ?>" class="btn btn-primary">View</a>
            </div>
        </div><!-- /.col-lg-12 -->
      </div><!-- /.row -->
          
      <div class="row mid-sec">      
        <div class="col-lg-12">  
        <div class="col-lg-2 col-lg-12"></div>     
        <div class="col-lg-8 col-lg-12 description-section">
            <p><i class="fa fa-fw fa-bars"></i> &nbsp;<span style="color:#498dcc;font-size: 12px;"><?php echo $lang['jobdescription'];?>: </span></p>
                    <p style="font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 12px;!important;"><?php echo substr($blurb, 0, 50); ?></p>
                    <p class="background-separator" style="color:#33a2e9">
                    <i></i>
                    <div class="background-none" style="color:#498dcc !important;font-size: 12px;">Skill Required:</div>
                    
                    <?php
                      $skills = explode(',',$row->skills );
                     
                      foreach ($skills as $key => $skill) { ?>
                        <span class="btn btn-primary btn-xs" style="font-size: 12px !important;line-height: 12px !important;margin-top: 5px;"><?php echo $skill; ?></span>
                     <?php  } 


                     ?> 
                      </p>
                     <!-- <div class="location-section">
                           <div class="col-lg-3 padding0">
                            <label><?php //echo $lang['location']; ?>:</label>
                            <p><i class="fa fa-map-marker"></i> <?php //echo $row->country; ?></p>
                           </div>
                           <div class="col-lg-3">
                            <label><?php //echo $lang['rate_hour']; ?></label>
                             <p><?php //echo $rangemin .($rangemax ? ' + '. $rangemax : '+'); ?></p>
                           </div>
                           <div class="col-lg-6">
                            <label><?php //echo $lang['timeposted']; ?></label>
                             <p><?php //echo $date_added; ?></p>
                           </div>
                      </div>--><!-- /.col-lg-12 -->

        </div><!-- /.col-lg-12 -->
        <div class="col-lg-2">
            <button class="btn" style="background: none; background-color: #ff7f27; color: white;position: relative;bottom: -69px;">Urgent</button>
        </div>
      </div><!-- /.row -->
      <div class="row bottom-sec">
          <div class="col-lg-12">
      
          </div><!-- /.col-lg-12 -->
          <div class="col-lg-12 col-xs-12">
              <div class="pull-right button-css-jobs">
                  <?php echo $tab1.$tab2.$tab3; ?>
              </div>
          </div>
      </div><!-- /.row -->
         
    </div><!-- /.job -->
  </div>
  <?php //} ?>

         <?php     
                       
                        

                        unset($serviceList);
                        unset($skills_each);
                         //unset($_POST);
                        //unset($sen);
                        $x++;
            } ?>

             <div class="pagination-div">
                      <?php
                          echo Pagination($total,$limit,$page);
                  
                        ?>
                  </div>
            </div>

            <div class="show-div" id="show-search" <?php if($total<5){?> style="display:none;" <?php }?>>
                      <select class="form-control" id="show">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                      </select>
                  </div>

            <?php  }else{ ?>
                   <div class="job"> 
                      <div class="row top-sec">
                        <div class="col-lg-12">
                          <div class="col-xs-12">
                            <p style="text-align:center;">
                              <?php echo 'No New jobs found for selected match. '; ?>
                              
                            </p>
                        </div>
                      </div>
                    </div>
                    </div>

              <?php  } ?>
              
              

            </div><!-- /.col-lg-8 ajax div-->
        </div><!-- /.row ajax div-->  
    </div><!-- /.container-fluid -->
</section><!-- /section -->

<!-- Include footer.php. Contains footer content. -->
<?php include 'includes/template/footer1.php'; ?>




  








<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->

<!-- Kafe JS -->


<script>

/*function getresult(url) {  

alert('hi');
alert(url);  
  $.ajax({
    url: url,
    type: "POST",
    //data:  {rowcount:$("#rowcount").val(),name:$("#name").val(),code:$("#code").val()},
    data:  {rowcount:$("#rowcount").val()},
    success: function(response){ 
      console.log(response);
       var success =  $($.parseHTML(response)).filter("#newslist"); 
        console.log(success);
      //$("#toys-grid").html(data); $('#add-form').hide();

     }
     });
  }
  getresult("newjobs.php");*/


$(document).ready(function()
{

      var hostname = "<?php echo 'https://'.$hostname; ?>";
     // alert(hostname);
  
      $("#search").click(function(){
         
         var location = $("#location").val();
         var type_id = $("#temp_type_id").val();
         var show_record = $("#show").val();

         var job_type = $("#job_type").val();
         

            
            $.ajax({
              type: "POST",
              url: hostname+"/getnewjob_results.php",
              dataType: "html",
              data: "location="+location+"&temp_type="+type_id+"&job_type="+job_type+"&show="+show_record,
              success: function(response)
              {
                  //alert(response);
                  $("#ajax_list").html($(response).find("#ajax_list").html());

                  if(response>=0)
                  {
                    $("#show-search").css("display","block");
                  }else{
                    $("#show-search").css("display","none");
                  }

                    bindClicks();
              }
            });
            return false;

        });



     $("#show").on('change',function(){
        
        
           // var order_by = $("#next_order").val();
            //var page_no = $("#page_no").val();
            //var field_name = $("#field_name").val();

            //var sort_by = $("#sort_by").val();
            //var keyword = $("#keyword").val();
            var location = $("#location").val();
            var type_id = $("#temp_type_id").val();
            var show_record = $("#show").val();
            var job_type = $("#job_type").val();
            //alert(show_record);
            $.ajax({
              type: "POST",
              url: hostname+"/getnewjob_results.php",
              dataType: "html",
              //data: "show_record="+show_record+"&sort_by="+sort_by+"&keyword="+keyword+"&field_name="+field_name+"&page_no="+page_no+"&order_by="+order_by,
              //data: "show_record="+show_record+"&sort_by="+sort_by+"&keyword="+keyword,
              data: "show="+show_record+"&location="+location+"&temp_type="+type_id+"&job_type="+job_type,
              success: function(response)
              {
                  //alert(response);
                  $("#ajax_list").html($(response).find("#ajax_list").html());

                  if(response>=0)
                  {
                    $("#show-search").css("display","block");
                  }else{
                    $("#show-search").css("display","none");
                  }
                  
                    bindClicks();
              }
            });
            return false;
      });


     function bindClicks() {
     //alert('hi');
    $(".paginate a").click(paginationClick);
        //$(".page-nav a, .sort").bind("click",paginationClick);
        //$("#search").click(paginationClick);
       // $("#search").click(paginationSearch);
  }


  function paginationClick() {
    var href = $(this).attr('href');
        var location = $("#location").val();
            var type_id = $("#temp_type_id").val();
            var show_record = $("#show").val();
            var job_type = $("#job_type").val();

        $.ajax({
      type: "POST",
      url: href,
      data:"location="+location+"&temp_type="+type_id+"&job_type="+job_type+"&show="+show_record,
      success: function(response)
      {
        //alert(response);
        //$("#rounded-corner").css("opacity","1");
                  var div = $('#ajax_list', $(response));
                    $('#ajax_list').html(div);
        //$("#promos").html(response);
        bindClicks();
      }
    });

    return false;
  }
     bindClicks();


});



</script>

<script src="assets/js/kafe.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Waypoints JS -->
<script src="assets/js/waypoints.min.js" type="text/javascript"></script>

</body>
</html>
