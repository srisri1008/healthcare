<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    ul.ps li {
        font-size: 20px;
        line-height: 39px;
    }
    ul.ps:first-child{
        list-style: none;
        position: relative;
        margin-left: 0;
        padding-left: 1.2em;
    }
    .navs-plus,.navs{
        position:relative;
    }
    .navs{
        margin-bottom:5px;
    }

    ul.ps:first-child li:before, .navs-plus div:before{
        content: "+";
        position: absolute;
        left: 0;
    }
    .text{
        text-align:center;
        color:#15b342;
    }
    .ptext {
        text-align: center;
        line-height: 30px;
    }
    .listylenone,.listylenone2 {
        position: relative;
        list-style: none;
        margin-left: 0;
        padding-left: 1.2em;
    }
    .listylenone li:before {
        content: "*";
        position: absolute;
        left: 0;
    }
    .listylenone2 li{
        line-height:2;
    }
    .listylenone2 li:before {
        content: "+ ";
        position: static;
    }
    .plus-green{
        background-color: #139438;
        margin-right: 5px;
        color: #fff;
        border-radius: 5px;
    }
    .healthcare_onboard {
        font-size: 16px;
        background-color: #ededed;
        margin-top: 14px;
        padding: 29px;
    }
</style>
<div class="container-fluid" style="margin-top: 80px;width: 87%;">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-7 pull-right">
            <img src="assets/img/group.png" alt="group" class="img-responsive"/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-5" style="background-color:#15b342; color:#fff;padding: 20px;height: 268px;">
            <ul class="ps">
                <li>Total Automation</li>
                <li>Connecting Temps & Hirers</li>
                <li>Healthcare Temps-on-Demand 24/7</li>
                <li>Dedicated to eliminating healthcare staff shortage</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h3 class="text">Our Origin</h3>
            <p style="text-align:center;">   Healthcare-temps UK was created from humble origins with a twin purpose in mind.<br />
                + To help individuals & organisations find & hire fully vetted idle healthcare professionals at affordable rates 24/7 and<br />
                + To provide healthcare professionals with a platform to choose when to work, for whom to work and self-determine
                the rewards they deserve.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text">Our Evolution</h3>
            <p style="text-align:center;">Care is critically challenged and constantly evolving. Healthcare-Temps has continued to evolve as well, growing from
                strength to strength with the addition of cutting edge technology that genuinely solves our users’ problems and fulfill
                their needs. Automated Labour-on-Demand or Healthcare-staff-on-demand is the next generation of staff
                procurement, eradicating agencies’ fees and giving back power to workers & hirers.</p>
            <p style="text-align:center;">As this new segment of the labour markets evolves, employers will discover ways to drastically cut labour cost,
                eliminate staff shortage, utilise the best talent nationally and enhance the delivery of quality care to service users.
                Likewise, out-patients in need of urgent private healthcare can opt for 1 -to-i virtual or face-face care.</p>
            <p style="text-align:center;">Healthcare-Temps is pioneering the Healthcare Tern ps-on-Demand revolution. Placing the procurement and
                management of temporary /adhoc healthcare workers in the palms of practice managers’ and owners’ hands. As a
                market leader in this industry, what makes us unique, is our ability to enable hirers to find and hire every type medical
                and healthcare talent of all grades and specialisms. Further, we employ cognitive computing technology to make the
                sourcing of medical & healthcare professionals simpler and smarter.</p>
            <p style="text-align:center;">Moreover, we have automated activities such as; staff interview, staff vetting, referencing / rating, e-rota, invoicing and
                e-payment. Now you can hire without any hassle or worry 24/7/365. We are also proud of the fact, we are drastically
                cut waiting times to be seen by a medical professional from 21 days to 1 day.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 pull-right">
            <img src="assets/img/computer.gif" style="    max-width: 63%;margin: 0 auto;margin-top: 45px;" alt="computre" class="img-responsive"/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <h3 class="text">Healthcare Temps-on-Demand 24/7</h3>
            <ul class="ps listylenone">
                <li>Tap into a world of idle healthcare talent</li>
                <li>Hire Temp workers within minutes</li>
                <li>Zero agency fees for life & it's 100% Free.</li>
                <li>Handpick the staff you want and when.</li>
                <li>Automated advance booking / Rota master</li>
                <li>Paperless invoice & Timesheet</li>
                <li>Created by care managers for employers</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <img src="assets/img/circle.gif" alt="circle" style="    max-width: 80%;" class="img-responsive"/>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <h3 class="text">Automated vetting and compliance</h3>
            <ul class="ps listylenone">
                <li> All temp staff are fully vetted & background checks done.</li>
                <li> CV & mandatory documents verified before working.</li>
                <li> No payment is released unless employer is satisfied Revalidation routinely checked or temp is locked out of site</li>
                <li> Staff profiles & photos are regularly checked.</li>
                <li> Video interview available before you hire a temp</li>
                <li> Hirers rate & write reviews about temp workers.</li>
                <li> 100% secure and safe payment solution.</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 healthcare_onboard">
            <h3 class="text">Type of Healthcare Temps onboard</h3>
            <div class="row navs  col-md-offset-2">
                <div class="col-xs-12 col-sm-4">Doctors</div>
                <div class="col-xs-12 col-sm-4">Dentists</div>
                <div class="col-xs-12 col-sm-4">Consultants</div>
            </div>
            <div class="row navs  col-md-offset-2">
                <div class="col-xs-12 col-sm-4">Nurses</div>
                <div class="col-xs-12 col-sm-4">Care Assistants</div>
                <div class="col-xs-12 col-sm-4">Cooks/Chefs</div>
            </div>
            <div class="row navs  col-md-offset-2">
                <div class="col-xs-12 col-sm-4">Pharmacists</div>
                <div class="col-xs-12 col-sm-4">Surgeons</div>
                <div class="col-xs-12 col-sm-4">Paediatric</div>
            </div>
            <div class="row navs   col-md-offset-2">
                <div class="col-xs-12 col-sm-4">Physiologists</div>
                <div class="col-xs-12 col-sm-4">ABA therapists</div>
                <div class="col-xs-12 col-sm-4">Home Maangers</div>
            </div>
            <div class="row navs  col-md-offset-2">
                <div class="col-xs-12 col-sm-4">Occupational therapists</div>
                <div class="col-xs-12 col-sm-4">Physiotherapists</div>
                <div class="col-xs-12 col-sm-4">Many other temp workers</div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12" style="font-size: 16px;">
            <h3 class="text">Medical & Healthcare Settings on Board</h3>
            <p class="ptext">healthcare-temp caters for almost any type of employer / service user who needs medical & healthcare temps.</p>
            <div class="row navs-plus col-md-offset-2">
                <div class="col-xs-12 col-sm-4">Hospitals</div>
                <div class="col-xs-12 col-sm-4">Care Homes</div>
                <div class="col-xs-12 col-sm-4">Day centres</div>
            </div>
            <div class="row navs-plus   col-md-offset-2">
                <div class="col-xs-12 col-sm-4">Dentistry</div>
                <div class="col-xs-12 col-sm-4">Clinic</div>
                <div class="col-xs-12 col-sm-4">Nursing Homes</div>
            </div>
            <div class="row navs-plus   col-md-offset-2">
                <div class="col-xs-12 col-sm-4">Home Care</div>
                <div class="col-xs-12 col-sm-4">Pharmacist</div>
                <div class="col-xs-12 col-sm-4">Charities</div>
            </div>
            <div class="row navs-plus   col-md-offset-2">
                <div class="col-xs-12 col-sm-4">Outpatient center</div>
                <div class="col-xs-12 col-sm-4">Sports Clubs</div>
                <div class="col-xs-12 col-sm-4">Surgeries</div>
            </div>
            <div class="row navs-plus   col-md-offset-2">
                <div class="col-xs-12 col-sm-4">Dyalasis Centre</div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12" style="font-size: 16px;">
            <h3 class="text">Solutions for all</h3>
            <ul class="listylenone2" style="text-align:center;">
                <li>Access every type of healthcare profession, wherever, whenever.</li>
                <li>Neutral temp-staff procurement solution that saves you real money.</li>
                <li>One-stop-shop for hiring healthcare professionals for temporary assignments.</li>
                <li>Automating the hiring process for employers & providing convenience.</li>
                <li>Video consultations for people requiring virtual care in remote locations.</li>
                <li>Providing call-care for out-patients demanding a quick & personal touch.</li>
                <li>Temps are in control of their shift patterns, work place settings and income.</li>
                <li>Transparency, honesty and dispute resolution efficiency.</li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12" style="text-align:center;margin-bottom:50px;">
            <h3 class="" style="color:#9c9c9c">Interested in joining today ? contact us for a free demo.</h3>
        </div>
        <div class="col-sm-4 col-sm-offset-4">
            <div class="form-group">
                <input type="email" placeholder="Enter email" class="form-control" style="padding: 17px">
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.navs div').prepend('<span class="plus-green">&nbsp;&nbsp;+&nbsp;&nbsp;</span>');
    });
</script>


<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>
<!----footer--------->

</body>
</html>
