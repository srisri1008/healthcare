<?php
//Check if init.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include('includes/template/header.php'); ?>
<body class="greybg" style="background-color: #fff;">

<!-- Include navigation.php. Contains navigation content. -->
<?php include('includes/template/navigation.php'); ?>

<style>
    .h2des{
        border-bottom: 3px solid #378139;
        display: inline;
        color:#3f9a41;
    }
    .pdes{
        margin-top: 22px;
        font-size: 20px;
        margin-bottom: 35px;
    }
    .container{
        margin-top: 50px;
        margin-bottom: 50px;
    }
    .otherkey{
        text-align: center;
        margin-top: 50px;
        margin-bottom: 50px;
    }
    b{
        line-height: 50px;
    }
    .remote{
        line-height: 50px;
    }
    .imglinehight{
        margin-top: 10px;
    }
</style>
<!-- ==============================================
Hello Section
=============================================== -->
<div id="pages-features" class="content user" style="margin-top: 100px;">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489"
                                        title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter"
                                       target="_blank"></a></li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113"
                                        title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw"
                                       title="Follow me on youtube" target="_blank"> </a></li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts"
                                           title="Follow me on google plus" target="_blank"></a></li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/"
                                       title="Follow me on blogger" target="_blank"></a></li>
            </ul>
        </div>
        <div id="js-head-menu" class="">
            <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <a class="show hor-space" title="Temps-direct.uk" href="/">
                                <img alt="" src="myassets/img/logo.png">
                            </a>
                        </div>
                        <div class="cantent">
                            <p>
                                Your log-out has been successful.</p>
                            <p>
                                Thank you for using Temps-Direct temp recruiting platform.</p>
                            <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                            <input name="feedback" type="commant">
                            <p>Thank you!</p>
                        </div>
                        <div class="modal-footer hide">
                            <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default"> Close</a>
                        </div>
                    </div>

                </div>
            </div>

            <script type="text/javascript">
                //<![CDATA[
                function getCookie(c_name) {
                    var c_value = document.cookie;
                    var c_start = c_value.indexOf(" " + c_name + "=");
                    if (c_start == -1) {
                        c_start = c_value.indexOf(c_name + "=");
                    }
                    if (c_start == -1) {
                        c_value = null;
                    } else {
                        c_start = c_value.indexOf("=", c_start) + 1;
                        var c_end = c_value.indexOf(";", c_start);
                        if (c_end == -1) {
                            c_end = c_value.length;
                        }
                        c_value = unescape(c_value.substring(c_start, c_end));
                    }
                    return c_value;
                }
                if (getCookie('il')) {
                    (function () {
                        var js = document.createElement('script');
                        js.type = 'text/javascript';
                        js.async = true;
                        js.src = "https://healthcare-temps.co.uk/users/show_header.js?u=";
                        var s = document.getElementById('js-head-menu');
                        s.parentNode.insertBefore(js, s);
                    })();
                } else {
                    document.getElementById('js-head-menu').className = '';
                }
                //]]>
            </script>
        </div>
        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container">
                    <div class="row" style="text-align: center;">
                        <h2 class="h2des">DISCOVER THE HT5,4G SMART WATCH</h2>
                        <p class="pdes">There is so much you can do...</p>
                    </div>
                    <div class="row" style="text-align: center;">
                        <div class="col-lg-4">
                            <img src="myassets/img/rm_monitor.png" >
                            <img class="imglinehight" src="myassets/img/rm_monitor_icon.png"> <br />
                            <p class="remote">Remote Monitoring</p>
                        </div>
                        <div class="col-lg-4">
                            <img src="myassets/img/virtual_care.png">
                            <img class="imglinehight" src="myassets/img/virtual_care_icon.png"> <br />
                            <p class="remote">Virtual Care</p>
                        </div>
                        <div class="col-lg-4">
                            <img src="myassets/img/call_out_care.png">
                            <img class="imglinehight" src="myassets/img/call_out_icon.png"> <br />
                            <p class="remote">Call out care</p>
                        </div>
                    </div>
                    <div class="row otherkey">
                        <h4 style=" border-bottom:2px solid gray; display: inline;">Other Key Features</h4>
                    </div>
                    <div class="row" style="text-align: center;margin-bottom: 25px">
                        <div class="col-lg-3 col-sm-2">
                            <img src="myassets/img/gps_icon.png"><br />
                            <b>GPS</b>
                        </div>
                        <div class="col-lg-3 col-sm-2">
                            <img src="myassets/img/electronic_icon.png"><br />
                            <b>Electronic fence</b>
                        </div><div class="col-lg-3 col-sm-2">
                            <img src="myassets/img/sos_icon.png"><br />
                            <b>Emergency call</b>
                        </div><div class="col-lg-3 col-sm-2">
                            <img src="myassets/img/4g_icon.png"><br />
                            <b>4G Calling</b>
                        </div>
                    </div>
                    <div class="row" style="text-align: center;">
                        <div class="col-lg-3 col-sm-2">
                            <img src="myassets/img/track_icon.png"><br />
                            <b>Track</b>
                        </div>
                        <div class="col-lg-3 col-sm-2">
                            <img src="myassets/img/street_icon.png"><br />
                            <b>Street view</b>
                        </div>
                        <div class="col-lg-3 col-sm-2">
                            <img src="myassets/img/search_icon.png"><br />
                            <b>Search</b>
                        </div>
                        <div class="col-lg-3 col-sm-2">
                            <img src="myassets/img/virtual_care_icon.png"><br />
                            <b>Smart buttons</b>
                        </div>
                    </div>
                </div>
                <a href="">
                </a>
            </div>
            <a href="">
            </a></div>
        <a href="">
            <!-- for modal -->
        </a>
        <div class="modal hide fade" id="js-ajax-modal"><a href="">
                <div class="modal-body"></div>
            </a>
            <div class="modal-footer"><a href=""></a><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a>
            </div>
        </div>
        <!-- for modal -->
        <!-- for modal -->
        <div class="modal hide fade" id="js-ajax-modal-child">
            <div class="modal-body"></div>
            <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a></div>
        </div>
        <!-- for modal -->

        <div class="clearfix"></div>
        <!-- for modal
        <div class="footer-push"></div> -->
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php //include ('includes/template/footer.php'); ?>

<a id="scrollup">Scroll</a>

<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Kafe JS -->
<script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
