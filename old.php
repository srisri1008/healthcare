<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg">

<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>
<!-- ==============================================
Header
=============================================== -->

<!-- BANNER -->
<div class="bg-light-gray">
    <section class="col-sm-6 slide-left">
        <div class="col-sm-12">
            <div class="animated fadeInDown chat-left pull-right">
                <p>Temp type:  RGN</p>
                <p>Rate offered: £26.00</p>
                <p>Location:  Oxford</p>
                <p>Shift: 20:00-08:00</p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="art btn_hire_temp">
            <a class="btn-primaryes" href="https://healthcare-temps.co.uk/page/intro"> Hire Temp </a>
        </div>
    </section>
    <section class="col-sm-6 slide-right">
        <div class="animated fadeInUp chat-right">
            <p>Name: Emma Duncan</p>
            <p>Job Title: RGN</p>
            <p>Proposal: Accepted</p>
            <p>PIN: P87364V</p>
        </div>
        <div class="clearfix"></div>
        <div class="art">
            <a class="btn-secondaryes" href="https://healthcare-temps.co.uk/page/intro"> Find Temp jobs </a>
        </div>

    </section>


    <section class="col-sm-6 col-lg-12 ">  <div class="connect"> <label>Let's Connect</label></div> </section>
    <div class="clearfix"></div>
</div>
<!-- WELCOME -->
<div id="welcome" class="bg-light-gray">
    <div class="container text-center">

        <h2 class="text-mauvu" id="adjust_h2">Welcome to Healthcare-Temps</h2>
        <p class="col-md-8 col-sm-offset-2 text-cont adjust_head_p">
            <span class="head_p">The UK's No. 1 Healthcare professional  On-Demand Hiring Platform</span>
            <br>
            <span class="bottom_p">Empowered by cognitive intelligence, enabling users to search &amp; hire healthcare-professionals based on speciality &amp; location.</span>
        </p>

    </div>
</div>
<div id="future">
    <div class="adjust_div">
        <div class="container">
            <div class="row">

                <div class="col-sm-5">
                    <section>
                        <div class="adjust_box">
                            <h2 class="text-mauvu">Urgent Hire</h2>
                            <hr>
                            <!-- <iframe width="351" height="204" src="https://drive.google.com/file/d/0B5h7XKF3CXyqN2hId0JiTDg0WUU/view" frameborder="0" allowfullscreen></iframe> -->
                            <br>
                            <!--<iframe width="90%" height="204" src="https://www.youtube.com/embed/CMQJbsHAj3Y?showinfo=0" frameborder="0" allowfullscreen></iframe>-->
                            <!-- <iframe width="90%" height="204" src="https://www.youtube.com/embed/C-ThRIg_AYM" frameborder="0" allowfullscreen></iframe>-->
                            <!--<iframe src="https://drive.google.com/file/d/0B-JqZGU2EdrIMHdvRVhCQ2hKMEU/preview" width="90%" height="204"></iframe>-->
                            <!-- <figure><img src="/img/new_images/img_video.png" alt="video" class="img-responsive"/></figure> -->
                            <div class="form-group signup-cont">
                                <input type="text" class="form-control" id="comapny" placeholder="Temp Type">


                                <input type="password" class="form-control" id="pwd" placeholder="Location">

                            </div>
                            <div class="submit-block clearfix">
                                <div class="submit form-sub">
                                    <a href="https://healthcare-temps.co.uk/users/register/manual" class="homebtn">Continue</a>&nbsp;&nbsp;
                                    <!--or &nbsp;&nbsp;<a href="https://healthcare-temps.co.uk/users/login" class="cancel-button">Continue</a>-->
                                </div>
                            </div></div></section>
                </div>
                <div class="col-sm-2 hidden-xs"></div>
                <div class="col-sm-5">
                    <section id="content-mass-bottom">
                        <div class="moduletable">
                            <div class="jobsHeader">
                                <h3>Urgent Jobs</h3>
                                <!-- <a class="all-jobs" href="https://healthcare-temps.co.uk/page/urgent-jobs-listing">Start Bidding Now!</a> </div>-->
                                <div id="top2"> <span class="featured_job">
              <span class="title_col"><a href="https://healthcare-temps.co.uk/job-search/328-surgical-nurses-kent/1060-surgical/kent/job">Surgical Nurses</a> </span>
              <span class="area_col"> <a href="https://healthcare-temps.co.uk/job-search/browse/kent-england/jobs">Kent</a> </span> <span class="amount"> <a href="https://healthcare-temps.co.uk/"> £30 phr. </a></span> 		              <span class="detail_col apply_col"> <a href="https://healthcare-temps.co.uk/page/urgent-jobs-listing">Bid </a> </span> </span>
                                    <span class="featured_job even"> <span class="title_col"><a href="https://healthcare-temps.co.uk/job-search/327-rmn/rna/rnld-hull/1000-mental-health-rmn/yorkshire/job">RMN/RNA/RNLD Hull</a></span>
              <span class="area_col"><a href="https://healthcare-temps.co.uk/job-search/browse/yorkshire-england/jobs">Yorkshire</a></span> <span class="amount"> <a href="https://healthcare-temps.co.uk/"> £30 phr. </a></span> <span class="detail_col apply_col"><a href="https://healthcare-temps.co.uk/page/urgent-jobs-listing">Bid </a></span> </span>



                                    <span class="featured_job"> <span class="title_col"><a href="https://healthcare-temps.co.uk/job-search/326-theatre-manager-essex/1040-theatre/essex/job">Theatre Manager</a></span> <span class="area_col"><a href="https://healthcare-temps.co.uk/job-search/browse/essex-england/jobs">Essex</a></span> <span class="amount"> <a href="https://healthcare-temps.co.uk/"> £30 phr. </a></span> <span class="detail_col apply_col"><a href="https://healthcare-temps.co.uk/page/urgent-jobs-listing">Bid </a></span> </span>


                                    <span class="featured_job even"> <span class="title_col"><a href="https://healthcare-temps.co.uk/job-search/325-anaesthetic-lead/recovery-practitioner/1009-anaesthetics/essex/job">Anaesthetic Lead / Recovery</a></span> <span class="area_col"><a href="https://healthcare-temps.co.uk/job-search/browse/essex-england/jobs">Essex</a></span> <span class="amount"> <a href="https://healthcare-temps.co.uk/"> £30 phr. </a></span> <span class="detail_col apply_col"><a href="https://healthcare-temps.co.uk/page/urgent-jobs-listing">Bid </a></span> </span>
                                    <span class="featured_job even"> <span class="title_col"><a href="https://healthcare-temps.co.uk/job-search/325-anaesthetic-lead/recovery-practitioner/1009-anaesthetics/essex/job">RMN/RNA/RNLD</a></span> <span class="area_col"><a href="https://healthcare-temps.co.uk/job-search/browse/essex-england/jobs">London</a></span> <span class="amount"> <a href="https://healthcare-temps.co.uk/"> £30 phr. </a></span> <span class="detail_col apply_col"><a href="https://healthcare-temps.co.uk/page/urgent-jobs-listing">Bid </a></span> </span> </div>
                            </div>
                    </section>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<!-- WHY JOIN TEMP D -->
<div class="bg-blue">
    <div class="container" id="join">
        <!--  <h3 class="text-center">Benefits</h3> -->
        <br>
        <div class="row">
            <section class="col-sm-6">
                <h3>
                    <figure><img src="myassets/images/benefit-1.png" alt="video" class="img-responsive"></figure>
                    <p class="ben-text">Benefits For Hirers</p>
                </h3>
                <ul>
                    <li>Find vetted medical &amp; healthcare temps in seconds</li>
                    <li>24/7 automated booking &amp; almost 0% staff cancellation</li>
                    <li>Advertise unlimited temp/locum jobs for absolutely free</li>
                    <li>Pay no agency fee or overheads (Tax, NI, Annual leave etc)</li>
                    <li>Negotiate rates directly with temps (&amp; cut labour cost by 55%)</li>
                    <li>Hire who you want &amp; only pay when you are satisfied</li>
                    <li>Safe online payment, Escrow &amp; Dispute resolution built-in</li>
                    <li>Online vetting, DBS &amp; background checks.</li>
                    <li>Free to join. No monthly fee or hidden charges.</li>
                </ul>
                <a href="https://healthcare-temps.co.uk/users/register/manual" class="btn btn-mauve">Create employer account</a> </section>
            <section class="col-sm-6">
                <h3> <figure><img src="myassets/images/benefit-2.png" alt="video" class="img-responsive"></figure>
                    <p class="ben-text">Benefits For Temp Workers</p>
                </h3>
                <ul>
                    <li>Choose the shifts, locations &amp; medical settings you want</li>
                    <li>Get paid weekly or monthly &amp; direct to your bank account</li>
                    <li>Automated time sheet, invoicing &amp; exclusive escrow account</li>
                    <li>Total flexibility to choose off days &amp; work days</li>
                    <li>Get similar benefits &amp; perks like a perm but without hassle</li>
                    <li>1,000s of hours to fill &amp; loads of loyalty bonuses to enjoy.</li>
                    <li>Create professional digital CV online</li>
                    <li>Book car hire, B&amp;B, groceries, fast-food etc with just two clicks</li>
                    <li>Free to join &amp; social media friendly</li>
                </ul>
                <a href="https://healthcare-temps.co.uk/users/register/manual" class="btn btn-mauve">Create Temp profile</a> </section>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- FOUR STEPS -->
<div class="container">
    <h3 class="maintcest text-center">Connect with healthcare professionals  24/7</h3>
    <div class="row">
        <section class="col-sm-3">
            <div class="fourcolumn">
                <div class="columheader"><img src="myassets/images/columnheader.png"></div>
                <div class="columnmenu"><img src="myassets/images/menu.png"></div>
                <div class="columnbody">
                    <h3>Find Temp Jobs</h3>
                    <div class="mainbody">
                        <figure class="text-center"><img src="myassets/images/tempjobs.png" alt=""></figure>
                        <p>Find 1,000s of healthcare jobs at hospitals, private clinics, nursing homes etc</p>
                        <a href="https://healthcare-temps.co.uk/page/find-temp-jobs" class="btnbleed">Touch</a>
                    </div>

                </div>
                <div class="columnfooter"><img src="myassets/images/columnfooter.png"></div>
            </div>
        </section>
        <section class="col-sm-3">
            <div class="fourcolumn">
                <div class="columheader"><img src="myassets/images/columnheader.png"></div>
                <div class="columnmenu"><img src="myassets/images/menu.png"></div>
                <div class="columnbody">
                    <h3>Hire Temps</h3>
                    <div class="mainbody">
                        <figure class="text-center"><img src="myassets/images/hirehealthcare.png" alt=""></figure>
                        <p>Discover a new way to hire idle healthcare professionals for temp assignments.</p>
                        <a href="https://healthcare-temps.co.uk/page/employer" class="btnbleed">Touch</a>
                    </div>

                </div>
                <div class="columnfooter"><img src="myassets/images/columnfooter.png"></div>
            </div>
        </section>
        <section class="col-sm-3">
            <div class="fourcolumn">
                <div class="columheader"><img src="myassets/images/columnheader.png"></div>
                <div class="columnmenu"><img src="myassets/images/menu.png"></div>
                <div class="columnbody">
                    <h3>Virtual Care</h3>
                    <div class="mainbody">
                        <figure class="text-center"><img src="myassets/images/econsulation.png" alt=""></figure>
                        <p>Book sessions with  registered nurses, GPs Pharmacist, Therapists.</p>
                        <a href="https://healthcare-temps.co.uk/#" class="btnbleed">Touch</a>
                    </div>

                </div>
                <div class="columnfooter"><img src="myassets/images/columnfooter.png"></div>
            </div>
        </section>
        <section class="col-sm-3">
            <div class="fourcolumn">
                <div class="columheader"><img src="myassets/images/columnheader.png"></div>
                <div class="columnmenu"><img src="myassets/images/menu.png"></div>
                <div class="columnbody">
                    <h3>Call-Out Care</h3>
                    <div class="mainbody">
                        <figure class="text-center"><img src="myassets/images/outpatientcare.png" alt=""></figure>
                        <p>Receive quality care wherever, whenever.At home, work, travel.</p>
                        <a href="https://healthcare-temps.co.uk/page/call-out-care" class="btnbleed">Touch</a>
                    </div>

                </div>
                <div class="columnfooter"><img src="myassets/images/columnfooter.png"></div>
            </div>
        </section>
        <div class="clearfix"></div>
    </div>
</div>
<!-- TESTIMONY-->
<!--<div class="bg-light-gray">
  <div class="container">-->
<h2 class="text-center text-mauvu">What Hirers are saying about us!</h2>


<div class="container virtue" style="height: 454px;">
    <div id="ca-container" class="ca-container"><div class="ca-nav"><span class="ca-nav-prev">Previous</span><span class="ca-nav-next">Next</span></div>
        <div class="ca-wrapper" style="overflow: hidden;">
            <a href="https://healthcare-temps.co.uk/page/testimony">
            </a>

            <a href="https://healthcare-temps.co.uk/page/testimony">
            </a>

            <a href="https://healthcare-temps.co.uk/page/testimony">
            </a>

            <a href="https://healthcare-temps.co.uk/page/testimony">
            </a>

            <a href="https://healthcare-temps.co.uk/page/testimony">
            </a>

            <a href="https://healthcare-temps.co.uk/page/testimony">
            </a>

            <div class="ca-item ca-item-4" style="position: absolute; left: 0px;"><a href="https://healthcare-temps.co.uk/page/testimony">
                    <div class="ca-item-main">
                        <img src="myassets/images/bgirl.jpg" style="width: 57%;display: block;margin: 0 auto;margin-top: 17px;">
                        <h5 style="font-family: sans-serif;text-align: center !important;font-size: 22px!important;color: #258c55;!importantfont-style: inherit;margin-bottom: 1px;">Dr. Aileen Phillips</h5>
                        <h6 style="text-align: center;font-size: 15px;">Mgr. Primary Health Trust</h6>

                        <div style="background-color: #32A3DB;height: 132px;">
                            <span class="fa fa-caret-down" style=" display: block;text-align: center;font-size: 50px;color: white;position: relative;top: -20px;"></span>
                            <p style="font-size: 16px;
									padding: 0px 27px;
									margin-bottom: 0px;
									color: white;
									padding-bottom: 31px;
									text-align: left;
									padding-top: 11px;
									position: absolute;top: 282px;
									">
                                It is so easy to book staff at short notice using this App.
                                Also, I can see who is available, their location &amp; negotiate rate instantly.
                            </p>
                        </div>
                    </div>
                </a><div class="ca-content-wrapper"><a href="https://healthcare-temps.co.uk/page/testimony">
                    </a><div class="ca-content"><a href="https://healthcare-temps.co.uk/page/testimony">
                            <h6>You can change the world</h6>
                        </a><a href="https://healthcare-temps.co.uk/#" class="ca-close">close</a>
                    </div>
                </div>
            </div><div class="ca-item ca-item-5" style="position: absolute; left: 330px;"><a href="https://healthcare-temps.co.uk/page/testimony">
                    <div class="ca-item-main">
                        <img src="myassets/images/wgirl.jpg" style="width: 57%;display: block;margin: 0 auto;margin-top: 17px;">
                        <h5 style="font-family: sans-serif;text-align: center !important;font-size: 22px!important;color: #258c55;!importantfont-style: inherit;margin-bottom: 1px;">Andreea Milan</h5>
                        <h6 style="text-align: center;font-size: 15px;">Live-in Career</h6>

                        <div style="background-color: #0DB14B;height: 132px;">
                            <span class="fa fa-caret-down" style=" display: block;text-align: center;font-size: 50px;color: white;position: relative;top: -20px;"></span>
                            <p style="font-size: 16px;
									padding: 0px 27px;
									margin-bottom: 0px;
									color: white;
									padding-bottom: 31px;
									text-align: left;
									padding-top: 11px;
									position: absolute;top: 282px;
									">As a self-employed healthcare -professional I have found it easy to manage my rota, calculate taxes and N.I. and holiday income.
                            </p>
                        </div>
                    </div>
                </a><div class="ca-content-wrapper"><a href="https://healthcare-temps.co.uk/page/testimony">
                    </a><div class="ca-content"><a href="https://healthcare-temps.co.uk/page/testimony">
                            <h6>You can change the world</h6>
                        </a><a href="https://healthcare-temps.co.uk/#" class="ca-close">close</a>
                    </div>
                </div>
            </div><div class="ca-item ca-item-6" style="position: absolute; left: 660px;"><a href="https://healthcare-temps.co.uk/page/testimony">
                    <div class="ca-item-main">
                        <img src="myassets/images/tman.jpg" style="width: 57%;display: block;margin: 0 auto;margin-top: 17px;">
                        <h5 style="font-family: sans-serif;text-align: center !important;font-size: 22px!important;color: #258c55;!importantfont-style: inherit;margin-bottom: 1px;">Gaj Ragunathan</h5>
                        <h6 style="text-align: center;font-size: 15px;">Director,Ashton Care Homes</h6>

                        <div style="background-color: #32A3DB;height: 132px;">
                            <span class="fa fa-caret-down" style=" display: block;text-align: center;font-size: 50px;color: white;position: relative;top: -20px;"></span>
                            <p style="font-size: 16px;
                                    padding: 0px 27px;
                                    margin-bottom: 0px;
                                    color: white;
                                    padding-bottom: 31px;
                                    text-align: left;
                                    padding-top: 11px;
                                    position: absolute;top: 282px;
                                    ">
                                I am truly impressed by the number of healthcare temps we were able to find and hire to fill vacancies across 5 Homes.
                            </p>
                        </div>
                    </div>
                </a><div class="ca-content-wrapper"><a href="https://healthcare-temps.co.uk/page/testimony">
                    </a><div class="ca-content"><a href="https://healthcare-temps.co.uk/page/testimony">
                            <h6>You can change the world</h6>
                        </a><a href="https://healthcare-temps.co.uk/#" class="ca-close">close</a>
                    </div>
                </div>
            </div><div class="ca-item ca-item-1" style="position: absolute; left: 990px;"><a href="https://healthcare-temps.co.uk/page/testimony">
                </a><div class="ca-item-main"><a href="https://healthcare-temps.co.uk/page/testimony">
                        <img src="myassets/images/girl.jpg" style="width: 57%;display: block;margin: 0 auto;margin-top: 17px;">
                        <h5 style="font-family: sans-serif;text-align: center !important;font-size: 22px!important;color: #258c55;!importantfont-style: inherit;margin-bottom: 1px;">Julie Vonn</h5>
                        <h6 style="text-align: center;font-size: 15px; ">RGN/RMN, Doncaster</h6>

                        <div style="background-color: #0DB14B;height: 132px;">
                            <span class="fa fa-caret-down" style=" display: block;text-align: center;font-size: 50px;color: white;position: relative;top: -20px;"></span>
                            <p style="
									font-size: 16px;
									padding: 0px 27px;
									margin-bottom: 0px;
									color: white;
									padding-bottom: 52px;
									text-align: left;
									padding-top: 11px;
									position: absolute;top: 282px;
									">
                                Healthcare-Temps allows me to create my own rota, choose when to work, where to work and the rate to work for.What users are saying about us!

                            </p>
                        </div>
                    </a><div class="ca-content-wrapper"><a href="https://healthcare-temps.co.uk/page/testimony">
                        </a><div class="ca-content"><a href="https://healthcare-temps.co.uk/page/testimony">
                                <h6>Animals are not commodities</h6>
                            </a><a href="https://healthcare-temps.co.uk/#" class="ca-close">close</a>
                            <div class="ca-content-text">
                                <p>Healthcare-Temps is the perfect platform for sourcing fully vetted temp staff quickly and at reduced labour cost.</p>
                                <p>When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream;</p>
                                <p>She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div><div class="ca-item ca-item-2" style="position: absolute; left: 1320px;"><a href="https://healthcare-temps.co.uk/page/testimony">
                    <div class="ca-item-main">
                        <img src="myassets/images/bman.jpg" style="width: 57%;display: block;margin: 0 auto;margin-top: 17px;">
                        <h5 style="font-family: sans-serif;text-align: center !important;font-size: 22px!important;color: #258c55;!importantfont-style: inherit;margin-bottom: 1px;">Warren Canicon</h5>
                        <h6 style="text-align: center;font-size: 15px;">Reg. Mgr, Minster Care Group</h6>

                        <div style="background-color: #32A3DB;height: 132px;">
                            <span class="fa fa-caret-down" style=" display: block;text-align: center;font-size: 50px;color: white;position: relative;top: -20px;"></span>
                            <p style="font-size: 16px;
									padding: 0px 27px;
									margin-bottom: 0px;
									color: white;
									padding-bottom: 32px;
									text-align: left;
									padding-top: 11px;
									position: absolute;top: 282px;
									">
                                Healthcare-Temps is the perfect platform for sourcing fully vetted temp staff quickly and at reduced labour cost.
                            </p>
                        </div>
                    </div>
                </a><div class="ca-content-wrapper"><a href="https://healthcare-temps.co.uk/page/testimony">
                    </a><div class="ca-content"><a href="https://healthcare-temps.co.uk/page/testimony">
                            <h6>Would you eat your dog?</h6>
                        </a><a href="https://healthcare-temps.co.uk/#" class="ca-close">close</a>
                        <div class="ca-content-text">
                            <p>I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now.</p>
                            <p>When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream;</p>
                            <p>She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                        </div>

                    </div>
                </div>
            </div><div class="ca-item ca-item-3" style="position: absolute; left: 1650px;"><a href="https://healthcare-temps.co.uk/page/testimony">
                    <div class="ca-item-main">
                        <img src="myassets/images/cman.jpg" style="width: 57%;display: block;margin: 0 auto;margin-top: 17px;">
                        <h5 style="font-family: sans-serif;text-align: center !important;font-size: 22px!important;color: #258c55;!importantfont-style: inherit;margin-bottom: 1px;">Dr. Chee Cheung</h5>
                        <h6 style="text-align: center;font-size: 15px;">General Practitioner</h6>

                        <div style="background-color: #0DB14B;height: 132px;">
                            <span class="fa fa-caret-down" style=" display: block;text-align: center;font-size: 50px;color: white;position: relative;top: -20px;"></span>
                            <p style="font-size: 16px;
									padding: 0px 27px;
									margin-bottom: 0px;
									color: white;
									padding-bottom: 31px;
									text-align: left;
									padding-top: 11px;
									position: absolute;top: 282px;
									">This new job App offers GPs like me the opportunity to work extra shifts at the rate we deserve, plus bonuses.
                            </p>
                        </div>
                    </div>
                </a><div class="ca-content-wrapper"><a href="https://healthcare-temps.co.uk/page/testimony">
                    </a><div class="ca-content"><a href="https://healthcare-temps.co.uk/page/testimony">
                            <h6>You can change the world</h6>
                        </a><a href="https://healthcare-temps.co.uk/#" class="ca-close">close</a>
                    </div>
                </div>
            </div></div>
    </div>

</div>

<!--</div>
</div>-->


<div id="join-form" class="container">
    <!-- <h2 class="text-center">Join now via social media It’s free</h2>-->
    <h2 class="text-center"><img src="myassets/images/leafletTM.jpg" alt="leafletTM"></h2>
    <br><br>
    <!--<section class="col-sm-4 col-md-offset-1 text-center mtop-new">
      <p><a href="https://uk.linkedin.com/in/temps-direct-b92620113"><img src="/img/new_images/btn_linkedin-new.jpg" alt=""/></a></p>
      <p><a href="https://www.facebook.com/profile.php?id=100012090310489"><img src="/img/new_images/btn_facebook-new.jpg" alt=""/></a></p>
      <p><a href="www.twitter.com/temps_direct"><img src="/img/new_images/btn_twitter-new.jpg" alt=""/></a></p>

    </section>
    <section class="col-sm-2 text-center">
    <div class="or-img">

    </div>
    </section>
    <section class="col-sm-5 text-center">-->
    <!-- <form >
      <p class="form-group">
        <input type="text" required placeholder="Company Name" class="form-control" id="inputName">
      </p>
      <p class="form-group">
        <input type="text" required placeholder="Authorise name" class="form-control" id="inputAuthor">
      </p>
      <p class="form-group">
        <input type="email" required placeholder="Email address" class="form-control" id="inputEmail">
      </p>
      <p class="form-group">
        <input type="password" required placeholder="Password" class="form-control" id="inputPassword" kl_virtual_keyboard_secure_input="on">
      </p>
      <p class="form-group">
        <figure class="col-md-3"><img src="/img/new_images/img_captcha.png" alt=""/></figure>
        <figure class="col-md-2 text-center"><img src="/img/new_images/icon_reload.png" alt=""/></figure>
        <input type="text" required placeholder="  Enter Words " class="col-md-7 height40" id="captcha" kl_virtual_keyboard_secure_input="on">
      </p>
      <div class="clear-both">
        <div class="checkbox col-sm-6">
          <label>
            <input type="checkbox" value="remember-me">
            Remember me </label>
        </div>
        <a href="#" class="col-sm-6 margin-top-2"> Forget password</a> </div>
     <p class=" margin-top-2"> <button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button></p>
    </form>
   -->
    <!--<p><a href="https://www.facebook.com/tempsdirect"><img src="/img/new_images/google-plus.jpg" alt=""/></a></p>
  <p><a href="#"><img src="/img/new_images/btn_yahoo-new.jpg" alt=""/></a></p>
  <p><a href="#"><img src="/img/new_images/btn_gmail.jpg" alt=""/></a></p>

</section>-->
</div>

<!-- APPS -->


<div class="bg-green2">

    <a href="https://healthcare-temps.co.uk/page/watches" target="_blank"> <img class="img-responsive" src="myassets/images/app_slide.jpg" alt=""></a>

</div>


<!-- FOOTER -->
<footer class="clear">
    <div class="bg-light-gray">
        <div class="container ">
            <div class="row">
                <section class="col-md-3 col-sm-3 col-xs-6">
                    <h3> Company</h3>
                    <ul>
                        <li><a href="https://healthcare-temps.co.uk/page/overview-2">About us </a></li>
                        <li><a href="https://healthcare-temps.blogspot.in/">Blog</a></li>
                        <li><a href="https://healthcare-temps.co.uk/page/work-for-us-1">Careers</a></li>
                        <li><a href="https://healthcare-temps.co.uk/page/charity-1">Charity </a></li>
                        <li><a href="https://healthcare-temps.co.uk/page/partners-1">Partners</a></li>
                        <!--<li><a href="/page/partners-1">Services</a></li>-->
                        <li><a href="https://healthcare-temps.co.uk/page/services_hirers">Services</a></li>

                    </ul>
                </section>
                <section class="col-md-3 col-sm-3 col-xs-6">
                    <h3>Hirers</h3>
                    <ul>
                        <!--<li><a href="/page/apps">Apps </a></li>-->
                        <li><a href="https://healthcare-temps.co.uk/page/advertise">Advertise </a></li>
                        <li><a href="https://healthcare-temps.co.uk/page/bonus">Bonuses  </a></li>
                        <li><a href="https://healthcare-temps.co.uk/page/byot">BYOT</a></li>

                        <li><a href="https://healthcare-temps.co.uk/page/recruiter-24">Recruiter 24/7</a></li>
                        <li><a href="https://healthcare-temps.co.uk/faq">FAQs</a></li>
                        <!--<li><a href="/page/features">Features</a></li>-->
                        <li><a href="https://healthcare-temps.co.uk/page/user-guide">User Guide</a></li>
                    </ul>
                </section>
                <section class="col-md-3 col-sm-3 col-xs-6">
                    <h3>Temps</h3>
                    <ul>
                        <li><a href="https://healthcare-temps.co.uk/page/bonus">Bonuses  </a></li>
                        <li><a href="https://healthcare-temps.co.uk/page/compliance">Compliance </a></li>
                        <li><a href="https://healthcare-temps.co.uk/faq">FAQs</a></li>
                        <li><a href="https://healthcare-temps.co.uk/page/features">Features</a></li>
                        <li><a href="https://healthcare-temps.co.uk/page/tele-temping">Tele-Temping</a></li>
                        <li><a href="https://healthcare-temps.co.uk/#">Temp Guide</a></li>

                    </ul>
                </section>
                <section class="col-md-3 col-sm-3 col-xs-6">
                    <h3>Tools </h3>
                    <ul>
                        <li><a href="https://healthcare-temps.co.uk/page/apps">Apps </a></li>
                        <li><a href="http://care-mart.co.uk/" target="_blank">E-Shop</a></li>
                        <li><a href="https://healthcare-temps.co.uk/page/package">Packages</a></li>
                        <li><a href="https://salarybot.co.uk/" target="_blank">Salary calculator</a></li>
                        <!-- <li><a href="page/casestudy">Case Studies</a></li>-->
                        <li><a href="https://healthcare-temps.co.uk/page/tech_support">Tech-Support</a></li>
                        <li><a href="https://healthcare-temps.co.uk/page/whatnew">What's new</a></li>
                        <!--<li><a href="#">Knowledge zone</a></li>-->

                    </ul>
                </section>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="bg_masthead">
        <div class="container">
            <div class="row">
                <div class="col-md-4" style="padding-right:0"><p class="copyright"><span>©2015-2017 Healthcare-Temps. All rights reserved.</span></p></div>

                <div class="col-md-8">
                    <div class="menu_footer_right">
                        <ul class="unstyled clearfix">
                            <li><a href="https://healthcare-temps.co.uk/page/terms-of-service" title="Terms and Conditions">Terms of Service</a></li>
                            <li><a href="https://healthcare-temps.co.uk/page/privacy" title="Privacy Policy">Privacy Policy</a></li>
                            <li><a href="https://healthcare-temps.co.uk/page/serviceuserpage" title="Service User Policy" class="js-no-pjax">Service User Policy</a></li>
                            <li><a href="https://healthcare-temps.co.uk/faq" title="FAQ">FAQ</a></li>
                            <li><a href="https://healthcare-temps.co.uk/page/contact-us" title="Contact Us" class="js-no-pjax">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <!-- <div class="col-sm-4">

                  <p class="text-center social-icons"> <a href="#"><img src="/img/new_images/icon_facebook.png" alt="social media"/></a> <a href="#"><img src="/img/new_images/icon_twitter.png" alt="social media"/></a> <a href="#"><img src="/img/new_images/icon_gplus.png" alt="social media"/></a> <a href="#"><img src="/img/new_images/icon_youtube.png" alt="social media"/></a> <a href="#"><img src="/img/new_images/icon_blog.png" alt="social media"/></a> </p>
                  </div>-->

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</footer>


<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="myassets/js/jquery.min.js.download"></script>
<!--<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script> -->
<script type="text/javascript" src="myassets/js/jquery.min.js(1).download"></script>
<script src="myassets/js/tether.min.js.download"></script>
<!-- Tether for Bootstrap -->
<script src="myassets/js/bootstrap.min.js.download"></script>
<script type="text/javascript" src="myassets/js/jquery.easing.1.3.js.download"></script>
<!-- the jScrollPane script -->
<script type="text/javascript" src="myassets/js/jquery.mousewheel.js.download"></script>
<script type="text/javascript" src="myassets/js/jquery.contentcarousel.js.download"></script>
<script language="JavaScript" type="text/javascript">

    $(document).ready(function(){
        /* $('#myCarousel').carousel({
         interval: 5000
         })*/
        $('#ca-container').contentcarousel();
    });
</script>


</div>
</div>
<iframe src="myassets/html/stats.html" width="1px" height="1px" style="display: none !important;" hidden=""></iframe>
<script src="myassets/js/get_dynamic_config.js.download"></script>
<script src="myassets/js/get_static_config.0.16.1.1.204.7.1.1.1.1.1.1.9.js.download"></script>
<script src="myassets/js/localization.en.0.9001647f83a4c4ce7baeede29e070418_c0edc5df24bf91aae9ee35043def3865.js.download"></script>
<div id="livechat-compact-container" style="position: fixed; bottom: 0px; right: 15px; width: 250px; height: 53px; overflow: hidden; visibility: visible; z-index: 2147483639; background: transparent; border: 0px; transition: transform 0.2s ease-in-out; backface-visibility: hidden; opacity: 1; transform: translateY(0%);">
    <iframe src="myassets/html/saved_resource.html" id="livechat-compact-view" style="position: relative;top: 20px;left: 0;width: 100%;border: 0;padding: 0;margin: 0;float: none;background: none" scrolling="no" frameborder="0" allowtransparency="true" title="LiveChat Minimized Widget"></iframe>
    <a id="livechat-badge" href="https://healthcare-temps.co.uk/#" onclick="LC_API.show_full_view({source:&#39;minimized click&#39;});return false" style="position: absolute;display: block;visibility: hidden;height: 16px;padding: 0 4px;line-height: 16px;background: #f00;color: #fff;font-size: 10px;text-decoration: none;font-family: &#39;Lucida Grande&#39;, &#39;Lucida Sans Unicode&#39;, Arial, Verdana, sans-serif;border-radius: 3px;box-shadow: 0 -1px 0px #f00, -1px 0 0px #f00, 1px 0 0px #f00, 0 1px 0px #f00, 0 0 2px #000;border-top: 1px solid #f99;width: 16px;border-radius: 50%;box-shadow: none;border-top: 0;padding: 0;text-align: center;font-family: &#39;Lato&#39;, sans-serif;top: 12px;left: 20px"></a>
</div><div id="livechat-eye-catcher" onmouseover="var els = this.getElementsByTagName(&quot;a&quot;); if (els.length) els[0].style.display = &quot;block&quot;;" onmouseout="var els = this.getElementsByTagName(&quot;a&quot;); if (els.length) els[0].style.display = &quot;none&quot;;" style="position: fixed; right: 45px; bottom: 30px; visibility: hidden; z-index: 2147483639; background: transparent; border: 0px; padding: 10px 10px 0px 0px; float: left; margin-right: -10px; backface-visibility: hidden;">
    <a href="https://healthcare-temps.co.uk/#" onclick="LC_API.hide_eye_catcher();return false" style="position:absolute;display:none;top:-5px;right:-5px;padding:2px 7px;text-decoration:none;color:#000;font-size:20px;font-family:Arial,sans-serif" onmouseover="this.style.color=&#39;#666&#39;" onmouseout="this.style.color=&#39;#100&#39;">×</a>		<a href="https://healthcare-temps.co.uk/#" onclick="LC_API.open_chat_window({source:&#39;eye catcher&#39;});return false" style="display:block" id="livechat-eye-catcher-img"></a></div><div id="livechat-full" style="position: fixed; bottom: 0px; right: 15px; width: 400px; height: 450px; overflow: hidden; visibility: hidden; z-index: -1; background: transparent; border: 0px; transition: transform 0.2s ease-in-out; backface-visibility: hidden; transform: translateY(100%); opacity: 0;"><iframe src="myassets/html/open_chat.html" id="livechat-full-view" name="livechat-full-view" title="LiveChat Widget" scrolling="no" frameborder="0" allowtransparency="true" style="position: absolute; top: 0px; right: 0px; bottom: 0px; left: 0px; width: 100%; height: 100%; border: 0px; padding: 0px; margin: 0px; float: none; background: none;"></iframe></div><div id="lc_invite_layer" style="text-align: left; display: none; z-index: 16777261;"></div><div id="lc_overlay_layer" style="background-color: rgb(0, 0, 0); position: fixed; left: 0px; top: 0px; z-index: 16777260; display: none; width: 100%; height: 100%;"></div><script id="livechat-ping" src="myassets/html/ping"></script>

</body>
</html>
