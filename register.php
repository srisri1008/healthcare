
<?php
//Check if frontinit.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php';	
}
//Get Site Settings Data
$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
 foreach($query->results() as $row) {
 	$title = $row->title;
 	$use_icon = $row->use_icon;
 	$site_icon = $row->site_icon;
 	$tagline = $row->tagline;
 	$description = $row->description;
 	$keywords = $row->keywords;
 	$author = $row->author;
 	$bgimage = $row->bgimage;
 }			
}

//Get Payments Settings Data
$q1 = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($q1->count()) {
 foreach($q1->results() as $r1) {
 	$currency = $r1->currency;
 	$membershipid = $r1->membershipid;
 }			
}

//Getting Payement Id from Database
$query = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
if ($query->count() === 1) {
  $q1 = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
} else {
  $q1 = DB::getInstance()->get("membership_agency", "*", ["membershipid" => $membershipid]);
}
if ($q1->count() === 1) {
 foreach($q1->results() as $r1) {
  $bids = $r1->bids;
 }
}

//Register Function
if (Input::exists()) {
// if(Token::check(Input::get('token'))){
 	 
    $errorHandler = new ErrorHandler;
	
	$validator = new Validator($errorHandler);
	
	$validation = $validator->check($_POST, [
	  'name' => [
		 'required' => true,
		 'minlength' => 2,
		 'maxlength' => 50
	   ],
	    'name' => [
		 'required' => true,
		],
	  'email' => [
	     'required' => true,
	     'email' => true,
	     'maxlength' => 100,
	     'minlength' => 2,
	     'unique' => 'freelancer',
	     'unique' => 'client'
	  ],			 
	  'username' => [
	     'required' => true,
	     'maxlength' => 20,
	     'minlength' => 3,
	     'unique' => 'freelancer',
	     'unique' => 'client'
	  ],
	   'password' => [
	     'required' => true,
	     'minlength' => 6
	   ],
	   'confirmPassword' => [
	     'match' => 'password'
	   ]
	]);
	 	
	if (!$validation->fails()) {

	      if (Input::get('user_type') === 'on') {
		  	  	$client = new Client();
		  
				$remember = (Input::get('remember') === 'on') ? true : false;
				$salt = Hash::salt(32);  
				$imagelocation = 'uploads/default.png';
				$clientid = uniqueid(); 
				try{

				  $client->create(array(
				   'clientid' => $clientid,
				   'username' => Input::get('username'),
				   'password' => Hash::make(Input::get('password'), $salt),
				   'salt' => $salt,
				   'name' => Input::get('name'),
		           'email' => Input::get('email'),
				   'imagelocation' => $imagelocation,
		           'joined' => date('Y-m-d H:i:s'),
		           'user_type' => 1
				  ));	

				if ($client) {

//			     $login = $client->login(Input::get('email'), Input::get('password'), $remember);
//				 Redirect::to('Client/');
                    Redirect::to('/send_mail_client.php?id='. $clientid);
			    }else {
			     $hasError = true;
			   }
					
				}catch(Exception $e){
				 die($e->getMessage());	
				}				      	
	          
	      } else {

			if($membershipid != '')
			{
				//your site secret key
				$secret = '6LdGqkMUAAAAAEGOZfvwgW9IGWTjzEcAHEPHR8Um';
				//get verify response data
				$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
				$responseData = json_decode($verifyResponse);
				if($responseData->success){
					
					$freelancer = new Freelancer();			  
					$remember = (Input::get('remember') === 'on') ? true : false;
					$salt = Hash::salt(32);  
					$imagelocation = 'uploads/default.png';
					$bgimage = 'uploads/bg/default.jpg';
					$freelancerid = uniqueid(); 
					try{

					  $freelancer->create(array(
					   'freelancerid' => $freelancerid,
					   'username' => Input::get('username'),
					   'password' => Hash::make(Input::get('password'), $salt),
					   'salt' => $salt,
					   'name' => Input::get('name'),
					   'email' => Input::get('email'),
					   'imagelocation' => $imagelocation,
					   'bgimage' => $bgimage,
					   'membershipid' => $membershipid,
					   'membership_bids' => $bids,
					   'membership_date' => date('Y-m-d H:i:s'),
					   'joined' => date('Y-m-d H:i:s'),
					   'user_type' => 1
					  ));	
					  
							if ($freelancer) {
							 $login = $freelancer->login(Input::get('email'), Input::get('password'), $remember);
							 //Redirect::to('tempworker/');
							 Redirect::to('temp_upload.php?id='.$freelancerid);
							}else {
							 $hasError = true;
						   }
						
					}catch(Exception $e){
					 die($e->getMessage());	
					}
				}
	        } else {
				  $memError = true;
			}
	      }
       
		
	} else {
	     $error = '';
	     foreach ($validation->errors()->all() as $err) {
	     	$str = implode(" ",$err);
	     	$error .= '
		           <div class="alert alert-danger fade in">
		            <a href="#" class="close" data-dismiss="alert">&times;</a>
		            <strong>Error!</strong> '.$str.'<br/>
			       </div>
			       ';
	     }
		 
      }

// }
  	
}

if (!function_exists('base_url')) {
    function base_url($atRoot=FALSE, $atCore=FALSE, $parse=FALSE){
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            $core = $core[0];

            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf( $tmplt, $http, $hostname, $end );
        }
        else $base_url = 'http://localhost/';

        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }

        return $base_url;
    }
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en" class="no-js"> 
<!--<![endif]-->
<head>

	    <!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta charset="utf-8">
        <title><?php echo escape($title) .' - '. escape($tagline) ; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="<?php echo escape($description); ?>">
        <meta name="keywords" content="<?php echo escape($keywords); ?>">
        <meta name="author" content="<?php echo escape($author); ?>">
		
		<!-- ==============================================
		Favicons
		=============================================== --> 
		<link rel="shortcut icon" href="img/favicons/favicon.ico">
		<link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-touch-icon-114x114.png">
		
	    <!-- ==============================================
		CSS
		=============================================== -->
        <!-- Style-->
        <link href="assets/css/login.css" rel="stylesheet" type="text/css" />
        <link href="myassets/css/main.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

		<!-- ==============================================
		Feauture Detection
		=============================================== -->
		<script src="assets/js/modernizr-custom.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		
		<!--[if lt IE 9]>
		 <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }
        .switch input {display:none;}

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #31a200;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #31a200;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
        /*Material Switch*/
        .material-switch{
            padding: 10px 0 20px;
        }

        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
            right: -80px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            left: -2px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 60px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 40px;
        }

        .material-switch span{
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-weight: 700;
            font-size: 14px;
            position: relative;
            left: 25px;
            color: #fff;
        }

        .list-group li.list-group-item .material-switch span{
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-weight: 700;
            font-size: 14px;
            color: #1A1A1A !important;
        }

        .list-group li.list-group-item .material-switch span.pull-right{
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-weight: 700;
            font-size: 14px;
            position: relative;
            left: -10px;
            color: #1A1A1A !important;
        }

        .material-switch > .label-success{
            background: #05CB95;
        }


        /* code for animated blinking cursor */
        .typed-cursor{
            opacity: 1;
            font-weight: 100;
            -webkit-animation: blink 0.7s infinite;
            -moz-animation: blink 0.7s infinite;
            -ms-animation: blink 0.7s infinite;
            -o-animation: blink 0.7s infinite;
            animation: blink 0.7s infinite;
        }
        @-keyframes blink{
        0% { opacity:1; }
        50% { opacity:0; }
        100% { opacity:1; }
        }
        @-webkit-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-moz-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-ms-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }
        @-o-keyframes blink{
            0% { opacity:1; }
            50% { opacity:0; }
            100% { opacity:1; }
        }

        #iphone {
            float: left;
            margin-left: 68px;
        }
        #weblog .fa-lock {
            display: inline-block;
            color: green;
            font-size: 17px;
            margin-left: 16px;
        }

        #weblog input {
            border: 0px solid black;
        }
        .givebrdr {
            border:1px solid #31a200;
            margin-top: 10px;
        }
        .inputs {
            display: inline-block;
            border-right: none;
            border-left: none;
            box-shadow: unset;
            width: 100%;
        }
        .inputs1 {
            display: inline-block;
            border-right: none;
            border-left: none;
            box-shadow: unset;
            width: 91%;
        }
    </style>

    <style>
        .label-success {
            border: none;
        }
        .logo_img {
            width: 22%;
        }
        .logo_img {
            width: 211px;
            margin-left: 33px;
        }
        .navbar.navbar-inverse.navigation_bar {
            height: 78px;
            background-color: #132943;
            border-radius: 0px;
        }
        .nav.navbar-nav.navbar-right.nav_right_side {
            margin: 13px 0px;
        }
        .nav.navbar-nav.navbar-right.nav_right_side li a:hover {
            color: #1FAC56;
            transition: 0.3s;
        }
        .nav.navbar-nav.navbar-right.nav_right_side li a {
            font-size: 16px;
        }

        .healthcare_temps h6 {
            color: #1FAC54;
            font-size: 29px;
            margin-bottom: 11px;
            margin-top: 48px;
        }
        .healthcare_temps hr {
            height: 3px;
            background-color: #1FAC54;
            width: 77px;
            margin-top: 1px;
        }

        .urgent_hjobs {
            background-color: #007FFF;
            color: white;
            text-align: center;
            margin-bottom: 27px;
        }
        .urgent_hjobs h6 {
            font-size: 23px;
            padding: 13px 0px;
            margin-top: 0px;
        }
        .urgent_hjob_text {
            padding: 0px 27px;
        }
        .hire_textbox {
            height: 39px;
            border-radius: 0px;
            margin-bottom: 21px;
        }
        .hire-job {
            border: 1px solid #007FFF;
            text-align: center;
        }
        .hire-job button {
            background-color: #007FFF;
            color: white;
            border: 1px solid #056cd4;
            padding: 6px 28px;
            margin-bottom: 26px;
            border-radius: 40px;
        }
        .hire-job button:hover {
            background-color: #0772de;
            transition: 0.3s;
        }
        .job_button {
            padding: 0px 19px !important;
            margin-bottom: 0px !important;
        }
        .table.urjent_job tr td {
            border: 0px;
        }
        .benifits_sec {
            background-color: #1FAC56;
            padding-bottom: 44px;
        }
        .container-fluid.main_container {
            padding: 0px 0px;
        }
        .benfit_hw img {
            width: 7%;
            display: block;
            margin: 0 auto;
            margin-top: 36px;
        }
        .benfit_hw p span img {
            margin: 0px 0px;
            display: inline-block;
            margin-right: 14px;
            width: 16px;
        }
        .benfit_hw p {
            line-height: 21px;
            color: white;
            margin-left: 137px;
        }
        .benfit_hw h6 {
            text-align: center;
            color: white;
            font-size: 23px;
            margin-bottom: 23px
        }
        .benfit_hw button {
            background-color: #3D91C5;
            color: white;
            border: 1px solid #1680c1;
            padding: 8px 17px;
            border-radius: 37px;
            display: table;
            margin: 0 auto !important;
            margin-bottom: 13px;
            margin-top: 39px !important;
        }
        .benfit_hw button:hover {
            background-color: #248bcc;
            transition: 0.3s;
        }

        .healthcare_temp_sec {
            margin-bottom: 79px;
        }
        .suitcase {
            margin-bottom: 19px !important;
        }
        .our_solution h6 {
            color: #24B3F7;
            text-align: center;
            font-size: 34px;
            margin-top: 52px;
            margin-bottom: 52px;
        }
        .solution_content img {
            width: 100%;
        }
        .solution_content button {
            background-color: #23B3F7;
            border: 1px solid #0c9bde;
            color: white;
            padding: 2px 30px;
            position: relative;
            top: -87px;
            left: 46px;
        }
        .solution_content button:hover {
            background-color: #15a6ea;
            transition: 0.3s;
        }
        .solution_cnt_container {
            margin-bottom: 20px;
        }
        .users_head h6 {
            text-align: center;
            font-size: 28px;
            color: #35A5D6;
            margin: 24px 0px;
        }
        .user_content img {
            width: 45%;
            display: block;
            margin: 0 auto;
        }
        .user_content h6 {
            color: #00B24D;
            text-align: center;
            font-size: 18px;
            margin-top: 14px;
        }
        .user_content h5 {
            text-align: center;
            font-size: 14px;
        }
        .user_text {
            background-color: #00B24D;
            color: white;
        }
        .user_content {
            padding: 16px 41px;
            background-color: white;
        }
        .col-lg-4.user_para_text {
            margin-bottom: 41px;
        }
        .user_text span {
            display: table;
            margin: 0 auto;
            font-size: 50px;
            position: relative;
            top: -21px;
        }
        .user_text p {
            padding: 0px 17px;
            position: relative;
            top: -14px;
        }
        .user_text.user_text_blue {
            background-color: #32A2D8;
        }
        .user_text.user_text_right {
            padding-bottom: 10px;
        }
        .users_sec {
            margin-bottom: 37px;
            background-color: #DBDDDC;
        }
        .reveloution_temp img {
            width: 100%;
        }
        .col-lg-12.virtual_care img {
            width: 100%;
        }
        .footer_head h6 {
            font-size: 25px;
            margin-top: 35px;
        }
        ul.footer_links {
            list-style-type: none;
            padding: 0px;
        }
        ul.footer_links li a {
            font-size: 15px;
            color: #7f7f7f;
            text-decoration: none;
        }
        ul.footer_links li a:hover {
            color: #525151;
        }
        .bottom_links {
            background-color: #132943;
        }
        .bottom_links_ul {
            list-style-type: none;
        }
        ul.bottom_links_ul li {
            float: left;
        }
        ul.bottom_links_ul li a {
            color: white;
            padding: 0px 15px;
        }
        .bottom_links {
            background-color: #132943;
            padding: 15px 0px;
            margin-top: 33px;
        }
        .bottom_links_ul {
            list-style-type: none;
            margin: 10px 269px;
        }
        .col-lg-12.bottom_links h6 {
            color: white;
            font-size: 15px;
            text-align: right;
            margin-right: 57px;
        }
        .footer {
            padding: 0px 0px !important;
            background: #FFFFFF !important;
        }


        /* New css */


    </style>
		
</head>

<body>

	<!-- Paste this code after body tag -->
    <div class="loader">
	<div class="se-pre-con"></div>
    </div>
	
    
 <? 
$basename = basename($_SERVER["REQUEST_URI"], ".php");
$editname = basename($_SERVER["REQUEST_URI"]);
$test = $_SERVER["REQUEST_URI"];
?>
     <!-- ==============================================
     Navigation Section
     =============================================== -->
    <?php /* ?>
	<header id="header" headroom="" role="banner" tolerance="5" offset="700" class="navbar navbar-fixed-top navbar--white ng-isolate-scope headroom headroom--top">
	  <nav role="navigation">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle header-nav__button" data-toggle="collapse" data-target=".navbar-main">
	        <span class="icon-bar header-nav__button-line"></span>
	        <span class="icon-bar header-nav__button-line"></span>
	        <span class="icon-bar header-nav__button-line"></span>
	      </button>
	      <div class="header-nav__logo">
	        <a class="header-nav__logo-link navbar-brand" href="index.php">
	       	<?php if($use_icon === '1'): ?>
	       		<i class="fa <?php echo $site_icon; ?>"></i>
	       	<?php endif; ?>  <?php echo escape($title); ?></a>
	      </div>
	    </div>
	    <div class="collapse navbar-collapse navbar-main navbar-right">
	      <ul class="nav navbar-nav header-nav__navigation">
	        <li class="header-nav__navigation-item
	         <?php echo $active = ($_SERVER['HTTP_HOST'] && $test === '') ? ' active' : ''; echo $active = ($basename == 'index') ? ' active' : ''; ?>">
	          <a href="index.php" class="header-nav__navigation-link">
	            <?php echo $lang['home']; ?>
	          </a>
	        </li>
	        <li class="header-nav__navigation-item <?php echo $active = ($basename == 'jobs') ? ' active' : ''; echo $active = ($editname == 'jobpost.php?title='. Input::get('title').'') ? ' active' : '';?>">
	          <a href="jobs.php" class="header-nav__navigation-link ">
	            <?php echo $lang['jobs']; ?>
	          </a>
	        </li>
	        <li class="header-nav__navigation-item <?php echo $active = ($basename == 'services') ? ' active' : ''; echo $active = ($editname == 'tempworker.php?a='. Input::get('a').'&id='. Input::get('id').'') ? ' active' : ''; ?>">
	          <a href="services.php" class="header-nav__navigation-link ">
	            <?php echo $lang['services']; ?>
	          </a>
	        </li>
	        <li class="header-nav__navigation-item <?php echo $active = ($basename == 'about') ? ' active' : ''; ?>">
	          <a href="about.php" class="header-nav__navigation-link ">
	            <?php echo $lang['about']; ?>
	          </a>
	        </li>
	        <li class="header-nav__navigation-item <?php echo $active = ($basename == 'how') ? ' active' : ''; ?>">
	          <a href="how.php" class="header-nav__navigation-link ">
	            <?php echo $lang['how']; ?> <?php echo $lang['it']; ?> <?php echo $lang['works']; ?>
	          </a>
	        </li>
	        <li class="header-nav__navigation-item <?php echo $active = ($basename == 'faq') ? ' active' : ''; ?>">
	          <a href="faq.php" class="header-nav__navigation-link ">
	            <?php echo $lang['faq']; ?>
	          </a>
	        </li>
	        <li class="header-nav__navigation-item <?php echo $active = ($basename == 'contact') ? ' active' : ''; ?>">
	          <a href="contact.php" class="header-nav__navigation-link ">
	            <?php echo $lang['contact']; ?>
	          </a>
	        </li>
	        
		 <?php
		 //Start new Admin object
		 $admin = new Admin();
		 //Start new Client object
		 $client = new Client();
		 //Start new Freelancer object
		 $freelancer = new Freelancer(); 
		 
		 if ($admin->isLoggedIn()) { ?>
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
            	<?php // echo $profileimg; ?>
                  <img src="Admin/<?php echo escape($admin->data()->imagelocation); ?>" class="user-image" alt="User Image"/>
                
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">
                  	<?php echo escape($admin->data()->name); ?>
                  </span>
                </a>
                <ul class="dropdown-menu">
						<li class="m_2"><a href="Admin/dashboard.php"><i class="fa fa-dashboard"></i><?php echo $lang['dashboard']; ?></a></li>
						<li class="m_2"><a href="Admin/profile.php?a=profile"><i class="fa fa-user"></i><?php echo $lang['view']; ?> <?php echo $lang['profile']; ?></a></li>
						<li class="m_2"><a href="Admin/logout.php"><i class="fa fa-lock"></i> <?php echo $lang['logout']; ?></a></li>	
        		</ul>
              </li>
		<?php } elseif($client->isLoggedIn()) { ?>
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
            	<?php // echo $profileimg; ?>
                  <img src="Client/<?php echo escape($client->data()->imagelocation); ?>" class="user-image" alt="User Image"/>
                
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">
                  	<?php echo escape($client->data()->name); ?>
                  </span>
                </a>
                <ul class="dropdown-menu">
						<li class="m_2"><a href="Client/dashboard.php"><i class="fa fa-dashboard"></i><?php echo $lang['dashboard']; ?></a></li>
						<li class="m_2"><a href="Client/profile.php?a=profile"><i class="fa fa-user"></i><?php echo $lang['view']; ?> <?php echo $lang['profile']; ?></a></li>
						<li class="m_2"><a href="Client/logout.php"><i class="fa fa-lock"></i> <?php echo $lang['logout']; ?></a></li>	
        		</ul>
              </li>
		<?php } elseif($freelancer->isLoggedIn()) { ?>
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
            	<?php // echo $profileimg; ?>
                  <img src="tempworker/<?php echo escape($freelancer->data()->imagelocation); ?>" class="user-image" alt="User Image"/>
                
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">
                  	<?php echo escape($freelancer->data()->name); ?>
                  </span>
                </a>
                <ul class="dropdown-menu">
						<li class="m_2"><a href="tempworker/index.php"><i class="fa fa-dashboard"></i><?php echo $lang['dashboard']; ?></a></li>
						<li class="m_2"><a href="tempworker/profile.php?a=profile"><i class="fa fa-user"></i><?php echo $lang['view']; ?> <?php echo $lang['profile']; ?></a></li>
						<li class="m_2"><a href="tempworker/logout.php"><i class="fa fa-lock"></i> <?php echo $lang['logout']; ?></a></li>
        		</ul>
              </li>
		<?php } else { ?>		 		        
	        <li class="header-nav__navigation-item <?php echo $active = ($basename == 'login') ? ' active' : ''; echo $active = ($basename == 'forgot') ? ' active' : ''; echo $active = ($basename == 'reset') ? ' active' : ''; ?>">
	          <a class="header-nav__navigation-link" href="login.php"><?php echo $lang['login']; ?></a>
	        </li>
	        <li class="header-nav__navigation-item <?php echo $active = ($basename == 'register') ? ' active' : ''; ?>">
	          <a class="header-nav__navigation-link header-nav__navigation-link--outline" href="register.php"><?php echo $lang['signup']; ?> <?php echo $lang['for']; ?> <?php echo $lang['free']; ?></a>
	        </li>
		 <?php } ?>              		 	

              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  	<?php echo $lang['languages']; ?>
                </a>
                <ul class="dropdown-menu">
					<li class="m_2"><a href="<?php echo $test; ?>?lang=english">English</a></li>
					<li class="m_2"><a href="<?php echo $test; ?>?lang=french">French</a></li>
					<li class="m_2"><a href="<?php echo $test; ?>?lang=german">German</a></li>	
					<li class="m_2"><a href="<?php echo $test; ?>?lang=portuguese">Portuguese</a></li>
					<li class="m_2"><a href="<?php echo $test; ?>?lang=spanish">Spanish</a></li>
					<li class="m_2"><a href="<?php echo $test; ?>?lang=russian">Russian</a></li>	
					<li class="m_2"><a href="<?php echo $test; ?>?lang=chinese">Chinese</a></li>	
        		</ul>
              </li>


              	        
	      </ul>
	    </div>
	  </nav>
	</header>      
	 <?php */
    //Include navigation.php. Contains navigation content
    include ('includes/template/navigation.php');
    ?>
     <!-- ==============================================
	 Header
	 =============================================== -->
	
	 <header class="header-login" style="
    background-color: #ffffff;
   background-size: cover;
  background-position: center center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  color: #fff;
  height: 55vh;
  width: 100%;
  
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center; ">
      <div class="container">
	   <div class="content">
	    <div class="row">
	     <!--<h1 class="revealOnScroll" data-animation="fadeInDown">
	       	<?php if($use_icon === '1'): ?>
	       		<i class="fa <?php //echo $site_icon; ?>"></i>
	       	<?php endif; ?>  <?php //echo escape($title); ?></h1>
		 <div id="typed-strings">
		  <span><?php //echo escape($tagline); ?></span>
		 </div>-->
		<!-- <p id="typed"></p>-->
		
		 <div id="typed-strings">
		  <span>Welcome</span>
		 </div>
		 <p><?php echo $lang['temp_register_title']; ?></p>
		 
        </div><!-- /.row -->
       </div><!-- /.content -->
	  </div><!-- /.container -->
     </header><!-- /header -->
	 
     <!-- ==============================================
     Banner Login Section
     =============================================== -->
	 <section class="banner-login" style="background-color: white">


         <div class="container-fluid" >
             <div class="row">

                 <?php if(isset($memError)) { //If errors are found ?>
                     <div class="alert alert-danger fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong><?php echo $lang['hasError']; ?></strong> <?php echo $lang['mem_error']; ?>
                     </div>
                 <?php } ?>

                 <?php if(isset($hasError)) { //If errors are found ?>
                     <div class="alert alert-danger fade in">
                         <a href="#" class="close" data-dismiss="alert">&times;</a>
                         <strong><?php echo $lang['hasError']; ?></strong> <?php echo $lang['login_error']; ?>
                     </div>
                 <?php } ?>

                 <?php if (isset($error)) {
                     echo $error;
                 } ?>

                 <div class="col-lg-offset-3 col-lg-5">
                     <form action="" method="post" id="weblog"style="border: 1px solid #c6c6c6; padding: 10px 45px; margin-bottom: 20px;">
                         <h2 style="text-align: center;">Welcome</h2>
                         <p style="text-align: center;">
                             If you are planning on either hiring quality temp staff,<br />
                             or you are a healthcare temp/locum looking for work.<br/>
                             simply follow our Three step sign-up process below.
                         </p>
                         <ul class="list-group">
                             <li class="list-group-item">
                                 <div class="material-switch pull-center">
                                     <span class="pull-left">Temp Worker</span>
                                     <input id="someSwitchOptionDefault" name="user_type" type="checkbox">
                                     <label for="someSwitchOptionDefault" class="label-success"></label>
                                     <span class="pull-right">Hirer</span>
                                 </div>
                             </li>
                         </ul>
                         <div class="form-group givebrdr">
                             <input type="text" class="form-control inputs" id="name" value="<?php echo escape(Input::get('name')); ?>" placeholder="ENTER NAME:" name="name">
                         </div>
                         <div class="form-group givebrdr">
                             <input type="email" class="form-control inputs" value="<?php echo escape(Input::get('email')); ?>" id="email" placeholder="ENTER EMAIL:" name="email">
                         </div>
                         <div class="form-group givebrdr">
                             <input type="text" class="form-control inputs" value="<?php echo escape(Input::get('username')); ?>" id="username" placeholder="USER NAME:" name="username">
                         </div>
                         <div class="form-group givebrdr">
                             <i class="fa fa-lock"></i>
                             <input type="password" class="form-control inputs1" id="password" placeholder="Password" name="password">
                         </div>
                         <div class="form-group givebrdr">
                             <i class="fa fa-lock"></i>
                             <input type="password" class="form-control inputs1" id="confpass" placeholder="CONFIRM PASSWORD:" name="confirmPassword">
                         </div>
                         <div class="form-row" style="margin-top: 12px; margin-bottom: 12px">
                             <div class="form-controls">
                                 <div class="g-recaptcha" name="g-recaptcha-response" data-sitekey="6LdGqkMUAAAAAHFSRx97kQqVfbU8Y3nkZRccH8iu"></div>
                             </div><!-- /.form-controls -->
                         </div><!-- /.form-row -->
                         <div style="text-align: center;">
                             <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                             <button type="submit" class="btn" style="padding: 5px 45px;background: #31a200; color: white;">REGISTER</button>
                         </div>

                         <div class="row" style="margin-top: 35px; text-align: center;">
                             <div class="col-lg-3 col-lg-offset-2"style="background: #31a200"></div>
                             <div class="col-lg-3" style="font-size: 25px;">OR</div>
                             <div class="col-lg-3"style="background: #31a200">
                             </div>
                         </div>
                         <p style="text-align: center;">
                             Social Media Sign-up - Hirers only.
                         </p>
                         <div class="row" style="text-align: center;font-size: 45px;">
                             <div class="col-lg-2 col-lg-offset-3">
                                 <p style="color:#4867aa"><i class="fab fa-facebook-square"></i></p>
                             </div>
                             <div class="col-lg-2">
                                 <p style="color:#007bb6"><i class="fab fa-linkedin"></i></p>
                             </div>
                             <div class="col-lg-1">
                                 <p style="color:#00aff0"><i class="fab fa-twitter-square"></i></p>
                             </div>
                         </div>
                     </form>
                 </div>
             </div>
         </div>

     </section><!-- /section -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include ('includes/template/footer.php'); ?>

    <!-- ==============================================
    Scripts
    =============================================== -->
	 
     <!-- jQuery 2.1.4 -->
     <script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
     <!-- Bootstrap 3.3.6 JS -->
     <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
     <!-- Typed JS -->
     <script src="assets/js/typed.min.js" type="text/javascript"></script>
     <!-- Kafe JS -->
     <script src="assets/js/kafe.js" type="text/javascript"></script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-2199989077469674",
            enable_page_level_ads: true
        });
    </script>

</body>
</html>
