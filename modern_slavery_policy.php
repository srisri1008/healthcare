<?php
//Check if init.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include('includes/template/header.php'); ?>
<body class="greybg" style="background-color: #fff;">

<!-- Include navigation.php. Contains navigation content. -->
<?php include('includes/template/navigation.php'); ?>

<style>
    .h2des{
        border-bottom: 3px solid #378139;
        display: inline;
        color:#3f9a41;
    }
    .pdes{
        margin-top: 22px;
        font-size: 20px;
        margin-bottom: 35px;
    }
    .container{
        margin-top: 50px;
        margin-bottom: 50px;
    }
    .otherkey{
        text-align: center;
        margin-top: 50px;
        margin-bottom: 50px;
    }
    b{
        line-height: 50px;
    }
    .remote{
        line-height: 50px;
    }
    .imglinehight{
        margin-top: 10px;
    }
</style>
<!-- ==============================================
Hello Section
=============================================== -->
<div id="pages-features" class="content user" style="margin-top: 100px;">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489"
                                        title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter"
                                       target="_blank"></a></li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113"
                                        title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw"
                                       title="Follow me on youtube" target="_blank"> </a></li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts"
                                           title="Follow me on google plus" target="_blank"></a></li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/"
                                       title="Follow me on blogger" target="_blank"></a></li>
            </ul>
        </div>
        <div id="js-head-menu" class="">
            <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <a class="show hor-space" title="Temps-direct.uk" href="/">
                                <img alt="" src="myassets/img/logo.png">
                            </a>
                        </div>
                        <div class="cantent">
                            <p>
                                Your log-out has been successful.</p>
                            <p>
                                Thank you for using Temps-Direct temp recruiting platform.</p>
                            <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                            <input name="feedback" type="commant">
                            <p>Thank you!</p>
                        </div>
                        <div class="modal-footer hide">
                            <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default"> Close</a>
                        </div>
                    </div>

                </div>
            </div>

            <script type="text/javascript">
                //<![CDATA[
                function getCookie(c_name) {
                    var c_value = document.cookie;
                    var c_start = c_value.indexOf(" " + c_name + "=");
                    if (c_start == -1) {
                        c_start = c_value.indexOf(c_name + "=");
                    }
                    if (c_start == -1) {
                        c_value = null;
                    } else {
                        c_start = c_value.indexOf("=", c_start) + 1;
                        var c_end = c_value.indexOf(";", c_start);
                        if (c_end == -1) {
                            c_end = c_value.length;
                        }
                        c_value = unescape(c_value.substring(c_start, c_end));
                    }
                    return c_value;
                }
                if (getCookie('il')) {
                    (function () {
                        var js = document.createElement('script');
                        js.type = 'text/javascript';
                        js.async = true;
                        js.src = "https://healthcare-temps.co.uk/users/show_header.js?u=";
                        var s = document.getElementById('js-head-menu');
                        s.parentNode.insertBefore(js, s);
                    })();
                } else {
                    document.getElementById('js-head-menu').className = '';
                }
                //]]>
            </script>
        </div>

        <div class="container">
            <div class="row">
                <div style="text-align: center; margin-bottom: 20px">
                    <h2 style="color: #0070c0">MODERN SLAVERY & HUMAN TRAFFICKING POLICY</h2>
                </div>
                <h3>Our MSHT Policy:</h3>
                <p>
                    Healthcare Temps Inc is against all forms of exploitation of human labour and the trafficking of human. Below, is an outline
                    of our Modern Slavery & Human Trafficking policy that all members of our company adheres to and promise to uphold.
                </p>
                <h3>Supply Chain Standards:</h3>
                <p>
                    In our standard procurement process, we issue purchase orders that incorporate our Terms of Service {ToS). The ToSs are
                    further supplemented by our web-guides, which expand on our expectations and hirers'; obligations to all temp workers.
                    For example, our Social Responsibility and Anti-Corruption policy prohibits all forms of child labour, forced labour
                    (including human trafficking), physical disciplinary abuse and any infraction of the law.<br /><br />

                    Internally, we comply with the European Human Rights, Basic Working Conditions and Corporate Responsibility and UK’s
                    Employment Law, such as, maximum working hours, minimum wage policy, anti-child labour, anti-forced labour, non-
                    discrimination, freedom of association, health and safety and the environment. This policy applies to our own operations,
                    and we encourage businesses throughout our supply chain to adopt and enforce similar policies in their own operations.
                </p>
                <h3>Slavery and Human Trafficking:</h3>
                <p>
                    Slavery and forced labour can take many forms, including human trafficking or child labour. These types of activities are
                    not tolerated within our organisation and we will decline to work with organisations that undertake such practices. Internal
                    audits are carried out quarterly to ensure such activities do not occur in-house. Further, we reserve the right to terminate
                    our relationship with a supplier if issues of noncompliance with our policies are discovered and/or noncompliance is not
                    addressed in a timely manner.
                </p>
                <ul style="font-size: 15px">
                    <li>We regularly assess risk related to human trafficking and forced labour associated with our supply base.</li>
                    <span>
                        Our preliminary assessment is based upon geography, the commodity purchased, supplier quality performance and the nature of the business transaction.
                    </span>
                    <li>We regularly conduct social responsibility audits.</li>
                    <span>
                        These audits evaluate supplier compliance with both local law and Healthcare Temps’ human rights expectations as communicated in other policies. These independent audits can be either announced or unannounced, and decisions about which premises to audit are based upon our risk assessment.
                    </span>
                </ul>
                <p>
                    The board of directors of the Company unanimous approval of this statement on the behalf to the CEO and Managing
                    Director of the Company has been rubber stamped, at its board meeting on May 03, 2018.
                </p>
            </div>
        </div>

    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

<a id="scrollup">Scroll</a>

<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Kafe JS -->
<script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
