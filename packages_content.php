<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    .maincontainer {
        margin-top: 80px;
    }
    .maincontainer > div {
        margin: 30px auto;
    }
    .mainheading {
        text-align: center;
    }
    .mainheading span {
        color: #00b300;
        font-size: 32px;
    }
    .iconcontainer > div {
        text-align: center;
        border: 1px solid lightgrey;
    }
    .iconcontainer h3 {
        color: #00b300;
    }
    .iconcontainer img {
        height: 100px;
    }
    .content-wrapper, .right-side {
        background-color: #FFFFFF !important;
    }
    .border{
        background-color: #888f99;
        height: 250px;

    }
    }
    .hrborder{
        border-bottom: 1px solid #d7d7d7;;
        text-align: center;
        line-height: 50px;
    }
    .pborder{
        color: #7f7f7f;
        border-bottom: 1px solid #d7d7d7;;
        text-align: center;
        line-height: 50px;

    }
</style>
<div class="container-fluid maincontainer">
    <div class="row" style="margin-bottom: 100px;">
        <h3 style="text-align: center;color: #00a2ed; margin-bottom:50px;">Our Home Care services consist of 3 packages.</h3>
        <div class="col-lg-3 col-xs-3 col-lg-offset-1">
            <div class="border">
                <div style="padding: 100px;">
                    <p style="border: 1px solid white; color: white; text-align: center;">BASIC</p>
                    <p style="text-align: center;color: white;">999</p>
                </div>
            </div>
            <h4 class="hrborder" style="text-align: center;">Expiration Date (month)</h4>
            <p class="pborder">65 Hours / Call-out Care</p>
            <p class="pborder">Five Hours / Virtual Care</p>
            <p class="pborder">Free E-prescription</p>
        </div>
        <div class="col-lg-3 col-xs-3">
            <div class="border">
                <div style="padding: 100px;">
                    <p class="bborder"style="border: 1px solid white; color: white; text-align: center;width: 100px;">ADVANCED</p>
                    <p style="text-align: center;color: white;">1499</p>
                </div>
            </div>
            <h4 class="hrborder" style="text-align: center;">Expiration Date (month)</h4>
            <p class="pborder">80 Hours / Call-out Care</p>
            <p class="pborder">10 Hours Virtual Care</p>
            <p class="pborder">Free E-prescription</p>
            <p class="pborder">Doctors & Nurses on call 9-5</p>
            <p class="pborder">Organic Breakfast x4 wks</p>
        </div>
        <div class="col-lg-3 col-xs-3">
            <div class="border">
                <div style="padding: 100px;">
                    <p class="bborder"style="border: 1px solid white; color: white; text-align: center;">GOLD</p>
                    <p style="text-align: center;color: white;">2,999</p>
                </div>
            </div>
            <h4 class="hrborder" style="text-align: center;">Expiration Date (3 month)</h4>
            <p class="pborder">80 Hours / Call-out Care</p>
            <p class="pborder">10 Hours Virtual Care</p>
            <p class="pborder">Doctors & Nurses on call 9-5</p>
            <p class="pborder">Social Care Assistant 24/7</p>
            <p class="pborder">Ambulance service</p>
        </div>
    </div>
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

</body>
</html>
