<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    .mycontainer .fa {
        font-size: 30px;
    }
    .mycontainer .single_info {
        border: 1px solid lightgrey;
        padding: 25px;
    }
    .mycontainer .btn {
        background-image: linear-gradient(to right, #358AA7, #358AA7);
        color: white;
        padding: 11px;
    }
    .mycontainer .row > div {
        height: 450px;
        width: 287px;
        margin-left: 1.8%;
        margin-top: 24px;
    }
    .mycontainer .single_info {
        border: 1px solid lightgrey;
        padding: 10px;
    }
    p {
        color: #979797;
    }
</style>
<div class="container-fluid mycontainer" style="text-align: center; margin-top: 80px;">
    <h2>We're here to help 24 hours a day, 7 days a week</h2>
    <div class="row" style="padding-left: 179px;">
        <div class="col-lg-4 single_info">
            <span class="fa fa-comments"></span>
            <h3 style="font-weight: bold">Open chat</h3>
            <p>Chat directly with a Support Rep<br />
                and get help with your questions.</p>
            <button class="btn">Chat with us now</button>
            <h3 style="font-weight: bold">Current wait time</h3>
            <p>4 minutes</p>
            <h3 style="font-weight: bold">Average Wait Time</h3>
            <p>5 minutes</p>
        </div>
        <div class="col-lg-4 single_info">
            <span class="fa fa-envelope"></span>
            <h3 style="font-weight: bold">Send an email</h3>
            <p>Reach us by email. We will be in<br />
                touch as soon as possible.</p>
            <button class="btn">Email us</button>
            <h3 style="font-weight: bold">Current response time</h3>
            <p>23 hours</p>
        </div>
        <div class="col-lg-4 single_info">
            <span class="fa fa-headphones"></span>
            <h3 style="font-weight: bold">Give us a call</h3>
            <p>Talk to us over the phone<br />
                for additional help.</p>
            <button class="btn">Give us a call</button>
            <h3 style="font-weight: bold">Current wait time</h3>
            <p>3 minutes</p>
            <h3 style="font-weight: bold">Average Wait Time</h3>
            <p>Less than a minute</p>
        </div>
    </div>
</div>
<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

</body>
</html>
