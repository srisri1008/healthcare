<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?php


//Check if init.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');
    exit;
}else{
 require_once 'core/frontinit.php';
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

    <!-- Include header.php. Contains header content. -->
    <?php include ('includes/template/header.php'); ?>

<body class="greybg">
<style>
    .white-blogpage, .col-lg-8.white {
        margin-top: 9px !important;
        border-radius: 4px;
        background-color: #fff;
    }

    .btn.post_job:hover{
        background-position:unset;
    }


    @media only screen and (max-width: 1000px) {
        button.search-job-temp{
            width:16%;
        }
    }
    @media only screen and (min-width: 1000px) {
        button.search-job-temp {
            width: 10% !important;
        }
    }
</style>
     <!-- Include navigation.php. Contains navigation content. -->
 	 <?php include ('includes/template/navigation.php'); ?>

     <!-- ==============================================
	 Header
	 =============================================== -->
     <header class="header-jobs">
      <div class="container">
	   <div class="content">
	    <div class="row">

		 <?php
		 //Start new Admin object
		 $admin = new Admin();
		 //Start new Client object
		 $client = new Client();
		 //Start new Freelancer object
		 $freelancer = new Freelancer();

		 if ($admin->isLoggedIn()) { ?>
		<?php } elseif($freelancer->isLoggedIn()) { ?>
		<?php } elseif($client->isLoggedIn()) { ?>
		 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		  <a href="hirer/addjob.php" style="display: none;" class="kafe-btn kafe-btn-mint full-width revealOnScroll" data-animation="bounceIn" data-timeout="400">
		  	<i class="fa fa-tags"></i> <?php echo $lang['post']; ?> <?php echo $lang['a']; ?> <?php echo $lang['job']; ?>, <?php echo $lang['it\'s']; ?> <?php echo $lang['free']; ?> !
		  </a>
		 </div><!-- /.col-lg-3 -->
		<?php } else { ?>
		 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		  <!--<a href="login.php" class="kafe-btn kafe-btn-mint full-width revealOnScroll" data-animation="bounceIn" data-timeout="400">
		  	<i class="fa fa-tags"></i> <?php echo $lang['post']; ?> <?php echo $lang['a']; ?> <?php echo $lang['job']; ?>, <?php echo $lang['it\'s']; ?> <?php echo $lang['free']; ?> !
		  </a>-->
		 </div><!-- /.col-lg-3 -->
		 <?php } ?>
        </div><!-- /.row -->
       </div><!-- /.content -->
	  </div><!-- /.container -->
     </header><!-- /header -->

     <!-- ==============================================
	 Jobs Section
	 =============================================== -->
     <section class="jobslist" style="padding-top: 49px;">
	  <div class="container">
	   <div class="row">
	   
	    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 white padding0">
		    <h3 style="text-align:center;font-size:24px;font-weight:bold;color: #3C9E72">Book HealthCare Professionals for temporary assignments. <br> No agency fees or subscription! </h3>

            <!--<h3 style="text-align:center;font-weight:bold;color: #3C9E72">
                No agency fees or subscription!
            </h3>-->

             <?php

               // print_r($_GET);


                $page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
                $limit = $service_limit;
                $startpoint = ($page * $limit) - $limit;

                $conditions = '';

                if((isset($_GET['type_id']) && !empty($_GET['type_id'])) && (isset($_GET['location']) && !empty($_GET['location'])))  {

                	$conditions .= ' AND p.type_id = "'.$_GET['type_id'].'" 
AND (LOWER(p.location) = "'.strtolower($_GET['location']).'" OR LOWER(p.city) = "'.strtolower($_GET['location']).'" OR LOWER(p.country) = "'.strtolower($_GET['location']).'")  ';
                }else{
                	$conditions .= '';
                }

                /*elseif(empty($_GET['location']) && !empty($_GET['type_id'])){

                	$conditions .= ' p.type_id = "'.$_GET['type_id'].'" ';
                }elseif(empty($_GET['type_id']) && !empty($_GET['location'])){

                	$conditions .= ' p.location = "'.$_GET['location'].'" OR p.city = "'.$_GET['location'].'" OR p.country = "'.$_GET['location'].'"  ';
                }*/

 				$urgentHirerQuery = DB::getInstance()->query("SELECT  f.*, p.*  FROM `freelancer` as f
Join profile as p on f.`freelancerid` = p.`userid` where f.`active` = '1' AND f.`delete_remove` = '0' ".$conditions."");

 				$data  = $urgentHirerQuery->results();
 				//echo '<pre>';print_r($urgentHirerQuery);echo '</pre>';	

 				

              

                //$q1 = DB::getInstance()->get("service", "*");
                //echo $total = count($urgentHirerQuery);

               // $query = DB::getInstance()->get("service", "*", ["ORDER" => "date_added DESC", "LIMIT" => [$startpoint, $limit], "AND" => ["active" => 1, "delete_remove" => 0]]);
            if(count($data) != 0) {

                	

                    $serviceList = '';
                    $x = 1;

                    foreach($urgentHirerQuery->results() as $row) {
					//echo '<pre>';print_r($row);echo '</pre>';

                        $serviceList = '';

                       

                     $TempTypeQuery =  DB::getInstance()->get("temp_type", "*", ["id" => $row->type_id]);

                     if ($TempTypeQuery->count()) {

                        $tempTypedetails = $TempTypeQuery->results();
                            foreach ($TempTypeQuery->results() as $r2) {
                                $TempTypeName = $r2->title;
                                
                            }
                        }



                     $ratequery =  DB::getInstance()->get("temp_rate", "*", ["id" => $row->rate_id]);

                     if ($ratequery->count()) {

                        $ratedetails = $ratequery->results();
                            foreach ($ratequery->results() as $r3) {
                                 $rangemin = $r3->rangemin;
                                $rangemax = $r3->rangemax;
                                $title = $r3->title;
                                $temp_rateId = $r3->id;
                                
                            }
                        }




                       /* $q1 = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $row->userid]);
                        if ($q1->count()) {
                            foreach ($q1->results() as $r1) {
                                $name1 = $r1->name;
                                $username1 = $r1->username;
                                $imagelocation = $r1->imagelocation;
                            }
                        }

                        $q2 = DB::getInstance()->get("profile", "*", ["userid" => $row->userid]);
                        if ($q2->count()) {
                            foreach($q2->results() as $r2) {
                                $country = $r2->country;
                                $skills = $r2->skills;
                                $arr=explode(',',$skills);
                            }
                        }*/

                        $blurb = truncateHtml($row->about, 400);

                        $skills_each =  '<label class="label label-success">'. $row->skills .'</label> &nbsp;';


                       /* $qj = DB::getInstance()->get("job", "*", ["AND" =>["freelancerid" => $row->userid, "invite" => "0", "delete_remove" => 0, "accepted" => 1,"chk_urgent" => 1]]);
                        if ($qj->count()) {
                            foreach($qj->results() as $rj) {


                                $q1 = DB::getInstance()->get("milestone", "*", ["AND" =>["jobid" => $rj->jobid]]);
                                if ($q1->count()) {
                                    foreach($q1->results() as $r1) {

                                        $query = DB::getInstance()->sum("transactions", "payment", ["AND" => ["membershipid" => $r1->id, "freelancerid" => $r1->clientid]]);
                                        foreach($query->results()[0] as $payy) {
                                            $paj[] = $payy;
                                        }

                                    }
                                }

                            }
                        }*/


                        $qr = DB::getInstance()->get("ratings", "*", ["AND" => ["freelancerid" => $row->userid]]);
                        $countt = $qr->count();
                        $rep = $countt/7;

                        $invite = '';
                        $live_chat ='';
                        $interview_room ='';

                        //Start new Admin object
                        $admin = new Admin();
                        //Start new Client object
                        $client = new Client();
                        //Start new Freelancer object
                        $freelancer = new Freelancer();

                        if ($admin->isLoggedIn()) {
                        } elseif($freelancer->isLoggedIn()) {

                        } elseif($client->isLoggedIn()) {

                            
                        $invite .='hirer/invite.php';


			 			$live_chat .= 'hirer/live_chat.php';

			 			$interview_room .= 'hirer/interviewroom.php';

                        } else {
                            $invite .='login.php';
                            $live_chat .='login.php';
                            $interview_room .='login.php';
                            
                        }

error_reporting(E_ALL);
ini_set('display_errors', 1);

                       
                        echo $serviceList .= '

		    
				 <div class="job job-section-hierer">	
				  
				  <div class="top-sec">
				   <div class="col-lg-12 padding0">
					<div class="col-lg-2 col-xs-12 padding0">
					<div style="padding:20px;width:100px;border:1px solid black" class"temp-img">
					 <a href="#">
					  <img style="width:70px; height: 70px;" class="img-responsive" src="tempworker/'. escape($row->imagelocation) .'"  alt="">
					  
					 </a>
					 </div>
					</div><!-- /.col-lg-2 -->
					<div  class="col-lg-8 col-xs-12"> 
					 <h4><a style="color:#66BD00"href="#">'.escape($TempTypeName) .'</a></h4>
					 <h5><a href="#">'. escape($row->country) .'</a></h5>
					</div><!-- /.col-lg-10 -->
					<div class="col-lg-2">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" style="background-color: #02b9f3" type="button" data-toggle="dropdown">Action
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Hire</a></li>
                                 <li><a href="hirer/addinvite.php">Invite to Bid</a></li>
                                 <li><a href="hirer/interviewroom.php">Invite to Interview</a></li> 
                                <li><a href="hirer/compose.php">Email Temp</a></li>
                                
                                <!--<li><a href="hirer/live_chat.php">Live Chat</a></li>
                                <li><a href="hirer/interviewroom.php">Interview</a></li> -->
                               
                            </ul>
                        </div>
                    </div> 
				   </div><!-- /.col-lg-12 -->
				  </div><!-- /.row -->
				  
				  <div class="mid-sec">			 
				   <div class="col-lg-12 padding0">			 
				   <div class="col-lg-12 padding0">
					
                     <p class="paddingtop10">'. $blurb .'</p>
                    
				   </div><!-- /.col-lg-12 -->
				   </div><!-- /.col-lg-12 -->
				  </div><!-- /.row -->
				  
				  <div class="bottom-sec">
				   <div class="col-lg-12 padding0">
					
					<div class="col-lg-12 padding0">
					 
					</div> 
					
					<div class="col-lg-2 padding0">
					 <h5>' . $lang['earned'] . '</h5>
					 <p></p>
					</div>
					<div class="col-lg-2">
                     <h5> ' . $lang['location'] . ' </h5>
                     <p><i class="fa fa-map-marker"></i> '. escape($row->city) .'</p>
                    </div>
					<div class="col-lg-2">
					 <h5> ' . $lang['rate/hr'] . ' </h5>
					 <p>'.$currency_symbol.' '. $rangemin .($rangemax ? ' - '. $rangemax : '+') .'</p>
					</div>
					<div class="col-lg-2">
					 <h5> ' . $lang['ratings'] . ' ('. $rep .')</h5>
					 <p><i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i></p>
					</div>

                   
					
					
				   </div><!-- /.col-lg-12 -->
				  </div><!-- /.row -->
				 
				 </div><!-- /.job -->


				 
		 			
					 ';

                        unset($serviceList);
                        unset($skills_each);
                        //unset($sen);
                        $x++;



            }
                }else 

                {
                    echo $serviceList = '<p style="text-align:center;">'.$lang['no_hirer'].'</p>';

                    

                }

                

                //print
                //echo Pagination($total,$limit,$page);
                ?>

                <div style="margin-top:10%;margin-left: 20%;" class="col-lg-8">
                            <span  style="margin-left: 40%;">
                                OR
                            </span>
                            <br>
                            <button class="btn post_job"  style="width: 90%;font-size: 17px;background-image: linear-gradient(to right, mediumseagreen, mediumseagreen); padding: 13px; color: white" >
                                Post a job, its Free!</button>
                        </div>


         
	    </div><!-- /.col-lg-8 -->
	   </div><!-- /.row -->
       <div>
            
            
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
          (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-2199989077469674",
            enable_page_level_ads: true
          });
        </script>


       </div>
	  </div><!-- /.container-fluid -->
     </section><!-- /section -->

      <!-- Include footer.php. Contains footer content. -->
	  <?php include 'includes/template/footer1.php'; ?>

     



     <!-- ==============================================
	 Scripts
	 =============================================== -->

	<script>
	 $(document).on("click",".post_job",function(){
            //redirect('hirer/addjob.php');

            <?php 
            	 if($client->isLoggedIn()) {
            ?>
            window.location.href = "hirer/addjob.php";

            <?php }else{ ?>
            	window.location.href = "login.php";
            <?php } ?>

        });
        </script>

     <!-- jQuery 2.1.4 -->
     <script src="assets/js/jQuery-2.1.4.min.js"></script>
     <!-- Bootstrap 3.3.6 JS -->
     <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
     <!-- Waypoints JS -->
     <script src="assets/js/waypoints.min.js" type="text/javascript"></script>
     <!-- Kafe JS -->
     <script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
