<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    #pages-bonus {
        margin-top: 104px;
    }
    .grid-div {
        display: grid;
        grid-template-columns: repeat(3, 300px);
        grid-gap: 25px;
        grid-auto-rows: minmax(100px, auto);
        grid-auto-columns: minmax(10px, auto);
        align-content: center;
        justify-content: center;
        margin-top: 70px;
        margin-bottom: 40px;
    }
    .grid-div > div {
        border: 1px solid lightgrey;
        padding: 11px;
        box-shadow: 0px 0px 7px;
    }
    .single-div > h4 {
        cursor: pointer;
        color: #1aa933;
    }
</style>
<div id="pages-bonus" class="content user">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
            </ul>
        </div>

        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container" style="text-align: center">
                    <div class="header-image">
                        <img src="myassets/img/virtual_care_progress.png" alt="Header image">
                    </div>
                    <div class="grid-div">
                        <div class="single-div">
                            <img src="myassets/img/counseling.png" alt="Counseling/therapists">
                            <h4>Counseling/Therapists</h4>
                        </div>
                        <div class="single-div">
                            <img src="myassets/img/cardiology.png" alt="Cardiology">
                            <h4>Cardiology</h4>
                        </div>
                        <div class="single-div">
                            <img src="myassets/img/dermatology.png" alt="Dermatology">
                            <h4>Dermatology</h4>
                        </div>
                        <div class="single-div">
                            <img src="myassets/img/general_medicine.png" alt="General Medicine">
                            <h4>General Medicine</h4>
                        </div>
                        <div class="single-div">
                            <img src="myassets/img/gynaecology.png" alt="Gynaecology">
                            <h4>Gynaecology</h4>
                        </div>
                        <div class="single-div">
                            <img src="myassets/img/mid-wives.png" alt="Mid-Wives">
                            <h4>Mid-Wives</h4>
                        </div>
                        <div class="single-div">
                            <img src="myassets/img/psychiatry.png" alt="Psychiatry">
                            <h4>Psychiatry</h4>
                        </div>
                        <div class="single-div">
                            <img src="myassets/img/radiology.png" alt="Radiology">
                            <h4>Radiology</h4>
                        </div>
                        <div class="single-div">
                            <img src="myassets/img/tele-stroke.png" alt="Tele-Stroke">
                            <h4>Tele-Stroke</h4>
                        </div>
                    </div>
                    <img src="myassets/img/coming_soon.png" alt="Coming soon on iOS and Android store" style="margin-bottom: 20px;">
                </div>
            </div>
        </div>
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

</body>
</html>
