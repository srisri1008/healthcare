<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    .maincontainer {
        margin-top: 80px;
    }
    .maincontainer ul {
        list-style: none;
        margin-left: 0;
        padding-left: 0;
        font-size: 18px;
    }

    .maincontainer li {
        list-style: none;
        margin-left: 0;
        padding-left: 0;
    }

    .maincontainer li {
        padding-left: 1em;
        text-indent: -1em;

    }

    .maincontainer li:before {
        content: "+";
        padding-right: 5px;
    }
    .maincontainer .iconcolor{
        color: #438023;
    }
    .maincontainer .btn-border{
        text-align: center;
        border-radius: 15px;
    }
    .maincontainer .h4pDes{
        color: #b5a4d8;
    }
    .maincontainer .icon{
        width: 200px;
    }
    .maincontainer .border{
        border: 1px solid #c7c7c7;
        text-align: center;
    }
    .maincontainer .pcolor{
        color: #1fb14c;
    }
</style>
<div class="container maincontainer">
    <div class="row">
        <div class="col-lg-12">
            <h1 style=" background-color:#1fb14c;line-height: 45px; color: white;padding-left: 22px; ">Out-Pateint Care</h1>
        </div>
        <div class="row">
            <p style="text-align: center; font-size: 44px;color: #1fb14c;margin-left: 28px;line-height: 45px;">
                Need a Healthcare professional to provide Private Care<br /> at home, office or on holidays?
            </p>
        </div>
        <p style="line-height: 45px;margin-left: 132px; font-size: 44px;">
            Just <span style="color: #1fb14c;font-size: 27px;text-align: center;">Log-in</span>, select, interview & hire!
        </p>

        <div class="col-lg-6" >
            <img src="myassets/images/screen.png"style="width: 540px;margin-top: 50px;">
        </div>
        <div class="col-lg-6" style="float:left;margin-top: 50px;">
            <h4 style="color:#1fb14c;font-size: 27px;">
                <b> Empowering out-patients via delivery</b>
            </h4>
            <p style="font-size: 21px">
                Bespoke healthcare delivered to you, regardiess<br/> of location
            </p>
            <ul>
                <li>
                    Medicial & Healthcare professionals come out to your place
                </li>
                <li>
                    Schedule booking 24/7 via web, mobile, tablet or desktop
                </li>
                <li>
                    Hourly, daily, weekly or monthly shift booking
                </li>
                <li>
                    Absolute transparency, security and no hidden fees
                </li>
                <li>
                    Secure payment processed online only.
                </li>
                <li>
                    Choose who you want, when and where.
                </li>
                <li>
                    Receive care at home, at school, at work or on hospital
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div id="map" style="width:400px;height:400px;background:yellow"></div>
    </div>
    <div class="row">
        <h3 style="background-color: black;line-height: 42px;color: white;padding-left: 32px;">Type of Call-Out Care</h3>
    </div>
    <div class="row" style="margin-top: -10px;">
        <div class="col-lg-6 border">
            <img class="icon" src="myassets/images/outPatient.png">
            <p class="pcolor">Out-Patient Care</p>
        </div>
        <div class="col-lg-6 border">
            <img class="icon" src="myassets/images/stoke.png">
            <p class="pcolor">Stroke Recovery Care</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 border">
            <img class="icon" src="myassets/images/abk.png">
            <p class="pcolor">ABA Child Support</p>
        </div>
        <div class="col-lg-6 border">
            <img class="icon" src="myassets/images/diabetese.png">
            <p class="pcolor">Diabetese/Insulin Care</p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 border">
            <img class="icon" src="myassets/images/ser.png">
            <p class="pcolor">Serious Medication</p>
        </div>
        <div class="col-lg-6 border">
            <img class="icon" src="myassets/images/diabetese.png">
            <p class="pcolor">Diabetese/Insulin Care</p>
        </div>
    </div>
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

</body>
</html>
