<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->

<div class="container" style="margin-top: 80px;">
    <div class="row">
        <div style="text-align: center">
            <h1>GDPR Policy</h1>
        </div>
        <h3>The Policy</h3>
        <p>
            Healthcare-Temps UK is committed to best practice, and all activities are carried out in line with relevant UK and EU
            legislation. This includes, but is not limited to, the Data Protection Act 1998 (DPA), the EU Data Protection Directive
            95/46/EC, and the forthcoming EU General Data Protection Regulation (“GDPR”) 2018.
            Data Protection Principals:
            1. Personal data shall be processed fairly and lawfully.
            2. Personal data shall be obtained for one or more specified and lawful purposes, and shall not be further
            processed in any manner incompatible with that purpose or those purposes.
            3. Personal data shall be adequate, relevant and not excessive in relation to the purpose or purposes
            for which they are processed.
            4. Personal data shall be accurate and, where necessary, kept up to date.
            5. Personal data processed for any purpose or purposes shall not be kept for longer than is necessary
            for that purpose or those purposes.
            6. Personal data shall be processed in accordance with the rights of data subjects under the Data Protection
            Act 1998, the EU Data Protection Directive 95/46/EC, and plans are in place to comply with the
            forthcoming EU General Data Protection Regulation (“GDPR”).
            7. Appropriate technical and organisational measures shall be taken against unauthorised and unlawful
            processing of personal data and against accidental loss or destruction of, or damage to, personal data.
            8. Personal data shall not be transferred to a country or territory outside the European Economic Area
            unless that country or territory ensures an adequate level of protection for the rights and freedoms of
            data subjects in relation to the processing of personal data.
        </p>

        <h2>Our Commitment</h2>
        <h3>Accountability</h3>
        <p>
            Although all staff have a responsibility for adhering to our Data Protection Policy, the Senior Management have
            day-to-day responsibility for developing, implementing and monitoring the data protection policy. This ensures
            the policy is effectively managed and co-ordinated.
        </p>

        <h3>Education and Awareness</h3>
        <p>
            All staff are briefed on their data protection responsibilities upon appointment, with training updates at regular
            intervals or when required. Specialist training for staff with specific duties, such as marketing, information
            security and database management, is provided.
        </p>

        <h3>Privacy Notices</h3>
        <p>
            To ensure the processing of data is fairly done by l Healthcare Temps, we will be transparent about how we intend to
            use your data. As good practice, the company includes privacy notices on its website and on any forms used to collect
            data. These clearly explain the reasons for using the data. Privacy Policy_H1.
        </p>

        <h3>Personal Data</h3>
        <p>
            Personal data is not processed in any manner that is &#39;incompatible&#39; with its specified purpose.
        </p>

        <h3>Responding to access requests</h3>
        <p>
            Personal data is processed in accordance with individual rights under the DPA, the EU Data Protection
            Directive 95/46/EC, and the forthcoming GDPR. Individual requests are recognised and responded to
            by the relevant Head of Department within statutory timescales. This includes the right of access.
            Responding to Access Requests Policy v1_P36_Mar17
        </p>

        <h3>Data quality &amp; accuracy</h3>
        <p>
            Healthcare Temps UK ensures that the personal data it holds is of sufficient quality to make decisions about
            individuals. Data is not collected without a legitimate business reason and collects only the minimum required to
            meet the purposes for which it is needed and which are specified in the privacy notice.
            All personal data held is accurate and, where necessary, kept up-to-date. Regular reviews of information are
            carried out to identify and correct inaccurate records, remove irrelevant ones and update out-of-date ones.
        </p>

        <h3>Retention and disposal</h3>
        <p>
            Healthcare-Temps UK ensures that personal data is not kept for longer than is necessary. Checks are carried out to
            identify which records or data sets are held, and when they should be deleted or anonymised. Heads of
            Department are accountable for recording retention and disposal dates for information they hold. Data is
            disposed of securely. Further information can be found in the Data Retention Policy_H1
        </p>

        <h3>Security policy</h3>
        <p>
            Healthcare-Temps UK has an established Information Security Management System Policy which sets the
            standards to be adhered to. In the unlikely event data and/or security is compromised, a Security Breach
            Procedure has been implemented and all staff are trained and aware of their responsibilities
        </p>

        <h3>Outsourcing</h3>
        <p>
            Healthcare-Temps UK ensures an adequate level of protection for any personal data processed by others on its
            behalf or transferred outside the European Economic Area.
            When determining whether to use an external provider, Healthcare-Temps UK requires proof of their adherence to
            Data Protection Legislation both in the UK and EU. New Supplier/Customer/Contractor Forms must be completed by
            all third parties, which request proof of their credentials and compliance requirements before Healthcare-Temps
            UK will consider engaging their services.
        </p>

        <h3>Privacy impact assessments</h3>
        <p>
            As required under The EU General Data Protection Regulation (GDPR), Healthcare-Temps UK ensures that any new
            projects or initiatives are privacy-proofed at the planning stage. Privacy considerations are an early part of all
            projects plans or initiatives that involve the processing of personal data. Privacy impact assessments (PIA) are
            conducted during the development, testing and delivery stages of any project to evaluate the origin, nature,
            particularity and severity of the risk to the rights and freedoms of natural persons before processing personally
            identifiable information. The PIA includes the measures, safeguards and mechanisms envisaged for mitigating” the
            identified risks.<br />
            All policies mentioned herein are available on request by emailing admin@healthcare-temps.co.uk
        </p>
    </div>
</div>


<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>
<!----footer--------->

</body>
</html>
