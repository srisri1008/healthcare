<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    #pages-bonus {
        margin-top: 104px;
    }
    img {
        width: 100%;
    }
    .computer_para {
        font-size: 15px;
        line-height: 2em;
        text-align: left;
        margin-left: 39px;
    }
    .identity_para {
        font-size: 15px;
        text-align: left;
        margin-left: 56px;
    }
    .green_color {
        color: #10c446;
    }
    .systems p {
        font-weight: bold;
    }
    .top_div p {
        font-size: 15px;
    }
    .onboard {
        width: 94%;
        text-align: left;
        background-color: rgb(240,240,240);
        margin-left: 50px;
        margin-top: 23px;
        margin-bottom: 23px;
        padding: 31px;
    }
    .onboard h3 {
        text-align: center;
        margin-top: -1px;
        margin-bottom: 19px;
    }
    .onboard i {
        color: green;
        font-size: 20px;
    }
    .onboard span {
        font-size: 15px;
    }
    .settings {
        width: 67%;
        margin-left: 216px;
        text-align: left;
    }
    .settings span{
        font-size: 15px;
    }
</style>
<div id="pages-bonus" class="content user">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
            </ul>
        </div>

        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container" style="text-align: center">
                    <img src="myassets/img/leaflet_header.png" alt="Header Image">
                    <div class="top_div">
                        <h3 class="green_color">Our Origin</h3>
                        <p>
                            Healthcare-temps originated from humble beginnings 2016 when two healthcare professionals came
                            together to find solutions to problems in the primary and social healthcare sectors. The first was to
                            provide a solution to help h#medical and healthcare employers recruit talented healthcare
                            professionals at low cost and simultaneous eradicate staff shortage problems. That’s how we came up
                            with the autonomous recruitment platform. The second idea was to provide a solution where
                            healthcare professionals could win back their freedom and reduce stress by self-determining when to
                            work, whom to work for and at what rate. Hence, the platform evolved to another level, enabling
                            healthcare staff to communicate and negotiate direct with employers. Today we continue to achieve
                            our twin mission, have provided jobs for 1,000s of staff and cut Hirers / Employers recruitment cost
                            by upto 40%.
                        </p>
                        <h3 class="green_color">Our Evolution</h3>
                        <p>
                            Care is critically challenged and constantly evolving. Healthcare-Temps has continued to evolve as well, growing from
                            strength to strength with the addition of cutting edge technology that genuinely solves our users’ problems and fulfill
                            their needs. Automated Labour-on-Demand or Healthcare-staff-on-demand is the next generation of staff
                            procurement, eradicating agencies, fees and giving back power to workers & hirers.<br />

                            As this new segment of the labour markets evolves, employers will discover ways to drastically cut labour cost,
                            eliminate staff shortage, utilise the best talent nationally and enhance the delivery of quality care to service users.
                            Likewise, out-patients in need of urgent private healthcare can opt for 1 -to-1 virtual or face-face care.<br />

                            Healthcare-Temps is pioneering the Hea lthcare Temps-on-Demand revolution. Placing the procurement and
                            management of temporary /adhoc healthcare workers in the palms of practice managers’ and owners’ hands. As a
                            market leader in this industry, what makes us unique, is our ability to enable hirers to find and hire every type medical
                            and healthcare talent of all grades and specialisms. Further, we employ cognitive computing technology to make the
                            sourcing of medical & healthcare professionals simpler and smarter.<br />

                            Moreover, we have automated activities such as; staff interview, staff vetting, referencing I rating, e-rota, invoicing and
                            e-payment. Now you can hire without any hassle or worry 24/7/365. We are also proud of the fact, we are drastically
                            cut waiting times to be seen by a medical professional from 21 days to 1 day.
                        </p>
                    </div>

                    <div class="systems">
                        <div class="row">
                            <div class="col-xs-12 xol-sm-12 col-md-6 col-lg-6">
                                <h3 class="green_color">Healthcare Temps-on-Demand 24/7</h3>
                                <p class="computer_para">
                                    * Tap into a world of idle healthcare talent<br />
                                    * Hire Temp workers within minutes<br />
                                    * Zero agency fees for life & it’s 1 00% Free.<br />
                                    * Handpick the staff you want and when.<br />
                                    * Automated advance booking I Rota master<br />
                                    * Paperless invoice & Timesheet<br />
                                    * Created by care managers for employers
                                </p>
                            </div>
                            <div class="col-xs-12 xol-sm-12 col-md-6 col-lg-6">
                                <img src="myassets/img/computer_doctor.png" alt="" class="computer_doctor">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 xol-sm-12 col-md-6 col-lg-6">
                                <img src="myassets/img/identity_documents.png" alt="" class="identity_document">
                            </div>
                            <div class="col-xs-12 xol-sm-12 col-md-6 col-lg-6">
                                <h3 class="green_color">Automated vetting and compliance</h3>
                                <p class="identity_para">
                                    * All temp staff are fully vetted & background checks
                                    done.<br />
                                    *  & mandatory documents verified before working.<br />
                                    * No payment is released unless employer is satisfied<br />
                                    * Revalidation routinely checked or temp is locked out
                                    of site<br />
                                    * Staff profiles & photos are regularly checked.<br />
                                    * Video interview available before you hire a temp<br />
                                    * Hirers rate & write reviews about temp workers.<br />
                                    * 100% secure and safe payment solution.<br />
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="row onboard">
                        <h3 class="green_color">Type of Healthcare Temps onboard</h3>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Doctors</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Dentists</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Consultants</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Nurses</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Care Assistants</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Cooks/Chefs</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Pharmacists</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Surgeons</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Paediatric</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Physiologists</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>ABA Therapists</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Home Managers</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Occupational therapists</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Physio therapists</span>
                        </div>
                        <div class="col-lg-4">
                            <i class="fa fa-plus-square"></i>
                            <span>Many other temp workers</span>
                        </div>
                    </div>

                    <div class="row settings">
                        <h3 class="green_color" style="text-align: center">Medical & Healthcare Settings on Board</h3>
                        <p style="font-weight: bold; text-align: center">healthcare-temps caters for almost any type of employer I service user who needs medical & healthcare temps.<br /></p>

                        <div class="col-lg-4">
                            <span>+ Hospitals</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Care Homes</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Day Centres</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Dentistry</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Clinics</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Nursing Homes</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Home care</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Pharmacist</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Charities</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Outpatient centre</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Sports Clubs</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Surgeries</span>
                        </div>
                        <div class="col-lg-4">
                            <span>+ Dyalasis Centre</span>
                        </div>
                    </div>

                    <div class="row">
                        <h3 class="green_color">Solutions for all</h3>
                        <p style="line-height: 2em;">
                            + Access every type of healthcare profession, wherever, whenever.<br />
                            + Neutral temp-staff procurement solution that saves you real money.<br />
                            + One-stop-shop for hiring healthcare professionals for temporary assignments.<br />
                            + Automating the hiring process for employers & providing convenience.<br />
                            + Video consultations for people requiring virtual care in remote locations.<br />
                            + Providing call-care for out-patients demanding a quick & personal touch.<br />
                            + Temps are in control of their shift patterns, work place settings and income.<br />
                            + Transparency, honesty and dispute resolution efficiency.
                        </p>
                    </div>

                    <h4 style="color: lightgrey">Interested in joining today? contact us for a free demo.</h4>

                </div>
            </div>
        </div>
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

</body>
</html>
