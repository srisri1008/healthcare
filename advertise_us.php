<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<div class="container-fluid" style="text-align:center; margin-top: 80px;">
    <style type="text/css">
        .iconinplaceholder textarea, input{
            padding:10px;
            font-family: FontAwesome, "Open Sans", Verdana, sans-serif;
            font-style: normal;
            font-weight: normal;
            text-decoration: inherit;

        }
        .gcolor{
            color: #83BC7B;
        }
        .dgcolor{
            color: #598F4D;
        }
        .content-wrapper, .right-side {
            background-color: #FFFFFF !important;
        }
        .row {
            margin-top: 10px;
        }
    </style>
    <div class="row">
        <h2 class="dgcolor">Advertise with Healthcare-Temps Today!</h2>
        <h2 class="dgcolor">Why us?</h2>
        <p>Temps Direct uk is pioneering online temp-on-demand revolution Gone are the days<br />whene Employers and medical & Healthcare temps used agencies to find staff or find temp<br />jobs Instead. employers can hire idle healthcare talent-on-demand 24/7.</p>
        <p>As acquiescence, we have the biggest reach. brodest audience the best e-targeting<br />technology to connect brands and businesses with the right company or individual.</p>
        <p>We will you reach a variety of medical and healthcare individuals and companies.<br />putting your company brand right infront of their faces</p>
        <h4 class="gcolor">Our Audience.</h4>
    </div>
    <div class="row">
        <div class="col-lg-6" >
            <h4>Companies</h4>
            <img src="hirer/uploads/images/companies.png" alt="companies">
        </div>
        <div class="col-lg-6">
            <h4>Individials</h4>
            <img src="hirer/uploads/images/individials.png" alt="individials">
        </div>
    </div>
    <div class="row">
        <h4 class="gcolor">Our Audience Characteristics</h4>
    </div>
    <div class="row" style="margin-top:40px; margin-bottom:40px;">
        <div class="col-lg-4 col-lg-offset-2" style="text-align:left" >
            <p>+ Average staff Nos. 100-420</p>
            <p>+ 90+ pateint occupancy rate</p>
            <p>+ Outsource training / CPD</p>
            <p>+ Credit worthy</p>
        </div>
        <div class="col-lg-5 col-lg-offset-1" style="text-align:left">
            <p>. Earns between <?=$currency_symbol?>30k - <?=$currency_symbol?>100k p.a.</p>
            <p>.Located & relocates nationwide</p>
            <p>.takes an average of two holidays a year</p>
            <p>. Age 35-44, degree holders & renters</p>
        </div>
    </div>
    <div class="row">
        <h4 class="gcolor">Our Monthly Traffic:</h4>
    </div>
    <div class="row col-lg-offset-1">
        <div class="col-lg-2">
            <h2>10K</h2>
            <p>Unique Visitors</p>
        </div>
        <div class="col-lg-2">
            <h2>100K</h2>
            <p>Unique Visitors</p>
        </div>
        <div class="col-lg-2">
            <h2>5K</h2>
            <p>Job Searches</p>
        </div>
        <div class="col-lg-2">
            <h2>1K</h2>
            <p>Job Views</p>
        </div>
        <div class="col-lg-2">
            <h2>100</h2>
            <p>CV's Added</p>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px; margin-top: 10px;">
        <h3 style="text-align: center;"  class="gcolor">Our Platforms</h3>
    </div>
    <div class="row col-lg-offset-1">
        <div class="col-lg-2 imgwh">
            <img src="hirer/uploads/images/onweb.png">
            <p>One Web</p>
        </div>
        <div class="col-lg-2">
            <img  src="hirer/uploads/images/onmobil.png">
            <p>On Mobile</p>
        </div>
        <div class="col-lg-2 imgwh">
            <img src="hirer/uploads/images/ontv.png">
            <p>On TV</p>
        </div>
        <div class="col-lg-2 imgwh">
            <img src="hirer/uploads/images/onsocialmedia.png">
            <p>On Social Media</p>
        </div>
        <div class="col-lg-2 imgwh">
            <img src="hirer/uploads/images/onyoutube.png">
            <p>On Youtube</p>
        </div>
    </div>
    <div class="row" >
        <h3 class="gcolor" style="text-align: center;" >Your Benifits</h3>
    </div>
    <div class="row" style=" margin-top:40px; margin-bottom: 40px;">
        <div class="col-lg-offset-2 col-lg-3" style="text-align:left">
            <p>. 30% ROI</p>
            <p>. Market Leadership</p>
        </div>
        <div class="col-lg-offset-2 col-lg-3" style="text-align:left">
            <p>. Brand recognition</p>
            <p>. Social Media followers</p>
        </div>
    </div>
</div>

<!-- Include footer.php. Contains footer content. -->
<?php //include ('includes/template/footer.php'); ?>

</body>
</html>
