<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php';	
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('includes/template/header.php'); ?>
<body class="greybg">
<script src="assets/js/jquery.bxslider.min.js"></script>
     <!-- Include navigation.php. Contains navigation content. -->
 	 <?php include ('includes/template/navigation.php'); ?> 
 	 
      <!-- ==============================================
	 Header
	 =============================================== -->
    <style>
        #main ul {
            list-style: none;
            margin-left: 0;
            padding-left: 0;
        }

        #main li {
            padding-left: 1em;
            text-indent: -1em;
        }

        #main li:before {
            content: "*";
            padding-right: 5px;
        }
        #pages-bonus {
            margin-top: 100px;
        }
    </style>
     <div id="pages-bonus" class="content user">
         <div class="wrapper">
             <div class="social_icn">
                 <ul class="unstyled clearfix pull-right media-icons">
                     <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                     <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                     <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                     <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                     <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                     <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
                 </ul>
             </div>
             <div id="js-head-menu" class="">
                 <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                     <div class="modal-dialog">

                         <!-- Modal content-->
                         <div class="modal-content">
                             <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">×</button>
                                 <a class="show hor-space" title="Temps-direct.uk" href="/">
                                     <img alt="" src="myassets/img/logo.png">
                                 </a>
                             </div>
                             <div class="cantent">
                                 <p>
                                     Your log-out has been successful.</p>
                                 <p>
                                     Thank you for using Temps-Direct temp recruiting platform.</p>
                                 <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                                 <input name="feedback" type="commant">
                                 <p>Thank you!</p>
                             </div>
                             <div class="modal-footer hide">
                                 <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default">  Close</a>
                             </div>
                         </div>

                     </div>
                 </div>
                 <script type="text/javascript">
                     //<![CDATA[
                     function getCookie (c_name) {var c_value = document.cookie;var c_start = c_value.indexOf(" " + c_name + "=");if (c_start == -1) {c_start = c_value.indexOf(c_name + "=");}if (c_start == -1) {c_value = null;} else {c_start = c_value.indexOf("=", c_start) + 1;var c_end = c_value.indexOf(";", c_start);if (c_end == -1) {c_end = c_value.length;}c_value = unescape(c_value.substring(c_start,c_end));}return c_value;}if (getCookie('il')) {(function() {var js = document.createElement('script'); js.type = 'text/javascript'; js.async = true;js.src = "https://healthcare-temps.co.uk/users/show_header.js?u=";var s = document.getElementById('js-head-menu'); s.parentNode.insertBefore(js, s);})();} else {document.getElementById('js-head-menu').className = '';}
                     //]]>
                 </script>
             </div>



             <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
             <div id="pjax-body">
                 <div id="main" class="main">
                     <div class="container">

                         <div id="myCarousel" class="carousel slide" data-ride="carousel">
                             <!-- Indicators -->
                             <ol class="carousel-indicators" style="bottom: -200px;">
                                 <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                 <li data-target="#myCarousel" data-slide-to="1"></li>
                                 <li data-target="#myCarousel" data-slide-to="2"></li>
                             </ol>

                             <!-- Wrapper for slides -->
                             <div class="carousel-inner">
                                 <div class="item active">
                                     <div class="" style="border: 1px solid #c5c1c1;border-radius: 15px;">
                                         <div class="row">
                                             <div class="col-xs-12 col-sm-5 col-lg-offset-1">
                                                 <h2 style="color: red;">UV Infection Control Lamp<br /> 40 Points</h2>
                                                 <ul>
                                                     <li>Powerful Ultraviolent sanitizer</li>
                                                     <li>Disinfect & Kill viruese (200cm radius)</li>
                                                     <li>UV Lights Limits spread of infection</li>
                                                     <li>ideal for bathroom, Kitchen & bedroom</li>
                                                     <li>Used in hospitals, care homes & clinics</li>
                                                     <li>Zaps bacteria, viruses, mites, odors etc</li>
                                                 </ul>
                                                 <div style="margin-left: 15px;">
                                                     <button type="button" style="padding: 3px 35px;color: white; text-align: center;background: linear-gradient(rgb(26, 164, 201),rgb(29, 118, 176) );}">Redeem this offer</button>
                                                 </div>

                                                 <p>UK Delivery only.P&P is free, promotional images strictly for illustration<br />You must be 18years of age or over to claim.</p>
                                                 <button type="button" style="padding: 3px 18px;margin-left: 105px;color: white; text-align: center;background: linear-gradient(rgb(21, 119, 166),rgb(28, 77, 109) );}">TELL A <br />FRIEND</button>

                                             </div>
                                             <div class="col-xs-12 col-sm-6" style="text-align: center;">
                                                 <img src="myassets/img/lamp.png">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="item">
                                     <div class="" style="border: 1px solid #c5c1c1;border-radius: 15px;">
                                         <div class="row">
                                             <div class="col-xs-12 col-sm-5 col-lg-offset-1">
                                                 <h2 style="color: red;">UV Infection Control Lamp<br /> 40 Points</h2>
                                                 <ul>
                                                     <li>Powerful Ultraviolent sanitizer</li>
                                                     <li>Disinfect & Kill viruese (200cm radius)</li>
                                                     <li>UV Lights Limits spread of infection</li>
                                                     <li>ideal for bathroom, Kitchen & bedroom</li>
                                                     <li>Used in hospitals, care homes & clinics</li>
                                                     <li>Zaps bacteria, viruses, mites, odors etc</li>
                                                 </ul>
                                                 <div style="margin-left: 15px;">
                                                     <button type="button" style="padding: 3px 35px;color: white; text-align: center;background: linear-gradient(rgb(26, 164, 201),rgb(29, 118, 176) );}">Redeem this offer</button>
                                                 </div>

                                                 <p>UK Delivery only.P&P is free, promotional images strictly for illustration<br />You must be 18years of age or over to claim.</p>
                                                 <button type="button" style="padding: 3px 18px;margin-left: 105px;color: white; text-align: center;background: linear-gradient(rgb(21, 119, 166),rgb(28, 77, 109) );}">TELL A <br />FRIEND</button>

                                             </div>
                                             <div class="col-xs-12 col-sm-6" style="text-align: center;">
                                                 <img src="myassets/img/lamp.png">
                                             </div>
                                         </div>
                                     </div>
                                 </div>

                                 <div class="item">
                                     <div class="" style="border: 1px solid #c5c1c1;border-radius: 15px;">
                                         <div class="row">
                                             <div class="col-xs-12 col-sm-5 col-lg-offset-1">
                                                 <h2 style="color: red;">UV Infection Control Lamp<br /> 40 Points</h2>
                                                 <ul>
                                                     <li>Powerful Ultraviolent sanitizer</li>
                                                     <li>Disinfect & Kill viruese (200cm radius)</li>
                                                     <li>UV Lights Limits spread of infection</li>
                                                     <li>ideal for bathroom, Kitchen & bedroom</li>
                                                     <li>Used in hospitals, care homes & clinics</li>
                                                     <li>Zaps bacteria, viruses, mites, odors etc</li>
                                                 </ul>
                                                 <div style="margin-left: 15px;">
                                                     <button type="button" style="padding: 3px 35px;color: white; text-align: center;background: linear-gradient(rgb(26, 164, 201),rgb(29, 118, 176) );}">Redeem this offer</button>
                                                 </div>

                                                 <p>UK Delivery only.P&P is free, promotional images strictly for illustration<br />You must be 18years of age or over to claim.</p>
                                                 <button type="button" style="padding: 3px 18px;margin-left: 105px;color: white; text-align: center;background: linear-gradient(rgb(21, 119, 166),rgb(28, 77, 109) );}">TELL A <br />FRIEND</button>

                                             </div>
                                             <div class="col-xs-12 col-sm-6" style="text-align: center;">
                                                 <img src="myassets/img/lamp.png">
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                     <span class="glyphicon glyphicon-chevron-left"></span>
                                     <span class="sr-only">Previous</span>
                                 </a>
                                 <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                     <span class="glyphicon glyphicon-chevron-right"></span>
                                     <span class="sr-only">Next</span>
                                 </a>
                             </div>
                         </div>

                         <div style="border: 1px solid #c5c1c1;border-radius: 15px; margin-top: 200px;">
                             <div class="col-lg-offset-1" >
                                 <p>
                                     Unlike other companies, we love to reward our users who remain loyal to us. That's why we are giving<br />
                                     away tons of gifts every week to both temp workers and employers.
                                 </p>
                                 <p>
                                     Every time a temp worker gets hired for her a job or a employer successfully post a job, Temps-Direct<br />
                                     UK rewards you with loyalty bonus points. It's our way of saying thank you for choosing us as your <br /> No.1 temp job websits.
                                 </p>
                                 <p>
                                     Let's explain the knots & bolts:
                                 </p>
                                 <p>
                                     Every time a temp worker place a bid for job, he/she must choose a budget ranging from 1 -scale 4
                                 </p>
                                 <p>
                                     Each scale applies to a single shift performed by a temp worker. Based on the number of shifts a temp <br />
                                     worker successfully performs, he/she will be reward a point(s) depending on the budget level or scale<br />
                                     of the job performed. Each scale accrues the following bonus points:
                                 </p>
                             </div>
                         </div>

                     </div>
                 </div>
             </div>


             <div class="clearfix"></div>
             <!-- for modal
             <div class="footer-push"></div> -->
         </div>
         <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
     </div>
	 
     <!-- Include footer.php. Contains footer content. -->
 	 <?php include ('includes/template/footer.php'); ?> 

</body>
</html>
