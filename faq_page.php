<?php
//Check if init.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include('includes/template/header.php'); ?>
<body class="greybg" style="background-color: #fff;">

<!-- Include navigation.php. Contains navigation content. -->
<?php include('includes/template/navigation.php'); ?>
<style>
    #pages-features ul {
        list-style: none;
        margin-left: 0;
        padding-left: 0;
    }

    #pages-features li {
        padding-left: 1em;
        text-indent: -1em;
        color: #589470;
        font-weight: bold;

    }

    #pages-features li:before {
        content: "";
        padding-right: 5px;
    }
    #pages-features .iconcolor{
        color: #438023;
    }
    #pages-features .btn-border{
        text-align: center;
        border-radius: 15px;
    }
    #pages-features .Q{
        font-size: 55px;
        float: right;
        color: #22b14c;
        margin-top: 6px;
    }
    #pages-features .h4hDes{
        color: #22b14c;
        white-space: nowrap;
    }
    p.h4pDes{
        font-size: 13px;
        text-align: justify;
    }
</style>
<!-- ==============================================
Hello Section
=============================================== -->
<div id="pages-features" class="content user" style="margin-top: 100px;">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489"
                                        title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter"
                                       target="_blank"></a></li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113"
                                        title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw"
                                       title="Follow me on youtube" target="_blank"> </a></li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts"
                                           title="Follow me on google plus" target="_blank"></a></li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/"
                                       title="Follow me on blogger" target="_blank"></a></li>
            </ul>
        </div>
        <div id="js-head-menu" class="">
            <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <a class="show hor-space" title="Temps-direct.uk" href="/">
                                <img alt="" src="myassets/img/logo.png">
                            </a>
                        </div>
                        <div class="cantent">
                            <p>
                                Your log-out has been successful.</p>
                            <p>
                                Thank you for using Temps-Direct temp recruiting platform.</p>
                            <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                            <input name="feedback" type="commant">
                            <p>Thank you!</p>
                        </div>
                        <div class="modal-footer hide">
                            <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default"> Close</a>
                        </div>
                    </div>

                </div>
            </div>

            <script type="text/javascript">
                //<![CDATA[
                function getCookie(c_name) {
                    var c_value = document.cookie;
                    var c_start = c_value.indexOf(" " + c_name + "=");
                    if (c_start == -1) {
                        c_start = c_value.indexOf(c_name + "=");
                    }
                    if (c_start == -1) {
                        c_value = null;
                    } else {
                        c_start = c_value.indexOf("=", c_start) + 1;
                        var c_end = c_value.indexOf(";", c_start);
                        if (c_end == -1) {
                            c_end = c_value.length;
                        }
                        c_value = unescape(c_value.substring(c_start, c_end));
                    }
                    return c_value;
                }
                if (getCookie('il')) {
                    (function () {
                        var js = document.createElement('script');
                        js.type = 'text/javascript';
                        js.async = true;
                        js.src = "https://healthcare-temps.co.uk/users/show_header.js?u=";
                        var s = document.getElementById('js-head-menu');
                        s.parentNode.insertBefore(js, s);
                    })();
                } else {
                    document.getElementById('js-head-menu').className = '';
                }
                //]]>
            </script>
        </div>

        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5 col-lg-offset-1">
                            <h2>Welcome to our FAQs:</h2>
                            <p>Below are some of the most Frequently Asked<br /> Question from both employers and temps.<br /> We hope you find the list below helpful !</p>
                            <ul>
                                <li> <span class="iconcolor" ><i class="fas fa-chevron-circle-down"></i></span> It is really free to use Healthcare-Temps?</li>
                                <li> <span class="iconcolor" ><i class="fas fa-chevron-circle-down"></i></span> How do employers post jobs & hire temps?</li>
                                <li> <span class="iconcolor" ><i class="fas fa-chevron-circle-down"></i></span> Are all temps/Locums fully vetted?</li>
                                <li> <span class="iconcolor" ><i class="fas fa-chevron-circle-down"></i></span> How do I cancel or dispute a temp's invoice?</li>
                            </ul>
                            <ul>
                                <li> <span class="iconcolor" ><i class="fas fa-chevron-circle-down"></i></span> How do I become a healthcare Temps?</li>
                                <li> <span class="iconcolor" ><i class="fas fa-chevron-circle-down"></i></span> What type of temp/Locum job can I find on here?</li>
                                <li> <span class="iconcolor" ><i class="fas fa-chevron-circle-down"></i></span> How do temp workers get paid?</li>
                                <li> <span class="iconcolor" ><i class="fas fa-chevron-circle-down"></i></span> Can I get job notifications on my mobile?</li>
                            </ul>
                            <div style="text-align: center;">
                                <button type="button" class="btn btn-default btn-border">See all answers below</button>
                            </div>
                        </div>
                        <div class="col-lg-5 col-lg-offset-1">
                            <img src="myassets/img/girl.png">
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row ">
                        <div class="col-xs-12" style="margin-bottom: 50px;color: #22b14c;text-align: center;margin-top: 20px;">
                            <span style="border-bottom: 2px solid #22b14c;font-size: 40px;">FAQ</span>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p class="Q">Q</p>
                                </div>
                                <div class="col-lg-10">
                                    <h4 class="h4hDes">Is Healthcare-Temps free for employers / Hirers?</h4>
                                    <p class="h4pDes">Yes, Healthcare-Temps is absolutely free for employers to used to recruit medical and healthcare workers through out the U.K. Hirers and Employers are charged nzero commission, no agency fee, subscription or sign a contract.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <p class="Q">Q</p>
                                </div>
                                <div class="col-lg-10">
                                    <h4 class="h4hDes">What type of Healthcare temps can I hire?</h4>
                                    <p class="h4pDes">Employers can hire almost every type of staff associated with the healthcare industry. From Therapists to Nurses, Carers, Doctors, Surgeons, Dentist, Pharmacist, Home manager, Practice manager, cook, cleaner, ambulance driver and many more. Just post ajob and wait for applications & bids.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <p class="Q">Q</p>
                                </div>
                                <div class="col-lg-10">
                                    <h4 class="h4hDes">How do employers post jobs & hire staff?</h4>
                                    <p class="h4pDes">To post a job and hire staff, first you must sign up and create an Hirer’s profile- Once signed-up and verified, you will be given a personal dashboard which will allow you to post jobs. search for temps. interview temps and hire the one that suits your job requirments</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <p class="Q">Q</p>
                                </div>
                                <div class="col-lg-10">
                                    <h4 class="h4hDes">Can Hirer’s dispute or cancel a Temp’s services?</h4>
                                    <p class="h4pDes">All Temp staffs are required to submit a timesheet and Invoice to the Hirer on completion of their task I job If an employer is dis-satisfied with a temp’s service, time keeping or invoice, the Hirer can raised a dispute with by clicking on the “dispute tab” and write their complaint Thereafter, our dispute management team will attempt to resolve the matter quickly</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <p class="Q">Q</p>
                                </div>
                                <div class="col-lg-10">
                                    <h4 class="h4hDes">How do I pay a Temp-worker for their services?</h4>
                                    <p class="h4pDes">Before a Temp-worker begins their shift, the Hirer must agree a fixed hourly rate with the temp-worker (including all bonuses). The agreed sum must be paid into escrow, then the temp worker will begin the shift Once the shift is completed. the Temp-worker submits a signed timesheet and invoice to our administrator and escrow funds released</p>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-2">
                                    <p class="Q">Q</p>
                                </div>
                                <div class="col-lg-10">
                                    <h4 class="h4hDes">How do I become a Healthcare Temp?</h4>
                                    <p class="h4pDes">To become a healthcare-temp and start earning money, you are required to go through a simple four step registration process. It takes about five minutes and requires potential temp workers to upload 6-8 compliance documents, including CRB, NiNo and l.D. Approval takes about 2-4 days after verification.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <p class="Q">Q</p>
                                </div>
                                <div class="col-lg-10">
                                    <h4 class="h4hDes">What type of Temp jobs can I find on this site?</h4>
                                    <p class="h4pDes">As a specialist healthcare recruitment platform, temp workers can find a wide variety of medical and healthcare jobs, ranging from GP, ABA Therapist, Support worker, Cook, Driver, Nurse, Surgeon and loads more.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <p class="Q">Q</p>
                                </div>
                                <div class="col-lg-10">
                                    <h4 class="h4hDes">How do temps get paid?</h4>
                                    <p class="h4pDes">Fully vetted Temp workers can choose to be paid either weekly or monthly. Direct to bank their bank or paypal account, in sterling or bitcoins Wages are paid on the first Monday after the date Timesheet and invoice are submitted</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <p class="Q">Q</p>
                                </div>
                                <div class="col-lg-10">
                                    <h4 class="h4hDes">How many employers can I work for?</h4>
                                    <p class="h4pDes">Temp workers can work for any many employers are they wish They are also afforded the flexibility to choose any care setting they desire (Hospital. Nursing home. Community. Charity etc). Some Temp-workers like to work on the sea-side, others in small villages or city centres with good night transport- The only caution here is to restrict staff to a maximum of 60 hours per week</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2">
                                    <p class="Q">Q</p>
                                </div>
                                <div class="col-lg-10">
                                    <h4 class="h4hDes">What taxes and other costs do I incur?</h4>
                                    <p class="h4pDes">All Temp-workers are regarded as self-employed and must posses a UTR No Once in possession of a Unique tax Reference No (UTR) you are responsible for paying your own Self-assessed Tax. NiNo. VAT and an administration fee of 12% for every shift allocated Terms of Service Privacy Policy Service User Policy FAO Contad Us Modem Slavery Stmt GDPR </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>



            </div>


    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
    </div>
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

<a id="scrollup">Scroll</a>

<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Kafe JS -->
<script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
