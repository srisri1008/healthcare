<?php
//Check if frontinit.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

if ($_GET['id'] == '') {
    Redirect::to('register.php');
}


//Get Site Settings Data
$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
    foreach ($query->results() as $row) {
        $title = $row->title;
        $use_icon = $row->use_icon;
        $site_icon = $row->site_icon;
        $tagline = $row->tagline;
        $description = $row->description;
        $keywords = $row->keywords;
        $author = $row->author;
        $bgimage = $row->bgimage;
    }
}

//Get Payments Settings Data
$q1 = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($q1->count()) {
    foreach ($q1->results() as $r1) {
        $currency = $r1->currency;
        $membershipid = $r1->membershipid;
    }
}

//Getting Payement Id from Database
$query = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
if ($query->count() === 1) {
    $q1 = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
} else {
    $q1 = DB::getInstance()->get("membership_agency", "*", ["membershipid" => $membershipid]);
}
if ($q1->count() === 1) {
    foreach ($q1->results() as $r1) {
        $bids = $r1->bids;
    }
}

if (!function_exists('base_url')) {
    function base_url($atRoot = FALSE, $atCore = FALSE, $parse = FALSE)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            $core = $core[0];

            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf($tmplt, $http, $hostname, $end);
        } else $base_url = 'http://localhost/';

        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }

        return $base_url;
    }
}
$conn = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
$conn->exec("SET CHARACTER SET utf8");
$sql = "SELECT * FROM freelancer WHERE freelancerid=" . $_GET['id'];
$result = $conn->query($sql);
$row = $result->fetch(PDO::FETCH_NUM);
$msg = '';
if (isset($row[0]) && $row[0] != '') {
    if(isset($_POST['_method'])) {
        foreach ($_POST as $file) {
            if(empty($_POST['nino_no']) || empty($_POST['utr_no']) || empty($_POST['company_no'])) {
                Redirect::to('compliance_upload_document.php?id='. $_GET['id']);
                exit;
            }
        }
        $appendTo = "";
        $sql = "SELECT * FROM freelancer_info WHERE freelancerid = ". $row[0];
        $res = $conn->query($sql)->fetch(PDO::FETCH_ASSOC);
        if(count($res)) {
            foreach ($_POST as $k => $v) {
                if($k != '_method') {
                    $appendTo .= $k .'='. $v . ',';
                }
            }
            $appendTo = rtrim($appendTo, ',');
            $sql = "UPDATE freelancer_info SET ". $appendTo . " WHERE freelancerid = ". $row[0];
            $conn->query($sql);
        } else {
            $keys = "";
            $values = "";
            foreach ($_POST as $k => $v) {
                if($k != '_method') {
                    $keys .= "$k".',';
                    $values .= "$values". ',';
                }
            }
            $keys = rtrim($keys, ',');
            $values = rtrim($values, ',');

            $sql = "INSERT INTO freelancer_info (" .$keys. ") VALUES (" .$values. ")";
            $conn->query($sql);
        }
        Redirect::to('send_mail.php?id='. $_GET['id']);
    }
} else {
    Redirect::to('register.php');
}
?>






<!DOCTYPE html>
<html lang="en">

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    .img-thumbnail inner_img img {
        width: 50px;
        height: 50px;
    }

    .img-thumbnail.inner_img {
        margin-bottom: 33px;
    }

    .compliance-content .img-thumbnail inner_img {
        display: block;
        max-width: 100%;
        height: auto;
        padding: 4px;
        line-height: 1.42857143;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
        margin: 2px auto;
    }

    .wrapper {
        display: grid;
        grid-gap: 10px;
        color: #444;
        margin-bottom: 23px;
    }

    img-thumbnail.inner_img {
        margin-bottom: 33px;
        display: block;
    }

    .img-responsive01 {
        width: 50%;
    }

    .img-thumbnail.inner_img.bottom_img {
        padding: 15px 46px;
    }

    .img-responsive01.bottom_img_01 {
        width: 89%;
    }

    .responsive01.bottom_img_01.bottom_img_02 {
        width: 75%;
    }
</style>
<div id="pages-bonus" class="content user">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
            </ul>
        </div>

        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container">
                    <section class="row ver-space ver-smspace">
                        <h2 class="tab-head bot-mspace text-16 clearfix green-head-bg">Social Media Registration:</h2>
                        <div class="thumbnail social_marketings">
                            <div class="clearfix">
                                <div class="row page-header">
                                    <div class="col-md-2"><span class="label share-follow ">Facebook</span></div>
                                    <div class="col-md-2"><span class="label share-follow ">Twitter</span></div>
                                    <div class="col-md-2"><span class="label share-follow ">Gmail</span></div>
                                    <div class="col-md-2"><span class="label share-follow ">Yahoo!</span></div>
                                    <div class="col-md-2"><span class="label share-follow badge-module">LinkedIn</span></div>
                                    <div class="col-md-2 hidden-xs"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="ver-space mspace clearfix">
                                <div class="tab-content" id="myTabContent">
                                    <div id="linkedin">
                                        <div class="alert alert-success">Will be added later.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <a href="send_mail.php?id=<?=$_GET['id']?>" title="Done" class="btn btn-success pull-right js-tooltip mspace">Done</a>  </div>
                        </div>
                        <div id="fb-root"></div>
                    </section>

                    <div class="row">
                        <div class="col-xs-12 col-md-offset-1" style="margin-top: 90px;">
                            <style>
                                .circle-bar .circles{
                                    border-radius: 50%;
                                    width: 40px;
                                    height: 40px;
                                    display: inline-block;
                                    background-color: #fff;
                                    margin-top: -17px;
                                }
                                .blankcircles{
                                    border-color: green;
                                    border: 1px solid green;
                                    border-radius: 50%;
                                    width: 40px;
                                    height: 40px;
                                    display: inline-block;
                                    background-color: #fff;
                                    margin-top: -17px;
                                }
                                .lgcircles{
                                    border-radius: 50%;
                                    width: 60px;
                                    height: 60px;
                                    border-color: green;
                                    border: 1px solid green;
                                    display: inline-block;
                                    background-color: #fff;
                                    margin-top: -23px;
                                }
                            </style>
                            <div class="circle-bar" style=" overflow: visible;position: relative;width: 80%;margin: 0 auto; height: 5px;background-color: green;">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <div class="circles">
                                            <i style="font-size: 37px; height: 40px;width: 40px; border-radius: 50%;color: white;background-color: green; " class="fa fa-check" aria-hidden="true"></i>
                                        </div>
                                        <p>Compliance<br> Upload Documents</p>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="circles">
                                            <i style="font-size: 37px; height: 40px;width: 40px; border-radius: 50%;color: white;background-color: green; " class="fa fa-check" aria-hidden="true"></i>
                                        </div>
                                        <p>Tax Compliance <br> &amp; Self-Employment</p>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class=" lgcircles">
                                            <i style="font-size: 56px; height: 40px;width: 40px; border-radius: 50%;color: green; " class="fa fa-check" aria-hidden="true"></i>
                                        </div>
                                        <p>Create CV <br> Profile</p>
                                    </div>
                                    <div class="col-xs-3">
                                        <div style="width: 100%;background-color: #fff;height: 5px;"></div>
                                        <div class="blankcircles">
                                        </div>
                                        <p> Delivered</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

</body>
</html>




