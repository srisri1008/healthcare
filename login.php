<?php
//Check if frontinit.php exists
if(!file_exists('core/frontinit.php')){
  header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php'; 
}
$dbObj = new DB();
//Get Site Settings Data
$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
 foreach($query->results() as $row) {
  $title = $row->title;
  $use_icon = $row->use_icon;
  $site_icon = $row->site_icon;
  $tagline = $row->tagline;
  $description = $row->description;
  $keywords = $row->keywords;
  $author = $row->author;
  $bgimage = $row->bgimage;
 }
}

//Log In Function
if (Input::exists()) {
    //your site secret key
    $secret = '6LdGqkMUAAAAAEGOZfvwgW9IGWTjzEcAHEPHR8Um';
    //get verify response data
    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    $responseData = json_decode($verifyResponse);
    if($responseData->success) {
        if (Token::check(Input::get('token'))) {

            $errorHandler = new ErrorHandler;

            $validator = new Validator($errorHandler);

            $validation = $validator->check($_POST, [
                'email' => [
                    'required' => true,
                    'maxlength' => 255,
                    'email' => true
                ],
                'password' => [
                    'required' => true,
                    'minlength' => 6
                ]
            ]);

            if (!$validation->fails()) {

                if (Input::get('user_type') === 'on') {

                    //Log Client In
                    $client = new Client();

                    $remember = (Input::get('remember') === 'on') ? true : false;
                    $login = $client->login(Input::get('email'), Input::get('password'), $remember);

                   if ($login === true) {
                        if (isset($_GET['act'])) {

                           // setcookie('userid', $cookie_value, ''); 
                            Redirect::to('hirer/overview.php?a=profile');
                        }elseif(isset($_GET['invite']) && $_GET['invite'] == '1') {

                           // setcookie('userid', $cookie_value, ''); 
                            Redirect::to('hirer/addinvite.php');
                        }elseif(isset($_GET['hirer']) && $_GET['hirer'] == '1') {

                           // setcookie('userid', $cookie_value, ''); 
                            Redirect::to('hirer/proposallist.php');
                        }elseif(isset($_GET['email']) && $_GET['email'] == '1') {

                           // setcookie('userid', $cookie_value, ''); 
                            Redirect::to('hirer/compose.php');
                        } else {
                            $_SESSION['showPopup'] = 'true';
                            Redirect::to('hirer/');
                        }
                    } else {
                        $hasError = true;
                    }

                } else {

                    //Log freelancer In
                    $freelancer = new Freelancer();

                    $remember = (Input::get('remember') === 'on') ? true : false;
                    $login = $freelancer->login(Input::get('email'), Input::get('password'), $remember);

                    if ($login === true) {
                        $_SESSION['choice_modal'] = 1;

                        $url = $_SESSION['job_url'];
                        $ty = $_SESSION['jobty'];
                        $needle = 'job_details.php';

                        if(strpos( $url, $needle ) !== false &&  $ty == 'new') {
                            //print_-r($_SESSION);exit;
                            unset($_SESSION['job_url']);
                            Redirect::to("https://healthcare-temps.co.uk/$url");
                        }
                        if($freelancer->data()->first_login == 0) {
                            $dbObj->query("UPDATE freelancer SET first_login = 1 WHERE id = ". $freelancer->data()->id);
                            Redirect::to('tempworker/edit_temp_profile.php?a=profile');
                        }
                        Redirect::to('tempworker/');
                    } else {
                        $hasError = true;
                    }

                }

            } else {
                $error = '';
                foreach ($validation->errors()->all() as $err) {
                    $str = implode(" ", $err);
                    $error .= '
                       <div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>Error!</strong> ' . $str . '
                       </div>
                       ';
                }
            }

        }
    }
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en" class="no-js"> 
<!--<![endif]-->
<head>

      <!-- ==============================================
    Title and Meta Tags
    =============================================== -->
    <meta charset="utf-8">
        <title><?php echo escape($title) .' - '. escape($tagline) ; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo escape($description); ?>">
        <meta name="keywords" content="<?php echo escape($keywords); ?>">
        <meta name="author" content="<?php echo escape($author); ?>">

    <!-- ==============================================
    Favicons
    =============================================== -->
    <link rel="shortcut icon" href="img/favicons/favicon.ico">
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-touch-icon-114x114.png">

      <!-- ==============================================
    CSS
    =============================================== -->
        <!-- Style-->
        <link href="assets/css/login.css" rel="stylesheet" type="text/css" />
        <link href="myassets/css/main.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
    

    <!-- ==============================================
    Feauture Detection
    =============================================== -->
    <script src="assets/js/modernizr-custom.js"></script>

    <!--[if lt IE 9]>
     <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <style>
        .label-success {
            border: none;
        }
        .logo_img {
            width: 22%;
        }
        .logo_img {
            width: 211px;
            margin-left: 33px;
        }
        .navbar.navbar-inverse.navigation_bar {
            height: 78px;
            background-color: #132943;
            border-radius: 0px;
        }
        .nav.navbar-nav.navbar-right.nav_right_side {
            margin: 13px 0px;
        }
        .nav.navbar-nav.navbar-right.nav_right_side li a:hover {
            color: #1FAC56;
            transition: 0.3s;
        }
        .nav.navbar-nav.navbar-right.nav_right_side li a {
            font-size: 16px;
        }

        .healthcare_temps h6 {
            color: #1FAC54;
            font-size: 29px;
            margin-bottom: 11px;
            margin-top: 48px;
        }
        .healthcare_temps hr {
            height: 3px;
            background-color: #1FAC54;
            width: 77px;
            margin-top: 1px;
        }

        .urgent_hjobs {
            background-color: #007FFF;
            color: white;
            text-align: center;
            margin-bottom: 27px;
        }
        .urgent_hjobs h6 {
            font-size: 23px;
            padding: 13px 0px;
            margin-top: 0px;
        }
        .urgent_hjob_text {
            padding: 0px 27px;
        }
        .hire_textbox {
            height: 39px;
            border-radius: 0px;
            margin-bottom: 21px;
        }
        .hire-job {
            border: 1px solid #007FFF;
            text-align: center;
        }
        .hire-job button {
            background-color: #007FFF;
            color: white;
            border: 1px solid #056cd4;
            padding: 6px 28px;
            margin-bottom: 26px;
            border-radius: 40px;
        }
        .hire-job button:hover {
            background-color: #0772de;
            transition: 0.3s;
        }
        .job_button {
            padding: 0px 19px !important;
            margin-bottom: 0px !important;
        }
        .table.urjent_job tr td {
            border: 0px;
        }
        .benifits_sec {
            background-color: #1FAC56;
            padding-bottom: 44px;
        }
        .container-fluid.main_container {
            padding: 0px 0px;
        }
        .benfit_hw img {
            width: 7%;
            display: block;
            margin: 0 auto;
            margin-top: 36px;
        }
        .benfit_hw p span img {
            margin: 0px 0px;
            display: inline-block;
            margin-right: 14px;
            width: 16px;
        }
        .benfit_hw p {
            line-height: 21px;
            color: white;
            margin-left: 137px;
        }
        .benfit_hw h6 {
            text-align: center;
            color: white;
            font-size: 23px;
            margin-bottom: 23px
        }
        .benfit_hw button {
            background-color: #3D91C5;
            color: white;
            border: 1px solid #1680c1;
            padding: 8px 17px;
            border-radius: 37px;
            display: table;
            margin: 0 auto !important;
            margin-bottom: 13px;
            margin-top: 39px !important;
        }
        .benfit_hw button:hover {
            background-color: #248bcc;
            transition: 0.3s;
        }

        .healthcare_temp_sec {
            margin-bottom: 79px;
        }
        .suitcase {
            margin-bottom: 19px !important;
        }
        .our_solution h6 {
            color: #24B3F7;
            text-align: center;
            font-size: 34px;
            margin-top: 52px;
            margin-bottom: 52px;
        }
        .solution_content img {
            width: 100%;
        }
        .solution_content button {
            background-color: #23B3F7;
            border: 1px solid #0c9bde;
            color: white;
            padding: 2px 30px;
            position: relative;
            top: -87px;
            left: 46px;
        }
        .solution_content button:hover {
            background-color: #15a6ea;
            transition: 0.3s;
        }
        .solution_cnt_container {
            margin-bottom: 20px;
        }
        .users_head h6 {
            text-align: center;
            font-size: 28px;
            color: #35A5D6;
            margin: 24px 0px;
        }
        .user_content img {
            width: 45%;
            display: block;
            margin: 0 auto;
        }
        .user_content h6 {
            color: #00B24D;
            text-align: center;
            font-size: 18px;
            margin-top: 14px;
        }
        .user_content h5 {
            text-align: center;
            font-size: 14px;
        }
        .user_text {
            background-color: #00B24D;
            color: white;
        }
        .user_content {
            padding: 16px 41px;
            background-color: white;
        }
        .col-lg-4.user_para_text {
            margin-bottom: 41px;
        }
        .user_text span {
            display: table;
            margin: 0 auto;
            font-size: 50px;
            position: relative;
            top: -21px;
        }
        .user_text p {
            padding: 0px 17px;
            position: relative;
            top: -14px;
        }
        .user_text.user_text_blue {
            background-color: #32A2D8;
        }
        .user_text.user_text_right {
            padding-bottom: 10px;
        }
        .users_sec {
            margin-bottom: 37px;
            background-color: #DBDDDC;
        }
        .reveloution_temp img {
            width: 100%;
        }
        .col-lg-12.virtual_care img {
            width: 100%;
        }
        .footer_head h6 {
            font-size: 25px;
            margin-top: 35px;
        }
        ul.footer_links {
            list-style-type: none;
            padding: 0px;
        }
        ul.footer_links li a {
            font-size: 15px;
            color: #7f7f7f;
            text-decoration: none;
        }
        ul.footer_links li a:hover {
            color: #525151;
        }
        .bottom_links {
            background-color: #132943;

        }
        .bottom_links_ul {
            list-style-type: none;
        }
        ul.bottom_links_ul li {
            float: left;
        }
        ul.bottom_links_ul li a {
            color: white;
            padding: 0px 15px;
        }
        .bottom_links {
            background-color: #132943;
            padding: 15px 0px;
            margin-top: 0px;
        }
        .bottom_links_ul {
            list-style-type: none;
            margin: 10px 269px;
        }
        .col-lg-12.bottom_links h6 {
            color: white;
            font-size: 15px;
            text-align: right;
            margin-right: 57px;
        }
        footer {
            padding: 0px 0px !important;
            background: #ccc !important;
            background-color: #ccc !important;
        }


        /* New css */


    </style>
</head>

<body>

  <!-- Paste this code after body tag -->
    <div class="loader">
  <div class="se-pre-con"></div>
    </div>
    
 <? 
$basename = basename($_SERVER["REQUEST_URI"], ".php");
$editname = basename($_SERVER["REQUEST_URI"]);
$test = $_SERVER["REQUEST_URI"];
?>
     <!-- ==============================================
     Navigation Section
     =============================================== -->
    <?php /* ?>
  <header id="header" headroom="" role="banner" tolerance="5" offset="700" class="navbar navbar-fixed-top navbar--white ng-isolate-scope headroom headroom--top">
    <nav role="navigation">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle header-nav__button" data-toggle="collapse" data-target=".navbar-main">
          <span class="icon-bar header-nav__button-line"></span>
          <span class="icon-bar header-nav__button-line"></span>
          <span class="icon-bar header-nav__button-line"></span>
        </button>
        <div class="header-nav__logo">
          <a class="header-nav__logo-link navbar-brand" href="index.php">
          <?php if($use_icon === '1'): ?>
            <i class="fa <?php echo $site_icon; ?>"></i>
          <?php endif; ?>  <?php echo escape($title); ?></a>
        </div>
      </div>
      <div class="collapse navbar-collapse navbar-main navbar-right">
        <ul class="nav navbar-nav header-nav__navigation">
          <li class="header-nav__navigation-item
           <?php echo $active = ($_SERVER['HTTP_HOST'] && $test === '') ? ' active' : ''; echo $active = ($basename == 'index') ? ' active' : ''; ?>">
            <a href="index.php" class="header-nav__navigation-link">
              <?php echo $lang['home']; ?>
            </a>
          </li>
          <li class="header-nav__navigation-item <?php echo $active = ($basename == 'jobs') ? ' active' : ''; echo $active = ($editname == 'jobpost.php?title='. Input::get('title').'') ? ' active' : '';?>">
            <a href="jobs.php" class="header-nav__navigation-link ">
              <?php echo $lang['jobs']; ?>
            </a>
          </li>
          <li class="header-nav__navigation-item <?php echo $active = ($basename == 'services') ? ' active' : ''; echo $active = ($editname == 'tempworker.php?a='. Input::get('a').'&id='. Input::get('id').'') ? ' active' : ''; ?>">
            <a href="services.php" class="header-nav__navigation-link ">
              <?php echo $lang['services']; ?>
            </a>
          </li>
          <li class="header-nav__navigation-item <?php echo $active = ($basename == 'about') ? ' active' : ''; ?>">
            <a href="about.php" class="header-nav__navigation-link ">
              <?php echo $lang['about']; ?>
            </a>
          </li>
              <li class="header-nav__navigation-item <?php echo $active = ($basename == 'features') ? ' active' : ''; ?>">
                  <a href="features.php" class="header-nav__navigation-link ">
                      Features
                  </a>
              </li>
          <li class="header-nav__navigation-item <?php echo $active = ($basename == 'how') ? ' active' : ''; ?>">
            <a href="how.php" class="header-nav__navigation-link ">
              <?php echo $lang['how']; ?> <?php echo $lang['it']; ?> <?php echo $lang['works']; ?>
            </a>
          </li>
          <li class="header-nav__navigation-item <?php echo $active = ($basename == 'faq') ? ' active' : ''; ?>">
            <a href="faq.php" class="header-nav__navigation-link ">
              <?php echo $lang['faq']; ?>
            </a>
          </li>

          <li class="header-nav__navigation-item <?php echo $active = ($basename == 'contact') ? ' active' : ''; ?>">
            <a href="contact.php" class="header-nav__navigation-link ">
              <?php echo $lang['contact']; ?>
            </a>
          </li>
          
     <?php
     //Start new Admin object
     $admin = new Admin();
     //Start new Client object
     $client = new Client();
     //Start new Freelancer object
     $freelancer = new Freelancer(); 
     
     if ($admin->isLoggedIn()) { ?>
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
              <?php // echo $profileimg; ?>
                  <img src="Admin/<?php echo escape($admin->data()->imagelocation); ?>" class="user-image" alt="User Image"/>
                
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">
                    <?php echo escape($admin->data()->name); ?>
                  </span>
                </a>
                <ul class="dropdown-menu">
            <li class="m_2"><a href="Admin/dashboard.php"><i class="fa fa-dashboard"></i><?php echo $lang['dashboard']; ?></a></li>
            <li class="m_2"><a href="Admin/profile.php?a=profile"><i class="fa fa-user"></i><?php echo $lang['view']; ?> <?php echo $lang['profile']; ?></a></li>
            <li class="m_2"><a href="Admin/logout.php"><i class="fa fa-lock"></i> <?php echo $lang['logout']; ?></a></li> 
            </ul>
              </li>
    <?php } elseif($client->isLoggedIn()) { ?>
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
              <?php // echo $profileimg; ?>
                  <img src="Client/<?php echo escape($client->data()->imagelocation); ?>" class="user-image" alt="User Image"/>
                
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">
                    <?php echo escape($client->data()->name); ?>
                  </span>
                </a>
                <ul class="dropdown-menu">
            <li class="m_2"><a href="Client/dashboard.php"><i class="fa fa-dashboard"></i><?php echo $lang['dashboard']; ?></a></li>
            <li class="m_2"><a href="Client/profile.php?a=profile"><i class="fa fa-user"></i><?php echo $lang['view']; ?> <?php echo $lang['profile']; ?></a></li>
            <li class="m_2"><a href="Client/logout.php"><i class="fa fa-lock"></i> <?php echo $lang['logout']; ?></a></li>  
            </ul>
              </li>
    <?php } elseif($freelancer->isLoggedIn()) { ?>
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
              <?php // echo $profileimg; ?>
                  <img src="tempworker/<?php echo escape($freelancer->data()->imagelocation); ?>" class="user-image" alt="User Image"/>
                
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">
                    <?php echo escape($freelancer->data()->name); ?>
                  </span>
                </a>
                <ul class="dropdown-menu">
            <li class="m_2"><a href="tempworker/index.php"><i class="fa fa-dashboard"></i><?php echo $lang['dashboard']; ?></a></li>
            <li class="m_2"><a href="tempworker/profile.php?a=profile"><i class="fa fa-user"></i><?php echo $lang['view']; ?> <?php echo $lang['profile']; ?></a></li>
            <li class="m_2"><a href="tempworker/logout.php"><i class="fa fa-lock"></i> <?php echo $lang['logout']; ?></a></li>
            </ul>
              </li>
    <?php } else { ?>               
          <li class="header-nav__navigation-item <?php echo $active = ($basename == 'login') ? ' active' : ''; echo $active = ($basename == 'forgot') ? ' active' : ''; echo $active = ($basename == 'reset') ? ' active' : ''; ?>">
            <a class="header-nav__navigation-link" href="login.php"><?php echo $lang['login']; ?></a>
          </li>
          <li class="header-nav__navigation-item <?php echo $active = ($basename == 'register') ? ' active' : ''; ?>">
            <a class="header-nav__navigation-link header-nav__navigation-link--outline" href="register.php"><?php echo $lang['signup']; ?> <?php echo $lang['for']; ?> <?php echo $lang['free']; ?></a>
          </li>
     <?php } ?>                   

              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <?php echo $lang['languages']; ?>
                </a>
                <ul class="dropdown-menu">
          <li class="m_2"><a href="<?php echo $test; ?>?lang=english">English</a></li>
          <li class="m_2"><a href="<?php echo $test; ?>?lang=french">French</a></li>
          <li class="m_2"><a href="<?php echo $test; ?>?lang=german">German</a></li>  
          <li class="m_2"><a href="<?php echo $test; ?>?lang=portuguese">Portuguese</a></li>
          <li class="m_2"><a href="<?php echo $test; ?>?lang=spanish">Spanish</a></li>
          <li class="m_2"><a href="<?php echo $test; ?>?lang=russian">Russian</a></li>  
          <li class="m_2"><a href="<?php echo $test; ?>?lang=chinese">Chinese</a></li>  
            </ul>
              </li>
        </ul>
      </div>
    </nav>
  </header>      
   <?php */
     //Include navigation.php. Contains navigation content
   include ('includes/template/navigation.php');
    ?>
     <!-- ==============================================
   Header
   =============================================== -->
    <!-- <header class="header-login" style="
    background: linear-gradient(
      rgba(34,34,34,0.7),
      rgba(34,34,34,0.7)
    ), url('<?php echo $bgimage; ?>') no-repeat center center fixed;
   background-size: cover;
  background-position: center center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  color: #fff;
  height: 60vh;
  width: 100%;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center; ">
      <div class="container">
     <div class="content">
      <div class="row">
       <h1 class="revealOnScroll" data-animation="fadeInDown">
          <?php if($use_icon === '1'): ?>
            <i class="fa <?php echo $site_icon; ?>"></i>
          <?php endif; ?>  <?php echo escape($title); ?></h1>
     <div id="typed-strings">
      <span><?php echo escape($tagline); ?></span>
     </div>
     <p id="typed"></p>
        </div>
       </div>
    </div>
     </header>/header -->
   
     <!-- ==============================================
     Banner Login Section
     =============================================== -->
   <section class="banner-login" style="padding-top: 122px;">
    <div class="container" >
            
     <div class="row">
     
        <main class="main main-signup col-lg-12">
       <div class="col-lg-offset-3 col-lg-5 text-center">
        
        <?php if(isset($hasError)) { //If errors are found ?>
        <div class="alert alert-danger fade in">
         <a href="#" class="close" data-dismiss="alert">&times;</a>
         <strong><?php echo $lang['hasError']; ?></strong> <?php echo $lang['login_error']; ?>
      </div>
        <?php } ?>
        
        <?php if (isset($error)) {
      echo $error;
    } ?>
        
        
      <div class="form-sign loginform">
     
  
       <form method="post">
        <div class="form-head">
      
         <a class="navbar-brand" href="/">
                 <img src="../../myassets/img/loginlogo.jpg" class="img-responsive" alt="Healthcare" title="healthcare">
             </a>
        <h2>Welcome Back</h2>
       <!-- <h3><?php //echo $lang['login']; ?></h3> -->
      </div><!-- /.form-head -->
            <div class="form-body"> 
              

            <!-- List group -->
            <ul class="list-group">
             <li class="list-group-item">
              <div class="material-switch pull-center">
             <span class="pull-left"><?php echo $lang['freelancer']; ?></span>
                <input id="someSwitchOptionDefault" name="user_type" type="checkbox"/>
                <label for="someSwitchOptionDefault" class="label-success"></label>
             <span class="pull-right"><?php echo $lang['client']; ?></span>
              </div>
             </li>
            </ul>               
              
       <div class="form-row">
        <div class="form-controls">
         <input name="email" placeholder="<?php echo $lang['email']; ?>" class="field" type="text">
        </div><!-- /.form-controls -->
       </div><!-- /.form-row -->

       <div class="form-row">
        <div class="form-controls">
         <input name="password" placeholder="<?php echo $lang['password']; ?>" class="field" type="password">
        </div><!-- /.form-controls -->
       </div><!-- /.form-row -->
                <div class="g-recaptcha" data-sitekey="6LdGqkMUAAAAAHFSRx97kQqVfbU8Y3nkZRccH8iu"></div>
       <div class="form-row">
       <div class="pull-left">
       <label class="check "><?php echo $lang['remember_me']; ?>
    <input type="checkbox" checked="checked" name="remember">
  <span class="checkmark"></span>
</label>
       <!--  <div class=" pull-left">
         <input id="someSwitchOptionSuccess" name="remember" type="checkbox"/>
         <label for="someSwitchOptionSuccess" class="label-success"></label>
         <span style="color: #ffffff !important;"><?php //echo $lang['remember_me']; ?></span>
        </div> -->
        </div>
        <div class="pull-right">
           <div class="form-head">
        <a href="forgot.php" class="more-link"><?php echo $lang['forgot_password']; ?></a>
       </div>
          
        </div>
       </div><!-- /.form-row -->
       
        </div><!-- /.form-body -->

      <div class="form-foot">
       <div class="form-actions">         
              <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
        <input value="<?php echo $lang['login']; ?>" class="form-btn" type="submit">
       </div><!-- /.form-actions -->
            
      </div><!-- /.form-foot -->
      <div class="dontAc">Don'thave an account? <a href="register.php"> Sign Up</a></div>
      <div class="row" style="text-align: center;">
                             <div class="col-lg-12 " >
                               
<div class="orLine"><span>OR</span></div>

                             </div>
                           
                         </div>
                         <p style="text-align: center;">
                             Social Media Sign-up - Hirers only.
                         </p>
                         <div class="row" style="text-align: center;font-size: 45px;">
                             <div class="col-lg-2 col-lg-offset-3">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 49.652 49.652" style="enable-background:new 0 0 49.652 49.652;" xml:space="preserve">
<g>
  <g>
    <path d="M24.826,0C11.137,0,0,11.137,0,24.826c0,13.688,11.137,24.826,24.826,24.826c13.688,0,24.826-11.138,24.826-24.826    C49.652,11.137,38.516,0,24.826,0z M31,25.7h-4.039c0,6.453,0,14.396,0,14.396h-5.985c0,0,0-7.866,0-14.396h-2.845v-5.088h2.845    v-3.291c0-2.357,1.12-6.04,6.04-6.04l4.435,0.017v4.939c0,0-2.695,0-3.219,0c-0.524,0-1.269,0.262-1.269,1.386v2.99h4.56L31,25.7z    " style="fill: rgb(72, 103, 170);"></path>
  </g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>
                             </div>
                             <div class="col-lg-2">
                               
<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 291.319 291.319" style="enable-background:new 0 0 291.319 291.319;" xml:space="preserve" " width="40px" height="40px">
<g>
  <path style="fill:#0E76A8;" d="M145.659,0c80.45,0,145.66,65.219,145.66,145.66s-65.21,145.659-145.66,145.659S0,226.1,0,145.66
    S65.21,0,145.659,0z"/>
  <path style="fill:#FFFFFF;" d="M82.079,200.136h27.275v-90.91H82.079V200.136z M188.338,106.077
    c-13.237,0-25.081,4.834-33.483,15.504v-12.654H127.48v91.21h27.375v-49.324c0-10.424,9.55-20.593,21.512-20.593
    s14.912,10.169,14.912,20.338v49.57h27.275v-51.6C218.553,112.686,201.584,106.077,188.338,106.077z M95.589,100.141
    c7.538,0,13.656-6.118,13.656-13.656S103.127,72.83,95.589,72.83s-13.656,6.118-13.656,13.656S88.051,100.141,95.589,100.141z"/>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>

                             </div>
                             <div class="col-lg-2">
                               
<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Generator: Adobe Illustrator 18.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   viewBox="0 0 112.197 112.197" style="enable-background:new 0 0 112.197 112.197;" xml:space="preserve" " width="40px" height="40px">
<g>
  <circle style="fill:#55ACEE;" cx="56.099" cy="56.098" r="56.098"/>
  <g>
    <path style="fill:#F1F2F2;" d="M90.461,40.316c-2.404,1.066-4.99,1.787-7.702,2.109c2.769-1.659,4.894-4.284,5.897-7.417
      c-2.591,1.537-5.462,2.652-8.515,3.253c-2.446-2.605-5.931-4.233-9.79-4.233c-7.404,0-13.409,6.005-13.409,13.409
      c0,1.051,0.119,2.074,0.349,3.056c-11.144-0.559-21.025-5.897-27.639-14.012c-1.154,1.98-1.816,4.285-1.816,6.742
      c0,4.651,2.369,8.757,5.965,11.161c-2.197-0.069-4.266-0.672-6.073-1.679c-0.001,0.057-0.001,0.114-0.001,0.17
      c0,6.497,4.624,11.916,10.757,13.147c-1.124,0.308-2.311,0.471-3.532,0.471c-0.866,0-1.705-0.083-2.523-0.239
      c1.706,5.326,6.657,9.203,12.526,9.312c-4.59,3.597-10.371,5.74-16.655,5.74c-1.08,0-2.15-0.063-3.197-0.188
      c5.931,3.806,12.981,6.025,20.553,6.025c24.664,0,38.152-20.432,38.152-38.153c0-0.581-0.013-1.16-0.039-1.734
      C86.391,45.366,88.664,43.005,90.461,40.316L90.461,40.316z"/>
  </g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
<g>
</g>
</svg>

                             </div>
                         </div>
       </form>
       
      </div><!-- /.form-sign -->
        <div id="iphone"><img src="assets/img/appstore.jpg"></div>
      <div id="android"><img src="assets/img/googleplay.jpg"></div>
      
       </div><!-- /.col-lg-6 -->

        </main>
    
     </div><!-- /.row -->
    </div><!-- /.container -->
     </section><!-- /section -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include ('includes/template/footer.php'); ?>

    <!-- ==============================================
    Scripts
    =============================================== -->
   
     <!-- jQuery 2.1.4 -->
     <script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
     <!-- Bootstrap 3.3.6 JS -->
     <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
     <!-- Typed JS -->
     <script src="assets/js/typed.min.js" type="text/javascript"></script>
     <!-- Kafe JS -->
     <script src="assets/js/kafe.js" type="text/javascript"></script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-2199989077469674",
            enable_page_level_ads: true
        });
    </script>

</body>
</html>
