<?php
//Check if init.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include('includes/template/header.php'); ?>
<body class="greybg" style="background-color: #fff;">

<!-- Include navigation.php. Contains navigation content. -->
<?php include('includes/template/navigation.php'); ?>

<style>
    .h2des{
        border-bottom: 3px solid #378139;
        display: inline;
        color:#3f9a41;
    }
    .pdes{
        margin-top: 22px;
        font-size: 20px;
        margin-bottom: 35px;
    }
    .container{
        margin-top: 50px;
        margin-bottom: 50px;
    }
    .otherkey{
        text-align: center;
        margin-top: 50px;
        margin-bottom: 50px;
    }
    b{
        line-height: 50px;
    }
    .remote{
        line-height: 50px;
    }
    .imglinehight{
        margin-top: 10px;
    }
</style>
<!-- ==============================================
Hello Section
=============================================== -->
<div id="pages-features" class="content user" style="margin-top: 100px;">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489"
                                        title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter"
                                       target="_blank"></a></li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113"
                                        title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw"
                                       title="Follow me on youtube" target="_blank"> </a></li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts"
                                           title="Follow me on google plus" target="_blank"></a></li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/"
                                       title="Follow me on blogger" target="_blank"></a></li>
            </ul>
        </div>
        <div id="js-head-menu" class="">
            <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <a class="show hor-space" title="Temps-direct.uk" href="/">
                                <img alt="" src="myassets/img/logo.png">
                            </a>
                        </div>
                        <div class="cantent">
                            <p>
                                Your log-out has been successful.</p>
                            <p>
                                Thank you for using Temps-Direct temp recruiting platform.</p>
                            <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                            <input name="feedback" type="commant">
                            <p>Thank you!</p>
                        </div>
                        <div class="modal-footer hide">
                            <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default"> Close</a>
                        </div>
                    </div>

                </div>
            </div>

            <script type="text/javascript">
                //<![CDATA[
                function getCookie(c_name) {
                    var c_value = document.cookie;
                    var c_start = c_value.indexOf(" " + c_name + "=");
                    if (c_start == -1) {
                        c_start = c_value.indexOf(c_name + "=");
                    }
                    if (c_start == -1) {
                        c_value = null;
                    } else {
                        c_start = c_value.indexOf("=", c_start) + 1;
                        var c_end = c_value.indexOf(";", c_start);
                        if (c_end == -1) {
                            c_end = c_value.length;
                        }
                        c_value = unescape(c_value.substring(c_start, c_end));
                    }
                    return c_value;
                }
                if (getCookie('il')) {
                    (function () {
                        var js = document.createElement('script');
                        js.type = 'text/javascript';
                        js.async = true;
                        js.src = "https://healthcare-temps.co.uk/users/show_header.js?u=";
                        var s = document.getElementById('js-head-menu');
                        s.parentNode.insertBefore(js, s);
                    })();
                } else {
                    document.getElementById('js-head-menu').className = '';
                }
                //]]>
            </script>
        </div>

        <style>
            .bold_text {
                font-weight: bolder;
            }
            .small_bold_text {
                font-weight: bolder;
                padding-left:26px;
            }

        </style>
        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container">
                    <h1 style="text-align: center; color: #0070c0">Privacy Policy</h1>
                    <div class="row">
                        <p>
                            Healthcare-Temps UK (hereafter referred to as “HT”, “we”, “us” or “our”) operates as a
                            healthcare temp staff on-demand hiring platform / marketplace which helps Employers
                            connect directly with idle medical and healthcare workers 24/7/365 via web, mobile and tablet.
                            We are a total automated recruitment platform with the aid of cognitive intelligence and
                            machine learning software.<br />
                            We acknowledge and respect your privacy and attempt to manager the information you
                            provide to us in a private and trustworthy way, protected against hackers etc. Healthcare-
                            Temps UK provides this Privacy Policy to let you know our rules and procedures regarding the
                            collection, use and disclosure of information through www.healthcare-temps.co.uk (the “Site”),
                            and any other websites, features, applications, widgets or online services that are owned or
                            controlled by Healthcare-Temps Inc which is share on this site is protected by this Privacy
                            Policy (together with the Site’s “Services”), as well as any information Healthcare-Temps UK
                            collects offline in connection with the Service. It also describes the choices available to you
                            regarding the use of, your access to, and how to update and correct your personal
                            information.By signing up to Healthcare-Temps, you are accepting the terms of this Privacy
                            Policy.
                        </p>

                        <span class="bold_text">What information does Healthcare-Temps collects gather?</span>
                        <p>
                            1. Information that you give us: We receive, store and process information that you
                            make available to us when accessing or using the Healthcare-Temps Web and Mobile
                            Platform. The following are examples, but are not limited to and when you:
                            1. Fill in any form, such as when you register or update the details of your user
                            account;
                            2. Access or use the web platform, such as to search for or post a Locum session,
                            make or accept an application, post comments or reviews, or communicate with
                            other users; and
                            3. Communicate with Healthcare-Temps uk.<br />

                            <span class="small_bold_text"># Personal Information:</span> In the course of using the Service (whether as an Employer
                            or Temp Worker), we may require or otherwise collect information that identifies you
                            as a specific individual and can be used to contact or identify you (“Personal
                            Information”). Examples of Personal Information include your name, email address,
                            postal address and phone number.<br />

                            <span class="small_bold_text"># Payment Information:</span> If you use the Service to make or receive payments, we will
                            also collect certain payment information, such as credit card, PayPal, Stripe,
                            Braintree or other financial account information, and billing address.<br />
                            <span class="small_bold_text"># Identity Verification:</span> We may collect Personal Information, such as your date of
                            birth DBS, Training certificates, Professional registration number, Passport,
                            References, NINO or unique tax code, to validate your identity, qualification, eligibility
                            to work or as may be required by law, such as to complete tax filings. We may
                            request documents to verify this information, such as a copy of your government-
                            issued identification or photo or a billing statement.<br />

                            <span class="small_bold_text"># General Audience Service:</span> The Service is general audience and intended for users
                            18 and older. We do not knowingly collect Personal Information from anyone younger
                            than age 18. If we become aware that a child younger than 18 has provided us with
                            Personal Information, we will use commercially reasonable efforts to delete such
                            information from our files. If you are the parent or legal guardian of a child younger
                            than age 18 and believe that Temps-Direct.uk has collected Personal Information
                            from your child, please contact us at: legal@healthcare-temsp.co.uk.<br />

                            <span class="small_bold_text"># Non-Identifying Information/Usernames:</span> We also may collect other information,
                            such as post codes, demographic data, information regarding your use of the Service,
                            and general project-related data (“Non-Identifying Information”). We may aggregate
                            information collected from Healthcare-Temps.co.uk registered and non-registered
                            users (“Healthcare-Temps.co.uk”). We consider user names to be Non-Identifying
                            Information. Usernames are made public through the Service and are viewable by
                            other Healthcare-Temps Users.<br />
                            In some cases, we may render Personal Information (generally, email address) into a form of
                            Non-Identifying Information referred to in this Privacy Policy as “Hashed Information.” This is
                            typically accomplished using a mathematical process (commonly known as a hash function)
                            to convert information into a code. The code does not identify you directly, but it may be used
                            to connect your activity and interests.
                        </p>

                        <div class="side_para">
                            <p>
                                In some cases, we may render Personal Information (generally, email address) into a form of
                                Non-Identifying Information referred to in this Privacy Policy as “Hashed Information.” This is
                                typically accomplished using a mathematical process (commonly known as a hash function)
                                to convert information into a code. The code does not identify you directly, but it may be used
                                to connect your activity and interests.<br />
                            </p>
                        </div>
                        <p>
                            <span class="bold_text"># Collection of the Third Party Personal Information:</span> We collect the following
                            personal information from you about your contacts or friends: First, last name and
                            email address when you provide it to us for the purpose of adding your contacts to a
                            message room.<br />
                        </p>

                        <div class="bold_text">Information Received from Third Parties</div>
                        <p>
                            We also may receive information about you from third parties. For example, we may
                            supplement the information we collect with outside records or third parties may provide<br />

                            information in connection with a co-marketing agreement or at your request (such as if you
                            choose to sign in with a third-party service). If we combine the information we receive from
                            others with information we collect through the Service, we will treat the combined information
                            as described in this Privacy Policy.
                        </p>

                        <div class="bold_text">Information Collected from Users Automatically</div>
                        <p>
                            We and our third party service providers, including analytics and third party content providers,
                            may automatically collect certain information from you whenever you access or interact with
                            the Service. This information may include, among other information, the browser and
                            operating system you are using, the URL or advertisement that referred you to the Service,
                            the search terms you entered into a search engine that led you to the Service, areas within
                            the Service that you visited, and other information commonly shared when browsers
                            communicate with websites. We may combine this automatically collected log information with
                            other information we collect about you. We do this to improve services we offer you, to
                            improve marketing, analytics, and site functionality.<br />

                            The information we collect also includes the Internet Protocol (“IP”) address or other unique
                            device identifier (“Device Identifier”) for any device (computer, mobile phone, tablet, etc.)
                            used to access the Service. A Device Identifier is a number that is automatically assigned or
                            connected to the device you use to access the Service, and our servers identify your device
                            by its Device Identifier. Some mobile service providers may also provide us or our third party
                            service providers with information regarding the physical location of the device used to access
                            the Service.
                        </p>
                        <div class="bold_text">The technology used to collect information automatically from Healthcare-Temps
                            Users may include the following:</div>
                        <p>
                            #<span class="small_bold_text"> Cookies:</span> Like many websites, we and our marketing partners, affiliates, analytics,
                            and service providers use “cookies” to collect information. A cookie is a small data file
                            that we transfer to your computer’s hard disk for record-keeping purposes. We use
                            both persistent cookies that remain on your computer or similar device (such as to
                            save your registration ID and login password for future logins to the Service and to
                            track your compliance with the Healthcare-Temps Terms of Service) and session ID
                            cookies, which expire at the end of your browser session (for example, to enable
                            certain features of the Service, to better understand how Healthcare-Temps Users
                            interact with the Service and to monitor aggregate usage by Healthcare-Temps Users
                            and web traffic routing on the Service). You may be able to instruct your browser, by
                            changing its options, to stop accepting cookies or to prompt you before accepting a<br />

                            cookie from the websites you visit. If you do not accept cookies, however, you may
                            not be able to use all functionality of the Service.
                            We use Local Storage, such as HTML5, to store content information and preferences. Third
                            parties with whom we partner to provide certain features on our website or to display
                            advertising based upon your web browsing activity also use HTML5 to collect and store
                            information. Various browsers may offer their own management tools for removing HTML5.
                        </p>
                    </div>

                    <div class="bold_text">
                        How does Healthcare-Temp use and process your personal information that is made
                        available by you
                    </div>
                    <p>
                        We use and process Information about you for the following general purposes:
                        1. to enable you to access and use the Platform;
                        2. to operate, protect and improve the Platform, Healthcare-Temp’s business, and our users’
                        experience, such as to perform analytics, conduct research, and for advertising and
                        marketing;
                        3. to help create and maintain a trusted and safe environment on the Platform, such as fraud
                        detection and prevention, verifying any identifications provided by you, and conducting checks
                        against databases such as public government databases;
                        4. to send you service, support and administrative messages, reminders, and information
                        requested by you;
                        5. to administer rewards, surveys, or other promotional activities or events sponsored or
                        managed by Healthcare-Temps or our business partners; and
                        6. to comply with our legal obligations.
                    </p>

                    <p>
                        Work Room and Work View As part of the Service, we collect information about a Temp
                        Worker’s work on a job for an Employer. This feature is known as Work Review Room. Work
                        Rooms include information provided by the Temp Worker, such as a care plan, as well as
                        automatically gathered information about the duties carried out on the job, such as, patient
                        records update, administering medication, personal hygiene, xrays, and regularly taken
                        screenshots (which may include photo and video). In order to use Work Rooms, you must
                        download and install the Team App software from www.healthcare-temps.co.uk.<br />

                        We will share information contained in Work Rooms with the relevant Employer and with any
                        manager or administrator of any applicable Government or medical Agency. We inform Temp
                        Workers each time we capture information for Work Roomy. As set forth in our Terms of
                        Service, End User License Agreement, and help pages on the Site, a Temp Worker may
                        choose to block or delete the screen shot and associated data for a recorded work interval,
                        but then the Temp Worker may not be paid for that work interval. Healthcare-Temps may use
                        general information from Work Rooms for statistical analysis, product development, marketing
                        and research.
                    </p>
                    <p>
                        Temp Profiles You may have the opportunity to create a Temp CV profile, which consists of
                        information about you, and may include Personal Information, photographs, examples of your
                        work history, education, information on work previously performed via the Service and outside
                        the Service, skills, hourly pay rates, feedback/rating information and other information,
                        including your username (“Profile”). The information in your Profile may be visible to all
                        Healthcare-Temps’ Users and the general public subject to the privacy choices you make
                        within your Healthcare-Temps’ Profile. You may edit certain information in your Profile via your
                        account and may choose to limit who is able to view certain content you post to your Profile.
                        Clients and Agencies of associated individual users or companies may also have the
                        opportunity to create an organization Profile. If, in any case, you believe that an unauthorized
                        profile has been created about you, you can request for it to be removed by contacting us
                        at https://healthcare-temps.co.uk/page/tech- help.
                    </p>

                    <span class="bold_text">Feedback</span>

                    <p>
                        We collect feedback from Healthcare-Temps’ Users about their experience with other
                        Healthcare-Temps’ Users of our Service. Please note that any feedback you provide via the
                        Service or feedback provided about you is publicly viewable via the Service. On very rare
                        occasions, we may remove feedback pursuant to the relevant provisions of our Terms of
                        Service, including the Terms of Use.
                    </p>

                    <span class="bold_text">Email to Friends and Referral Program</span>

                    <p>
                        Healthcare-Temps uk lets you send project postings to friends via email or social media.
                        Healthcare-Temps uk also offers the ability to send friends emails about providing or
                        purchasing services through the Service. If you choose to use either feature, your friend’s
                        email address will be used to send the requested posting and your email address will be used
                        to copy you on the message to your friend or to send the message on your behalf.
                        Healthcare-Temps stores this information for the sole purpose of sending this one-time email
                        and tracking the success of our referral program.<br />

                        Your friend may contact us at support Healthcare-Temps to request that we remove this
                        information from our database.
                    </p>

                    <span class="bold_text">Social Networking Services</span>

                    <p>You may register to join the Service directly via the Service or by logging into your account
                        with a third party social networking service (“SNS”) via our Service (e.g., Facebook, LinkedIn,

                        Google+ and other third party services that let you sign in using your existing credentials with
                        those services). If you choose to register via an SNS, or to later link your account with the
                        Service to your account with an SNS, we will use the Personal Information you have provided
                        to the SNS (such as your name, email address, gender and other information you make
                        publicly available via the SNS) to create your account. Note that the information we collect
                        from and through an SNS may depend on the privacy settings you have set with the SNS and
                        the permissions you grant to us in connection with linking your account with the Service to
                        your account with an SNS. Other than what we may share with the SNS as described below,
                        the Personal Information an SNS has about you is obtained by the SNS independent of our
                        Service, and Healthcare-Temps is not responsible for it.<br />

                        The Healthcare-Temps Service also may permit additional interactions between it and a third
                        party website, service, or other content provider, such as enabling you to “like” or share
                        content to a third party SNS. If you choose to “like” or share content, or to otherwise share
                        information from or via our Service with a third party site or service, that information may be
                        publicly displayed, and the third party may have access to information about you and your use
                        of our Service (and we may have access to information about you from that third party).
                        These features may collect your IP address, which page you are visiting on our site, and may
                        set a cookie to enable the feature to function properly. Your interactions with third parties
                        through an SNS or similar features are governed by the respective privacy policies of those
                        third parties.<br />

                        You represent that you are entitled to use your SNS account for the purposes described
                        herein without breach by you of any of the terms and conditions that govern the SNS, and
                        without obligating Healthcare-Temps UK to pay any fees or making Healthcare-Temps UK
                        subject to any usage limitations imposed by such SNS. You can disable the link between your
                        Healthcare-Temps UK account and your SNS account at any time though the “Settings”
                        section of our Service. Please note that your relationship with any SNS is governed solely by
                        your agreement(s) with such SNS. If your SNS account terminates, then functions enabled
                        through the link between your Temps-Direct.uk account and your SNS account will terminate
                        as well.
                    </p>

                    <p>
                        <span class="small_bold_text"># Service Providers:</span> We may employ third party companies and individuals to
                        facilitate our Service, to provide the Service on our behalf, to perform Service-related
                        services (e.g., without limitation, maintenance services, database management, web
                        analytics and online advertising, payment processing, fraud detection and
                        improvement of Healthcare-Temps’ features) or to assist us in analyzing how our
                        Service is used. These third parties may have access to your Personal Information in
                        order to perform these tasks on our behalf.

                        <span class="small_bold_text"># What Happens If You Agree to Receive Information From Third Parties or
        Request That We Share Your Information:</span>
                        # You may be presented with an opportunity to receive information and/or marketing
                        offers from one or more third parties. If you agree at that time to have your Personal
                        Information shared, your Personal Information will be disclosed to that third party (or
                        parties) and will be subject to the privacy policy and practices of that third party. We
                        are not responsible for the privacy policies and practices of third parties, and, if you
                        later decide that you no longer want to receive communications from a third party,
                        you will need to contact that third party directly. You also may request, sometimes
                        through your use of an SNS or similar interactive feature or third party application,
                        that we share information about you with a third party and we will typically do so
                        under those circumstances.
                    </p>

                    <span class="bold_text">Legal and Investigative Purposes:</span>
                    <p>
                        Healthcare-Temps UK will share information with government agencies as required by law,
                        including without limitation, in connection with reporting earnings. We cooperate with
                        government and law enforcement officials and private parties to enforce and comply with the
                        law. We will disclose information about you to government or law enforcement officials or
                        private parties as we, in our sole discretion, believe necessary or appropriate to respond to
                        claims and legal process (including but not limited to subpoenas), at the request of
                        governmental authorities or other third parties conducting an investigation, to protect the
                        property and rights of Healthcare-Temps.uk or a third party, to protect the safety of the public
                        or any person, or to prevent or stop activity we may consider to be, or to pose a risk of being,
                        illegal, fraudulent, unethical or legally actionable activity. We may also use Device Identifiers
                        to identify Healthcare-Temps.uk Users, and may do so in cooperation with third parties at our
                        discretion.<br />

                        <span class="bold_text">YOUR CHOICES AND OPTING OUT</span>
                        <br>
                        Registered Healthcare-Temps.uk Users may update their choices regarding the types of
                        communications you receive from us through your online account. You also may opt-out of
                        receiving marketing emails from us by following the opt-out instructions provided in those
                        emails. Please note that we reserve the right to send you certain communications relating to
                        your account or use of the Service (for example, administrative and service announcements)
                        via email and other means and these transactional account messages may be unaffected if
                        you opt-out from receiving marketing communications. You may opt-out of receiving text
                        messages by replying “STOP” to any text message received. Registered Healthcare-Temps
                        uk Users who access the Service by using an Healthcare-Temps uk mobile application may,
                        with permission, receive push notifications. Similarly, registered Healthcare-Temps uk Users
                        who access the Service by using certain desktop browsers may, with permission, receive

                        push notifications. Notification preferences can be modified in the settings menu for the
                        mobile application or the applicable browser.
                        SECURITY

                        Healthcare-Temps.uk takes commercially reasonable steps to help protect and secure the
                        information it collects and stores about Healthcare-Temps.uk Users. All access to the Site is
                        encrypted using industry-standard transport layer security technology (TLS). When you enter
                        sensitive information (such as tax identification number), we encrypt the transmission of that
                        information using secure socket layer technology (SSL). We also use HTTP strict transport
                        security to add an additional layer of protection for our Healthcare-Temps.uk Users. But
                        remember that no method of transmission over the Internet, or method of electronic storage,
                        is 100% secure. Thus, while we strive to protect your personal data, Healthcare-Temps.uk
                        cannot ensure and does not warrant the security of any information you transmit to us.
                        For other requests, please contact us at support@temps-direct.uk or the following
                        address:<br /><br />
                        Healthcare-Temps.uk<br />
                        152 City Road</p>bnaraswa<br />
                        London<br />
                        EC1V 2NX<br />
                        CONTACT US
                    </p>
                </div>
                <a href="">
                </a>
            </div>
            <a href="">
            </a></div>
        <a href="">
            <!-- for modal -->
        </a>
        <div class="modal hide fade" id="js-ajax-modal"><a href="">
                <div class="modal-body"></div>
            </a>
            <div class="modal-footer"><a href=""></a><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a>
            </div>
        </div>
        <!-- for modal -->
        <!-- for modal -->
        <div class="modal hide fade" id="js-ajax-modal-child">
            <div class="modal-body"></div>
            <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a></div>
        </div>
        <!-- for modal -->

        <div class="clearfix"></div>
        <!-- for modal
        <div class="footer-push"></div> -->
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php //include ('includes/template/footer.php'); ?>

<a id="scrollup">Scroll</a>

<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Kafe JS -->
<script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
