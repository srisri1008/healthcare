<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php';	
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('includes/template/header.php'); ?> 

<body class="greybg">
	
     <!-- Include navigation.php. Contains navigation content. -->
 	 <?php include ('includes/template/navigation.php'); ?> 
 	 
      <!-- ==============================================
	 Header
	 =============================================== -->
     <div id="pages-how-it-works" class="content user">
         <div class="wrapper">
             <div class="social_icn">
                 <ul class="unstyled clearfix pull-right media-icons">
                     <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                     <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                     <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                     <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                     <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                     <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
                 </ul>
             </div>
             <div id="js-head-menu" class="">
                 <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                     <div class="modal-dialog">

                         <!-- Modal content-->
                         <div class="modal-content">
                             <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">×</button>
                                 <a class="show hor-space" title="Temps-direct.uk" href="/">
                                     <img alt="" src="https://healthcare-temps.co.uk/img/logo.png">
                                 </a>
                             </div>
                             <div class="cantent">
                                 <p>
                                     Your log-out has been successful.</p>
                                 <p>
                                     Thank you for using Temps-Direct temp recruiting platform.</p>
                                 <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                                 <input name="feedback" type="text">
                                 <p>Thank you!</p>
                             </div>
                             <div class="modal-footer hide">
                                 <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default">  Close</a>
                             </div>
                         </div>

                     </div>
                 </div>
                 <header id="header">
                     <div class="navbar no-mar pr z-top">
                         <div class="navigation-wrapper">
                             <div class="navbar-inner no-pad no-round">
                                 <div class="container clearfix">
                                     <h1 class="brand top-smspace"> <a href="/" title="healthcare-temps.co.uk" class="show hor-space"><img src="https://healthcare-temps.co.uk/img/logo.png" alt=""></a></h1>
                                     <a class="btn btn-navbar mspace js-no-pjax" data-toggle="collapse" data-target=".nav-collapse"> <i class="icon-align-justify icon-24 whitec"></i></a>
                                     <div class="nav-collapse">
                                         <ul class="nav dc">
                                             <!--li class=" divider-left divider-right"> <a href="/page/employer" class="js-no-pjax bootstro" data-bootstro-step="1" data-bootstro-title="Employer?" data-bootstro-placement="bottom"> <i class="icon-briefcase text-20 no-pad"></i> <span class="show top-mspace">Employer?</span></a>
                             </li>
                                             <li class=" divider-left divider-right">
                               <a href="/page/freelancer" class="bootstro" data-bootstro-step="2" data-bootstro-title="Freelancer?" data-bootstro-placement="bottom" title="Freelancer?"><i class="icon-male text-20 no-pad"></i> <span class="show top-mspace">Freelancer?</span></a>				</li-->

                                         </ul>
                                         <div class="pull-right mob-clr tab-clr dc">
                                             <ul class="nav nav-right dc">
                                                 <li class="divider-left divider-right tab-sep-none "><a href="/users/register/manual" title="Register">Register</a></li>

                                                 <li class="divider-left tab-sep-none divider-right "><a href="/users/login" title="Login">Login</a></li>
                                                 <li class=" divider-left tab-sep-none divider-right">
                                                     <a href="/page/how-it-works" title="How it Works">How it Works</a>
                                                 </li>
                                                 <li class="divider-right tab-sep-none">
                                                     <form class="form-search bot-mspace mob-searchform">
                                                         <div class="input select">
                                                             <select id="search_type_id" name="search_type_id">
                                                                 <option value="0">Portfolios</option>
                                                                 <option value="1">Jobs</option>
                                                                 <option value="2">Freelance Projects</option>
                                                                 <option value="3">Freelancers</option>
                                                                 <option value="4">Services</option>
                                                             </select>
                                                         </div>
                                                         <div class="input text">
                                                             <label class="hide" for="search">Search</label>
                                                             <input id="search" placeholder="Search" name="q" class="span18" type="search">
                                                             <label class="hide" for="Search">Search</label>
                                                             <input value="Search" title="Search" class="btn" id="Search" type="submit">
                                                         </div>
                                                     </form>
                                                     <a id="trigger-search" href="#" class="js-no-pjax" title="Search"><i class="icon-search text-20"></i></a>
                                                     <!--?php// } ?-->
                                                 </li>
                                                 <li class="divider-left tab-sep-none ">
                                                     <a href="/" title="Home">Home</a>
                                                 </li>
                                                 <li class="divider-left tab-sep-none ">
                                                     <a href="#"><span style="display: block;position: absolute;margin-left: 22px;">0800-368-8769</span><i class="fa fa-phone" aria-hidden="true" style="font-size: 30px;"></i></a>
                                                 </li>

                                             </ul>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div id="nav-search">
                             <div class="search-input-container">
                                 <form action="/users" class="form-search blue-btn-bg bot-mspace home-search-form clearfix {&quot;user_url&quot;:&quot;/users&quot;}" id="UserViewForm" method="get" accept-charset="utf-8"><div style="display:none;"><input name="_Token" value="49033fb435e00b71e1b384d6f208e7ffac46560e" id="Token1509966075" type="hidden"></div><div class="input select"><select name="search_type_id" id="search_type_id">
                                             <option value="3">Temps</option>
                                             <option value="2">Temp Jobs</option>
                                         </select></div><div class="input text right-space"><label for="UserQ">Location</label><input name="q" id="UserQ" type="text"></div><div class="submit"><input value="Go" type="submit"></div><div style="display:none;"><input name="_Token" value="2b7195b6dfe8c13a4d606cf90df5de63477dad34%3A" id="TokenFields1538408921" type="hidden"><input name="_Token" value="Page.Add%7CPage.Preview%7CPage.Update%7CPage.content%7CPage.description_meta_tag%7CPage.id%7CPage.meta_keywords%7CPage.parent_id%7CPage.slug%7CPage.status_option_id%7CPage.title%7C_wysihtml5_mode" id="TokenUnlocked472114817" type="hidden"></div></form>	  </div>
                         </div>
                     </div>
                 </header>

             </div>



             <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
             <div id="pjax-body">
                 <div id="main" class="main">
                     <div class="container">
                         <style>
                             .img-responsive {
                                 margin: 29px auto;
                                 width: 70%;
                             }
                         </style>
                         <section class="row ver-space ver-smspace">
                             <div class="page">
                                 <h2 class="tab-head bot-mspace text-16 clearfix green-head-bg">How it Works</h2>
                                 <!-- <h3 class="grayc">Projects</h3> -->
                                 <div class="js-pagination clearfix round-5 project-chart-block">
                                     <div class="how-it-works-td-top">
                                         <div class="howitworktop">
                                             Hire Healthcare-Professionals-24/7/365
                                         </div>
                                     </div>
                                 </div>
                                 <div class="bs-example">
                                     <ul class="nav nav-tabs">
                                         <li class=""><a data-toggle="tab" href="#sectionA" id="seca" style="color: rgb(0, 0, 0);">How it works for Hirers</a></li>
                                         <li class="active"><a data-toggle="tab" href="#sectionB" id="secb">How it works for Temps</a></li>
                                         <li class=""><a data-toggle="tab" href="#sectionC" id="secc">FAQs</a></li>

                                     </ul>
                                     <div class="tab-content">
                                         <div id="sectionA" class="tab-pane fade">
                                             <a href="/register.php"><img src="myassets/img/how-it-works-td-02.jpg" alt="how it works" class="img-responsive"></a>
                                         </div>
                                         <div id="sectionB" class="tab-pane fade active in">

                                             <a href="/register.php"><img src="myassets/img/how-it-works-td-01.jpg" alt="how it works" class="img-responsive"> </a>
                                         </div>
                                         <div id="sectionC" class="tab-pane fade">
                                             FAQs
                                         </div>

                                     </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-lg-offset-3 col-lg-8">
                                         <iframe width="500" height="275" src="https://www.youtube.com/embed/"></iframe>
                                     </div>
                                 </div>
                                 <style type="text/css">
                                     .page:before {
                                         content: "";
                                         width: 100%;
                                         height: 100%;
                                         display: table-column !important;
                                         border-bottom: 4px solid #e0e0e0;
                                         padding-bottom: 10px;
                                     }
                                     .img-responsive01{
                                         max-width:100%;
                                     }
                                     .mtop{
                                         margin:20px 0px;
                                     }
                                     .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
                                         color: #60B947;
                                     }
                                     .nav > li > a {
                                         color: #000;
                                     }
                                 </style>
                             </div>
                         </section>

                     </div>
                 </div>
             </div>
             <!-- for modal -->
             <div class="modal hide fade" id="js-ajax-modal">
                 <div class="modal-body"></div>
                 <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a> </div>
             </div>
             <!-- for modal -->
             <!-- for modal -->
             <div class="modal hide fade" id="js-ajax-modal-child">
                 <div class="modal-body"></div>
                 <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a> </div>
             </div>
             <!-- for modal -->

             <div class="clearfix"></div>
             <!-- for modal
             <div class="footer-push"></div> -->
         </div>
         <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
     </div>

     <!-- Include footer.php. Contains footer content. -->
     <?php include ('includes/template/footer.php'); ?>

</body>
</html>
