<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    .maincontainer {
        margin-top: 80px;
    }
    .maincontainer > div {
        margin: 30px auto;
    }
    .mainheading {
        text-align: center;
    }
    .mainheading span {
        color: #00b300;
        font-size: 32px;
    }
    .iconcontainer > div {
        text-align: center;
        border: 1px solid lightgrey;
    }
    .iconcontainer h3 {
        color: #00b300;
    }
    .iconcontainer img {
        height: 100px;
    }
</style>
<div class="container-fluid maincontainer">
    <div class="mainheading">
        <h2 style="color: #00b300">Need a Healthcare professional to provide Private Care<br />at home, office or on holidays?</h2>
        <h2>Just <a href="/login.php"><span>log-in</span></a>, select interview & hire!</h2>
    </div>
    <div class="row" style="width: 80%;margin: 0 auto;">
        <div class="col-lg-6">
            <img src="myassets/images/computer_pc.png" alt="Doctors" style="width: 100%">
        </div>
        <div class="col-lg-6">
            <h3 style=" font-size: 20px;line-height: 26px;margin-top:-10px;font-weight: bold; color: #00b300">Empowering out-patients via delivery</h3>
            <h3>Bespoke healthcare delivered to you, regardless of location</h3>
            <p style="font-size: 18px;white-space:nowrap;">+Medical & Healthcare professionals come out to you<br />
                +Schedule bookings 24/7 via web, mobile, tablet<br />
                +Hourly, daily, weekly or monthly shift bookings<br />
                +Absolute transparency, security or no hidden fees<br />
                +Secure payment processed online only<br />
                +Choose who you want, when and where<br />
                +Receive care at home, at school, at work or on home</p>
        </div>
    </div>
    <div style="position:relative;"><img src="myassets/images/map.png" alt="Maps" style="width: 60%;margin: 0 auto;text-align: center;margin-left: 10%;">

        <div style="display:inline-block;white-space:  nowrap;position:  absolute;z-index:  999;top: 50px;left: 290px;background-color:  #fff;padding:  5px;">
            <select name="country" id="findTemps" class="" style="
    float:  left;width:auto;
">
                <option value="">Location</option>
                <option value="Aberdeen">Aberdeen</option>
                <option value="Ashford, Kent">Ashford, Kent</option>
                <option value="Aylesbury">Aylesbury</option>
                <option value="Aldershot">Aldershot</option>
                <option value="Arnold, Nottinghamshire">Arnold, Nottinghamshire</option>
                <option value="Birmingham">Birmingham</option>
                <option value="Belfast">Belfast</option>
                <option value="Brighton">Brighton</option>
                <option value="Bristol">Bristol</option>
                <option value="Bradford">Bradford</option>
                <option value="Blackburn">Blackburn</option>
                <option value="Blackpool">Blackpool</option>
                <option value="Burley">Burley</option>
                <option value="Cardiff">Cardiff</option>
                <option value="Cambridge">Cambridge</option>
                <option value="Chelmsford">Chelmsford</option>
                <option value="Chelthenham">Chelthenham</option>
                <option value="Chesterfield">Chesterfield</option>
                <option value="Cornwall">Cornwall</option>
                <option value="Coventry">Coventry</option>
                <option value="Colchester">Colchester</option>
                <option value="Darlington">Darlington</option>
                <option value="Dartford">Dartford</option>
                <option value="Derby">Derby</option>
                <option value="Devon">Devon</option>
                <option value="Doncaster">Doncaster</option>
                <option value="Dorset">Dorset</option>
                <option value="Dudley">Dudley</option>
                <option value="Dundee">Dundee</option>
                <option value="Durham">Durham</option>
                <option value="Eastbourne">Eastbourne</option>
                <option value="Eastleigh">Eastleigh</option>
                <option value="East Kilbridge">East Kilbridge</option>
                <option value="Exeter">Exeter</option>
                <option value="Essex">Essex</option>
                <option value="Fareham">Fareham</option>
                <option value="Farnborough">Farnborough</option>
                <option value="Fleet">Fleet</option>
                <option value="Fleetwood">Fleetwood</option>
                <option value="Folkestone">Folkestone</option>
                <option value="Glasglow">Glasglow</option>
                <option value="Gloucester">Gloucester</option>
                <option value="Gravesend">Gravesend</option>
                <option value="Grays">Grays</option>
                <option value="Grantham">Grantham</option>
                <option value="Greasby">Greasby</option>
                <option value="Hull">Hull</option>
                <option value="Huddersfield">Huddersfield</option>
                <option value="Halesowen">Halesowen</option>
                <option value="Halifax">Halifax</option>
                <option value="Hereford">Hereford</option>
                <option value="Ipswich">Ipswich</option>
                <option value="Irvine">Irvine</option>
                <option value="Ilkeston">Ilkeston</option>
                <option value="Inverness">Inverness</option>
                <option value="Irlam">Irlam</option>
                <option value="Ingoldmells">Ingoldmells</option>
                <option value="Jonstone">Jonstone</option>
                <option value="Kettering">Kettering</option>
                <option value="Kiddminster">Kiddminster</option>
                <option value="Kingswood">Kingswood</option>
                <option value="Keighley">Keighley</option>
                <option value="Kendal">Kendal</option>
                <option value="Kingston">Kingston</option>
                <option value="Lancaster">Lancaster</option>
                <option value="London East">London East</option>
                <option value="London North">London North</option>
                <option value="London South">London South</option>
                <option value="London West">London West</option>
                <option value="London Greater">London Greater</option>
                <option value="Leeds">Leeds</option>
                <option value="Leicester">Leicester</option>
                <option value="Liverpool">Liverpool</option>
                <option value="Lincoln">Lincoln</option>
                <option value="Livingston">Livingston</option>
                <option value="Littlehampton">Littlehampton</option>
                <option value="Luton">Luton</option>
                <option value="Machester">Machester</option>
                <option value="Macclesfield">Macclesfield</option>
                <option value="Maidenhead">Maidenhead</option>
                <option value="Mansfield">Mansfield</option>
                <option value="Maidstone">Maidstone</option>
                <option value="Middlesborough">Middlesborough</option>
                <option value="Milton Keynes">Milton Keynes</option>
                <option value="Middlewich">Middlewich</option>
                <option value="Morley">Morley</option>
                <option value="Newcastle">Newcastle</option>
                <option value="Newport">Newport</option>
                <option value="Northampton">Northampton</option>
                <option value="Norwhich">Norwhich</option>
                <option value="Nottingham">Nottingham</option>
                <option value="Nuneaton">Nuneaton</option>
                <option value="Nuffield">Nuffield</option>
                <option value="Oldham">Oldham</option>
                <option value="Oadby">Oadby</option>
                <option value="Oakdale">Oakdale</option>
                <option value="Oakham">Oakham</option>
                <option value="Oxford">Oxford</option>
                <option value="Perth, Scotlan">Perth, Scotland</option>
                <option value="Peterborough">Peterborough</option>
                <option value="Peterlee">Peterlee</option>
                <option value="Poole">Poole</option>
                <option value="Portsmouth">Portsmouth</option>
                <option value="Plymouth">Plymouth</option>
                <option value="Preston">Preston</option>
                <option value="Paignton">Paignton</option>
                <option value="Reading">Reading</option>
                <option value="Redhill">Redhill</option>
                <option value="Redditch">Redditch</option>
                <option value="Rotherham">Rotherham</option>
                <option value="Rochdale">Rochdale</option>
                <option value="Sheffield">Sheffield</option>
                <option value="Slough">Slough</option>
                <option value="Smethwick">Smethwick</option>
                <option value="Solihul">Solihul</option>
                <option value="Southampton">Southampton</option>
                <option value="Stafford">Stafford</option>
                <option value="St Albans">St Albans</option>
                <option value="St Helens">St Helens</option>
                <option value="Stockport">Stockport</option>
                <option value="Stevenage">Stevenage</option>
                <option value="Sunderland">Sunderland</option>
                <option value="Swansea">Swansea</option>
                <option value="Tamworth">Tamworth</option>
                <option value="Taunton">Taunton</option>
                <option value="Telford">Telford</option>
                <option value="Torquay">Torquay</option>
                <option value="Tonbridge">Tonbridge</option>
                <option value="Uckfield">Uckfield</option>
                <option value="Underwood">Underwood</option>
                <option value="Wakefield">Wakefield</option>
                <option value="Warrington">Warrington</option>
                <option value="Watford">Watford</option>
                <option value="Walsall">Walsall</option>
                <option value="West Bromwich">West Bromwich</option>
                <option value="Weston Super-Mere">Weston Super-Mere</option>
                <option value="Wigan">Wigan</option>
                <option value="Wolsley">Wolsley</option>
                <option value="Wolverhampton">Wolverhampton</option>
                <option value="Wolverton">Wolverton</option>
                <option value="Woking, Surrey">Woking, Surrey</option>
                <option value="Worthing">Worthing</option>
                <option value="Worchester">Worchester</option>
                <option value="Yapton">Yapton</option>
                <option value="Yately">Yately</option>
                <option value="Yeovil">Yeovil</option>
                <option value="York">York</option>
            </select>
            <input type="button" style="
    border: 1px solid green;
    color:  #fff;
    background-color:  green;
    padding: 7px 20px;
    float:  left;
    margin-left: 5px !important;
" value="Find Temp">
        </div>
    </div>
    <div class="row" style="background-color: #1FB14C;width: 90%;margin: 30px auto;text-align:center;">
        <h3 style="margin-left: 20px; color: white">Type of Call-Out Care</h3>
    </div>
    <div class="row iconcontainer" style="width:60%">
        <div class="col-lg-6">
            <img src="myassets/images/bimar.png" alt="Sick man">
            <h3>Out-Patient Care</h3>
        </div>
        <div class="col-lg-6">
            <img src="myassets/images/human_brain.png" alt="Human brain">
            <h3>Stroke Recovery Care</h3>
        </div>
        <div class="col-lg-6">
            <img src="myassets/images/aba_support.png" alt="ABA Child Support">
            <h3>ABA Child Support</h3>
        </div>
        <div class="col-lg-6">
            <img src="myassets/images/tikka.png" alt="Syringe">
            <h3>Diabetese/Insulin Care</h3>
        </div>
        <div class="col-lg-6">
            <img src="myassets/images/ser.png" alt="Serious Medication">
            <h3>Serious Medication</h3>
        </div>
        <div class="col-lg-6">
            <img src="myassets/images/diabetese.png" alt="Diabetese/Insulin Care">
            <h3>Diabetese/Insulin Care</h3>
        </div>
    </div>
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

</body>
</html>
