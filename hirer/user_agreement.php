<?php 
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
  Redirect::to('../index.php');	
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>
<!-- AdminLTE CSS -->
<link href="../assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- Datetime Picker -->
<link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery UI CSS -->
<link href="../assets/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
<!-- Progress CSS -->
<link href="../assets/css/progress.css" rel="stylesheet" type="text/css" />

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>

<script src="../assets/js/jquery.knob.min.js"></script>


<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <!-- Include currency.php. Contains header content. -->
            <?php include ('template/currency.php'); ?>

            <div id="sendtoid" style="display: none"></div>
            <div class="row">
                <div class="col-lg-11">
                    <div style="text-align: center; color: #0070c0; margin-bottom: 30px">
                        <h2>User Agreement</h2>
                    </div>
                    <p>
                        This Agreement was last modified on 1st May 2018.
                        This User Agreement describes the terms and conditions which you accept by using our Web &amp; Mobile application or our
                        Services. We have incorporated by reference some linked information.
                    </p>

                    <h3>In this User Agreement:</h3>
                    <p>
                        <b>&quot;Account&quot;</b> means the account associated with your email address or log-in name.<br />
                        <b>&quot;Buyer&quot;</b> means a Hirer or the user who purchases a service or item from Sellers or Temp Workers or service provider
                        through this Website. A User may be both a Buyer and a Seller under this agreement.<br />
                        <b>&quot;Dispute Resolution Process&quot;</b> means the process to be followed by Buyers and Sellers in accordance with the Dispute
                        Resolution Policy.<br />
                        <b>&quot;Temp Worker Verified&quot;</b> Sellers/Service Providers have been satisfactorily vetted and verified under the Know your
                        Customer and Identity, CRB checks, work references and PIN Verification.<br />
                        <b>&quot;Inactive Account&quot;</b> means a User Account that has not been logged into for a 6 months period, or other period determined
                        by us from time to time.<br />
                        <b>&quot;Local Job&quot; or &quot;</b>Local Jobs&quot; means a service we provide to match a Buyer with a Seller in relation to the provision of
                        location specific services.<br />
                        <b>&quot;Job &quot;Listing&quot;</b> means a job posted by a Buyer via the Website, which may include a Project or temporary employment
                        opportunity listed by a Buyer via the Website.<br />
                        <b>&quot;Seller&quot;</b> means a User that offers and provides services/skills/consultancy or identifies as a Seller through the Website. A
                        User may be both a Buyer and a Seller under this agreement.<br />
                        <b>&quot;Seller Services&quot;</b> means all services provided by a Seller.<br />
                        <b>&quot;User&quot;, &quot;you&quot;</b> or &quot;your&quot; means an individual who visits or uses the Website, including via the API.
                    </p>
                    <h3>1. Overview</h3>
                    <p>
                        By accessing the Website, you agree to the following terms with healthare-Temps UK.
                        We may amend this User Agreement and any linked information from time to time by posting amended terms on the
                        Website, without notice to you.
                        This Website is an online automated healthcare temp/locum hiring platform where Users buy and sell the Seller’s / Service
                        provider services. Buyers and Sellers must register for an Account in order to buy or sell Services and/or items. The
                        Website enables Users to work together online to complete and pay for jobs/tasks. We are not a party to any contractual
                        agreements between Buyer and Seller in this online venue, we merely facilitate connections between the parties.
                        We may, from time to time, and without notice, change or add to the Website or the information, products or services
                        described in it. However, we do not undertake to keep the Website updated. We are not liable to you or anyone else if any
                        error occurs in the information on the Website or if that information is not current.
                    </p>
                    <h3>2. Scope</h3>
                    <p>
                        Before using the Website, you must read the whole User Agreement, the Website policies and all linked information.

                        You must read and accept all of the terms in, and linked to, this User Agreement, the Terms of Service, the temp worker
                        Privacy Policy and all Website policies. By accepting this User Agreement as you access our Website, you agree that this
                        User Agreement will apply whenever you use the Website, or when you use the tools we make available to interact with
                        the Website. Some Websites may have additional or other terms that we provide to you when you use those services.
                    </p>
                    <h3>3. Eligibility</h3>
                    <p>
                        You will not use the Website if you:
                        1. are not able to form legally binding contracts;
                        2. are under the age of 17;
                        3. a person barred from receiving and rendering services under the laws of United Kingdom;
                        4. are suspended from using the Website; or
                        5. do not hold a valid email address (or CRB if you are a service provider.
                        All free user accounts are associated with individuals. Login credentials should not be shared by users with others. The
                        individual associated with the account will be held responsible for all actions taken by the account, without limitation.
                        Users may provide a business name or a company name, which is associated with the User&#39;s Account. Users acknowledge
                        and agree that where a business name or company name is associated with their Account, this User Agreement is a
                        contract with the User as an individual (not the business or company) and Users remain solely responsible for all activity
                        undertaken in respect of their Account.
                        A company, corporation, trust, partnership or other non-individual corporate entity may be a User subject to an eligible
                        corporate account which pays corporate subscriptions.
                        We may, at our absolute discretion, refuse to register any person or entity as a User.
                        You cannot transfer or assign any rights or obligations you have under this agreement without prior written consent.
                    </p>
                    <h3>4. Using Healthcare-Temps UK</h3>
                    <p>
                        While using the Website, you will not attempt to or otherwise do any of the following:
                        1. post content or items in inappropriate categories or areas on our Websites and services;
                        2. infringe any laws, third party rights or our policies, such as the Code of Conduct;
                        3. fail to deliver payment for services delivered to you;
                        4. fail to deliver Seller Services purchased from you;
                        5. circumvent or manipulate our fee structure, the billing process, or fees owed to Temp Workers;
                        6. post false, inaccurate, misleading, deceptive, defamatory or offensive content (including personal information);
                        7. take any action that may undermine the feedback or reputation systems (such as displaying, importing or
                        exporting feedback information or using it for purposes unrelated to the Website);
                        8. transfer your Temp worker account (including feedback) and Username to another party without our consent;
                        9. distribute or post spam, unsolicited, or bulk electronic communications, chain letters, or pyramid schemes;
                        10. distribute viruses or any other technologies that may harm healthcare-Temp, the Website, or the interests or
                        property of Healthcare-Temps’ users (including their Intellectual Property Rights, privacy and publicity rights) or
                        is unlawful, threatening, abusive, defamatory, invasive of privacy, vulgar, obscene, profane or which may harass
                        or cause distress or inconvenience to, or incite hatred of, any person;
                        11. download and aggregate listings from our website for display with listings from other websites without our
                        express written permission, &quot;frame&quot;, &quot;mirror&quot; or otherwise incorporate any part of the Website into any other
                        website without our prior written authorisation;
                        12. attempt to modify, translate, adapt, edit, decompile, disassemble, or reverse engineer any software programs
                        used by us in connection with the Website;
                        13. copy, modify or distribute rights or content from the Website or Healthcare-Temps&#39;s copyrights and trademarks;
                        or
                        14. harvest or otherwise collect information about Users, including email addresses, without their consent.
                    </p>

                    <h3>5. Fees and Services</h3>
                    <p>
                        We charge fees for certain services, such as urgent job listing fees, featured job fees, Tele-Temping, CV marketing, CRB and
                        company registration. When you use a service that has a fee, you have an opportunity to review and accept the fees that
                        you will be charged based on our schedule, which we may change from time to time and will update by placing on our
                        Website. We may choose to temporarily change the fees for our services for promotional events (for example, discounts
                        on memberships) or new services, and such changes are effective when we post a temporary promotional event or new
                        service on the Websites, or as notified through promotional correspondence.
                        We do not charge for posting jobs or viewing temp workers’ CV. This site is virtually a free service to employers and hirers.
                        Hirers pay a fee direct to temp workers in return for the temp providing a service to the former.
                        Unless otherwise stated, all fees are quoted in Pounds Sterling.
                    </p>
                    <h3>6. Taxes</h3>
                    <p>
                        You are responsible for paying any taxes, including any goods and services or value added taxes, which may be applicable
                        depending on the jurisdiction of the services provided. You acknowledge that you must comply with your obligations under
                        income tax provisions in your jurisdiction.
                    </p>
                    <h3>7. Payment Administration Agent</h3>
                    <p>
                        You acknowledge and agree that we may in our sole discretion, from time to time, appoint our related bodies corporate,
                        affiliates, or any other third party to act as our agent to accept or make payments (including merchant facilities) from or to
                        Users on our behalf.
                        Such affiliates may include, without limitation, Stripe, Paypal and/or Braintree. Such a third party will have the same rights,
                        powers and privileges that we have under this User Agreement and will be entitled to exercise or enforce their rights,
                        powers and privileges as our agent or in their own name. In no event shall we be liable to any User for any loss, damage or
                        liability resulting from the Payment Administration Agent&#39;s negligence and/or acts beyond the authority given by
                        Freelancer.
                    </p>
                    <h3>8. Promotion</h3>
                    <p>
                        We may display your company or business name, logo, images or other media as part of the Healthcare-Temps UK Services
                        and/or other marketing materials relating to the Website, except where you have explicitly requested that we do not do
                        this and we have agreed to such a request in writing.
                        You acknowledge that we may use the public description of your job and the content of your profile information on the
                        Website for marketing and other related purposes.
                    </p>
                    <h3>9. Content</h3>
                    <p>
                        When you give us content, you grant us a worldwide, perpetual, irrevocable, royalty-free, sublicensable (through multiple
                        tiers) right to exercise any and all copyright, trademark, publicity, and database rights (but no other rights) you have in the
                        content, in any media known now or in the future.
                        You acknowledge and agree that: (1) we act only as a forum for the online distribution and publication of User content. We
                        make no warranty that User content is made available on the Website. We have the right (but not the obligation) to take
                        any action deemed appropriate by us with respect to your User content; (2) we have no responsibility or liability for the
                        deletion or failure to store any content, whether or not the content was actually made available on the Website; and (3)
                        any and all content submitted to the Website is subject to our approval. We may reject, approve or modify your User
                        content at our sole discretion.
                        You represent and warrant that your content:

                        1. will not infringe upon or misappropriate any copyright, patent, trademark, trade secret, or other intellectual
                        property right or proprietary right or right of publicity or privacy of any person;
                        2. will not violate any law or regulation;
                        3. will not be defamatory or trade libellous;
                        4. will not be obscene or contain child pornography;
                        5. will not contain the development, design, manufacture or production of missiles, or nuclear, chemical or
                        biological weapons
                        6. will not contain material linked to terrorist activities
                        7. will not include incomplete, false or inaccurate information about User or any other individual; and
                        8. will not contain any viruses or other computer programming routines that are intended to damage, detrimentally
                        interfere with, surreptitiously intercept or expropriate any system, data or personal information.
                    </p>
                    <p>
                        You acknowledge and agree that we may transfer your personal information to a related body corporate and your
                        information may be transferred outside of United Kingdom, EU or the EEC-area. If you wish to withdraw your consent, you
                        acknowledge and agree that we may be unable to provide you with access to the Website and Freelancer Services and may
                        close your Account.
                        Information on the Website may contain general information about legal, financial, health and other matters. The
                        information is not advice,and should not be treated as such. You must not rely on the information on the Website as an
                        alternative to professional advice. If you have specific questions about any matter you should consult your professional
                        adviser.
                        We provide unmonitored access to third party content, including User feedback and articles with original content and
                        opinions (or links to such third party content). We only act as a portal and have no liability based on, or related to, third
                        party content on the Website, whether arising under the laws of copyright or other intellectual property, defamation, libel,
                        privacy, obscenity, or any other legal discipline.
                        The Website may contain links to other third party websites. We do not control the websites to which we link from the
                        Website. We do not endorse the content, products, services, practices, policies or performance of the websites we link to
                        from the Website. Use of third party content, links to third party content and/or websites is at your risk.
                        In relation to deletion or hiding of any information or content, using the Website to delete, hide or otherwise dispose of
                        information does not imply permanent deletion of content or information. Information may be retained for a period of
                        time to fulfil record keeping, regulatory, compliance, statistical, law enforcement and other obligations.
                    </p>
                    <h3>10. Feedback, Reputation and Reviews</h3>
                    <p>
                        You acknowledge that you transfer copyright of any feedback, reputation or reviews you leave consisting of comments and
                        any rating(s) (e.g. quality, communication etc.) together with any composite rating by us. You acknowledge that such
                        feedback, reputation and reviews belong solely to us, notwithstanding that we permit you to use it on our Website while
                        you remain a User. You must not use, or deal with, such feedback, reputation and reviews in any way inconsistent with our
                        policies as posted on the Website from time to time without our prior written permission.
                        You may not do (or omit to do) anything that may undermine the integrity of Healthcare-Temps feedback system. We are
                        entitled to suspend or terminate your Account at any time if we, in our sole and absolute discretion, are concerned by any
                        feedback about you, or your feedback rating, where we believe our feedback system may be subverted.
                        Our feedback ratings belong to us and may not be used for any purpose other than facilitating the provision of Seller
                        Services via the Website. You may not use your Seller or Buyer feedback (including, but not limited to, marketing or
                        exporting your any or all of your composite rating(s) or feedback comments) in any real or virtual venue other than a
                        website operated by Freelancer or its related entities, without our written permission.
                    </p>
                    <h3>11. Advertising</h3>
                    <p>
                        Unless otherwise agreed with us, you must not advertise an external website, product or service on the Website. Any
                        website address posted on the Website, including in a listing, bid, listing description, clarification board or the message
                        board, must relate to a specific job to be performed on the Website.
                    </p>
                    <h3>12. Communication With Other Users</h3>
                    <p>
                        Communication with other users on the Website must be conducted through the text, audio and or video chat
                        functionality, along with message boards, public clarification boards, Project message board, direct message sending and
                        other communication channels provided on the Website.
                        You must not post your email address or any other contact information (including but not limited to Skype ID or other
                        identifying strings on other platforms) on the Website, except in the &quot;email&quot; field of the signup form, at our request or as
                        otherwise permitted by us on the Website.
                        Unless you have a prior relationship with a User, you must only communicate with Users via the Website. You must not,
                        and must not attempt to, communicate with other Users through any other means including but not limited to email,
                        telephone, Skype, ICQ, Whatsapp, AIM, MSN Messenger, WeChat, GTalk, GChat or Yahoo.
                        In relation to video chat and audio chat, any terms agreed to between any Users must be confirmed in writing using the
                        chat or direct message function.
                    </p>
                    <h3>13. Identity / Know Your Customer</h3>
                    <p>
                        You authorise us, directly or through third parties, to make any inquiries we consider necessary to validate your identity.
                        You must, at our request: (1) provide further information to us, which may include your date of birth and or other
                        information that will allow us to reasonably identify you; (2) take steps to confirm ownership of your email address or
                        financial instruments; or (3) verify your information against third party databases or through other sources.
                        You must also, at our request, provide copies of identification documents (such as your passport or drivers&#39; licence). We
                        may also ask you to provide photographic identification holding a sign with a code that we provide as an additional identity
                        verification step.
                        We reserve the right to close, suspend, or limit access to your Account, the Website and/or Temp Services in the event we
                        are unable to obtain or verify to our satisfaction the information which we request under this section.
                        If you are not a verified temp worker you may not be able to withdraw funds from your Temp account, and other
                        restrictions may apply. See the Know Your Customer and Identity Verification Policy for more details.
                    </p>
                    <h3>14. User Services</h3>
                    <p>
                        Upon the Buyer awarding a job to the Seller, and the Seller&#39;s acceptance on the Website, or the purchase of an item by a
                        Buyer from the Seller, the Buyer and Seller will be deemed to have entered into a User Contract under which the Buyer
                        agrees to purchase, and the Seller agrees to deliver the Seller Services. You agree not to enter into any contractual
                        provisions in conflict with the User Agreement.
                        You are solely responsible for ensuring that you comply with your obligations to other Users. If you do not, you may
                        become liable to that User. You must ensure that you are aware of any domestic laws (including common law),
                        international laws, statutes, ordinances and regulations relevant to you as a Buyer or Seller, or in any other uses you make
                        of the Website.
                        If another User breaches any obligation to you, you are solely responsible for enforcing any rights that you may have. For
                        the avoidance of doubt, we have no responsibility for enforcing any rights under a User Contract.
                        Each User acknowledges and agrees that the relationship between Buyers and Sellers is that of an independent contractor.
                        Nothing in this User Agreement creates a partnership, joint venture, agency or employment relationship between Users.
                        Nothing in this User Agreement shall in any way be construed as forming a joint venture, partnership or an employer-
                        employee relationship between Freelancer and any User.
                    </p>
                    <h3>15. Special Provisions for Local Jobs</h3>
                    <p>
                        Each User acknowledges:

                        1. Healthcare-Temps UK does not review, approve, recommend or verify any of the credentials, licences or
                        statements of capability in relation to Jobs (or, for the avoidance of doubt, any non Local Jobs on the Website);
                        2. Healthcare-Temps provides matchmaking and platform services only. Users agree that Healthcare-temps UK has
                        no liability for any other aspect of service delivery or interaction between Buyer and Seller. Freelancer is not a
                        party to any disputes between Buyer and Seller, although we provide a dispute resolution mechanism to assist
                        the parties in resolving issues;
                        3. Healthcare-Temps may from time to time include map features on Healthcare-Temps and may display the
                        location of Users to persons browsing the Website on that map. Every Buyer seeking services for Local Jobs will
                        be asked to provide the location where the Local Job is to be performed. You expressly agree that Healthcare-
                        Temps UK has no liability for displaying such information.
                        4. A User must never disclose, in any Job posted, personal details such as the User&#39;s name, street number, phone
                        number or the email address in any Project description for a Local Job or in any other public communication on
                        the Website (these may be disclosed for Local Jobs as required in private direct messages);
                        5. Healthcare-Temps UK may collect location related data from you via technologies including but not limited to
                        GPS, IP address location, wifi, and other methods. This data may be shared in the context of facilitating services
                        for Local Jobs and each User specifically consents to this collection and sharing as part of this agreement;
                        6. Upon completion of a Local Job Project, Seller User must log on to the Website and click the &quot;Complete&quot; button
                        for that Local Job, as soon as practicable.
                        7. Failure to complete the service or task will constitute a breach of this User Agreement; and
                        8. Our fees are applied to the amount of the awarded Seller&#39;s bid to perform the services for the Job. Any items
                        purchased by the Seller as part of performing the service are between the Buyer and Seller.
                    </p>

                    <h3>16. Funds</h3>
                    <p>
                        If you are a Temp Worker, you may have positive funds if you have successfully completed a job or sold a service (i.e.
                        consultancy or tele-health), and funds have been released to you. There are also circumstances where funds may have
                        been credited to your Account in relation to an affiliate program or a referral program.
                        Funds in your Account are held by us in our operating accounts held with financial institutions. Funds in your Account are
                        not held separately by us, and may be commingled with our general operating funds, and/or funds of other User&#39;s
                        Accounts.
                        If your Account has negative funds, we may:
                        1. set-off the negative amount with funds that you subsequently receive into your Account;
                        2. if you have funds in multiple currencies in your Account and one of the currencies becomes negative for any
                        reason, we may set-off the negative amount against funds you maintain in a different currency (at an exchange
                        rate applied by us);
                        3. reverse payments you have made from your Account to other User Accounts on the Website;
                        4. deduct amounts you owe us from money you subsequently add or receive into your Account; or
                        5. immediately suspend or limit your Account until such time as your Account no longer has a negative amount.
                        In the event that we offset a negative amount of funds pursuant to this section, it may be bundled with another debit
                        coming out of your Account.
                        We reserve the right to collect any funds owed to us by any other legal means.
                        You acknowledge and agree that:
                        1. we are not a bank or other licensed financial institution and do not provide banking services or any financial
                        services to you;
                        2. the funds shown in your Account represents our unsecured obligations to you with respect to your rights to
                        direct us to make payment in relation to the purchase and sale of Seller Services through the Website and
                        provision of the temp Services;
                        3. if you were a User acquired in an acquisition and your account was migrated to the Website, we are responsible
                        for your positive funds only to the extent of the legal documentation between us and any acquired marketplace,
                        along with this agreement, and you acknowledge specifically that the onus is on you to confirm the validity of
                        your fund, and that any understatement or misstatement in relation to this is not a claim against us, and belongs
                        with the counterparty of any prior user agreement to which you agreed;

                        4. to the extent that we are required to release funds from your Account to you, you will become our unsecured
                        creditor until such funds are paid to you;
                        5. we are not acting as a trustee or fiduciary with respect to such funds or payments;
                        6. the amount of funds showing in your Account is not insured and is not a guaranteed deposit;
                        7. funds may only be loaded into your Account, or released from your Account, by us and you must only use the
                        mechanisms available on the Website to pay for, or receive funds in respect of Seller Services;
                        8. any refunds required to be processed in your favour will be returned only to the source of the original deposit,
                        and cannot be redirected to any other payment source;
                        9. we will hold funds in respect of the amount of your Account (including Escrow) in an account held by us with a
                        financial institution (or in any manner that we decide in our sole discretion from time to time) and such funds are
                        not segregated into a separate account; and
                    </p>

                    <h3>17. Limits &amp; Fraud Prevention</h3>
                    <p>
                        We reserve the right to suspend a User withdrawal request if the source of the funds is suspected to be fraudulent.
                        If we become aware that any funds received into an Account from another Account as a result of a fraudulent transaction,
                        this will be reversed immediately. If those funds have already been released to you, you must pay the funds into your
                        Account. If you do not do so, we may suspend, limit or cancel your account, or take action against you to recover those
                        funds.
                        We may, in our sole discretion, place a limit on any or all of the funds in your Account (thereby preventing any use of the
                        funds) if:
                        1. we believe there may be an unacceptable level of risk associated with you, your Account, or any or all of your
                        transactions, including if we believe that there is a risk that such funds will be subject to reversal or chargeback;
                        2. we believe that the beneficiary of the payment is someone other than you;
                        3. we believe that the payment is being made to a country where we do not offer our Service; or
                        4. we are required to do so by law or applicable law enforcement agencies.
                        If you are involved in a dispute, we may (in certain circumstances) place a temporary limit on the funds in your Account to
                        cover the amount of any potential liability. If the dispute is resolved in your favour, we will lift the limit on your funds and
                        those funds may be released to you. If the dispute is not resolved in your favour, we may remove the funds from your
                        Account. We may also place a limit on your account in circumstances where we suspect you of fraudulent or other
                        unacceptable behaviour, while we investigate any such matter.
                    </p>
                    <h3>18. Refunds</h3>
                    <p>
                        You may ask for a refund at any time for any funds that was paid into your Account except if the amount to refund relates
                        to an Escrow Payment or relates to fees or charges payable to us.
                        If we agree to the refund, the funds will be received by the User via the same payment method(s) that the User used to
                        make the original payment to us.
                        You can request a refund by using our customer support website or emailing us at support@healthcare-temps.co.uk . If
                        you initiate any chargeback request or other &quot;Request for Information&quot; or similar process, you expressly agree and consent
                        to us to share any and all information in relation to your agreement of these terms and conditions, in order to defeat any
                        such chargeback request.
                        If you have already initiated a chargeback request with your credit card issuer, you must not request a refund of funds by
                        contacting us and must not seek double recovery. If we reasonably determine, having considered all the relevant
                        circumstances, that you have made an excessive or unreasonable number of requests to refund funds back to you or
                        chargebacks, we may suspend, limit or close your Account.
                    </p>
                    <h3>19. Withdrawals</h3>
                    <p>
                        Your first withdrawal of funds earned may be delayed for up to fifteen days for security and fraud purposes.

                        Subsequent withdrawals may be delayed for up to fifteen days where our fraud prevention policies require a delay.
                        We may impose a minimum withdrawal amount for funds earned. This is set out in our schedule of Fees and Charges. The
                        maximum you can withdraw per month is £10,000 unless otherwise specifically agreed with support.
                        We may require you to be Fully Verified before you can withdraw funds from your Healthcare-Temps account, irrespective
                        of whether or not a delay has been enforced. For details of how to become Temp worker Verified please read the
                        Verification Policy.
                        You acknowledge and agree that we may seek to verify your identity or request additional information from you as part of
                        our anti-fraud, healthcare compliance and Know Your Customer policy, as outlined in the agreement above.
                    </p>
                    <h3>20. Chargebacks</h3>
                    <p>
                        A chargeback (being a challenge to a payment that a User files with their card issuer or financial institution), and any
                        subsequent reversal instruction, is made by the payment product issuer or third parties (such as payment processors) and
                        not by us. We are bound to follow such instructions.
                        You acknowledge and agree that we will be entitled to recover any chargebacks and reversals that may be imposed on us
                        by a payment product issuer or third parties (such as payment processors) on funds paid to you by Buyers through the
                        Website, as well as any processing or any other fees whatsoever incurred by us on those chargebacks and reversals.
                        You agree that we may reverse any such payments made to you, which are subject to chargeback or reversal instruction via
                        your payment product issuer or third parties (such as payment processors). If you initiate any chargeback request or other
                        &quot;Request for Information&quot; or similar process, you expressly agree and consent to us to share any and all information in
                        relation to your agreement of these terms and conditions, in order to defeat any such chargeback request.
                    </p>
                    <h3>21. Inactive Accounts</h3>
                    <p>
                        User Accounts that have not been logged into for a period of time will incur a maintenance fee per month, until either the
                        account is closed or reactivated, for storage, bandwidth, support and management costs of providing hosting of the User&#39;s
                        profile, portfolio storage, listing in directories, promotion of your profile on the Website and elsewhere, provision of the
                        HireMe service, file storage, message transmission, general administrative matters and message and other storage costs.
                        We reserve the right to close an Inactive Account. We reserve the right to close an account with nil or negative funds.
                    </p>
                    <h3>22. Right to Refuse Service</h3>
                    <p>
                        We may close, suspend or limit your access to your Account without reason. Without limiting the foregoing, we may close,
                        suspend or limit your access to your Account:
                        1. if we determine that you have breached, or are acting in breach of, this User Agreement;
                        2. if you under-bid on any jobs in an attempt to renegotiate the actual price privately, to attempt to avoid fees;
                        3. if we determine that you have infringed legal rights (resulting in actual or potential claims), including infringing
                        Intellectual Property Rights;
                        4. if we determine that you have engaged, or are engaging, in fraudulent, or illegal activities;
                        5. you do not respond to account verification requests;
                        6. you do not complete account verification when requested within 3 months of the date of request;
                        7. to manage any risk of loss to us, a User, or any other person; or
                        8. for other reasons.
                        If we close your Account due to your breach of this User Agreement, you may also become liable for certain fees as
                        described in this User Agreement.
                        Without limiting our other remedies, to the extent you have breached this User Agreement, you must pay us all fees owed
                        to us and reimburse us for all losses and costs (including any and all of our employee time) and reasonable expenses
                        (including legal fees) related to investigating such breach and collecting such fees.

                        You acknowledge and agree that: (1) the damages that we will sustain as a result of your breach of this User Agreement
                        will be substantial and will potentially include (without limitation) fines and other related expenses imposed on us by our
                        payment processors and Users and that those damages may be extremely difficult and impracticable to ascertain; (2) if you
                        breach this User Agreement, we may take legal action against you to recover losses.
                        If we close your Account for a reason other than as a result of your breach of this User Agreement, unless as otherwise
                        specified in this User Agreement, you will be entitled to receive any payment due from us to you.
                        In the event that we close your Account, you will have no claim whatsoever against us in respect of any such suspension or
                        termination of your Account.
                    </p>
                    <h3>23. Dispute Resolution Services</h3>
                    <p>
                        Healthcare-Temps offers a Dispute Resolution Services to Users who have elected to use the feature. You agree and
                        acknowledge that: (i) Healthcare-Temps UK is not providing legal services; (ii) Healthcare-Temps UK will not advise you
                        regarding any legal matters; and (iii) if you desire to have legal counsel, you will seek an independent legal counsel from
                        those licensed to practice law in your jurisdiction. You will not rely on Healthcare-Temps UK for any such counsel.
                        In the event of a dispute between a Buyer and a Seller regarding a return or release of Payments, either Buyer or Seller
                        may elect to use the Dispute Resolution Services offered by Healthcare Temps as set out in the Disputes Policy. The Buyer
                        and Seller will then be notified that the matter will be addressed through Dispute Resolution Services.
                        You agree to indemnify and (to the maximum extent permitted by law) hold Healthcare-Temps UK and any of our affiliates
                        harmless against any damages or liability you may suffer as a result of using the Escrow Payments and/or Dispute
                        Resolution Services.
                        Healthcare-Temps UK will respond to disputes initiated by a Seller or a Buyer in accordance with the Dispute Resolution
                        Services as set out in this clause and the Disputes Resolution Policy and in relation to disputes that arise between both
                        parties and in relation to chargebacks set out in clauses 19 and 21 of this agreement
                    </p>
                    <h3>24. Other Disputes With Users</h3>
                    <p>
                        You acknowledge and agree that in the event that a dispute arises between you and another User in relation to any job /
                        Project that you will first attempt to resolve any differences that you have in relation to such job/project, including in
                        relation to the quality of the services provided.
                        If you continue to have any difficulties or problems in relation to a dispute with another User in relation to a Project we
                        encourage you to contact us as set out in the Clause entitled &quot;Contacting us&quot;.
                        You agree that any dispute that is not related to a Payment (which must be dealt with in accordance with the Disputes
                        Policy) arising between you and another User will be handled in accordance with this clause. Healthcare-Temps UK will
                        have full rights and powers to make a determination for all such disputes. Upon receipt of a dispute, Healthcare-temps UK
                        shall have the right to request the Seller and the Buyer to provide documentation in support of their claim or position in
                        relation to the dispute. You agree that Healthcare-Temps UK has absolute discretion to accept or reject any document
                        provided. You also acknowledge that Healthcare-Temps UK is not a judicial or alternative dispute resolution institution and
                        that we will make the determinations only as an ordinary reasonable person. In addition, we do not warrant that the
                        documents provided by the parties to the dispute will be true, complete or correct and you agree to indemnify and (to the
                        maximum extent permitted by law) hold Healthcare-Temps UK and any of our affiliates harmless against any damages or
                        liability you may suffer as a result of any documentation or material subsequently being found to be false or misleading.
                        A User found to be in breach of the Code of Conduct during the Dispute Resolution Service process may automatically lose
                        the dispute in favour of the other party involved, regardless of the origin of the dispute. The User who breached the Code
                        of Conduct may also incur further disciplinary action. For more information, read the Code of Conduct.
                    </p>
                    <h3>25. Disputes With Us</h3>
                    <p>
                        If a dispute arises between you and Freelancer, our goal is to address your concerns immediately and, if we are unable to
                        do so to your satisfaction, to provide you with a means of resolving the dispute quickly. We strongly encourage you to first

                        contact us directly to seek a resolution by using our customer support website or emailing us at support@healthcare-
                        temps.co.uk.
                        Healthcare-Temps UK&#39;s failure to act with respect to a breach by you or others does not waive our right to act with respect
                        to subsequent or similar breaches.
                    </p>
                    <h3>26. Currencies</h3>
                    <p>
                        On this Web and Mobile application, we deal only in pounds sterling and/or crypto currencies
                    </p>
                    <h3>27. Survival and Release</h3>
                    <p>
                        This agreement supersedes any other agreement between you and the Company. If any part of this document is found to
                        be unenforceable, that part will be limited to the minimum extent necessary so that this document will otherwise remain
                        in full force and effect. Our failure to enforce any part of this document is not a waiver of any of our rights to later enforce
                        that or any other part of this documents. We may assign any of our rights and obligations under this document from time
                        to time.
                        If there is a dispute between participants on this site, or between users and any third party, you agree that the Company is
                        under no obligation to become involved. In the event that you have a dispute with one or more other users, you release
                        the Company, its officers, employees, agents, and successors from claims, demands, and damages of every kind or nature,
                        known or unknown, suspected or unsuspected, disclosed or undisclosed, arising out of or in any way related to such
                        disputes and/or our Services.
                    </p>
                    <h3>28. Access and Interference</h3>
                    <p>
                        You agree that you will not use any robot, spider, scraper or other automated means to access the Website via any means,
                        including for the avoidance of doubt access to our API or application programming interface, for any purpose without our
                        express written permission.
                        Additionally, you agree that you will not:
                        1. take any action that imposes or may impose (in our sole discretion, exercised reasonably) an unreasonable or
                        disproportionately large load on our infrastructure;
                        2. interfere with, damage, manipulate, disrupt, disable, modify, overburden, or impair any device, software system
                        or network connected to or used (by you or us) in relation to the Website or your Account, or assist any other
                        person to do any of these things, or take any action that imposes, or may impose, in our discretion, an
                        unreasonable or disproportionately large load on our infrastructure;
                        3. copy, reproduce, modify, create derivative works from, distribute, or publicly display any content (except for your
                        information) from the websites without the prior express written permission of Freelancer and the appropriate
                        third party, as applicable;
                        4. interfere or attempt to interfere with the proper working of the Websites, services or tools, or any activities
                        conducted on or with the Websites, services or tools; or
                        5. bypass our robot exclusion headers or other measures we may use to prevent or restrict access to the Website.
                    </p>
                    <h3>29. Closing Your Account</h3>
                    <p>
                        You may close your Account at any time. The option is located in your Dashboard Menu.
                        Account closure is subject to:
                        1. not having any outstanding listings on the Website;
                        2. resolving any outstanding matters (such as a suspension or restriction on your Account); and
                        3. paying any outstanding fees or amounts owing on the Account.
                        We may retain some of your personal information to satisfy regulatory requirements and our own external obligations.
                        Closing your account does not necessarily delete or remove all of the information we hold.
                    </p>
                    <h3>30. Privacy</h3>
                    <p>
                        We use your information as described in the Healthcare-Temps’ Privacy Policy. If you object to your information being
                        transferred or used in this way then you must not use our services. For the avoidance of doubt, your name and personal
                        details shall be used for identity purposes in the normal course of conducting business in this online marketplace. This may
                        include on invoices and purchase orders including but not limited to between transacting parties, including those
                        automatically generated on awarding, accepting and payment.
                    </p>
                    <h3>31. Indemnity</h3>
                    <p>
                        You will indemnify us (and our officers, directors, agents, subsidiaries, joint venturers and employees) against any claim or
                        demand, including legal fees and costs, made against us by any third party due to or arising out of your breach of this
                        Agreement, or your infringement of any law or the rights of a third party in the course of using the Website and Freelancer
                        Services.
                        In addition, we can apply any funds in your Account against any liabilities you owe to us or loss suffered by us as a result of
                        your non-performance or breach of this User Agreement.
                    </p>
                    <h3>32. Security</h3>
                    <p>
                        You must immediately notify us upon becoming aware of any unauthorised access or any other security breach to the
                        Website, your Account or the Freelancer Services and do everything possible to mitigate the unauthorised access or
                        security breach (including preserving evidence and notifying appropriate authorities). Your User Account is yours only, and
                        you must not share your password with others. You are solely responsible for securing your password. We will not be liable
                        for any loss or damage arising from unauthorised access of your account resulting from your failure to secure your
                        password.
                    </p>
                    <h3>33. No Warranty as to Each User&#39;s Purported Identity</h3>
                    <p>
                        We cannot and do not confirm each User&#39;s purported identity on the Website. We may provide information about a User,
                        such as a strength or risk score, geographical location, or third party background check or verification of identity or
                        credentials. However, such information is based solely on data that a User submits and we provide such information solely
                        for the convenience of Users and the provision of such information is not an introduction, endorsement or
                        recommendation by us.
                    </p>
                    <h3>34. No Warranty as to Content</h3>
                    <p>
                        The Website is a dynamic time-sensitive Website. As such, information on the Website will change frequently. It is possible
                        that some information could be considered offensive, harmful, inaccurate or misleading or mislabelled or deceptively
                        labelled accidentally by us or accidentally or purposefully by a third party.
                        Our Services, the Website and all content on it are provided on an &#39;as is&#39;, &#39;with all faults&#39; and &#39;as available&#39; basis and without
                        warranties of any kind either express or implied. Without limiting the foregoing, we make no representation or warranty
                        about:
                        1. the Website or any Seller Services or Temp worker Services;
                        2. the accuracy, reliability, availability, veracity, timeliness or content of the Website or any Seller Services or Temp
                        Worker Services;
                        3. whether the Website or Seller Services or Temp Worker Services will be up-to-date, uninterrupted, secure, error-
                        free or non-misleading;
                        4. whether defects in the Website will be corrected;
                        To every extent permitted by law, we specifically disclaim any implied warranties of title, merchantability, fitness for a
                        particular purpose, quality, suitability and non-infringement.
                    </p>
                    <h3>35. Limitation of Liability</h3>
                    <p>
                        In no event shall we, our related entities, our affiliates or staff be liable, whether in contract, warranty, tort (including
                        negligence), or any other form of liability, for:
                        1. any indirect, special, incidental or consequential damages that may be incurred by you;
                        2. any loss of income, business or profits (whether direct or indirect) that may be incurred by you;
                        3. any claim, damage, or loss which may be incurred by you as a result of any of your transactions involving the
                        Website.
                        The limitations on our liability to you above shall apply whether or not we, our related entities, our affiliates or staff have
                        been advised of the possibility of such losses or damages arising.
                        Notwithstanding the above provisions, nothing in this User Agreement is intended to limit or exclude any liability on the
                        part of us and our affiliates and related entities where and to the extent that applicable law prohibits such exclusion or
                        limitation including those within the Competition and Consumer Act 2010 (Cth) and relevant state fair trading legislation.
                        To the extent that we are able to limit the remedies available under this User Agreement, we expressly limit our liability for
                        breach of a non-excludable condition or warranty implied by virtue of any legislation to the following remedies (the choice
                        of which is to be at our sole discretion) to the supply of the Temp Worker services again or the payment of the cost of
                        having the Temp Worker services supplied again.
                    </p>
                    <h3>36. Legal Limitations</h3>
                    <p>
                        As some jurisdictions do not allow some of the exclusions or limitations as established above, some of these exclusions or
                        limitations may not apply to you. In that event, the liability will be limited as far as legally possible under the applicable
                        legislation. We may plead this User Agreement in bar to any claim, action, proceeding or suit brought by you, against us for
                        any matter arising out of any transaction or otherwise in respect of this User Agreement.
                        You and we agree that you and we will only be permitted to bring claims against the other only on an individual basis and
                        not as a plaintiff or class member in any purported class or representative action or proceeding. Unless both you and we
                        agree otherwise, the arbitrator may not consolidate or join more than one person&#39;s or party&#39;s claims and may not
                        otherwise preside over any form of a consolidated, representative, or class proceeding. In addition, the arbitrator may
                        award relief (including monetary, injunctive, and declaratory relief) only in favour of the individual party seeking relief and
                        only to the extent necessary to provide relief necessitated by that party&#39;s individual claim(s). Any relief awarded cannot
                        affect other Users.
                    </p>
                    <h3>37. Notices</h3>
                    <p>
                        Legal notices will be served to the email address you provide to healthcare-Temps UK during the registration process.
                        Notice will be deemed given 24 hours after email is sent, unless the sending party is notified that the email address is
                        invalid or that the email has not been delivered. Alternatively, we may give you legal notice by mail to the address
                        provided by you during the registration process. In such case, notice will be deemed given three days after the date of
                        mailing.
                        Any notices to Healthcare-Temps UK must be given by registered ordinary post.
                    </p>
                    <h3>38. Legal and Jurisdiction Disputes</h3>
                    <p>
                        This Agreement will be governed in all respects by the laws of England &amp; Wales. We encourage you to try and resolve
                        disputes using certified mediation (such as online dispute resolution processes). If a dispute cannot be resolved then you
                        and Healthcare-Temps UK irrevocably submit to the non-exclusive jurisdiction of the courts of England and Wales.
                    </p>
                    <h3>39. Severability</h3>
                    <p>
                        The provisions of this User Agreement are severable, and if any provision of this User Agreement is held to be invalid or
                        unenforceable, such provision may be removed and the remaining provisions will be enforced. This Agreement may be
                        assigned by us to an associated entity at any time, or to a third party without your consent in the event of a sale or other
                        transfer of some or all of our assets. In the event of any sale or transfer you will remain bound by this User Agreement.
                    </p>
                    <h3>40. Interpretation</h3>
                    <p>
                        Headings are for reference purposes only and in no way define, limit, construe or describe the scope or extent of such
                        section.
                    </p>
                    <h3>41. No Waiver</h3>
                    <p>
                        Our failure to act with respect to an anticipated or actual breach by you or others does not waive our right to act with
                        respect
                    </p>
                    <h3>42. Abusing Healthcare-Temp platform:</h3>
                    <p>
                        Healthcare-Temps UK reserves to the greatest extent possible all rights, without limiting any other remedies, to limit,
                        suspend or terminate our service(s) and or user account(s), suspend or ban access to our services, remove any content,
                        and to take any and all technical or legal steps to ban users.
                        Without limiting the reasons for taking the aforementioned actions, conduct giving rise to this response could include:
                        1. use of our services for any illegitimate or non bona fide purpose
                        2. creating problems with other users or potential legal liabilities
                        3. infringing the intellectual property rights of third parties
                        4. acting inconsistently with the letter or spirit of any of our policies
                        5. abuse of any staff members including inappropriate or unreasonable communications
                        6. abuse or poor performance in the Preferred Freelancer Program
                        7. any attempt to use Freelancer&#39;s platform or services for any objectionable purpose
                    </p>
                    <h3>43. Feedback</h3>
                    <p>
                        If you have any questions about this User Agreement or if you wish to report breaches of this User Agreement, please
                        contact us by using our customer support website or emailing us at support@healthcare-temps.co.uk.
                    </p>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->
<!-- ==============================================
 Scripts
 =============================================== -->

</body>
</html>
