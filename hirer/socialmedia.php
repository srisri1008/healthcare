<?php 
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
  Redirect::to('../index.php');	
}

//Get Instructor's Data
$query = DB::getInstance()->get("client", "*", ["clientid" => $client->data()->clientid, "LIMIT" => 1]);
if ($query->count()) {
    foreach($query->results() as $row) {
        $nid = $row->id;
        $name = $row->name;
        $username = $row->username;
        $email = $row->email;
        $phone = $row->phone;
    }
}

//Get Social Data
$querySocial = DB::getInstance()->get("client_info", "*", ["clientid" => $client->data()->id, "LIMIT" => 1]);
if ($querySocial->count()) {
    $flag = 1;
    $socialArr = array();
    foreach($querySocial->results() as $row) {
        $socialArr = $row;
    }
}

//Update Social Data
if(isset($_POST['updatebtn'])){
    if (Input::exists()) {
        if(Token::check(Input::get('token'))){

            $errorHandler = new ErrorHandler;

            $validator = new Validator($errorHandler);

            $validation = $validator->check($_POST, [
                'phone' => [
                    'required' => true,
                ]
            ]);

            if (!$validation->fails()) {
                //Info update
                $arrToPassInfo = [];
                $infoArr = $_POST['userInfo'];
                foreach ($infoArr AS $k => $v) {
                    $arrToPassInfo[$k] = $v;
                }
                //Check if already exists or not
                if(isset($flag)) {
                    //Update Freelancer Info
                    $freelancerInfoUpdate = DB::getInstance()->update('client_info', $arrToPassInfo, [
                        'clientid' => $nid
                    ]);
                    if($freelancerInfoUpdate) {
                        header('Location: ' . $_SERVER['PHP_SELF']);
                        $updatedError = true;
                    } else {
                        $hasError = true;
                    }
                } else {
                    //Insert Client Info
                    $arrToPassInfo['clientid'] = $nid;
                    $freelancerInfoInsert = DB::getInstance()->insert('client_info', $arrToPassInfo);
                    if($freelancerInfoInsert) {
                        header('Location: ' . $_SERVER['PHP_SELF']);
                        $updatedError = true;
                    } else {
                        $hasError = true;
                    }
                }



            } else {
                $error = '';
                foreach ($validation->errors()->all() as $err) {
                    $str = implode(" ",$err);
                    $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
                }
            }

        }
    }
}

?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

    <!-- Include header.php. Contains header content. -->
    <?php include ('template/header.php'); ?>
  
<body class="skin-green sidebar-mini">
     
     <!-- ==============================================
     Wrapper Section
     =============================================== -->
	 <div class="wrapper">
	 	
        <!-- Include navigation.php. Contains navigation content. -->
	 	<?php include ('template/navigation.php'); ?> 
        <!-- Include sidenav.php. Contains sidebar content. -->
	 	<?php include ('template/sidenav.php'); ?> 
	 	
	  <!-- Content Wrapper. Contains page content -->
         <div class="content-wrapper">
             <!-- Content Header (Page header) -->
             <section class="content-header">
                 <h1>Social Media<small class="hidden"><?php echo $lang['section']; ?></small></h1>
                 <ol class="breadcrumb">
                     <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                     <li class="active">Social Media</li>
                 </ol>
             </section>

             <!-- Main content -->
             <div class="container" style="margin-top: 20px;">
                 <form method="post">
                     <div class="row">
                         <div class="col-lg-offset-1 col-lg-10">
                             <h4 style="padding:15px 20px; background-color: #23B14D;">Social Media Links</h4>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-lg-5 col-lg-offset-1">
                             <div class="form-group">
                                 <label>Facebook</label>
                                 <input type="url" name="userInfo[fb]" value="<?=isset($flag) ? $socialArr->fb : ''?>" placeholder="Your Facebook URL starts with http://" class="form-control">
                             </div>
                             <div class="form-group">
                                 <label>LinkedIn URL</label>
                                 <input type="url" name="userInfo[linkedin]" value="<?=$flag ? $socialArr->linkedin : ''?>" placeholder="Your LinkedIn URL starts with http://" class="form-control">
                             </div>
                             <div class="form-group">
                                 <label>Twitter</label>
                                 <input type="text" name="userInfo[twitter]" value="<?=$flag ? $socialArr->twitter : ''?>" class="form-control">
                             </div>
                             <div class="form-group">
                                 <label>Google +</label>
                                 <input type="text" name="userInfo[google]" value="<?=$flag ? $socialArr->google : ''?>" class="form-control">
                             </div>
                         </div>
                         <div class="col-lg-5">
                             <div class="form-group">
                                 <label>Skype</label>
                                 <input type="url" name="userInfo[skype]" value="<?=$flag ? $socialArr->skype : ''?>" placeholder="Your Skype ID" class="form-control">
                             </div>
                             <div class="form-group">
                                 <label>Whatsapp</label>
                                 <input type="url" name="userInfo[whatsapp]" value="<?=$flag ? $socialArr->whatsapp : ''?>" class="form-control">
                             </div>
                             <div class="form-group">
                                 <label>Blogger</label>
                                 <input type="text" name="userInfo[blogger]" value="<?=$flag ? $socialArr->oovoo : ''?>" placeholder="Your Oovoo ID" class="form-control">
                             </div>
                             <div class="form-group">
                                 <label>Gruveo</label>
                                 <input type="email" name="userInfo[gruveo]" value="<?=$flag ? $socialArr->gruveo : ''?>" placeholder="Your Gruveo email" class="form-control">
                             </div>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-lg-offset-1 col-lg-10">
                             <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                             <input class="btn" type="submit" name="updatebtn" value="Update" style="background-color: #23B14D; color: #ffffff">
                         </div>
                     </div>
                 </form>
             </div>
         </div><!-- /.content-wrapper -->
	 	
      <!-- Include footer.php. Contains footer content. -->	
	  <?php include 'template/footer.php'; ?>	
	 	
     </div><!-- /.wrapper -->   

	
	 <!-- ==============================================
	 Scripts
	 =============================================== -->
	 
    <!-- jQuery 2.1.4 -->
    <script src="../assets/js/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.6 JS -->
    <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../assets/js/app.min.js" type="text/javascript"></script>
    <!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable({
        /* No ordering applied by DataTables during initialisation */
        "order": []
        });
      });
    </script>
    <script type="text/javascript">
	$(function() {
	
	
	$(".btn-danger").click(function(){
	
	//Save the link in a variable called element
	var element = $(this);
	
	//Find the id of the link that was clicked
	var id = element.attr("id");
	
	//Built a url to send
	var info = 'id=' + id;
	 if(confirm("<?php echo $lang['delete_job']; ?>"))
			  {
			var parent = $(this).parent().parent();
				$.ajax({
				 type: "GET",
				 url: "template/delete/deletejob.php",
				 data: info,
				 success: function()
					   {
						parent.fadeOut('slow', function() {$(this).remove();});
					   }
				});
			 
	
			  }
		   return false;
	
		});
	
	});
	</script>

	<script type="text/javascript">
	$(function() {
	
	$(".btn-info").click(function(){
	
	//Save the link in a variable called element
	var element = $(this);
	
	//Find the id of the link that was clicked
	var id = element.attr("id");
	
	//Built a url to send
	var info = 'id=' + id;
	 if(confirm("<?php echo $lang['activate_job']; ?>"))
			  {
			var parent = $(this).parent().parent();
				$.ajax({
				 type: "GET",
				 url: "template/actions/activatejob.php",
				 data: info,
				 success: function()
					   {
						window.location.reload();
					   }
				});
			 
	
			  }
		   return false;
	
		});	
	
	});
	</script>
	
	<script type="text/javascript">
	$(function() {
	
	$(".btn-default").click(function(){
	
	//Save the link in a variable called element
	var element = $(this);
	
	//Find the id of the link that was clicked
	var id = element.attr("id");
	
	//Built a url to send
	var info = 'id=' + id;
	 if(confirm("<?php echo $lang['deactivate_job']; ?>"))
			  {
			var parent = $(this).parent().parent();
				$.ajax({
				 type: "GET",
				 url: "template/actions/deactivatejob.php",
				 data: info,
				 success: function()
					   {
						window.location.reload();
					   }
				});
			 
	
			  }
		   return false;
	
		});		
	
	});
	</script>	
	<script type="text/javascript">
	$(function() {
	
	$(".btn-kafe").click(function(){
	
	//Save the link in a variable called element
	var element = $(this);
	
	//Find the id of the link that was clicked
	var id = element.attr("id");
	
	//Built a url to send
	var info = 'id=' + id;
	 if(confirm("<?php echo $lang['make_public']; ?>"))
			  {
			var parent = $(this).parent().parent();
				$.ajax({
				 type: "GET",
				 url: "template/actions/makepublic.php",
				 data: info,
				 success: function()
					   {
						window.location.reload();
					   }
				});
			 
	
			  }
		   return false;
	
		});	
	
	});
	</script>
	
	<script type="text/javascript">
	$(function() {
	
	$(".btn-warning").click(function(){
	
	//Save the link in a variable called element
	var element = $(this);
	
	//Find the id of the link that was clicked
	var id = element.attr("id");
	
	//Built a url to send
	var info = 'id=' + id;
	 if(confirm("<?php echo $lang['hide_public']; ?>"))
			  {
			var parent = $(this).parent().parent();
				$.ajax({
				 type: "GET",
				 url: "template/actions/hidepublic.php",
				 data: info,
				 success: function()
					   {
						window.location.reload();
					   }
				});
			 
	
			  }
		   return false;
	
		});		
	
	});
	</script>		
    
</body>
</html>
