<?php 
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Client object
$client = new Client();
$obj = new DB();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
  Redirect::to('../index.php');	
}


$temp_type = $obj->getData("SELECT * FROM temp_type ORDER BY title ASC");
$temp_rate = $obj->getData("SELECT * FROM temp_rate ORDER BY rangemin ASC");
$temp_category = $obj->getData("SELECT * FROM temp_category ORDER BY title ASC");
$temp_skill = $obj->getData("SELECT * FROM temp_skill");
$temp_shift = $obj->getData("SELECT * FROM temp_shift ORDER BY title ASC");

//Add Category Function
if (Input::exists()) {
 if(Token::check(Input::get('token'))){
     $bonusStr = "";
     if(isset($_POST['bonus'])) {
         foreach ($_POST['bonus'] AS $k => $v) {
             $bonusStr .= $k . ',';
         }
         $bonusStr = rtrim($bonusStr, ',');
     }
	$errorHandler = new ErrorHandler;
	
	$validator = new Validator($errorHandler);
	
	$validation = $validator->check($_POST, [
	  'title' => [
		 'required' => true,
	  ],
	  'country' => [
	     'required' => true
	   ],
	  'category' => [
	     'required' => true
	   ],
	  'budget' => [
	     'required' => true,
	   ],
	  'start_date' => [
	     'required' => true
	   ],
	  'end_date' => [
	     'required' => true
	   ],
	  'skills_name[]' => [
	     'required' => true,
	  ],
	  'description' => [
	     'required' => true
	   ]
	]);
		 
    if (!$validation->fails()) {
    	  	 
			try{
			   $jobid = uniqueid();	
			   $skills = Input::get('skills_name');
               $choice1=implode(',',$skills);
			   $slug = seoUrl(Input::get('title'));	
			   $jobInsert = DB::getInstance()->insert('job', array(
				   'description' => Input::get('description'),
				   'jobid' => $jobid,
				   'clientid' => $client->data()->clientid,
				   'temp_category_id' => Input::get('temp_category_id'),
				   'temp_type_id' => Input::get('temp_type_id'),
				   'slug' => $slug,
				   'country' => Input::get('country'),
				   'job_type' => Input::get('job_type'),
				   'temp_shift_id' => Input::get('temp_shift_id'),
				   'temp_rate_id' => Input::get('temp_rate_id'),
				   'start_date' => Input::get('start_date'),
				   'end_date' => Input::get('end_date'),
				   'skills' => $choice1,
				   'active' => 1,
				   'delete_remove' => 0,
				   'public' => Input::get('make_public'),
				   'invite' => 0,
				   'chk_urgent' => Input::get('chk_urgent') ? 1 : 0,
				   'chk_prefunded' => Input::get('chk_prefunded') ? 1 : 0,
				   'chk_recruit' => Input::get('chk_recruit') ? 1 : 0,
				   'date_added' => date('Y-m-d H:i:s'),
                   'jobtimetype' => Input::get('jobtimetype'),
                   'bonus' => $bonusStr
			    ));	
					
			  if (count($jobInsert) > 0) {
				$noError = true;
			  } else {
				$hasError = true;
			  }
				  
			  
			}catch(Exception $e){
			 die($e->getMessage());	
			} 
	
	 } else {
     $error = '';
     foreach ($validation->errors()->all() as $err) {
     	$str = implode(" ",$err);
     	$error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
     }
   }
	
 }	  
}

?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('template/header.php'); ?> 
    <!-- Bootstrap Datetimepicker CSS -->
    <link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Select CSS-->
    <link  href="../assets/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

 <body class="skin-green sidebar-mini">
     
     <!-- ==============================================
     Wrapper Section
     =============================================== -->
	 <div class="wrapper">
	 	
        <!-- Include navigation.php. Contains navigation content. -->
	 	<?php include ('template/navigation.php'); ?> 
        <!-- Include sidenav.php. Contains sidebar content. -->
	 	<?php include ('template/sidenav.php'); ?> 
	 	
	 	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1><?php echo $lang['job']; ?><small><?php echo $lang['section']; ?></small></h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
            <li class="active"><?php echo $lang['add']; ?> <?php echo $lang['job']; ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">	
		    <!-- Include currency.php. Contains header content. -->
		    <?php include ('template/currency.php'); ?>   	
		 <div class="row">	
		 	
		 <div class="col-lg-12">	
         <?php if(isset($hasError)) { //If errors are found ?>
	       <div class="alert alert-danger fade in">
	        <a href="#" class="close" data-dismiss="alert">&times;</a>
	        <strong><?php echo $lang['hasError']; ?></strong> <?php echo $lang['has_Error']; ?>
		   </div>
	      <?php } ?>
	
		  <?php if(isset($noError) && $noError == true) { //If email is sent ?>
		   <div class="alert alert-success fade in">
		   <a href="#" class="close" data-dismiss="alert">&times;</a>
		   <strong><?php echo $lang['noError']; ?></strong> <?php echo $lang['saved_success']; ?></strong>
		   </div>
		  <?php } ?>
		 	
		  <?php if (isset($error)) {
			  echo $error;
		  } ?>

          </div>
           
		 <div class="col-lg-12">
		 
		 <!-- Input addon -->
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title"><?php echo $lang['add']; ?> <?php echo $lang['job']; ?></h3>
                </div>
                <div class="box-body">
                 <form role="form" method="post" id="addform">
                  <div class="form-group">	
				    <label>Temp Type</label>
                   <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-info"></i></span>
                    <!--<input type="text" name="title" class="form-control" placeholder="<?php //echo $lang['title']; ?>" value="<?php //echo escape(Input::get('title')); ?>"/>-->
                       <select name="temp_type_id" class="form-control">
                           <?php
                           foreach($temp_type AS $ttype) {
                               ?>
                               <option value="<?=$ttype['id']?>"><?=$ttype['title']?></option>
                               <?php
                           }
                           ?>
                       </select>
                   </div>
                  </div>
                  
                  <div class="form-group">	
				    <label>Location</label>
                   <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-info"></i></span>
					<select name="country" class="form-control">
                        <option value="Aberdeen">Aberdeen</option>
                        <option value="Ashford, Kent">Ashford, Kent</option>
                        <option value="Aylesbury">Aylesbury</option>
                        <option value="Aldershot">Aldershot</option>
                        <option value="Arnold, Nottinghamshire">Arnold, Nottinghamshire</option>
                        <option value="Birmingham">Birmingham</option>
                        <option value="Belfast">Belfast</option>
                        <option value="Brighton">Brighton</option>
                        <option value="Bristol">Bristol</option>
                        <option value="Bradford">Bradford</option>
                        <option value="Blackburn">Blackburn</option>
                        <option value="Blackpool">Blackpool</option>
                        <option value="Burley">Burley</option>
                        <option value="Cardiff">Cardiff</option>
                        <option value="Cambridge">Cambridge</option>
                        <option value="Chelmsford">Chelmsford</option>
                        <option value="Chelthenham">Chelthenham</option>
                        <option value="Chesterfield">Chesterfield</option>
                        <option value="Cornwall">Cornwall</option>
                        <option value="Coventry">Coventry</option>
                        <option value="Colchester">Colchester</option>
                        <option value="Darlington">Darlington</option>
                        <option value="Dartford">Dartford</option>
                        <option value="Derby">Derby</option>
                        <option value="Devon">Devon</option>
                        <option value="Doncaster">Doncaster</option>
                        <option value="Dorset">Dorset</option>
                        <option value="Dudley">Dudley</option>
                        <option value="Dundee">Dundee</option>
                        <option value="Durham">Durham</option>
                        <option value="Eastbourne">Eastbourne</option>
                        <option value="Eastleigh">Eastleigh</option>
                        <option value="East Kilbridge">East Kilbridge</option>
                        <option value="Exeter">Exeter</option>
                        <option value="Essex">Essex</option>
                        <option value="Fareham">Fareham</option>
                        <option value="Farnborough">Farnborough</option>
                        <option value="Fleet">Fleet</option>
                        <option value="Fleetwood">Fleetwood</option>
                        <option value="Folkestone">Folkestone</option>
                        <option value="Glasglow">Glasglow</option>
                        <option value="Gloucester">Gloucester</option>
                        <option value="Gravesend">Gravesend</option>
                        <option value="Grays">Grays</option>
                        <option value="Grantham">Grantham</option>
                        <option value="Greasby">Greasby</option>
                        <option value="Hull">Hull</option>
                        <option value="Huddersfield">Huddersfield</option>
                        <option value="Halesowen">Halesowen</option>
                        <option value="Halifax">Halifax</option>
                        <option value="Hereford">Hereford</option>
                        <option value="Ipswich">Ipswich</option>
                        <option value="Irvine">Irvine</option>
                        <option value="Ilkeston">Ilkeston</option>
                        <option value="Inverness">Inverness</option>
                        <option value="Irlam">Irlam</option>
                        <option value="Ingoldmells">Ingoldmells</option>
                        <option value="Jonstone">Jonstone</option>
                        <option value="Kettering">Kettering</option>
                        <option value="Kiddminster">Kiddminster</option>
                        <option value="Kingswood">Kingswood</option>
                        <option value="Keighley">Keighley</option>
                        <option value="Kendal">Kendal</option>
                        <option value="Kingston">Kingston</option>
                        <option value="Lancaster">Lancaster</option>
                        <option value="London East">London East</option>
                        <option value="London North">London North</option>
                        <option value="London South">London South</option>
                        <option value="London West">London West</option>
                        <option value="London Greater">London Greater</option>
                        <option value="Leeds">Leeds</option>
                        <option value="Leicester">Leicester</option>
                        <option value="Liverpool">Liverpool</option>
                        <option value="Lincoln">Lincoln</option>
                        <option value="Livingston">Livingston</option>
                        <option value="Littlehampton">Littlehampton</option>
                        <option value="Luton">Luton</option>
                        <option value="Machester">Machester</option>
                        <option value="Macclesfield">Macclesfield</option>
                        <option value="Maidenhead">Maidenhead</option>
                        <option value="Mansfield">Mansfield</option>
                        <option value="Maidstone">Maidstone</option>
                        <option value="Middlesborough">Middlesborough</option>
                        <option value="Milton Keynes">Milton Keynes</option>
                        <option value="Middlewich">Middlewich</option>
                        <option value="Morley">Morley</option>
                        <option value="Newcastle">Newcastle</option>
                        <option value="Newport">Newport</option>
                        <option value="Northampton">Northampton</option>
                        <option value="Norwhich">Norwhich</option>
                        <option value="Nottingham">Nottingham</option>
                        <option value="Nuneaton">Nuneaton</option>
                        <option value="Nuffield">Nuffield</option>
                        <option value="Oldham">Oldham</option>
                        <option value="Oadby">Oadby</option>
                        <option value="Oakdale">Oakdale</option>
                        <option value="Oakham">Oakham</option>
                        <option value="Oxford">Oxford</option>
                        <option value="Perth, Scotlan">Perth, Scotland</option>
                        <option value="Peterborough">Peterborough</option>
                        <option value="Peterlee">Peterlee</option>
                        <option value="Poole">Poole</option>
                        <option value="Portsmouth">Portsmouth</option>
                        <option value="Plymouth">Plymouth</option>
                        <option value="Preston">Preston</option>
                        <option value="Paignton">Paignton</option>
                        <option value="Reading">Reading</option>
                        <option value="Redhill">Redhill</option>
                        <option value="Redditch">Redditch</option>
                        <option value="Rotherham">Rotherham</option>
                        <option value="Rochdale">Rochdale</option>
                        <option value="Sheffield">Sheffield</option>
                        <option value="Slough">Slough</option>
                        <option value="Smethwick">Smethwick</option>
                        <option value="Solihul">Solihul</option>
                        <option value="Southampton">Southampton</option>
                        <option value="Stafford">Stafford</option>
                        <option value="St Albans">St Albans</option>
                        <option value="St Helens">St Helens</option>
                        <option value="Stockport">Stockport</option>
                        <option value="Stevenage">Stevenage</option>
                        <option value="Sunderland">Sunderland</option>
                        <option value="Swansea">Swansea</option>
                        <option value="Tamworth">Tamworth</option>
                        <option value="Taunton">Taunton</option>
                        <option value="Telford">Telford</option>
                        <option value="Torquay">Torquay</option>
                        <option value="Tonbridge">Tonbridge</option>
                        <option value="Uckfield">Uckfield</option>
                        <option value="Underwood">Underwood</option>
                        <option value="Wakefield">Wakefield</option>
                        <option value="Warrington">Warrington</option>
                        <option value="Watford">Watford</option>
                        <option value="Walsall">Walsall</option>
                        <option value="West Bromwich">West Bromwich</option>
                        <option value="Weston Super-Mere">Weston Super-Mere</option>
                        <option value="Wigan">Wigan</option>
                        <option value="Wolsley">Wolsley</option>
                        <option value="Wolverhampton">Wolverhampton</option>
                        <option value="Wolverton">Wolverton</option>
                        <option value="Woking, Surrey">Woking, Surrey</option>
                        <option value="Worthing">Worthing</option>
                        <option value="Worchester">Worchester</option>
                        <option value="Yapton">Yapton</option>
                        <option value="Yately">Yately</option>
                        <option value="Yeovil">Yeovil</option>
                        <option value="York">York</option>
					</select>		
				   </div>			    
                  </div> 
                  
                  <div class="form-group">	
				    <label>Job <?php echo $lang['category']; ?></label>
                   <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-pencil-square"></i></span>
                       <select name="temp_category_id" class="form-control">
                           <?php
                           foreach($temp_category AS $tcat) {
                               ?>
                               <option value="<?=$tcat['id']?>"><?=$tcat['title']?></option>
                               <?php
                           }
                           ?>
                       </select>
					<!--<select name="category" type="text" class="form-control">
					 <?php
                     /*
					  $query = DB::getInstance()->get("category", "*", ["AND" => ["active" => 1, "delete_remove" => 0]]);
						if ($query->count()) {
						   $categoryname = '';
						   $x = 1;
							 foreach ($query->results() as $row) {
							  echo $categoryname .= '<option value = "' . $row->catid . '">' . $row->name . '</option>';
							  unset($categoryname); 
							  $x++;
						     }
						}
                     */
					 ?>	
					</select>-->
                   </div>
                  </div>

                <div class="form-group">
                    <label>Temp <?php echo $lang['skills']; ?></label>
                    <div class="input-group">
                     <span class="input-group-addon"><i class="fa fa-pencil-square"></i></span>
                     <select class="selectpicker form-control" name="skills_name[]" type="text" title="Choose one of the following..." data-live-search="true" data-width="30%" data-selected-text-format="count > 3" multiple="multiple" data-max-options="6">
                         <?php
                         $query = DB::getInstance()->get("temp_skill", "*", ["ORDER" => "title ASC"]);
                         if ($query->count()) {
                             foreach($query->results() as $row) {
                                 $names[] = $row->title;
                             }
                         }

                         foreach($names as $key=>$name){
                             echo $skills .= '<option value = "'.$name.'" data-tokens="'.$name.'" >'.$name.'</option>';
                             unset($skills);
                             unset($name);
                         }
                         ?>
                     </select>
                    </div>
                </div>

                     <div class="form-group">
				    <label><?php echo $lang['budget']; ?></label>
                   <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                    <!--<input type="text" name="budget" class="form-control" placeholder="<?php //echo $lang['budget']; ?>" value="<?php //echo escape(Input::get('budget')); ?>"/>-->
                       <select name="temp_rate_id" class="form-control">
                           <?php
                           foreach($temp_rate AS $tbug) {
                               ?>
                               <option value="<?=$tbug['id']?>"><?=$tbug['title']?>: <?=$tbug['rangemin']?> <?=$tbug['rangemax'] ? '-' : '+'?> <?=$tbug['rangemax']?></option>
                               <?php
                           }
                           ?>
                       </select>
                   </div>
                  </div>

                  <div class="form-group">
				    <label>Shift Type</label>
                   <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                    <!--<input type="text" name="shift_type" class="form-control" value="<?php //echo escape(Input::get('budget')); ?>"/>-->
                       <select name="temp_shift_id" class="form-control">
                           <?php
                           foreach($temp_shift AS $tshift) {
                               ?>
                               <option value="<?=$tshift['id']?>"><?=$tshift['title']?></option>
                               <?php
                           }
                           ?>
                       </select>
                   </div>
                  </div>

				<!--  <div class="form-group">
                   <label for="dtp_input1"><?php echo $lang['estimated']; ?> <?php echo $lang['start']; ?> <?php echo $lang['date']; ?></label>
                    <div class="input-group date form_datetime_start" data-date-format="dd MM yyyy" data-link-field="dtp_input1">
                    <input name="start_date" class="form-control" type="text" value="" readonly>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
					<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                    </div>
				   <input type="hidden" id="dtp_input1" value="" /><br/>
		           <input name="mirror_field_start" type="hidden" id="mirror_field_start" class="form-control" readonly />
		           <input name="mirror_field_start_date" type="hidden" id="mirror_field_start_date" class="form-control" readonly />
                  </div> 
                  
				  <div class="form-group">
                   <label for="dtp_input1"><?php echo $lang['estimated']; ?> <?php echo $lang['end']; ?> <?php echo $lang['date']; ?></label>
                    <div class="input-group date form_datetime_end" data-date-format="dd MM yyyy" data-link-field="dtp_input1">
                    <input name="end_date" class="form-control" type="text" value="" readonly>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
					<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                    </div>
				   <input type="hidden" id="dtp_input1" value="" /><br/>
		           <input name="mirror_field_start" type="hidden" id="mirror_field_start" class="form-control" readonly />
		           <input name="mirror_field_start_date" type="hidden" id="mirror_field_start_date" class="form-control" readonly />
                  </div> -->
                                
                                <div class="form-group">

               <div class="input-group">
                     <label for="dtp_input1"><?php echo $lang['estimated']; ?> <?php echo $lang['start']; ?> <?php echo $lang['date']; ?></label>
                      <div class="input-group date form_datetime_start" data-date-format="dd MM yyyy" data-link-field="dtp_input1">
                          <input name="start_date" class="form-control" type="text" value="" readonly>
                          <span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
      					           <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                      </div>
  				            <input type="hidden" id="dtp_input1" value="" /><br/>
        		           <input name="mirror_field_start" type="hidden" id="mirror_field_start" class="form-control" readonly />
        		           <input name="mirror_field_start_date" type="hidden" id="mirror_field_start_date" class="form-control" readonly />

                 </div>
                  <div class="input-group">
                    <label for="dtp_input1"><?php echo $lang['estimated']; ?> <?php echo $lang['end']; ?> <?php echo $lang['date']; ?></label>
                      <div class="input-group date form_datetime_end" data-date-format="dd MM yyyy" data-link-field="dtp_input1">
                          <input name="end_date" class="form-control" type="text" value="" readonly>
                          <span class="input-group-addon"><i class="glyphicon glyphicon-remove"></i></span>
                          <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                      </div>
                      <input type="hidden" id="dtp_input1" value="" /><br/>
                       <input name="mirror_field_start" type="hidden" id="mirror_field_start" class="form-control" readonly />
                       <input name="mirror_field_start_date" type="hidden" id="mirror_field_start_date" class="form-control" readonly />

                  </div>


                  </div> 

				  <br/>                          
                  
                  <div class="form-group">	
				    <label><?php echo $lang['job']; ?> <?php echo $lang['description']; ?></label>
                      <textarea type="text" id="summernote" name="description" class="form-control"></textarea>
                  </div>

                  <h3 style="color: limegreen">Add some extras!</h3>
                  <h2 style="color: limegreen; margin-bottom: 0px">Bonuses:</h2>
                     <small>Please tick the relevant box(es) below if you wish to offer incentives to attract the best temp worker.</small>
                     <div class="row" style="border-right: 1px solid #B70000; margin-right: 22px;">
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[travelpaid]" />Travel Paid</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[accommodationprovided]" />Accommodation Provided</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[freemeals]" />Free Meals</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[taxipaid]" />Taxi Paid</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[uniformprovided]" />Uniform Provided</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[freetraining]" />Free Training</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[mileagepaid]" />Mileage Paid</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[teacoffeeprovided]" />Tea & Coffee Provided</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[freecarrental]" />Free Car Rental</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[crbpaid]" />CRB Paid</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[inductionprovided]" />Induction Provided</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[freevoucherstickets]" />Free Vouchers/Tickets</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[holidayspaid]" />Holidays Paid</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[maternityleave]" />Maternity Leave</label>
                             </div>
                         </div>
                         <div class="col-lg-4">
                             <div class="checkbox">
                                 <label><input type="checkbox" name="bonus[other]" />Other</label>
                             </div>
                         </div>
                     </div>
                     <div class="row" style="width:calc(100% - 25px); margin-top: 10px;">
                         <div style="border: 1px solid green;box-sizing: border-box;/*! width: 95%; */margin-left: 20px;border-radius: 3px;padding: 10px" class="col-xs-12">
                             <div style="display: inline-block;">
                                 <input name="chk_urgent" style="position: relative;top: -8px;margin-right: 10px;" type="checkbox">
                                 <span class="fa fa-clock"></span>
                             </div>
                             <div style="display: inline-block;">
                                 <p style="margin-bottom: 0px; font-size: 16px;font-weight: bold">Mark your job as urgent<span style="margin-left: 12px;border-radius:5px;background-color: orange; font-weight: normal; padding:3px">Urgent</span></p>
                                 <p style="margin-bottom: 0;">Get you temp vacancy fill 5x faster</p>
                             </div>
                         </div>
                     </div>
                     <div class="row" style="width:calc(100% - 25px); margin-top: 10px;">
                         <div style="border: 1px solid  green;box-sizing: border-box;/*! width: 95%; */margin-left: 20px;border-radius: 3px;padding: 10px" class="col-xs-12">
                             <div style="display: inline-block;">
                                 <input name="chk_prefunded" style="position: relative;top: -8px;margin-right: 10px;" type="checkbox">
                                 <span class="fa fa-clock"></span>
                             </div>
                             <div style="display: inline-block;">
                                 <p style="margin-bottom: 0px; font-size: 16px;font-weight: bold">NDA<span style="margin-left: 12px;border-radius:5px;background-color: lightgreen; font-weight: normal; padding:3px">Pre-funded</span></p>
                                 <p style="margin-bottom: 0;">Get temps to sign a confidentiality agreement to protect your company's privacy.</p>
                             </div>
                         </div>
                     </div>
                     <div class="row" style="width:calc(100% - 25px); margin-top: 10px;">
                         <div style="border: 1px solid  green;box-sizing: border-box;/*! width: 95%; */margin-left: 20px;border-radius: 3px;padding: 10px" class="col-xs-12">
                             <div style="display: inline-block;">
                                 <input name="chk_recruit" style="position: relative;top: -8px;margin-right: 10px;" type="checkbox">
                                 <span class="fa fa-clock"></span>
                             </div>
                             <div style="display: inline-block;">
                                 <p style="margin-bottom: 0px; font-size: 16px;font-weight: bold">Le us do the Scounting for you<span style="margin-left: 12px;border-radius:5px;background-color: violet; font-weight: normal; padding:3px">Recruiter 24/7</span></p>
                                 <p style="margin-bottom: 0;">Let us send you a short list of best talents that matches your vacancy</p>
                             </div>
                         </div>
                     </div>
                  <div class="form-group">	
				    <label><?php echo $lang['make']; ?> <?php echo $lang['job']; ?> <?php echo $lang['public']; ?></label>
				    <div class="checkbox">
					  <label><input type="hidden" name="make_public" value="0"></label>
					  <label><input type="checkbox" name="make_public" value="1"><?php echo $lang['make_public']; ?></label>
					</div>								    
                  </div>
                  <div class="form-group">
                      <label class="radio-inline"><input type="radio" value="15" name="jobtimetype" checked>Urgent Post</label>
                      <label class="radio-inline"><input type="radio" value="10" name="jobtimetype">NDA</label>
                      <label class="radio-inline"><input type="radio" value="0" name="jobtimetype">Free Post</label>
                  </div>

                     <div class="form-group">
                         <label>Decalaration</label>
                         <div class="checkbox">
                             <label><input type="checkbox" name="make_public" value="1" required>
                             <p>
                                 I confirm that the information provided in the job post, is true and accurate, according to the best of
                                 my ability. I understand that any false statement or misinterpretation will amount to sufficient grounds
                                 to bring legal action against me and/or terminate my membership. I also understand that the data provided
                                 here, complies with the Healthcare-temps inc privacy policy & GDPR 2018.
                             </p>
                             </label>
                         </div>
                     </div>

                  <div class="box-footer">
                    <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                    <button type="submit" name="data" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                  </div>
                 </form> 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
		 
		</div><!-- /.col -->
		
        
			 
	    </div><!-- /.row -->		  		  
	   </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	 
      <!-- Include footer.php. Contains footer content. -->	
	  <?php include 'template/footer.php'; ?>	
	 	
     </div><!-- /.wrapper -->   

	
	<!-- ==============================================
	 Scripts
	 =============================================== -->
	 
    <!-- jQuery 2.1.4 -->
    <script src="../assets/js/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.6 JS -->
    <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../assets/js/app.min.js" type="text/javascript"></script>
    <!-- Datetime Picker -->
    <script src="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script type="text/javascript">
     $('.form_datetime_start').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1, 
        startDate: new Date(),
        pickTime: false, 
        minView: 2,      
        pickerPosition: "bottom-left",
        linkField: "mirror_field_start",
        linkFormat: "hh:ii",
        linkFieldd: "mirror_field_start_date",
        linkFormatt: "dd MM yyyy"
    });
     $('.form_datetime_end').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1, 
        startDate: new Date(),
        pickTime: false, 
        minView: 2,      
        pickerPosition: "bottom-left",
        linkField: "mirror_field_start",
        linkFormat: "hh:ii",
        linkFieldd: "mirror_field_start_date",
        linkFormatt: "dd MM yyyy"
    });
   </script>
    <!-- Summernote WYSIWYG-->
    <script src="../assets/js/summernote.min.js" type="text/javascript"></script>    
    <script>
    $(document).ready(function() {
     $('#summernote').summernote({
		  height: 300,                 // set editor height
		
		  minHeight: null,             // set minimum height of editor
		  maxHeight: null,             // set maximum height of editor
		
		  focus: false,                 // set focus to editable area after initializing summernote
		});    
    });
    </script>
    <!-- Bootstrap Select JS-->
    <script src="../assets/js/bootstrap-select.js"></script>
</body>
</html>
