<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Client object
$client = new Client();
$dbObj = new DB();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
  Redirect::to('../index.php');	
}

//Get Instructor's Data
$query = $dbObj->getData("SELECT * FROM profile WHERE userid = ". $client->data()->clientid);
if (count($query)) {
    $flag = 1;
    $profileArr = $query[0];
}
$client_social = $dbObj->getData("SELECT * FROM client_info WHERE clientid = " . $client->data()->id)[0];

$rating = $dbObj->getData("SELECT sum(star) AS count FROM ratings_client WHERE clientid = ". $client->data()->clientid. " AND star_type = 5");
if(count($rating)) {
    $counts = $rating[0]['count'];
}
$completedJobs = $dbObj->getData("SELECT count(id) AS count FROM job WHERE clientid = ". $client->data()->clientid. " AND completed = 1");
if(count($completedJobs)) {
    $completedJobsCounts = $completedJobs[0]['count'];
}
$postedJobs = $dbObj->getData("SELECT count(id) AS count FROM job WHERE clientid = ". $client->data()->clientid);
if(count($postedJobs)) {
    $postedJobsCounts = $postedJobs[0]['count'];
}
$inviteJobs = $dbObj->getData("SELECT count(id) AS count FROM job WHERE clientid = ". $client->data()->clientid. " AND invite = 1");
if(count($inviteJobs)) {
    $inviteJobsCounts = $inviteJobs[0]['count'];
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('template/header.php'); ?> 

 <body class="skin-green sidebar-mini">
     <!-- ==============================================
     Wrapper Section
     =============================================== -->
	 <div class="wrapper">
        <!-- Include navigation.php. Contains navigation content. -->
	 	<?php include ('template/navigation.php'); ?> 
        <!-- Include sidenav.php. Contains sidebar content. -->
	 	<?php include ('template/sidenav.php'); ?> 
	 	
	  <!-- Content Wrapper. Contains page content -->
         <style>
             .skin-green .wrapper, .skin-green .main-sidebar, .skin-green .left-side {
                 background-color: #FFF;
             }
             .gogreen {
                 color: #20B34B;
             }
             .goblock, .info_line {
                 display: block;
             }
             .profile_box,.overview_box, .performance_box, .social_box {
                 border: 1px solid lightgrey;
                 padding: 18px;
                 margin-top: 25px;
                 background-color: #FFF;
                 box-shadow: 2px 2px 5px -3px;
             }
             /*.profile_image {*/
                 /*border: 1px solid black;*/
                 /*margin-top: 64px;*/
                 /*padding: 13px;*/
                 /*width: 150px;*/
                 /*height: 150px;*/
             /*}*/
             span.gogreen {
                 font-size: 17px;
                 font-weight: bold;
             }
             span.goblack {
                 font-weight: bold;
                 color: grey;
                 margin-left: 17px;
             }
             .goblock_heading {
                 font-weight: bold;
                 padding: 10px;
             }
             span.goblock {
                 padding: 10px;
                 color: grey;
                 font-size: 17px;
             }
             .contact_icons > span {
                 margin-right: 18px;
                 margin-top: 27px;
                 font-size: 19px;
                 color: green;
             }
             .post_job_btn {
                 margin-left: 63px;
                 font-size: 15px;
                 border-radius: 0px;
                 color: #FFF;
                 background-color: #20B34B !important;
             }
             .image_frame {
                 padding: 6px;
                 margin-right: -4px;
                 border: 2px solid #A1AAB1;
             }
             .profile_image {
                 max-height: 130px;
                 max-width: 130px;
             }
         </style>
         <div class="container col-lg-offset-2" style="background-color: #EEEFF3">
             <div class="row">
                 <div class="col-lg-9">
                     <div class="row col-lg-offset-2 profile_box" style="margin-top: 15px;padding: 15px;">
                         <div class="col-lg-3" style="padding-top: 22px;">
                             <div class="image_frame">
                                <img class="profile_image" src="<?=isset($flag) ? $client->data()->imagelocation : ''?>" alt="">
                             </div>
                             <div class="contact_icons" style="margin-left: 22px;">
                                 <span class="fa fa-envelope"></span>
                                 <span class="fa fa-user" onclick="window.location.href = 'interviewroom.php'"></span>
                                 <span class="fa fa-phone" onclick="window.location.href = 'live_chat.php'"></span>
                             </div>
                         </div>
                         <div class="col-lg-6">
                             <div class="about_box">
                                 <div class="info_line">
                                     <span class="gogreen">Company Name:</span> <span class="goblack"><?=isset($flag) ? $profileArr['company_name'] : ''?></span>
                                 </div>
                                 <div class="info_line">
                                     <span class="gogreen">Manager's Name:</span> <span class="goblack"><?=$client->data()->name?></span>
                                 </div>
                                 <div class="info_line">
                                     <span class="gogreen">Care Settings:</span> <span class="goblack"><?=isset($flag) ? $profileArr['care_settings'] : ''?></span>
                                 </div>
                                 <div class="info_line">
                                     <span class="gogreen">Service Users:</span> <span class="goblack">NHS Patients</span>
                                 </div>
                                 <div class="info_line">
                                     <span class="gogreen">Location:</span> <span class="goblack"><?=isset($flag) ? $profileArr['location'] : ''?></span>
                                 </div>
                                 <div class="info_line">
                                     <span class="gogreen">Avg Pay Rate:</span> <span class="goblack"><?=isset($flag) ? $currency_symbol.' '.$profileArr['rate'] : ''?></span>
                                 </div>
                                 <div class="info_line">
                                     <span class="gogreen">No. of Employees:</span> <span class="goblack"><?=isset($flag) ? $profileArr['no_patients'] : ''?></span>
                                 </div>
                             </div>
                         </div>
                         <a href="addjob.php"><button class="btn post_job_btn" style="">Post Job</button></a>
                     </div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-lg-8" style="margin-left: 139px;padding-right: 40px;">
                     <div class="overview_box">
                         <h2 class="gogreen">Overview:</h2>
                         <p><?=isset($flag) ? $profileArr['about'] : ''?></p>
                     </div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-lg-8" style="margin-left: 140px;padding-right: 40px;margin-bottom: 22px;">

                     <div class="performance_box">
                         <h2 class="gogreen">Performance:</h2>
                         <div class="row">
                             <div class="col-lg-6">
                                 <span class="goblock_heading">Jobs Posted</span>
                                 <span class="goblock"><?=isset($postedJobsCounts) ? $postedJobsCounts : '0'?></span>
                                 <span class="goblock_heading">Jobs Invites</span>
                                 <span class="goblock"><?=isset($inviteJobsCounts) ? $inviteJobsCounts : '0'?></span>
                                 <span class="goblock_heading">Jobs Completed</span>
                                 <span class="goblock"><?=isset($completedJobsCounts) ? $completedJobsCounts : '0'?></span>
                             </div>
                             <div class="col-lg-6">
                                 <span class="goblock_heading">Ratings (<?=isset($counts) ? $counts : ''?>)</span>
                                 <span class="goblock"><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span><span class="fa fa-star"></span></span>
                                 <span class="goblock_heading">Jobs Payments</span>
                                 <span class="goblock"><?=$currency_symbol?> 90</span>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="row">
                 <div class="col-lg-8" style="margin-left: 140px;padding-right: 40px;margin-bottom: 22px;">
                     <div class="social_box">
                         <h2 class="gogreen">Follow us on Social Media:</h2>
                         <div class="row">
                             <div class="col-lg-6">
                                 <span class="goblock_heading">Facebook</span>
                                 <span class="goblock"><?=count($client_social) ? $client_social['fb'] : ''?></span>
                                 <span class="goblock_heading">Skype</span>
                                 <span class="goblock"><?=count($client_social) ? $client_social['skype'] : ''?></span>
                             </div>
                             <div class="col-lg-6">
                                 <span class="goblock_heading">Linkedin</span>
                                 <span class="goblock"><?=count($client_social) ? $client_social['linkedin'] : ''?></span>
                                 <span class="goblock_heading">Gruveo</span>
                                 <span class="goblock"><?=count($client_social) ? $client_social['gruveo'] : ''?></span>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
	 	
      <!-- Include footer.php. Contains footer content. -->	
	  <?php include 'template/footer.php'; ?>	
	 	
     </div><!-- /.wrapper -->   

	
	<!-- ==============================================
	 Scripts
	 =============================================== -->

     <!-- jQuery 2.1.4 -->
     <script src="../assets/js/jQuery-2.1.4.min.js"></script>
     <!-- Bootstrap 3.3.6 JS -->
     <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
     <!-- AdminLTE App -->
     <script src="../assets/js/app.min.js" type="text/javascript"></script>

     <script src="../assets/js/jquery.knob.min.js"></script>

    
</body>
</html>
