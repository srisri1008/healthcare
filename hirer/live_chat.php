<?php 
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
  Redirect::to('../index.php');	
}
function arrayToObject($d) {
    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return (object) array_map(__FUNCTION__, $d);
    }
    else {
        // Return object
        return $d;
    }
}
//Get temps for chat
//$allTemps = DB::getInstance()->get('freelancer', '*', ['LIMIT' => 100]);
//if($allTemps->count()) {
//    $allTemps = $allTemps->results();
//}
$ob = new DB();
$result = $ob->getData("SELECT freelancer.* FROM job LEFT JOIN freelancer ON freelancer.freelancerid = job.freelancerid WHERE job.clientid = ".$client->data()->clientid." AND freelancer.freelancerid <> '' GROUP BY freelancer.id");
if(count($result)) {
    $allTemps = arrayToObject($result);
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>
<!-- AdminLTE CSS -->
<link href="../assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- Datetime Picker -->
<link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery UI CSS -->
<link href="../assets/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
<!-- Progress CSS -->
<link href="../assets/css/progress.css" rel="stylesheet" type="text/css" />

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>

<script src="../assets/js/jquery.knob.min.js"></script>


<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <!-- Include currency.php. Contains header content. -->
            <?php include ('template/currency.php'); ?>
            <style>
                ul.client_list {
                    list-style-type: none;
                    padding: 0px;
                }
                li.single_client:hover {
                    background-color: rgba(180,180,180,0.5);
                }
                .single_client{
                    margin-bottom: 10px;
                    cursor: pointer;
                }
                .single_client img {
                    border-radius: 50%;
                    width: 64px;
                    height: 64px;
                    display: inline-block;
                }

                .single_client h4 {
                    display:  inline-block;
                    margin-left: 12px;
                }
                .chat-section > div {
                    border: 1px solid lightgrey;
                }
                .message_client ,.message_temp{
                    background-color: #00acd6;
                    display: inline-block;
                    padding: 5px 30px;
                    border-radius: 15px;
                    float: left;
                    clear: both;
                    color: #ffffff;
                }
                .message_temp {
                    background-color: #00b300;
                    float: right;
                    clear: both;
                }
            </style>
            <div id="sendtoid" style="display: none"></div>
            <div class="row chat-section">
                <div class="col-lg-3">
                    <script>
                        function getMessages(thisRef) {
                            userid = $(thisRef).data('id');
                            $.ajax({
                                url: "getmessages.php",
                                type: 'POST',
                                data: {userid},
                                success: function (data) {
                                    $('#sendtoid').html(userid);
                                    $('#message_form').css('display', '');
                                    var parsed = JSON.parse(data);
                                    parsed.sort(function (a, b) {
                                        return a.sent_at > b.sent_at;
                                    });
                                    var toAppend = "";
                                    var current_messagetype = "";
                                    var last_messagetype = "";
                                    parsed.forEach(function (value, index) {
                                        current_messagetype = value.user_type;
                                        if(current_messagetype == last_messagetype) {
                                            toAppend += '<br />'+value.message;
                                        } else {
                                            if(value.user_type == 2) {
                                                toAppend += '<h4 class="message_temp" data-type="2">'+value.message;
                                            } else {
                                                toAppend += '<h4 class="message_client" data-type="1">'+value.message;
                                            }
                                        }
                                        last_messagetype = value.user_type;
                                    });
                                    $('.single_message').html(toAppend);
                                }
                            })
                        }
                    </script>
                    <!-- foreach get all chatted clients -->
                    <?php
                    if(isset($allTemps)) {
                        echo '<ul class="client_list">';
                        foreach ($allTemps AS $singleTemp) {
                            ?>
                            <li class="single_client" onclick="getMessages(this)" data-id="<?=$singleTemp->id?>" data-name="<?=$singleTemp->name?>">
                                <img src="https://<?=$_SERVER['HTTP_HOST']?>/tempworker/<?=$singleTemp->imagelocation?>" alt="">
                                <h4><?=$singleTemp->name?></h4>
                            </li>
                            <?php
                        }
                        echo '</ul>';
                    }
                    ?>
                </div>
                <div class="col-lg-8" style="max-height: 400px;overflow-y: auto;">
                    <!-- Chat with Client section -->
                    <div class="single_message">
                        <!-- if client message left css class otherwise message to right -->
                        <h2>Please select user to chat</h2>
                    </div><br />
                    <form style="display: none" id="message_form">
                        <div class="form-group">
                            <input type="text" class="form-control" id="typehere"><br />
                            <button class="btn btn-primary" id="btnsend">Send Message</button>
                        </div>
                    </form>
                    <div id="results"></div>
                    <iframe id="ifrm" style="display: none" src="//healthcare-temps.co.uk:3000"></iframe>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->
<!-- ==============================================
 Scripts
 =============================================== -->
<script>
    function getMessage (e) {
        data = JSON.parse(e.data);
        console.log(e);
        if(data.send_to_id == <?=$client->data()->id?>) {
            $('.single_message').append('<h4 class="message_temp">'+data.message+'</h4>');
        }
    }
    if (window.addEventListener) {
        // For standards-compliant web browsers
        window.addEventListener("message", getMessage, false);
    }
    else {
        window.attachEvent("onmessage", getMessage);
    }

    $(function () {
        $('#btnsend').click(function (e) {
            e.preventDefault();
            var dataToSend = JSON.stringify({"user_type": "1", "send_from_id": "<?=$client->data()->id?>", "send_to_id": $('#sendtoid').html(),"message": $('#typehere').val()});
            $.ajax({
                url: 'savemessages.php',
                type: 'POST',
                data: {"user_type": "1", "send_from_id": "<?=$client->data()->id?>", "send_to_id": 34,"message": $('#typehere').val()},
                success: function (data) {
                    console.log(data);
                }
            });
            document.getElementById('ifrm').contentWindow.postMessage(dataToSend, '*');
            $('.single_message').append('<h4 class="message_client">'+$('#typehere').val()+'</h4>');
            $('#typehere').val('');
        });

    });
</script>

</body>
</html>
