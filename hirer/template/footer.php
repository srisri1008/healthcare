<!-- Modal -->
<div id="dispute_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">DISPUTE RESOLUTION:</h4>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <form method="post" id="dispute_form" onsubmit="return false;">
                            <h3>Initiate Dispute</h3>
                            <p>
                                Before initiating a dispute, please refer to the dispute resolution procedures in the applicableescrow
                                instructions. We also ask that you:
                            </p>
                            <h4>1. Talk to your Temp worker / Hirer</h4>
                            <p>
                                We recommend giving your buyer or supplier a chance to resolve the issue before lodging a dispute. Temp
                                workers have the ability to refund hours/money directly, so working it out directly usually results in the best
                                outcome for everyone.
                            </p>
                            <h4>2. Check your billing cycle</h4>
                            <p>
                                Hours from the previous week, from the time an invoice is received on Monday through the following Friday
                                night at midnight GMT, are the only ones eligible for dispute.
                            </p>
                            <h4>Dispute only time worked</h4>
                            <p>
                                If your hourly-rate temp worker rejects your dispute and Healthcare-Temps Inc steps in to mediate, we will not
                                evaluate the quality of the work performed. Instead, we will review the Work sheet/time-sheet to determine if
                                the Temp worker has performed according to best practices. Refunds will be issued if we find you were
                                charged for time not spent working on your job.<br />
                                We hope this issue is resolved to your satisfaction. Best of luck and thanks for using Healthcare-Temps.
                            </p>
                            <p>
                                What is the reason for the dispute?
                                <div class="checkbox">
                                    <label><input type="checkbox" name="dispute_reason[Quality of work]" value="Quality of work">Quality of work</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="dispute_reason[Missed deadlines]" value="Missed deadlines">Missed deadlines</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="dispute_reason[Temp worker not responsive]" value="Temp worker not responsive">Temp worker not responsive</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="dispute_reason[Excessive hours logged]" value="Excessive hours logged">Excessive hours logged</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="dispute_reason[Excessive bonus request]" value="Excessive bonus request">Excessive bonus request</label>
                                </div>
                            </p>
                            <div class="form-group">
                                <label for="disputed_hours">How many hours do you dispute?</label>
                                <input type="text" name="dispute_hours">
                            </div>

                            <div class="checkbox">
                                <label><input type="checkbox" name="dispute_reason[other]" value="Other">Other</label>
                            </div>
                            <div class="form-group">
                                <label for="comment">Please provide as many details about the dispute as possible:</label>
                                <textarea class="form-control" name="comment" rows="5" id="comment"></textarea>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox" id="agreed" value="">I certify, I have read and understand the Healthcare-temps dispute resolution policy.</label>
                            </div>
                            <input type="hidden" name="request">
                            <button class="btn btn-primary" id="dispute_submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $(function () {
        $('#dispute_submit').click(function () {
            if($('#agreed').prop('checked') != true) {
                alert('You need to agree to the terms\n to proceed');
                return false;
            }
            $.ajax({
                url: 'submit_dispute.php',
                type: 'POST',
                data: $('#dispute_form').serialize(),
                success: function (response) {
                    if(response === 'success') {
                        $('#dispute_modal').modal('hide');
                        document.getElementById("dispute_form").reset();
                        alert('Message sent');
                    }
                    else {
                        alert('Unable to process reuqest at the time.');
                    }
                }
            })
        });
    })
</script>

<?php echo $google_analytics; ?>
  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Version 2.0
    </div>
    <!-- Default to the left -->
    <strong><a href="index.php"><?php echo escape($title); ?></a> <?php auto_copyright('2016','All Rights Reserved'); ?>.</strong>
  </footer>
