<?
$client = new Client();

$basename = basename($_SERVER["REQUEST_URI"], ".php");
$editname = basename($_SERVER["REQUEST_URI"]);
?>
<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar" style="background-color: #23B14D;">
          <style>
              .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
                  color: #fff;
                  background-color: #23B14D;
              }
          </style>
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo escape($client->data()->imagelocation); ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?php echo escape($client->data()->name); ?></p>
              <!-- Status -->
              <a href="profile.php?a=profile"><i class="fa fa-circle text-success"></i> <?php echo $lang['online']; ?></a>
            </div>
          </div>


          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header"><?php echo $lang['sidenav_header_2']; ?></li>
            <!-- Optionally, you can add icons to the links -->
            <li class="<?php echo $active = ($basename == 'index') ? ' active' : ''; ?>">
            	<a href="index.php"><i class='fa fa-dashboard'></i> <span><?php echo $lang['dashboard']; ?></span></a>
            </li>
            <li class="treeview<?php echo $active = ($basename == 'joblist') ? ' active' : ''; echo $active = ($basename == 'addjob') ? ' active' : ''; echo $active = ($editname == 'editjob.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
              <a href="#"><i class='fa fa-align-left'></i> <span><?php echo $lang['jobs']; ?></span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="joblist.php"><?php echo $lang['job']; ?> <?php echo $lang['list']; ?></a></li>
                <li><a href="addjob.php"><?php echo $lang['post']; ?> <?php echo $lang['a']; ?> <?php echo $lang['job']; ?></a></li>
              </ul>
            </li> 
            <li class="treeview<?php echo $active = ($basename == 'jobinvite') ? ' active' : ''; echo $active = ($basename == 'addinvite') ? ' active' : ''; echo $active = ($editname == 'editinvite.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'invite.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'viewinvite.php?id='. Input::get('id').'') ? ' active' : '';?>">
              <a href="#"><i class='fa fa-filter'></i> <span><?php echo $lang['jobs']; ?> <?php echo $lang['invites']; ?></span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="jobinvite.php"><?php echo $lang['job']; ?> <?php echo $lang['invites']; ?> <?php echo $lang['list']; ?></a></li>
                <li><a href="addinvite.php"><?php echo $lang['post']; ?> <?php echo $lang['a']; ?> <?php echo $lang['job']; ?> <?php echo $lang['invite']; ?></a></li>
              </ul>
            </li> 
            <li class="treeview<?php 
               echo $active = ($basename == 'proposallist') ? ' active' : ''; echo $active = ($editname == 'proposallist.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'editproposal.php?id='. Input::get('id').'') ? ' active' : ''; echo $active = ($editname == 'viewproposal.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
             <a href="proposallist.php"><i class='fa fa-files-o'></i> <span><?php echo $lang['proposals']; ?></span>
                <span class="label label-info pull-right">
                    <?php 	
                     $q1 = DB::getInstance()->get("proposal", ["[>]job" => ["proposal.jobid" => "jobid"]], "*", ["AND" => ["job.clientid" => $client->data()->clientid, "proposal.opened" => 0, "proposal.delete_remove" => 0]]);
					 echo $q1->count();
                     ?></span></a>
            </li>    
            <li class="<?php echo $active = ($basename == 'jobassigned') ? ' active' : ''; echo $active = ($editname == 'job.php?a='. Input::get('a').'&id='. Input::get('id').'') ? ' active' : '';?>">
            	<a href="jobassigned.php"><i class='fa fa-address-card'></i> <span><?php echo $lang['bids']; ?> <?php echo $lang['assigned']; ?></span></a>
            </li>       
            <li class="header"><?php echo $lang['payments']; ?></li> 
            <li class="<?php echo $active = ($basename == 'paymentlist') ? ' active' : '';?>">
            	<a href="paymentlist.php"><i class='fa fa-list'></i> <span><?php echo $lang['payments']; ?> <?php echo $lang['list']; ?></span></a>
            </li>
            <li class="<?php echo $active = ($basename == 'addpayment') ? ' active' : '';?>">
            	<a href="addpayment.php"><i class='fa fa-list'></i> <span>Add Payment Type</span></a>
            </li>
              <li class="<?php echo $active = ($basename == 'rota') ? ' active' : '';?>">
                  <a href="#"><i class='fa fa-list'></i> <span>Rota Management</span></a>
              </li>
            <li class="<?php echo $active = ($basename == 'resources') ? ' active' : '';?>">
            	<a href="resources.php"><i class='fa fa-list'></i> <span>Resources</span></a>
            </li>
            <li class="header"><?php echo $lang['sidenav_header_3']; ?></li>          
            <li class="treeview<?php 
               echo $active = ($basename == 'overview') ? ' active' : '';   ?>">
             <a href="overview.php?a=profile"><i class='fa fa-info-circle'></i> <span>Edit Profile</span></a>
            </li>
            <li class="treeview<?php
               echo $active = ($basename == 'overview') ? ' active' : '';   ?>">
             <a href="view_profile.php"><i class='fa fa-info-circle'></i> <span>View Profile</span></a>
            </li>
            <li class="treeview<?php
               echo $active = ($basename == 'overview') ? ' active' : '';   ?>">
             <a href="/hire_temps.php"><i class='fa fa-info-circle'></i> <span>Temp Search</span></a>
            </li>
            <li class="header"><?php echo $lang['sidenav_header_4']; ?></li>
            <li class="blue-padding <?php echo $active = ($basename == 'livechat') ? 'active' : ''; ?>">
              <a href="live_chat.php"><i class='fa fa-dashboard'></i> <span><?php echo $lang['livechat']; ?></span></a>
            </li>
            <li class="treeview<?php echo $active = ($basename == 'inbox') ? ' active' : ''; echo $active = ($basename == 'compose') ? ' active' : ''; echo $active = ($basename == 'sent') ? ' active' : ''; echo $active = ($basename == 'favorite') ? ' active' : ''; echo $active = ($basename == 'trash') ? ' active' : ''; echo $active = ($editname == 'message.php?id='. Input::get('id').'') ? ' active' : '';?>">
              <a href="#">
                <i class="fa fa-envelope"></i> <span><?php echo $lang['mailbox']; ?></span>
                <span class="label label-info pull-right" style="margin-right: 20px;">
                    <?php 	
                     $q1 = DB::getInstance()->get("message", "*", ["AND" => ["user_to" => $client->data()->clientid, "opened" => 0, "delete_remove" => 0, "disc" => 0]]);
					 echo $q1->count();
                     ?></span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="inbox.php"><?php echo $lang['inbox']; ?> 
                <span class="label label-info pull-right">
                    <?php 	
                     $q1 = DB::getInstance()->get("message", "*", ["AND" => ["user_to" => $client->data()->clientid, "opened" => 0, "delete_remove" => 0, "disc" => 0]]);
					 echo $q1->count();
                     ?></span></a></li>
                <li><a href="compose.php"><?php echo $lang['compose']; ?></a></li>
              </ul>
              <li class="<?php echo $active = ($basename == 'interviewroom') ? ' active' : '';?>">
                  <a href="interviewroom.php"><i class='fa fa-list'></i> <span>Interview Room</span></a>
              </li>
              <li class="<?php echo $active = ($basename == 'advertise') ? ' active' : '';?>">
                  <a href="advertise.php"><i class='fa fa-list'></i> <span>Advertise</span></a>
              </li>
              <li class="<?php echo $active = ($basename == 'socialmedia') ? ' active' : '';?>">
                  <a href="socialmedia.php"><i class='fa fa-list'></i> <span>Social Media</span></a>
              </li>
              <li class="<?php echo $active = ($basename == 'choosemembership') ? ' active' : '';?>">
                  <a href="/hirer/membership_plans.php"><i class='fa fa-list'></i> <span>Choose Membership</span></a>
              </li>
              <li class="treeview<?php echo $active = ($basename == 'policies') ? ' active' : ''; echo $active = ($basename == 'addjob') ? ' active' : ''; echo $active = ($editname == 'policies.php?id='. Input::get('id').'') ? ' active' : ''; ?>">
                  <a href="#"><i class='fa fa-align-left'></i> <span>Policies</span> <i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                      <li><a href="javascript:;">Service User Agreement</a></li>
                      <li><a href="javascript:;">Privacy Agreement</a></li>
                      <li><a href="policy_escrow.php">Escrow Agreement</a></li>
                      <li><a href="javascript:;">Hirer Guide</a></li>
                      <li><a href="/hirer/gdpr_policy.php">GDPR</a></li>
                      <li><a href="javascript:;" data-toggle="modal" data-target="#dispute_modal">Dispute Management</a></li>
                  </ul>
              </li>
              </li>
          
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
<style>
    /* Social Icons */
    #social_side_links {
        position: fixed;
        top: 33%;
        left: 97%;
        padding: 0;
        list-style: none;
        z-index: 99;
    }

    #social_side_links li a {display: block;}

    #social_side_links li a img {
        display: block;
        max-width:40px;
        padding: 10px;
        -webkit-transition:  background .2s ease-in-out;
        -moz-transition:  background .2s ease-in-out;
        -o-transition:  background .2s ease-in-out;
        transition:  background .2s ease-in-out;
    }

    #social_side_links li a:hover img {background: rgba(0, 0, 0, .2);}
</style>
<!-- Social Icons -->
<ul id="social_side_links">
    <li><a style="background-color: #3c5a96;" href="https://facebook.com" target="_blank"><img src="../../myassets/images/facebook-icon.png" alt="" /></a></li>
    <li><a style="background-color: #1178b3;" href="https://www.linkedin.com/in/healthcare-temps-b92620113?trk=org-employees_mini-profile_cta" target="_blank"><img src="../../myassets/images/linkedin-icon.png" alt="" /></a></li>
    <li><a style="background-color: #1dadeb;" href="https://twitter.com" target="_blank"><img src="../../myassets/images/twitter-icon.png" alt="" /></a></li>
    <li><a style="background-color: rgb(237, 28, 36);" href="https://google.com" target="_blank"><img src="../../myassets/images/google-plus.png" alt="" /></a></li>
    <li><a style="background-color: rgb(255, 163, 26);" href="https://healthcare-temps.blogspot.co.uk/" target="_blank"><img src="../../myassets/images/blogger_icon.png" alt="" /></a></li>
    <li><a style="background-color: rgb(207, 34, 0);" href="https://www.youtube.com/channel/UCqQw7gCVKdPpktRVpnhj0ZQ" target="_blank"><img src="../../myassets/images/youtube_icon.png" alt="" /></a></li>
</ul>
