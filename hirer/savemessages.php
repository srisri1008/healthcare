<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
    Redirect::to('../index.php');
}
$status = false;
//echo '<pre>';print_r($_POST);die;
if(isset($_POST)) {
    $status = DB::getInstance()->insert('chats', ['user_type' => 1, 'message'=> $_POST['message'],'send_from_id'=>$_POST['send_from_id'], 'send_to_id' => $_POST['send_to_id'], 'sent_at' => time()]);
}

echo json_encode(['status'=>$status]);
