<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');
    exit;
}else{
 require_once '../core/init.php';
}

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
  Redirect::to('../index.php');
}


?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('template/header.php'); ?> 
    <!-- Theme style -->
    <link href="../assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <style>
        .content-wrapper, .right-side {
            background-color: #FFFFFF !important;
        }
        .row {
            margin-top: 10px;
        }
        .ad_image {
            width: 142px;
            height: 155px;
        }
    </style>

 <body class="skin-green sidebar-mini">
     
     <!-- ==============================================
     Wrapper Section
     =============================================== -->
	 <div class="wrapper">
	 	
        <!-- Include navigation.php. Contains navigation content. -->
	 	<?php include ('template/navigation.php'); ?> 
        <!-- Include sidenav.php. Contains sidebar content. -->
	 	<?php include ('template/sidenav.php'); ?> 
	 	
	 	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Advertiser<small><?php echo $lang['section']; ?></small></h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
            <li class="active">Advertiser</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">	 	
		    <!-- Include currency.php. Contains header content. -->
		    <?php include ('template/currency.php'); ?>
            <div class="container">
                <div class="row">
                    <h1 style="font-weight: bold">Why Advertise With Us?</h1>
                    <p style="font-weight: bold; font-size: 17px;">With millions of views each month by healthcare professionalism,<br />
                        we are a trusted source for connecting with your target market.</p>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <h2 style="font-weight: bold">Target Market</h2>
                        <p style="font-weight: bold; font-size: 17px;">Ideal opportunity to get your<br />
                            brand infront of a wide range<br />
                            of healthcare professionals.</p>
                    </div>
                    <div class="col-lg-6">
                        <img src="uploads/images/advertisers.png" alt="advertiser image" style="margin-left: -74px;">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-7">
                        <div class="row" style="text-align: center">
                            <div class="col-lg-3">
                                <img class="ad_image" src="uploads/images/advertiser_circle1.png" alt="">
                                <h3 style="margin-top: 0px;">Educated</h3>
                                <p style="font-size: 10px; font-family: Calibri">Higher college<br />or degree<sup>3</sup></p>
                            </div>
                            <div class="col-lg-3">
                                <img class="ad_image" src="uploads/images/advertiser_circle2.png" alt="">
                                <h3 style="margin-top: 0px;">Millennial</h3>
                                <p style="font-size: 10px; font-family: Calibri">69% of visitors are<br />18-34 years old<sup>2</sup></p>
                            </div>
                            <div class="col-lg-3">
                                <img class="ad_image" src="uploads/images/advertiser_circle3.png" alt="">
                                <h3 style="margin-top: 0px;">Affluent</h3>
                                <p style="font-size: 10px; font-family: Calibri">1% make $100k+</p>
                            </div>
                            <div class="col-lg-3">
                                <img class="ad_image" src="uploads/images/advertiser_circle4.png" alt="">
                                <h3 style="margin-top: 0px;">Balanced Genders</h3>
                                <p style="font-size: 10px; font-family: Calibri">58% are female and<br />42% are male<sup>2</sup></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <h2 style="font-weight: bold">Audience</h2>
                        <p style="font-weight: bold; font-size: 17px;">Perfect platform to<br />engage with the medical<br />and professionals<br />audiences.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-offset-3 col-lg-2" style="margin-top: 17px;">
                        <form action="">
                            <div class="form-group">
                                <input type="text" placeholder="Company Name" name="company" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Email Address" name="email" class="form-control">
                            </div>
                            <button class="btn" style="border-radius: 0px; padding: 7px 30px; background-color: rgb(13, 170 ,65); text-align: center; color: #FFFFFF; ">Get Started Now</button>
                        </form>
                    </div>
                    <div class="col-lg-3">
                        <img src="uploads/images/inquire_now.png" alt="inquire">
                    </div>
                </div>
	   </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	 
      <!-- Include footer.php. Contains footer content. -->	
	  <?php include 'template/footer.php'; ?>	
	 	
     </div><!-- /.wrapper -->
	<!-- ==============================================
	 Scripts
	 =============================================== -->
    <!-- jQuery 2.1.4 -->
    <script src="../assets/js/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.6 JS -->
    <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../assets/js/app.min.js" type="text/javascript"></script>
    <!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable({
        "pagingType": "full_numbers",
        "order": []
        });
      });
    </script>
    <script type="text/javascript">
	function doStar(id, userid, state) {
		// id = unique id of the message
		// type = 1 do the like, 2 do the dislike
		$('#like_btn'+id).html('<div class="privacy_loader"></div>');
		$('#doStar'+id).removeAttr('onclick');
		$.ajax({
			type: "POST",
			url: "template/requests/star.php",
			data: "id="+id+"&userid="+userid+"&state="+state, 
			cache: false,
			success: function(html) {
				$('#message-action'+id).empty();
				$('#message-action'+id).html(html);
			}
		});
	}	
	function delete_not(id, clientid) {
		// id = unique id of the message/comment/chat
		// type = type of post: message/comment/chat
		$('#del_comment_'+id).html('<div class="preloader-retina"></div>');
		
		$.ajax({
			type: "POST",
			url: "template/requests/delete_not.php",
			data: "id="+id+"&clientid="+clientid, 
			cache: false,
			success: function(html) {
				if(html == '1') {
				   $('#comment'+id).fadeOut(500, function() { $('#comment'+id).remove(); });
				} else {
				   $('#comment'+id).html($('#del_comment_'+id).html('Sorry, the message could not be removed, please refresh the page and try again.'));
				}
			}
		});
	}			
	</script> 
</body>
</html>
