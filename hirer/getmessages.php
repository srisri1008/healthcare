<?php
    //Check if init.php exists
    if(!file_exists('../core/init.php')){
        header('Location: ../install/');
        exit;
    }else{
        require_once '../core/init.php';
    }

    //Start new Client object
    $client = new Client();

    //Check if Client is logged in
    if (!$client->isLoggedIn()) {
        Redirect::to('../index.php');
    }

    //return client's message
    $messages = DB::getInstance()->get('chats', '*', ['OR' => ['send_from_id'=> $_POST['userid'], 'send_to_id' => $_POST['userid']], 'ORDER'=>'sent_at ASC']);
    echo json_encode($messages->results());