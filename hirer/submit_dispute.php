<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
    die('error');
}

$status = 'error';
$obj = new DB();
if(isset($_POST['request'])) {
    $dispute_reason = $_POST['dispute_reason'];
    if(isset($dispute_reason) && is_array($dispute_reason) && count($dispute_reason)) {
        $reasons = implode(',', $dispute_reason);
        $disputed_hours = $_POST['dispute_hours'] ? $_POST['dispute_hours'] : 'n/a';
        $clientid = $client->data()->clientid;
        $comment = $_POST['comment'];
        $time = time();
        $obj->insertData("INSERT INTO dispute_requests (clientid, dispute_reasons, dispute_hours, comment, created_at) VALUES ($clientid, '$reasons', '$disputed_hours', '$comment', $time)");
        $status = 'success';
    }
}

echo $status;
