<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
    Redirect::to('../index.php');
}

//Get Freelancer's Data
$freelancerid = Input::get('id');
$query = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
if ($query->count() === 1) {
    foreach($query->results() as $row) {
        $freeID = $row->id;
        $name = $row->name;
        $username = $row->username;
        $email = $row->email;
        $phone = $row->phone;
        $freelancer_imagelocation = $row->imagelocation;
        $freelancer_bgimage = $row->bgimage;
    }

    $userDocuments = DB::getInstance()->get("documentupload", "*", ["user_id" => $freeID, "LIMIT" => 1]);
    if ($userDocuments->count()) {
        echo $userDocuments->results()[0]->id;die;
//    echo '<pre>';print_r($freelancer->data()); print_r($query->results());print_r($userDocuments->results());die;
    }

//Get Freelancer's Data
    $query = DB::getInstance()->get("profile", "*", ["userid" => $freelancerid, "LIMIT" => 1]);

    $query_temp_references = DB::getInstance()->get("temp_references", "*", ["freelancer_id" => $freeID]);
    $temp_ref = array();
    $temp_ref1= array();
    $temp_ref2 = array();
    $temp_ref3 = array();
    $temp_ref4 = array();

//echo "<pre>";
//
//print_r($query_temp_references->results());
//die;

    if ($query_temp_references->count()) {
        foreach($query_temp_references->results() as $key=>$row) {

//     echo $value->organization;



            $temp_ref[$key]['name']= $row->name;
            $temp_ref[$key]['organization']= $row->organization;
            $temp_ref[$key]['occupation']= $row->occupation;
            $temp_ref[$key]['email']= $row->email;
            $temp_ref[$key]['id']= $row->id;
            $temp_ref[$key]['freelancer_id']= $row->freelancer_id;

        }
    }



    $temp_ref1 = $temp_ref[0];
    $temp_ref2 = $temp_ref[1];
    $temp_ref3 = $temp_ref[2];
    $temp_ref4 = $temp_ref[3];

//Get multiple rows of freelancer
    $queryAll = DB::getInstance()->get("temp_education", "*", ["user_id" => $freeID]);
    $education = array();
    if ($queryAll->count()) {
        foreach($queryAll->results() as $row) {
            $education[] = $row;
        }
    }




    $userDocuments = DB::getInstance()->get("documentupload", "*", ["user_id" => $freeID, "LIMIT" => 1]);
    if($userDocuments->count()) {
        $flag = 1;
        foreach($userDocuments->results() as $row) {
            $cbr_dbs_status = $row->cbr_dbs_status;
            $proof_status = $row->proof_status;
            $certificates_status = $row->certificates_status;
            $proofaddress_status = $row->proofaddress_status;
            $hepatitis_status = $row->hepatitis_status;
            $prof_status = $row->prof_status;
            $registration_status = $row->registration_status;
            $utr_status = $row->utr_status;
            $NHSSmart_status = $row->NHSSmart_status;
            $wp_status = $row->wp_status;
            $ref1_status = $row->ref1_status;
            $ref2_status = $row->ref2_status;
        }
    }
} else {
    Redirect::to('services.php');
}

//Get temp worker profile data====================================
$query = DB::getInstance()->get("profile", "*", ["userid" => $freelancerid, "LIMIT" => 1]);
if ($query->count()) {
    foreach($query->results() as $row) {
        $nid = $row->id;
        $location = $row->location;
        $city = $row->city;
        $country = $row->country;
        $rate = $row->rate;
        $rate_id = $row->rate_id;
        $website = $row->website;
        $about = $row->about;
        $education_profile = $row->education;
        $work_profile = $row->work;
        $awards_profile = $row->awards;
        $skills = $row->skills;
        $type = $row->type_id;
        $category= $row->category_id;
        $availability = $row->availability_id;
        $arr=explode(',',$skills);
    }
} else {
    Redirect::to('services.php');
}


//Get category.
$category_query = DB::getInstance()->get("category", "*", ['id'=>$category]);

if ($category_query->count()) {
    foreach($category_query->results() as $row) {
        $category_name = $row->name;
    }
}

//Get all availability/shift types.
$availability_query = DB::getInstance()->get("availability", "*", ['id'=>$availability]);

if ($availability_query->count()) {
    foreach($availability_query->results() as $row) {
        $availability_title = $row->title;
    }
}

//Get all temp rates from profile rate_id.
$temp_rate_query = DB::getInstance()->get("temp_rate", "*", ['id'=>$rate_id]);

if ($temp_rate_query->count()) {
    foreach($temp_rate_query->results() as $row) {
        $rate_range_min = $row->rangemin;
        $rate_range_max = $row->rangemax;
    }
}
$query_type = DB::getInstance()->get("temp_type", "*", ["id" => $type, "LIMIT" => 1]);
$temp_type = $query_type->results();


?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>
<!-- Bootstrap Datetimepicker CSS -->
<link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Select CSS-->
<link  href="../assets/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
<style>

    .basic_details div.row label.basic_details_label
    {
        color:#207CCA;
        font-weight:bold;
        font-size: 20px;

        text-align: left;

    }
    .basic_details div.row
    {
        /*padding:2% 0;*/
    }

    .basic_details_val
    {
        font-weight: bold;
        text-align: left;
    }

    div.skills_exp span
    {
        background-color: #207CCA;
        padding:1%
    }

    div.skills_exp label{
        color:#207CCA;
        font-weight:bold;
        font-size: 20px;

        text-align: left;


    }
    div.skills label.label-success {

        color: white !important;




        padding: .2em .6em .3em;

        font-weight: 700;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25em;
    }

    div.skills div.box-header label
    {
        color:#207CCA;
        font-weight:bold;
        font-size: 20px;

    }
    div.skills div.box-header
    {
        margin-left: -3%;

    }

    div.box-header
    {
        color:#207CCA;
        font-weight:bold;
        font-size: 20px;
    }

    .label-success {
        border: 1px solid #207CCA;
        color: white !important;
        background: #207CCA !important;
    }


    .list .list-group-item.active:hover {
        background: #207CCA;
        border-color: #207CCA;
    }

    .list .list-group-item.active:hover {
        background: #207CCA;
        border-color: #207CCA;
    }


    .list .list-group-item.active {
        background: #207CCA;
        border-color: #207CCA;
    }
    @media only screen and (min-width: 768px) {
        a.send-job-invite {
            position: absolute;
            top: 10%;
            left: 67%;
            background: #207CCA;
            border-color: #207CCA;

        }
    }
</style>
<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 style="color:#207CCA" ><?php echo $lang['temp_digital_cv']; ?><small><?php echo $lang['section']; ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active"><?php echo $lang['add']; ?> <?php echo $lang['job']; ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content overview">

            <div class="container" style="text-align: center; background-color: white">
                <div class="row" style="text-align: left;">


                    <div class="col-lg-4">
                        <img style="max-width: 50%;" src="../tempworker/<?=isset($freelancer_imagelocation) ? $freelancer_imagelocation : ''?>" class="img-thumbnail img-responsive revealOnScroll" data-animation="fadeInDown" data-timeout="200" alt="">
                        <h1 class="revealOnScroll" data-animation="bounceIn" data-timeout="200"> <?=isset($name) ? $name : ''?></h1>
                        <p class="revealOnScroll" data-animation="fadeInUp" data-timeout="400"><i class="fa fa-map-marker"></i> <?=isset($location) ? $location : '' ?>, <?=isset($city) ? $city : '' ?>, <?=isset($country) ? $country : '' ?></p>
                        <?php
                        //Start new Admin object
                        $admin = new Admin();
                        //Start new Client object
                        $client = new Client();
                        //Start new Freelancer object
                        $freelancer = new Freelancer();

                        if ($admin->isLoggedIn()) {
                        } elseif($freelancer->isLoggedIn()) {

                        } elseif($client->isLoggedIn()) {
                            echo $sen .='	 
				
			 ';
                        } else {
                            echo $sen .='	 
				 <a href="login.php" class="kafe-btn kafe-btn-mint-small">
				 <i class="fa fa-align-left"></i> ' . $lang['send'] . ' ' . $lang['job'] . ' ' . $lang['invitation'] . '</a>
			 ';
                        }
                        ?>
                    </div><!-- /.col-lg-12 -->

                    <div style="margin-left: -1%;" class="basic_details col-lg-5">
                        <div class="box box-info" style="border-top-color: #207CCA; ">
                            <div style="margin-left: 2%;" class="box-body">
                                <div class="row"><label class="basic_details_label">Temp Name:</label>
                                    <span style="margin-left: 3%" class="basic_details_val"><?php echo $name;?></span></div>
                                <div class="row"><label class="basic_details_label">Temp Type:</label>
                                    <span style="margin-left: 5%" class="basic_details_val"><?php echo $temp_type[0]->title;?></span></div>
                                <div class="row"><label class="basic_details_label">Location:</label>
                                    <span  style="margin-left: 6%" class="basic_details_val"><?php echo $location;?></span></div>
                                <div class="row"><label class="basic_details_label">Avg. Pay Rate:</label>
                                    <span style="margin-left: 1%" class="basic_details_val">£
                                        <?php echo $rate_range_min;?>
                                        <?php if($rate_range_max !=="" ) echo "-£".$rate_range_max;?> PHR
                                </span></div>
                                <div class="row"><label class="basic_details_label">Availability:</label>
                                    <span style="margin-left: 3%"  class="basic_details_val"><?php echo $availability_title;?></span></div>
                                <div class="row"><label class="basic_details_label">Verified:</label>
                                    <span style="margin-left: 7%"  class="basic_details_val">Yes</span></div>

                            </div>
                        </div>
                        <div class="box-body skills">
                            <div class="box-header">
                                <label class="basic_details_label skills_exp">Skill & Expertise:</label></div>
                            <label class="label-success">1ShoppingCart </label> &nbsp;
                            <label class="label-success">2D Design </label> &nbsp;
                            <label class="label-success">Bootstrap </label> &nbsp;
                            <label class="label-success">CSS</label> &nbsp;
                            <label class="label-success">CSS3 </label> &nbsp;
                            <label class="label-success">HTML </label> &nbsp;
                            <label class="label-success">HTML5 </label> &nbsp;
                            <label class="label-success">Object Oriented PHP </label> &nbsp;
                            <label class="label-success">PHP </label> &nbsp;
                            <label class="label-success">phpMyAdmin </label> &nbsp;
                        </div>



                    </div>
                    <div class="col-lg-2">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Hire
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu" style="margin-left: -67px;">
                                <li><a href="javascript:;" onclick="hireMe(<?=$_GET['proid']?>)">Hire</a></li>
                                <li><a href="#">Invite to Bid</a></li>
                                <li><a href="./interviewroom.php">Invite for Interview</a></li>
                                <li><a href="./live_chat.php">Email</a></li>
                            </ul>
                        </div>
                    </div>

                    <script>
                        function hireMe(id) {
                            //Built a url to send
                            var info = 'id=' + id;
                            if(confirm("Are you sure you want to Assign this Job to this Temp worker?")) {
                                $.ajax({
                                    type: "GET",
                                    url: "template/actions/assign.php",
                                    data: info,
                                    success: function()
                                    {
                                        window.location.reload();
                                    }
                                });
                            }
                            return false;
                        }
                    </script>

                    <div id="sidebar" class="col-lg-5">
                        <div class="list">
                            <div class="list-group">

                                <a class="list-group-item active cat-list">
                                    <em class="fa fa-fw fa-user"></em>&nbsp;&nbsp;&nbsp;<?=isset($lang['freelancer']) ? $lang['freelancer'] : '' ?>
                                </a>

                            </div><!-- ./.list-group -->
                        </div><!-- ./.list -->

                        <?php $overview = (Input::get('a') == 'overview') ? ' active' : ''; ?>
                        <?php $portfolio = (Input::get('a') == 'portfolio') ? ' active' : ''; ?>
                        <?php $services = (Input::get('a') == 'services') ? ' active' : ''; ?>
                        <?php $jobs = (Input::get('a') == 'jobs') ? ' active' : ''; ?>
                        <?php $reviews = (Input::get('a') == 'reviews') ? ' active' : ''; ?>
                        <div class="list">
                            <div class="list-group">
                                <a href="freelancer.php?a=overview&id=<?php echo $freelancerid ?>" class="list-group-item <?php echo $overview; ?> cat-list">
                                    <em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['overview']; ?>
                                </a>
                                <a href="freelancer.php?a=portfolio&id=<?php echo $freelancerid ?>" class="list-group-item <?php echo $portfolio; ?> cat-list">
                                    <em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['portfolio']; ?>
                                </a>
                                <a href="freelancer.php?a=services&id=<?php echo $freelancerid ?>" class="list-group-item <?php echo $services; ?> cat-list">
                                    <em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['services']; ?>
                                </a>
                                <a href="freelancer.php?a=jobs&id=<?php echo $freelancerid ?>" class="list-group-item <?php echo $jobs; ?> cat-list">
                                    <em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['jobs']; ?>
                                    <?php echo $lang['completed']; ?> &
                                    <?php echo $lang['assigned']; ?>
                                </a>
                                <a href="freelancer.php?a=reviews&id=<?php echo $freelancerid ?>" class="list-group-item <?php echo $reviews; ?> cat-list">
                                    <em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['reviews']; ?>
                                </a>
                                <div class="dropdown">
                                    <button class="list-group-item  cat-list dropdown-toggle" type="button" data-toggle="dropdown">
                                        <em class="fa fa-fw fa-align-justify"></em>&nbsp;&nbsp;&nbsp;Approval & Verifications
                                    </button>
                                    <style>
                                        .documentsverification > li > h4 {
                                            padding: 0px 17px;
                                        }
                                    </style>
                                    <ul class="dropdown-menu documentsverification">
                                        <?php if(isset($flag) && isset($userDocuments) && count($userDocuments)) { ?>
                                            <li><h4>CBR/DBS     <span class="<?=$cbr_dbs_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>Photo ID    <span class="<?=$proof_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>Training Certificate    <span class="<?=$certificates_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>Address Proof   <span class="<?=$proofaddress_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>Flu Jab help   <span class="<?=$hepatitis_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>Professional Indeminity   <span class="<?=$prof_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>Registration Number   <span class="<?=registration_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>NINO    <span class="<?=$utr_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>NHS Smart Card    <span class="<?=$NHSSmart_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>Work Permit    <span class="<?=$wp_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>Reference X1    <span class="<?=$ref1_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                            <li><h4>Reference X2    <span class="<?=$ref2_status== 1 ? 'fa fa-check' : ''?>"></span></h4></li>
                                        <?php } else {
                                            ?>
                                            <li>
                                                <h4>CBR/DBS</h4>
                                            </li>
                                            <li>
                                                <h4>Photo ID</h4>
                                            </li>
                                            <li>
                                                <h4>Training Certificate</h4>
                                            </li>
                                            <li>
                                                <h4>Address Proof</h4>
                                            </li>
                                            <li>
                                                <h4>Flu Jab help</h4>
                                            </li>
                                            <li>
                                                <h4>Professional Indeminity</h4>
                                            </li>
                                            <li>
                                                <h4>Registration Number</h4>
                                            </li>
                                            <li>
                                                <h4>NINO</h4>
                                            </li>
                                            <li>
                                                <h4>NHS Smart Card</h4>
                                            </li>
                                            <li>
                                                <h4>Work Permit</h4>
                                            </li>
                                            <li>
                                                <h4>Reference X1</h4>
                                            </li>
                                            <li>
                                                <h4>Reference X2</h4>
                                            </li>
                                            <?php
                                        }?>
                                    </ul>
                                </div>
                            </div><!-- ./.list-group -->
                        </div><!-- ./.list -->
                    </div><!-- ./.col-lg-4 -->


                    <div class="col-lg-8 white-2">
                        <div class="about">
                            <h3><?php echo $lang['about']; ?> <?php echo $lang['me']; ?></h3>
                            <div class="col-lg-12 top-sec">
                                <?php echo $about; ?>


                            </div><!-- /.col-lg-12 -->
                            <div class="col-lg-12">

                                <div class="col-lg-12">
                                    <hr class="small-hr">
                                </div><!-- /.col-lg-12 -->


                            </div><!-- /.col-lg-12 -->
                            <div class="row bottom-sec">


                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.about -->

                        <div class="education">
                            <h3><?php echo $lang['education']; ?></h3>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-md-12">
                                        <?php echo $education_profile; ?>
                                    </div><!-- /.col-lg-12 -->
                                </div><!-- /.col-lg-12 -->
                            </div><!-- /.row -->

                        </div><!-- Education-->

                        <div class="work">
                            <h3><?php echo $lang['work']; ?> <?php echo $lang['experience']; ?></h3>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-12">
                                        <?php echo $work_profile; ?>
                                    </div><!-- /.col-lg-12 -->
                                </div> <!-- /.col-lg-12 -->
                            </div><!-- /.row -->

                        </div><!-- Work-->

                        <div class="awards">
                            <h3><?php echo $lang['awards']; ?> <?php echo $lang['and']; ?> <?php echo $lang['achievements']; ?></h3>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-12">
                                        <?php echo $awards_profile; ?>
                                    </div><!-- /.col-lg-12 -->
                                </div><!-- /.col-lg-12 -->
                            </div><!-- /.row -->

                        </div><!-- Awards-->
                        <?php

                        $query = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1]);
                        if ($query->count()) {
                            foreach($query->results() as $row) {
                                $membershipid = $row->membershipid;

                            }
                        }

                        $q = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
                        if ($q->count() === 1) {
                            $q1 = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
                        } else {
                            $q1 = DB::getInstance()->get("membership_agency", "*", ["membershipid" => $membershipid]);
                        }
                        if ($q1->count()) {
                            foreach($q1->results() as $r1) {
                                $team_membership = $r1->team;
                            }
                        }
                        ?>


                        <div class="ourteam">
                            <h3><?php echo $lang['references']; ?> </h3>
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php
                                    $query_temp_freelancer = DB::getInstance()->get("temp_references", "*", ["freelancer_id" => $freeID]);
                                    if ($query_temp_freelancer->count()) {

                                        $teamList = '';
                                        $x = 1;

                                        foreach($query_temp_freelancer->results() as $row) {

                                            echo $teamList .= '
				      
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 profile">
                                            
                                              <h4>'. escape($row->name) .'</h4>
                                              <h5>'. escape($row->occupation) .'</h5>
                                              <p>'. escape($row->organization) .'</p>
                                              <p>'. escape($row->email) .'</p>
                                            </div><!-- /.col-lg-4 -->			
                                                 ';
                                            unset($teamList);
                                            $x++;
                                        }
                                    } else {
                                        echo $teamList .='';
                                    }

                                    ?>
                                </div><!-- /.col-lg-12 -->
                            </div><!-- /.row -->
                        </div><!-- Awards-->

                        <div class="about">
                            <h3><?php echo $lang['summary']; ?></h3>

                            <div class="row bottom-sec">

                                <div class="col-lg-12">


                                    <div class="col-lg-3">
                                        <h5> <?php echo $lang['location']; ?> </h5>
                                        <p><i class="fa fa-map-marker"></i> <?php echo $country; ?></p>
                                    </div><!-- /.col-lg-2 -->
                                    <div class="col-lg-3">
                                        <h5><?php echo $lang['jobs']; ?> <?php echo $lang['invites']; ?> </h5>
                                        <p>
                                            <?php
                                            $query = DB::getInstance()->get("job", "*", ["AND" => ["freelancerid" => $freelancerid, "invite" => 1]]);
                                            echo $query->count();
                                            ?>
                                        </p>
                                    </div><!-- /.col-lg-2 -->
                                    <div class="col-lg-3">
                                        <h5><?php echo $lang['jobs']; ?> <?php echo $lang['assigned']; ?> </h5>
                                        <p>
                                            <?php
                                            $query = DB::getInstance()->get("job", "*", ["AND" => ["freelancerid" => $freelancerid, "accepted" => 1]]);
                                            echo $query->count();
                                            ?>
                                        </p>
                                    </div><!-- /.col-lg-2 -->
                                    <div class="col-lg-3">
                                        <h5><?php echo $lang['jobs']; ?> <?php echo $lang['completed']; ?> </h5>
                                        <p>
                                            <?php
                                            $query = DB::getInstance()->get("job", "*", ["AND" => ["freelancerid" => $freelancerid, "completed" => 1]]);
                                            echo $query->count();
                                            ?>
                                        </p>
                                    </div><!-- /.col-lg-2 -->
                                </div><!-- /.col-lg-12 -->


                                <div class="col-lg-12">

                                    <div class="col-lg-12">
                                        <hr class="small-hr">
                                    </div><!-- /.col-lg-12 -->

                                    <div class="col-lg-6">
                                        <h5> <?php echo $lang['ratings']; ?>
                                            (<?php
                                            $query = DB::getInstance()->get("ratings", "*", ["AND" => ["freelancerid" => $freelancerid]]);
                                            $count = $query->count();
                                            echo $re = $count/7;
                                            ?>)</h5>
                                        <p><i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i></p>
                                    </div><!-- /.col-lg-2 -->
                                    <div class="col-lg-6">
                                        <h5><?php echo $lang['payments']; ?> <?php echo $lang['received']; ?> </h5>
                                        <p>
                                            <?php
                                            $query = DB::getInstance()->get("job", "*", ["AND" =>["freelancerid" => $freelancerid, "invite" => "0", "delete_remove" => 0, "accepted" => 1]]);
                                            if ($query->count()) {
                                                foreach($query->results() as $row) {


                                                    $q1 = DB::getInstance()->get("milestone", "*", ["AND" =>["jobid" => $row->jobid]]);
                                                    if ($q1->count()) {
                                                        foreach($q1->results() as $r1) {

                                                            $query = DB::getInstance()->sum("transactions", "payment", ["AND" => ["membershipid" => $r1->id, "freelancerid" => $r1->clientid]]);
                                                            foreach($query->results()[0] as $payy) {
                                                                $paj[] = $payy;
                                                            }

                                                        }
                                                    }

                                                }
                                            }
                                            echo $currency_symbol.'&nbsp;';
                                            echo array_sum($paj);
                                            ?>
                                        </p>
                                    </div><!-- /.col-lg-2 -->
                                </div><!-- /.col-lg-12 -->



                                <div class="col-lg-12">

                                    <div class="col-lg-12">
                                        <hr class="small-hr">
                                    </div><!-- /.col-lg-12 -->

                                    <div class="col-lg-4">
                                        <h5> <?php echo $lang['linkedin_url']; ?> </h5>
                                        <p><?php echo $website; ?></p>
                                    </div><!-- /.col-lg-3 -->
                                    <div class="col-lg-2">
                                        <h5> <?php echo $lang['rate_hour']; ?> </h5>
                                        <p><?php echo $currency_symbol; ?> <?php echo $rate_range_min;?>
                                            <?php if($rate_range_max !=="" ) echo "-£".$rate_range_max;?> PHR</p>
                                    </div><!-- /.col-lg-1 -->
                                    <div class="col-lg-3">
                                        <h5> <?php echo $lang['availability']; ?> </h5>
                                        <p><?php echo $availability_title; ?></p>
                                    </div><!-- /.col-lg-3 -->
                                    <!--                                <div class="col-lg-3">-->
                                    <!--                                    <h5> --><?php //echo $lang['category']; ?><!-- </h5>-->
                                    <!--                                    <p> --><?php //echo $category_name; ?><!--</p>-->
                                    <!--                                </div>-->
                                    <div class="col-lg-3">
                                        <h5>Fully Vetted  </h5>
                                        <p>YES</p>
                                    </div><!-- /.col-lg-3 -->

                                </div><!-- /.col-lg-12 -->
                                <div class="col-lg-12">
                                    <hr class="small-hr">
                                </div><!-- /.col-lg-12 -->

                            </div><!-- /.col-lg-12 -->
                        </div><!-- /.about -->
                    </div><!-- /.col-lg-8 -->





                </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<!-- Include footer.php. Contains footer content. -->
<?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->


<!-- ==============================================
 Scripts
 =============================================== -->

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
<!-- Datetime Picker -->
<script src="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $('.form_datetime_start').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        startDate: new Date(),
        pickTime: false,
        minView: 2,
        pickerPosition: "bottom-left",
        linkField: "mirror_field_start",
        linkFormat: "hh:ii",
        linkFieldd: "mirror_field_start_date",
        linkFormatt: "dd MM yyyy"
    });
    $('.form_datetime_end').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1,
        startDate: new Date(),
        pickTime: false,
        minView: 2,
        pickerPosition: "bottom-left",
        linkField: "mirror_field_start",
        linkFormat: "hh:ii",
        linkFieldd: "mirror_field_start_date",
        linkFormatt: "dd MM yyyy"
    });
</script>
<!-- Summernote WYSIWYG-->
<script src="../assets/js/summernote.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 300,                 // set editor height

            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor

            focus: false,                 // set focus to editable area after initializing summernote
        });
    });
</script>
<!-- Bootstrap Select JS-->
<script src="../assets/js/bootstrap-select.js"></script>
</body>
</html>
