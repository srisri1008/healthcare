<?php

//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
  Redirect::to('../index.php');	
}

require_once("brain/vendor/autoload.php");

if(file_exists(__DIR__ . "/../.env")) {
    $dotenv = new Dotenv\Dotenv(__DIR__ . "/../");
    $dotenv->load();
}

//$gateway = new Braintree\Gateway([
//    'environment' => 'sandbox',
//    'merchantId' => 'ksm2cdtzf4rjghgk',
//    'publicKey' => 'wz6p9cgsxsch3wyw',
//    'privateKey' => '6bea586e74849a357c57a7776723f0e2'
//]);

$gateway = new Braintree\Gateway([
    'environment' => 'sandbox',
    'merchantId' => '9d6wt37vqztsryk9',
    'publicKey' => 'zvfygmr7qszq3d26',
    'privateKey' => 'de40a49d09a237cda9a54328f6f3c1f5'
]);

?>

<html>
<head>
    <meta charset="UTF-8">
    <title>BraintreePHPExample</title>
    <link rel=stylesheet type=text/css href="brain/public_html/css/app.css">
    <link rel=stylesheet type=text/css href="brain/public_html/css/overrides.css">
</head>
<body>

<header class="main">
    <div class="container wide">
        <div class="content slim">
            <div class="set">
                <div class="fit">
                    <a class="braintree" href="https://developers.braintreepayments.com/guides/drop-in" target="_blank">Braintree</a>
                </div>
            </div>
        </div>
    </div>

    <div class="notice-wrapper">
        <?php if(isset($_SESSION["errors"])) : ?>
            <div class="show notice error notice-error">
                <span class="notice-message">
                    <?php
                    echo($_SESSION["errors"]);
                    unset($_SESSION["errors"]);
                    ?>
                    <span>
            </div>
        <?php endif; ?>
    </div>
</header>


<div class="wrapper">
    <div class="checkout container">
        <form method="post" id="payment-form" action="brain/public_html/checkout.php">
            <section>
                <label for="amount">
                    <span class="input-label">Amount</span>
                    <div class="input-wrapper amount-wrapper">
                        <input id="amount" name="amount" type="tel" min="1" placeholder="Amount" value="10">
                    </div>
                </label>

                <div class="bt-drop-in-wrapper">
                    <div id="bt-dropin"></div>
                </div>
            </section>

            <input id="nonce" name="payment_method_nonce" type="hidden" />
            <button class="button" type="submit"><span>Test Transaction</span></button>
        </form>
    </div>
</div>

<script src="https://js.braintreegateway.com/web/dropin/1.9.4/js/dropin.min.js"></script>
<script>
    var form = document.querySelector('#payment-form');
    var client_token = "<?php echo($gateway->ClientToken()->generate()); ?>";

    braintree.dropin.create({
        authorization: client_token,
        selector: '#bt-dropin',
        paypal: {
            flow: 'vault'
        }
    }, function (createErr, instance) {
        if (createErr) {
            console.log('Create Error', createErr);
            return;
        }
        form.addEventListener('submit', function (event) {
            event.preventDefault();

            instance.requestPaymentMethod(function (err, payload) {
                if (err) {
                    console.log('Request Payment Method Error', err);
                    return;
                }

                // Add the nonce to the form and submit
                document.querySelector('#nonce').value = payload.nonce;
                form.submit();
            });
        });
    });
</script>
<script>
    'use strict';

    (function () {
        var amount = document.querySelector('#amount');
        var amountLabel = document.querySelector('label[for="amount"]');

        amount.addEventListener('focus', function () {
            amountLabel.className = 'has-focus';
        }, false);
        amount.addEventListener('blur', function () {
            amountLabel.className = '';
        }, false);
    })();

</script>
</body>
</html>
