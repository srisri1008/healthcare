<?php 
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Client object
$client = new Client();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
  Redirect::to('../index.php');	
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>
<!-- AdminLTE CSS -->
<link href="../assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- Datetime Picker -->
<link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery UI CSS -->
<link href="../assets/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
<!-- Progress CSS -->
<link href="../assets/css/progress.css" rel="stylesheet" type="text/css" />

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>

<script src="../assets/js/jquery.knob.min.js"></script>

<body class="skin-green sidebar-mini">

<style>
    .h3 {
        font-size: 20px;
    }
</style>
<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <!-- Include currency.php. Contains header content. -->
            <?php include ('template/currency.php'); ?>

            <div id="sendtoid" style="display: none"></div>
            <div class="row">
                <div class="col-lg-11">
                    <div style="text-align: center; color: #0070c0; margin-bottom: 30px">
                        <h2>PAYROLL POLICY</h2>
                    </div>
                    <p>
                        Effective date from 30 April 2018:<br /><br />
                        When a Temp-Worker signs up to use Healthcare-Temps.co.uk, they are agreeing to out Payroll Policy, as this
                        Payroll Agreement (this“Agreement”) applies to all Temp Workers. Your use of the Site after the effective
                        date will signify your acceptance of and agreement to this Agreement. Please refer to the <a href="https://www.upwork.com/legal/terms/" target="_blank">Terms of Service</a>
                        for the complete terms governing your usage of the Site and Site Services. Capitalized terms not defined in this
                        Agreement have the meanings in the Terms of Services.<br />

                        This Agreement is a binding legal agreement by and between Temps Worker and Healthcare-Temps Inc.
                        (“Healthcare-Temps”or“we”) and supersedes and replaces all prior agreements between Temp Worker and
                        Healthcare-Temps concerning Healthcare-Temps Payroll.
                    </p>

                    <div class="h3">1. UPWORK PAYROLL STAFFING PROVIDER</div>
                    <p>
                        When a Client uses Healthcare-temps Payroll, which is described on the Site <a href="https://www.upwork.com/i/payroll-client/" target="_blank">here</a>(“Healthcare-Temps
                        Payroll”), a third-party staffing vendor will employ the Temp Worker (the“Service Provider”). Temp worker (if
                        accepted for employment as described below) will become a self-employed freelancer. The Hirer will assign
                        work to the service provider, and the Hirer will be responsible for supervising the temp worker. When, and
                        only if, a Temp Worker has been accepted for employment by the Hirer and is assigned to the Hirer online on a
                        long-term assignment for more than six month, then the temp worker becomes a“Payroll Employee”for
                        purposes of this Agreement, but also remains self-employed temp under the Terms of Service.
                    </p>
                    <div class="h3">2. HIRING PROCESS</div>
                    <p>
                        As a Hirer, you will select a Temp Worker to be employed by the Service Provider being assigned to work for
                        you via Healthcare-Temps Payroll. No work can begin until the engagement is active on Healthcare-Temps
                        Website, and the Hirer is informed that the Temp worker has started employment and may begin work. After
                        Temp worker’s employment begins, the Hirer will continue to pay for each temp worker’s services through the
                        Site unless the Hirer pays the Opt-Out Fee.
                    </p>
                    <div class="h3">3. HIRING DECISIONS; LIMITATIONS ON HIRING</div>
                    <p>
                        Hirer acknowledges and agrees that Hirer selects a Temp Worker to become a Payroll Employee based upon
                        Hirer’s determination that temp worker possesses the skills, qualification, experience, and education to satisfy

                        the requirements of the assignment. Healthcare-temps does not (a) select the Temp worker; (b) have the
                        authority or ability to decide to engage or end the engagement of the Temp Worker on behalf of Hirer; (c)
                        make any independent evaluation or investigation or otherwise conduct any due diligence regarding temp
                        workers, their resumes, qualifications, skills, background, or prior experience; or (d) make any representations
                        or warranties as to the skills, experience, background, or education of any Temp worker or Payroll Employee.
                        All information regarding a Payroll Employee posted on or to the Site or provided by Healthcare-Temps is
                        intended to be indicative or illustrative only and is not intended to be a guarantee or warranty on the part of
                        Healthcare-Temps. Healthcare-temps is not responsible for and shall have no liability for Hirer’s use of or
                        reliance on any Temp Worker information posted or provided by Healthcare-Temps.

                        The Hirer has the right not to hire a temp worker for any lawful reason or no reason at all, including if the temp
                        worker does not meet the Hirer’s standards for employment or the job is considered, in the Hirer’s sole
                        discretion, too hazardous or not covered by the applicable workers’ compensation insurance policy. No Payroll
                        Employee may be engaged to perform services that require the Payroll Employee to handle hazardous
                        materials or operate heavy machinery.
                    </p>
                    <div class="h3">4. DISCLAIMER OF LIABILITY FOR UPWORK</div>
                    <p>
                        <b>
                            Hirer acknowledges and agrees that Hirer and Temp Worker are solely responsible for Temp worker Services
                            and for all work performed and Work Product. Healthcare-Temps provides no express warranty of, will have
                            no implied warranty of, and will have no responsibility for, the Hirer’s services, Temp worker Services and/or
                            Work Product. Healthcare-Temp expressly disclaims all express and implied warranties for Temp worker
                            Services and/or Work Product, including, without limitation, warranties of non-infringement,
                            merchantability, and fitness for a particular purpose. As between Healthcare-Temps and Hirer, Temp worker
                            Services, Work Product, and deliverables are provided AS IS.
                        </b>
                    </p>

                    <div class="h3">5. WORK ENVIRONMENT</div>
                    <p>
                        Hirer acknowledges and agrees that neither Healthcare-Temps nor the Hirer will control the work environment
                        in which a temp worker will perform services for the Hirer. The Hirer will be solely responsible for ensuring
                        that the work environment is safe and free from harassment and discrimination as required by applicable law.
                    </p>
                    <div class="h3">5. PAYROLL EMPLOYEE SUPERVISION AND LIMITATIONS ON SCOPE OF EMPLOYMENT</div>
                    <p>
                        Hirer will be solely responsible for protecting its property, including by implementing and enforcing any
                        agreements, policies or procedures to protect Hirer’s intellectual property and confidential information.

                        If Hirer assigns a temp worker any supervisory duties or gives a Payroll Employee authority to sign tax returns,
                        render accounting or legal opinions, issue negotiable instruments, or make final decisions of the nature of

                        those generally made by Hirer’s executives, officers, or directors, Hirer agrees that Hirer and Temp worker are
                        solely responsible for any such decisions or actions by the Payroll Employee.
                    </p>
                    <div class="h3">6 GENERAL PAYMENT OBLIGATIONS</div>
                    <p>
                        Your payments for Healthcare-temps Payroll services are governed by the terms below, as well as the
                        <a href="https://www.upwork.com/legal/escrow-hourly/" target="_blank">Hourly and Bonus Policy and Escrow Policy</a>.<br />

                        You recognize and agree that all hours recorded and submitted to Healthcare-Temps by your Payroll
                        Employees will automatically be deemed approved by you, and you authorise those hours to be paid by you. In
                        addition, Payroll Employees do not pay any Service Fees to Healthcare-Temps. Instead, the Hirer pays the fees,
                        as described below. Hirer also understands and agrees that Hirer is responsible for covering the cost of all
                        Payroll Employee wages, even if, for example, a Payroll Employee reports having worked more than the
                        number of hours authorised by Hirer.
                    </p>
                    <div class="h3">7. FUNDING AND MAKING PAYMENTS</div>
                    <p>
                        Client’s payments of amounts for wages, expense reimbursements and bonuses are governed by the
                        <a href="https://www.upwork.com/legal/escrow-hourly/" target="_blank">Hourly, Bonus Policy and Escrow Instructions</a>.
                    </p>
                    <div class="h3">8. INDEMNIFICATION</div>
                    <p>
                        In addition to the indemnification obligations set forth in the <a href="https://www.upwork.com/legal/terms/" target="_blank">User Agreement</a>,Hirer agrees to (a) release,
                        indemnify, hold harmless, protect, and defend Healthcare-Temps and its respective partners, employees,
                        directors, officers, agents, affiliates, and subsidiaries (each an“Indemnified Party”) from and against any and
                        all claims by any person or entity, including a Temp worker or Payroll Employee; and (b) reimburse any
                        Indemnified Party for any claims, expenses, costs (including attorneys’ fees and court costs), obligations,
                        losses, and damages (whether joint or several and including special, consequential, compensatory, or punitive
                        damages), whether arising in contract, tort, or otherwise, related to: (i) actual or alleged acts, errors, or
                        omissions (including, but not limited to, breaches of contract or violations of statutes, laws, rules, or
                        regulations) by Hirer, its employees (including Payroll Employee(s)), partners, agents, affiliates, members,
                        subsidiaries, representatives, and officers; (ii) any breach of or failure to perform under this Agreement,
                        including, but not limited to, Hirer’s failure to accurately or timely report wages, hours, expenses, and other
                        reimbursements owed a Payroll Employee or payment of taxes for which Hirer is responsible; (iii) any alleged
                        failure to properly classify one or more Payroll Employees as exempt from overtime;
                    </p>
                    <div class="h3">9. CHANGE OF STAFFING PROVIDER</div>
                    <p>
                        A new third-party company may be designated to act as the Hirer of Payroll Employees (“New Company”) at
                        any time in Healthcare-temps’ sole discretion. Upon such designation, or at any other time as directed by

                        Healthcare-Temps, Hirer will cooperate with such New Company in every reasonable manner to ensure
                        uninterrupted performance of Healthcare-temps Payroll.
                    </p>
                    <div class="h3">10. GOVERNING LAW</div>
                    <p>
                        This Agreement and any Claim or action related thereto will be governed by and construed in accordance with
                        the laws of England &amp; Wales, without regard to its conflict of law provisions and excluding the United Nations
                        Convention on Contracts for the International Sale of Goods (CISG).
                    </p>
                    <div class="h3">11. NO ASSIGNMENT</div>
                    <p>
                        This Agreement, and the parties’ rights and obligations herein, may not be assigned, subcontracted, delegated,
                        or otherwise transferred by a party without the other party’s prior written consent, and any attempted
                        assignment, subcontract, delegation, or transfer in violation of the foregoing will be null and void.
                    </p>
                    <div class="h3">12. WAIVER</div>
                    <p>
                        Any waiver or failure to enforce any provision of this Agreement on one occasion will not be deemed a waiver
                        of any other provision or of such provision on any other occasion.
                    </p>
                    <div class="h3">13. CONTACTING US</div>
                    <p>
                        If you have any questions, or need assistance, please <a href="mailto:contactpayroll@healthcare-temps.co.uk">contactpayroll@healthcare-temps.co.uk</a>
                    </p>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->
<!-- ==============================================
 Scripts
 =============================================== -->

</body>
</html>
