<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Client object
$client = new Client();
$dbObj = new DB();

//Check if Client is logged in
if (!$client->isLoggedIn()) {
  Redirect::to('../index.php');	
}

//Get Instructor's Data
$query = $dbObj->getData("SELECT * FROM profile WHERE userid = ". $client->data()->clientid);
if (count($query)) {
    $flag = 1;
    $profileArr = $query[0];
}
$client_social = $dbObj->getData("SELECT * FROM client_info WHERE clientid = " . $client->data()->id)[0];

$rating = $dbObj->getData("SELECT sum(star) AS count FROM ratings_client WHERE clientid = ". $client->data()->clientid. " AND star_type = 5");
if(count($rating)) {
    $counts = $rating[0]['count'];
}
$completedJobs = $dbObj->getData("SELECT count(id) AS count FROM job WHERE clientid = ". $client->data()->clientid. " AND completed = 1");
if(count($completedJobs)) {
    $completedJobsCounts = $completedJobs[0]['count'];
}
$postedJobs = $dbObj->getData("SELECT count(id) AS count FROM job WHERE clientid = ". $client->data()->clientid);
if(count($postedJobs)) {
    $postedJobsCounts = $postedJobs[0]['count'];
}
$inviteJobs = $dbObj->getData("SELECT count(id) AS count FROM job WHERE clientid = ". $client->data()->clientid. " AND invite = 1");
if(count($inviteJobs)) {
    $inviteJobsCounts = $inviteJobs[0]['count'];
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('template/header.php'); ?> 

 <body class="skin-green sidebar-mini">
     <!-- ==============================================
     Wrapper Section
     =============================================== -->
	 <div class="wrapper">
        <!-- Include navigation.php. Contains navigation content. -->
	 	<?php include ('template/navigation.php'); ?> 
        <!-- Include sidenav.php. Contains sidebar content. -->
	 	<?php include ('template/sidenav.php'); ?> 
	 	
	  <!-- Content Wrapper. Contains page content -->
         <style>
             .skin-green .wrapper, .skin-green .main-sidebar, .skin-green .left-side {
                 background-color: #FFF;
             }
             .gogreen {
                 color: #20B34B;
             }
             .goblock, .info_line {
                 display: block;
             }
             .profile_box,.overview_box, .performance_box, .social_box {
                 border: 1px solid lightgrey;
                 padding: 18px;
                 margin-top: 25px;
                 background-color: #FFF;
                 box-shadow: 2px 2px 5px -3px;
             }
             /*.profile_image {*/
                 /*border: 1px solid black;*/
                 /*margin-top: 64px;*/
                 /*padding: 13px;*/
                 /*width: 150px;*/
                 /*height: 150px;*/
             /*}*/
             span.gogreen {
                 font-size: 17px;
                 font-weight: bold;
             }
             span.goblack {
                 font-weight: bold;
                 color: grey;
                 margin-left: 17px;
             }
             .goblock_heading {
                 font-weight: bold;
                 padding: 10px;
             }
             span.goblock {
                 padding: 10px;
                 color: grey;
                 font-size: 17px;
             }
             .contact_icons > span {
                 margin-right: 18px;
                 margin-top: 27px;
                 font-size: 19px;
                 color: green;
             }
             .post_job_btn {
                 margin-left: 63px;
                 font-size: 15px;
                 border-radius: 0px;
                 color: #FFF;
                 background-color: #20B34B !important;
             }
             .image_frame {
                 padding: 6px;
                 margin-right: -4px;
                 border: 2px solid #A1AAB1;
             }
             .profile_image {
                 max-height: 130px;
                 max-width: 130px;
             }
             .thek{
                 font-weight:900;
                 color: #2fbd6e;
             }
         </style>
         <div class="container-fluid" style="background-color: #EEEFF3">
             <div class="row">
                 <div class="col-sm-offset-2 col-sm-10 col-md-offset-2 col-md-10 col-lg-offset-2 col-lg-10">
                     <h1 style="text-align: center;color: #3ba25b;">Membership Plans</h1>
                     <div class="row" style="text-align: center">
                         <div class="col-lg-offset-6 col-lg-3">
                             <h3 style="padding: 16px 22px; background-color: blue; color: white; margin-bottom: 0;">Standard</h3>
                             <h3 style="padding: 30px 22px; background-color: #cbd8ff; margin-top: 0">$0/mo*</h3>
                         </div>
                         <div class="col-lg-3">
                             <h3 style="padding: 16px 22px; background-color: #b6a4d4; color: white; margin-bottom: 0;">Recruiter</h3>
                             <h3 style="padding: 30px 22px; background-color: #e5e9ec; margin-top: 0">$99/mo*</h3>
                         </div>
                     </div>
                     <div class="row" style="border-top: 2px solid #d7d4d4;border-bottom: 2px solid #d7d4d4;line-height: 35px;margin-top: -10px;">
                         <div class="col-lg-4">
                             <i class="fa fa-save"></i>
                             <span>SYNDICATION</span>
                         </div>
                         <div class="col-lg-8">
                             <p>Post jobs to free and premium job boards.</p>
                         </div>
                     </div>
                     <table class="table table-responsive">
                         <tbody>
                         <tr>
                             <td>Personalised Temp Vetting</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                             <td>check</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                         </tr>
                         <tr>
                             <td>Handpick the best talent</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                             <td>check</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                         </tr>
                         <tr>
                             <td>Customise Staff profiles</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                             <td>check</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                         </tr>
                         <tr>
                             <td>Hirer Premium Candidates</td>
                             <td>-</td>
                             <td>check</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                         </tr>
                         <tr>
                             <td>New Candidate Email Alerts</td>
                             <td>-</td>
                             <td>check</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                         </tr>
                         <tr>
                             <td>Mobile Resume Screener</td>
                             <td>-</td>
                             <td>check</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                         </tr>
                         <tr>
                             <td>Receive copies of all compliance documents</td>
                             <td>-</td>
                             <td>check</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                         </tr>
                         <tr>
                             <td>Receive copies of temp's reference</td>
                             <td>-</td>
                             <td>check</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                         </tr>
                         <tr>
                             <td>Your own dedicated recruiter on-call 24/7</td>
                             <td>-</td>
                             <td>check</td>
                             <td class="thek"><i class="fa fa-check"></i></td>
                         </tr>
                         </tbody>
                     </table>
                 </div>
             </div>
         </div>
	 	
      <!-- Include footer.php. Contains footer content. -->	
	  <?php include 'template/footer.php'; ?>	
	 	
     </div><!-- /.wrapper -->   

	
	<!-- ==============================================
	 Scripts
	 =============================================== -->

     <!-- jQuery 2.1.4 -->
     <script src="../assets/js/jQuery-2.1.4.min.js"></script>
     <!-- Bootstrap 3.3.6 JS -->
     <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
     <!-- AdminLTE App -->
     <script src="../assets/js/app.min.js" type="text/javascript"></script>

     <script src="../assets/js/jquery.knob.min.js"></script>

    
</body>
</html>
