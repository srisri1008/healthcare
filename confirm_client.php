<?php
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

if(isset($_GET['code'])) {
    $code = $_GET['code'];
    $conn = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
    $conn->exec("SET CHARACTER SET utf8");
    $sql = "SELECT * FROM client WHERE temp_code = '" . $code. "'";
    $result = $conn->query($sql);
    $row = $result->fetch(PDO::FETCH_ASSOC);
    if(isset($row['id']) && !empty($row['id'])) {
        if($row['active'] == 0) {
            $conn->query("UPDATE client SET active = 1 WHERE id = " .$row['id']);
            echo '<script>';
            echo 'alert("Account activated");';
            echo 'window.location.href = "/login.php?act=ask";';
            echo '</script>';
        }
        echo "<script>alert('Already registered'); window.location.href = '/login.php';</script>";
    } else {
        echo "<script>window.location.href = '/register.php';</script>";
    }
} else {
    echo "<script>window.location.href = '/register.php';</script>";
}