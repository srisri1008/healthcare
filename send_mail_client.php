<?php
//Check if frontinit.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

if ($_GET['id'] == '') {
    Redirect::to('register.php');
}

$conn = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
$conn->exec("SET CHARACTER SET utf8");
$sql = "SELECT * FROM client WHERE clientid=" . $_GET['id'];
$result = $conn->query($sql);
$row = $result->fetch(PDO::FETCH_ASSOC);
$msg = '';
//echo '<pre>';print_r($row);die;
if (isset($row['id']) && $row['id'] != '') {
    $isActive = $row['active'];
    if(!$isActive) {
        $temp_code = md5(mt_rand(18828383331882838, 98823338381882838)); //Generating random token number
        $conn->query("UPDATE client SET temp_code = '". $temp_code. "' WHERE id = ". $row['id']);
        $email = $row['email'];
//        $email = 'rajnish.virtuelogics@gmail.com';
        $title = "Welcome To Health Care";
        $subject = "Verify Your Health Care Account";
        $message = '<!DOCTYPE html>
                    <html lang="en"><head>
                        <title>Demo</title>
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <!--Bootstrap Css-->
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                        <!--Jquery-->
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                        <!--Javascript-->
                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                        <!--Font awesome-->
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                        <style>
                            .mylogocontainer {
                                display: table;
                                padding: 60px 62px;
                                margin: 0 auto;
                            }
                            .mylogo {
                                width: 350px;
                                margin:  0 auto;
                                margin-bottom: 40px;
                                display:  block;
                            }
                            .welcometext {
                                color: #1DB2CB;
                                font-size: 31px;
                                text-align:  center;
                            }
                            .largestmedical {
                                text-align:  center;
                                font-size: 22px;
                            }
                            .clickbelow {
                                margin-top: 32px;
                                font-size: 20px;
                                text-align:  center;
                                color: #EE9692;
                            }
                            .getstarted {
                                background-color: #53A2D7;
                                border: 1px solid #484848;
                                color: white;
                                padding: 4px 13px;
                                font-size: 20px;
                                border-radius: 4px;
                            }
                            
                        </style>
                    </head>
                    <body>
                    <div class="mylogocontainer">
                        <table>
                            <tbody>
                                <tr>
                                    <td><img class="mylogo" src="'.$_SERVER['HTTP_HOST'].'/assets/img/HT_2.png"></td>
                                </tr>
                                <tr>
                                    <td class="welcometext">Hi '.ucfirst($row['username']).', welcome to Healthcare-Temps</td>
                                </tr>
                                <tr>
                                    <td class="largestmedical">The U.K. Largest medical &amp; healthcare temp recruitment platform.</td>
                                </tr>
                                <tr>
                                    <td class="largestmedical">Hospital jobs, private clinics, care homes, day centres, the choice is yours.</td>
                                </tr>
                                <tr>
                                    <td><p class="clickbelow">Firstly, let\'s help you create a digital Temp CV</p></td>
                                </tr>
                                <tr>
                                    <td style="text-align:  center;"><a href="'.$_SERVER['HTTP_HOST'].'/confirm_client.php?code='.$temp_code.'" style="cursor: pointer;"><button class="getstarted">Let\'s Get Started</button></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </body></html>';

        $message =
                    '<!doctype html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport"
                              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <!--Bootstrap Css-->
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                        <title>Document</title>
                    </head>
                    <body style="width: 100%;height: 100%;max-width: 768px;background-color: #fff;">
                   <div style="background-image: url(\'https://healthcare-temps.co.uk/myassets/img/HirerWelcome.jpg\');background-repeat: no-repeat;background-position: center center;width: 768px;height: 455px;background-size: 100% 100%;margin: 0 auto;"><a href="'.$_SERVER['HTTP_HOST'].'/confirm_client.php?code='.$temp_code.'" style="color: #fff;
margin-top: 355px;
float: right;
margin-right: 125px;
text-decoration: none;
padding: 7px 30px;
background-color: rgb(26,214,41);
font-size: 20px;
border-radius: 5px;">Get Started Now</a></div>
                    </body>
                    </html>';
        sendMail($email, $message, $subject, $title);
        ?>
        <html lang="en"><head>
            <title>Demo</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <!--Bootstrap Css-->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <!--Jquery-->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <!--Javascript-->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <!--Font awesome-->
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <!-- Custom Css-->
        </head>
        <body>
            <div class="container" style="border: 2px solid lightgray; text-align: center; margin: 154px 465px; width:438px;">
                <div class="row">
                    <img src="myassets/images/dr.jpg" style="width:150px; height:133px; border-radius: 200px; margin-top: -80px;">
                </div>
                <div class="row">
                    <img src="myassets/images/chota-tick.png" style="margin-top:10px">
                </div>
                <h3 style="color: green; margin-top:10px;">Congratulations</h3>
                <p style="padding: 10px 0px;">
                    Your account is now activated<br>
                    Please check your email account for verification link.
                </p>
                <a href="/bonus.php"><button class="btn btn-success" style="padding: 10px 0px 10px 0px;margin-bottom: 26px;width: 381px;">Finish</button></a>
            </div>
        </body>
        </html>
        <?php
    }
    else {
        Redirect::to('login.php');
    }

} else {
    Redirect::to('register.php');
}
