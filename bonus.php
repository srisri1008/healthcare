<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php';	
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('includes/template/header.php'); ?> 

<body class="greybg">
<script src="assets/js/jquery.bxslider.min.js"></script>
     <!-- Include navigation.php. Contains navigation content. -->
 	 <?php include ('includes/template/navigation.php'); ?> 
 	 
      <!-- ==============================================
	 Header
	 =============================================== -->
     <style>
         .page:before {
             content: "";
             width: 100%;
             height: 100%;
             display: table-column !important;
             border-bottom: 4px solid #e0e0e0;
             padding-bottom: 10px;
         }
     </style>
     <div id="pages-bonus" class="content user">
         <div class="wrapper">
             <div class="social_icn">
                 <ul class="unstyled clearfix pull-right media-icons">
                     <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                     <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                     <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                     <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                     <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                     <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
                 </ul>
             </div>
             <div id="js-head-menu" class="">
                 <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                     <div class="modal-dialog">

                         <!-- Modal content-->
                         <div class="modal-content">
                             <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal">×</button>
                                 <a class="show hor-space" title="Temps-direct.uk" href="/">
                                     <img alt="" src="myassets/img/logo.png">
                                 </a>
                             </div>
                             <div class="cantent">
                                 <p>
                                     Your log-out has been successful.</p>
                                 <p>
                                     Thank you for using Temps-Direct temp recruiting platform.</p>
                                 <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                                 <input name="feedback" type="commant">
                                 <p>Thank you!</p>
                             </div>
                             <div class="modal-footer hide">
                                 <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default">  Close</a>
                             </div>
                         </div>

                     </div>
                 </div>
                 <script type="text/javascript">
                     //<![CDATA[
                     function getCookie (c_name) {var c_value = document.cookie;var c_start = c_value.indexOf(" " + c_name + "=");if (c_start == -1) {c_start = c_value.indexOf(c_name + "=");}if (c_start == -1) {c_value = null;} else {c_start = c_value.indexOf("=", c_start) + 1;var c_end = c_value.indexOf(";", c_start);if (c_end == -1) {c_end = c_value.length;}c_value = unescape(c_value.substring(c_start,c_end));}return c_value;}if (getCookie('il')) {(function() {var js = document.createElement('script'); js.type = 'text/javascript'; js.async = true;js.src = "https://healthcare-temps.co.uk/users/show_header.js?u=";var s = document.getElementById('js-head-menu'); s.parentNode.insertBefore(js, s);})();} else {document.getElementById('js-head-menu').className = '';}
                     //]]>
                 </script>
             </div>



             <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
             <div id="pjax-body">
                 <div id="main" class="main">
                     <div class="container">
                         <script type="text/javascript" src="/js/new_js/jquery.bxslider.min.js"></script><script type="text/javascript" src="/js/new_js/jquery.bpopup.min.js"></script><link rel="stylesheet" type="text/css" href="/css/new_css/jquery.bxslider.css"><div id="email_popup" style="width: 500px; height: 350px; display: none; background: white;">Email Popup</div>
                         <section class="row ver-space ver-smspace">
                             <div class="tab-head bot-mspace text-13 green-head-bg clearfix">
                                 <h2 class="pull-left text-16 no-mar vikas123">Bonus</h2>
                             </div>
                             <div id="bonus_page">
                                 <div style="text-align: center">
                                     <span style="color: deepskyblue; display: inline-block; font-size: 36px; margin-top: 18px; line-height:36px;">Trade in yours bonus points in return for free gifts!</span>
                                     <p style="color: seagreen;
    display: inline-block;
    font-size: 20px;
    margin-bottom: 17px;
    margin-top: 6px;">It's our way of saying thank you for working with us.</p>
                                 </div>
                                 <div id="slider">
                                     <div class="bx-wrapper" style="max-width: 100%;"><div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 437px;"><ul class="bxslider" style="width: 415%; position: relative; transition-duration: 0s; transform: translate3d(-1780px, 0px, 0px);"><li style="float: left; list-style: outside none none; position: relative; width: 890px;" class="bx-clone"><img src="myassets/img/image002.png">
                                                     <button style="height: 36px;
                left: 34px;
                position: absolute;
                top: 275px;
                width: 220px;" type="button" class="btn btn-primary slider_redeem_btn">Redeem this offer</button>
                                                 </li>
                                                 <li style="float: left; list-style: outside none none; position: relative; width: 890px;">
                                                     <img src="myassets/img/image001.png">
                                                     <button style="height: 36px;
                left: 34px;
                position: absolute;
                top: 275px;
                width: 220px;" type="button" class="btn btn-primary slider_redeem_btn">Redeem this offer</button>
                                                 </li>
                                                 <li style="float: left; list-style: outside none none; position: relative; width: 890px;"><img src="myassets/img/image002.png">
                                                     <button style="height: 36px;
                left: 34px;
                position: absolute;
                top: 275px;
                width: 220px;" type="button" class="btn btn-primary slider_redeem_btn">Redeem this offer</button>
                                                 </li>
                                                 <li style="float: left; list-style: outside none none; position: relative; width: 890px;" class="bx-clone">
                                                     <img src="myassets/img/image001.png">
                                                     <button style="height: 36px;
                left: 34px;
                position: absolute;
                top: 275px;
                width: 220px;" type="button" class="btn btn-primary slider_redeem_btn">Redeem this offer</button>
                                                 </li></ul></div><div class="bx-controls bx-has-pager bx-has-controls-direction bx-has-controls-auto"><div class="bx-pager bx-default-pager"><div class="bx-pager-item"><a href="" data-slide-index="0" class="bx-pager-link">1</a></div><div class="bx-pager-item"><a href="" data-slide-index="1" class="bx-pager-link active">2</a></div></div><div class="bx-controls-direction"><a class="bx-prev" href="">Prev</a><a class="bx-next" href="">Next</a></div><div class="bx-controls-auto"><div class="bx-controls-auto-item"><a class="bx-start active" href="">Start</a></div><div class="bx-controls-auto-item"><a class="bx-stop" href="">Stop</a></div></div></div></div>
                                     <div class="clearfix"></div>
                                 </div>

                                 <div id="howItWorks">
                                     <div class="tab-head bot-mspace text-13 clearfix green-head-bg">

                                         <h2 class="text-16 no-mar">How it works!</h2>
                                     </div>
                                     <div class="content img-thumbnail">
                                         <p>Unlike other companies, we love to reward our users who remain loyal to us. That's why we are giving away
                                             tons of gifts every week to both temp workers and employers.</p>
                                         <p>Every time a temp worker gets hired for her a job or a employer successfully post a job, Temps-Direct UK
                                             rewards you with loyalty bonus points. It's our way of saying thank you for choosing us as your No. 1
                                             temp job website.</p>
                                         <br>
                                         <p>Let's explain the knots &amp; bolts:</p>
                                         <p>Every time a temp worker place a bid for job, he/she must choose a budget ranging from 1 -scale 4.</p>
                                         <p>Each scale applies to a single shift performed by a temp worker. Based on the number of shifts a temp
                                             worker successfully performs, he/she will be reward a point(s) depending on the budget level or scale of
                                             the job performed. Each scale accrues the following bonus points:</p>
                                         <br>
                                         <p>Scale 1 budget x 4 shifts = 1 point</p>
                                         <p>Scale 2 budget x 4 shifts = 3 points</p>
                                         <p>Scale 3 budget x 4 shifts = 8 points</p>
                                         <p>Scale 4 budget x 4 shifts = 12 points</p>
                                         <br>
                                         <p>* In order to redeem your points, simply click on the redeem button or email us via our contact us
                                             page.</p>
                                     </div>
                                 </div>
                                 <div id="upcomingBonuses">
                                     <div class="tab-head bot-mspace text-13 clearfix green-head-bg">

                                         <h2 class="text-16 no-mar">Upcoming Bonuses</h2>
                                     </div>
                                     <div class="content">
                                         <div id="features">
                                             <div class="cms-pages">
                                                 <div class="row">
                                                     <div class="col-md-3">
                                                         <div id="feature1">
                                                             <span class="ribbons">13 pts</span>
                                                             <span class="featureContent">Weekend spa break</span>
                                                             <span class="featureImage"><img src="myassets/img/features1.png"></span>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-3">
                                                         <div id="feature2">
                                                             <span class="ribbons">25 pts</span>
                                                             <span class="featureContent">Windows &amp; Android OS Medical Tablet PC</span>
                                                             <span class="featureImage"><img src="myassets/img/features2.png"></span>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-3">
                                                         <div id="feature3">
                                                             <span class="ribbons">60 pts</span>
                                                             <span class="featureContent">Caribbean Cruise for Two</span>
                                                             <span class="featureImage"><img src="myassets/img/features3.png"></span>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-3">
                                                         <div id="feature4">
                                                             <span class="ribbons">15 pts</span>
                                                             <span class="featureContent">Free Grocery &amp; Delivery - 1 month</span>
                                                             <span class="featureImage"><img src="myassets/img/features4.png"></span>
                                                         </div>
                                                     </div>
                                                     <div class="clearfix"></div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 <p style="color: grey; font-size: 12px; margin-top: 10px;">*See our terms and conditions for more details on how
                                     to redeem loyalty bonus points.</p>
                             </div>
                         </section>
                         <script>

                             $(document).ready(function () {
                                 $('.bxslider').bxSlider({
                                     auto: true,
                                     autoControls: true,
                                     options: function(currentIndex) {
                                         alert('dfdsfs');
                                         $('.slider_redeem_btn').click(function () {
                                             $('div#email_popup').bPopup();
                                         });
                                     }
                                 });
                             });
                         </script>                                  </div>
                 </div>
             </div>
             <!-- for modal -->
             <div class="modal hide fade" id="js-ajax-modal">
                 <div class="modal-body"></div>
                 <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a> </div>
             </div>
             <!-- for modal -->
             <!-- for modal -->
             <div class="modal hide fade" id="js-ajax-modal-child">
                 <div class="modal-body"></div>
                 <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a> </div>
             </div>
             <!-- for modal -->

             <div class="clearfix"></div>
             <!-- for modal
             <div class="footer-push"></div> -->
         </div>
         <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
     </div>
	 
     <!-- Include footer.php. Contains footer content. -->
 	 <?php include ('includes/template/footer.php'); ?> 

</body>
</html>
