<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    .page:before {
        content: "";
        width: 100%;
        height: 100%;
        display: table-column !important;
        border-bottom: 4px solid #e0e0e0;
        padding-bottom: 10px;
    }
    .buttonBtn{
        background-color: #93BD39;
        color: white;
        padding: 8px 50px;
        text-align: center;
    }
    .row.bgop > div {
        padding: 20px;
    }
</style>
<div id="pages-bonus" class="content user">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
            </ul>
        </div>
        <div id="js-head-menu" class="">
            <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <a class="show hor-space" title="Temps-direct.uk" href="/">
                                <img alt="" src="myassets/img/logo.png">
                            </a>
                        </div>
                        <div class="cantent">
                            <p>
                                Your log-out has been successful.</p>
                            <p>
                                Thank you for using Temps-Direct temp recruiting platform.</p>
                            <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                            <input name="feedback" type="commant">
                            <p>Thank you!</p>
                        </div>
                        <div class="modal-footer hide">
                            <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default">  Close</a>
                        </div>
                    </div>

                </div>
            </div>
            <script type="text/javascript">
                //<![CDATA[
                function getCookie (c_name) {var c_value = document.cookie;var c_start = c_value.indexOf(" " + c_name + "=");if (c_start == -1) {c_start = c_value.indexOf(c_name + "=");}if (c_start == -1) {c_value = null;} else {c_start = c_value.indexOf("=", c_start) + 1;var c_end = c_value.indexOf(";", c_start);if (c_end == -1) {c_end = c_value.length;}c_value = unescape(c_value.substring(c_start,c_end));}return c_value;}if (getCookie('il')) {(function() {var js = document.createElement('script'); js.type = 'text/javascript'; js.async = true;js.src = "https://healthcare-temps.co.uk/users/show_header.js?u=";var s = document.getElementById('js-head-menu'); s.parentNode.insertBefore(js, s);})();} else {document.getElementById('js-head-menu').className = '';}
                //]]>
            </script>
        </div>

        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container">
                    <style>
                        img{
                            width:120px;
                            height:70px;
                        }
                        .btn{
                            background-color: #93BD39;
                            color:white;
                            padding:8px 50px;
                            text-align:center;

                        }
                        .button{
                            margin-top: 60px;
                            text-align:center;
                            margin-bottom: 50px;

                        }
                        .col-xs-6,.col-sm-4{
                            text-align:center;
                        }


                    </style>
                    <div class="row" style="text-align: center;margin: 18px 0px;margin-top:90px;">
                        <img src="myassets/pg_images/pg_ad_logo.png" alt="" style="width: 282px;margin: 15px 0px;">
                    </div>
                    <div class="row bgop">
                        <div class="col-xs-6 col-sm-4 col-md-2 col-md-offset-1"><img src="myassets/pg_images/airbnb.png" alt="airbnd"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/redox.png" alt="redox"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/echo.jpg" alt="echo"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/crb.jpeg" alt="crb"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/careart.jpg" alt="careart"></div>

                        <div class="col-xs-6 col-sm-4 col-md-2 col-md-offset-1"><img src="myassets/pg_images/shocas.jpg" alt="shocas"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/aurico.jpg" alt="aurico"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/entelo.png" alt="entelo"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/braintree.png" alt="braintree"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/github.png" alt="github"></div>

                        <div class="col-xs-6 col-sm-4 col-md-2 col-md-offset-1"><img src="myassets/pg_images/gptools.jpg" alt="gptools"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/gpdiary.jpg" alt="gpdiary"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/hireright.jpg" alt="hireright"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/adp1.png" alt="adp"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/bitcoin.jpg" alt="bitcoin"></div>

                        <div class="col-xs-6 col-sm-4 col-md-2 col-md-offset-1"><img src="myassets/pg_images/hscic.png" alt="hscic"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/mulesoft.jpeg" alt="mulesoft"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/onesource.jpeg" alt="onesource"></div>
                        <div class="col-xs-6 col-sm-4 col-md-2"><img src="myassets/pg_images/taxamo.png" alt="taxamo"></div>
                    </div>
                    <div class="button">
                        <button class="buttonBtn" name="btn">JOIN TODAY</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- for modal -->
        <div class="modal hide fade" id="js-ajax-modal">
            <div class="modal-body"></div>
            <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a> </div>
        </div>
        <!-- for modal -->
        <!-- for modal -->
        <div class="modal hide fade" id="js-ajax-modal-child">
            <div class="modal-body"></div>
            <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a> </div>
        </div>
        <!-- for modal -->

        <div class="clearfix"></div>
        <!-- for modal
        <div class="footer-push"></div> -->
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php //include ('includes/template/footer.php'); ?>

</body>
</html>
