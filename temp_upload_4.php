<?php
//Check if frontinit.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

if ($_GET['id'] == '') {
    Redirect::to('register.php');
}


//Get Site Settings Data
$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
    foreach ($query->results() as $row) {
        $title = $row->title;
        $use_icon = $row->use_icon;
        $site_icon = $row->site_icon;
        $tagline = $row->tagline;
        $description = $row->description;
        $keywords = $row->keywords;
        $author = $row->author;
        $bgimage = $row->bgimage;
    }
}

//Get Payments Settings Data
$q1 = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($q1->count()) {
    foreach ($q1->results() as $r1) {
        $currency = $r1->currency;
        $membershipid = $r1->membershipid;
    }
}

//Getting Payement Id from Database
$query = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
if ($query->count() === 1) {
    $q1 = DB::getInstance()->get("membership_freelancer", "*", ["membershipid" => $membershipid]);
} else {
    $q1 = DB::getInstance()->get("membership_agency", "*", ["membershipid" => $membershipid]);
}
if ($q1->count() === 1) {
    foreach ($q1->results() as $r1) {
        $bids = $r1->bids;
    }
}

if (!function_exists('base_url')) {
    function base_url($atRoot = FALSE, $atCore = FALSE, $parse = FALSE)
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir = str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            $core = $core[0];

            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf($tmplt, $http, $hostname, $end);
        } else $base_url = 'http://localhost/';

        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }

        return $base_url;
    }
}
$conn = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
$conn->exec("SET CHARACTER SET utf8");
$sql = "SELECT * FROM freelancer WHERE freelancerid=" . $_GET['id'];
$result = $conn->query($sql);
$row = $result->fetch(PDO::FETCH_NUM);
$msg = '';

if (isset($row[0]) && $row[0] != '') {
    if (isset($_POST['_method'])) {
        $appendTo = "";
        $sql = "SELECT * FROM freelancer_info WHERE freelancerid = ". $row[0];
        $res = $conn->query($sql)->fetch(PDO::FETCH_ASSOC);
        if(count($res) > 1) {
            foreach ($_POST as $k => $v) {
                if($k != '_method' && $k != 'data') {
                    $appendTo .= $k .'= "'. $v . '",';
                }
            }
            $appendTo = rtrim($appendTo, ',');
            $sql = "UPDATE freelancer_info SET ". $appendTo . " WHERE freelancerid = ". $row[0];
            $conn->query($sql);
        } else {
            $keys = "freelancerid,";
            $values = "$row[0],";
            foreach ($_POST as $k => $v) {
                if($k != '_method' && $k != 'data' && !empty($v)) {
                    $keys .= "$k,";
                    $values .= "'$v',";
                }
            }
            $keys = rtrim($keys, ',');
            $values = rtrim($values, ',');

            $sql = "INSERT INTO freelancer_info (" .$keys. ") VALUES (" .$values. ")";
            $conn->query($sql);
        }
        Redirect::to('temp_upload_5.php?id='. $_GET['id']);
    }
} else {
    Redirect::to('register.php');
}
?>



<!DOCTYPE html>
<html lang="en">

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    .img-thumbnail inner_img img {
        width: 50px;
        height: 50px;
    }

    .img-thumbnail.inner_img {
        margin-bottom: 33px;
    }

    .compliance-content .img-thumbnail inner_img {
        display: block;
        max-width: 100%;
        height: auto;
        padding: 4px;
        line-height: 1.42857143;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
        margin: 2px auto;
    }

    .wrapper {
        display: grid;
        grid-gap: 10px;
        color: #444;
        margin-bottom: 23px;
    }

    img-thumbnail.inner_img {
        margin-bottom: 33px;
        display: block;
    }

    .img-responsive01 {
        width: 50%;
    }

    .img-thumbnail.inner_img.bottom_img {
        padding: 15px 46px;
    }

    .img-responsive01.bottom_img_01 {
        width: 89%;
    }

    .responsive01.bottom_img_01.bottom_img_02 {
        width: 75%;
    }
</style>
<div id="pages-bonus" class="content user">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
            </ul>
        </div>

        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container">
                    <div class="js-validation-part">
                        <div class="userProfiles form js-responses">
                            <section class="row ver-space ver-smspace">
                                <div class="js-corner round-5">
                                    <div class="tab-head bot-mspace text-13 clearfix blue-head-bg">
                                        <h2 class="pull-left text-16 no-mar">Edit Profile : sefafa </h2>
                                    </div>


                                    <p class="dc bluec">Your Digital CV Profile is your opportunity to advertise your skills to
                                        employers and other recruiters. By marketing your skills, qualification, experience and
                                        writing a brief description about yourself, you are 5 times likely to get more better
                                        paying jobs from top medical and healthcare employers. Your profile must be approved and
                                        vetted, so take time to insert accurate information..</p>


                                    <div class="form-blocks js-corner">
                                        <form action="" class="form-horizontal js-geo-form" id="UserProfileEditForm" method="post" accept-charset="utf-8">
                                            <div style="display:none;"><input type="hidden" name="_method" value="POST"><input
                                                        type="hidden" name="data[_Token][key]"
                                                        value="2c72f57adc85a840e7c26b00fbff40cd753a42eb" id="Token677384830">
                                            </div>

                                            <div class="clearfix">
                                                <div class="form-content-blocks clearfix show">
                                                    <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Personal
                                                        Info:</h3>
                                                    <div class="row">
                                                        <div class="pull-right col-md-2">
                                                            <div>
                                                                <a href="/user_profiles/profile_image/546" class="">Change
                                                                    Image</a>
                                                                <span class="">
                                                            <span class="avtar-box">
                                                                <a href="/user/sefafa" title="sefafa" class="js-tooltip  show no-pad ">
                                                                    <img src="/img/big_thumb/UserAvatar/1.22bb67ea7586f030ead30b33adaf7520.jpg" width="100" height="100" class="js-tooltip  img-thumbnail " alt="[Image: sefafa]" title="">
                                                                </a>
                                                            </span>
                                                        </span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-8">

                                                            <div class="input text  bot-space">
                                                                <div class="input text required validation:{'rule1':{'rule':'notempty','allowEmpty':false,'message':'Required'}} error">
                                                                    <input name="first_name" placeholder="First Name" maxlength="100" type="text" value="" id="UserProfileFirstName">
                                                                    <div class="error-message">Required</div>
                                                                </div>
                                                            </div>
                                                            <div class="input text  bot-space">
                                                                <div class="input text">
                                                                    <input name="last_name" placeholder="Last Name" maxlength="100" type="text" value="" id="UserProfileLastName">
                                                                </div>
                                                            </div>
                                                            <div class="input text  bot-space">
                                                                <div class="input text">
                                                                    <label for="UserProfileMiddleName">Middle Name</label>
                                                                    <input name="middle_name" placeholder="Temp Name" maxlength="100" type="text" value="" id="UserProfileMiddleName">
                                                                </div>
                                                            </div>
                                                            <div class="bot-space">
                                                                <div class="input text">
                                                                    <label for="UserProfileUtrCode">UTR Code</label><input name="utr_no" type="text" value="0" id="UserProfileUtrCode">
                                                                </div>
                                                            </div>
                                                            <div class="ver-space">
                                                                <div class="input text">
                                                                    <input name="hourly_rate" placeholder="Hourly Rate (£)" maxlength="8" type="text" id="UserProfileHourRate">
                                                                </div>
                                                            </div>

                                                            <div class="input select  required  bot-space">
                                                                <div class="input select required validation:{'rule1':{'rule':'notempty','allowEmpty':false,'message':'Required'}}">
                                                                    <label for="UserProfileGenderId">Gender</label>
                                                                    <select name="gender" placeholder="Gender" id="UserProfileGenderId">
                                                                        <option value="">Please Select</option>
                                                                        <option value="Male">Male</option>
                                                                        <option value="Female">Female</option>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="input text date-time select">
                                                                <div class="js-datetime  required  xltriggered"
                                                                     data-displayed="true">
                                                                    <div class="js-cake-date">
                                                                        <select placeholder="DOB" id="UserProfileDobMonth">
                                                                            <option value="">Please Select</option>
                                                                            <option value="01">January</option>
                                                                            <option value="02">February</option>
                                                                            <option value="03">March</option>
                                                                            <option value="04">April</option>
                                                                            <option value="05">May</option>
                                                                            <option value="06">June</option>
                                                                            <option value="07">July</option>
                                                                            <option value="08">August</option>
                                                                            <option value="09">September</option>
                                                                            <option value="10">October</option>
                                                                            <option value="11">November</option>
                                                                            <option value="12">December</option>
                                                                        </select>-
                                                                        <select placeholder="DOB" id="UserProfileDobDay">
                                                                            <option value="">Please Select</option>
                                                                            <option value="01">1</option>
                                                                            <option value="02">2</option>
                                                                            <option value="03">3</option>
                                                                            <option value="04">4</option>
                                                                            <option value="05">5</option>
                                                                            <option value="06">6</option>
                                                                            <option value="07">7</option>
                                                                            <option value="08">8</option>
                                                                            <option value="09">9</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                            <option value="13">13</option>
                                                                            <option value="14">14</option>
                                                                            <option value="15">15</option>
                                                                            <option value="16">16</option>
                                                                            <option value="17">17</option>
                                                                            <option value="18">18</option>
                                                                            <option value="19">19</option>
                                                                            <option value="20">20</option>
                                                                            <option value="21">21</option>
                                                                            <option value="22">22</option>
                                                                            <option value="23">23</option>
                                                                            <option value="24">24</option>
                                                                            <option value="25">25</option>
                                                                            <option value="26">26</option>
                                                                            <option value="27">27</option>
                                                                            <option value="28">28</option>
                                                                            <option value="29">29</option>
                                                                            <option value="30">30</option>
                                                                            <option value="31">31</option>
                                                                        </select>-
                                                                        <select placeholder="DOB" id="UserProfileDobYear">
                                                                            <option value="">Please Select</option>
                                                                            <option value="1918">1918</option>
                                                                            <option value="1919">1919</option>
                                                                            <option value="1920">1920</option>
                                                                            <option value="1921">1921</option>
                                                                            <option value="1922">1922</option>
                                                                            <option value="1923">1923</option>
                                                                            <option value="1924">1924</option>
                                                                            <option value="1925">1925</option>
                                                                            <option value="1926">1926</option>
                                                                            <option value="1927">1927</option>
                                                                            <option value="1928">1928</option>
                                                                            <option value="1929">1929</option>
                                                                            <option value="1930">1930</option>
                                                                            <option value="1931">1931</option>
                                                                            <option value="1932">1932</option>
                                                                            <option value="1933">1933</option>
                                                                            <option value="1934">1934</option>
                                                                            <option value="1935">1935</option>
                                                                            <option value="1936">1936</option>
                                                                            <option value="1937">1937</option>
                                                                            <option value="1938">1938</option>
                                                                            <option value="1939">1939</option>
                                                                            <option value="1940">1940</option>
                                                                            <option value="1941">1941</option>
                                                                            <option value="1942">1942</option>
                                                                            <option value="1943">1943</option>
                                                                            <option value="1944">1944</option>
                                                                            <option value="1945">1945</option>
                                                                            <option value="1946">1946</option>
                                                                            <option value="1947">1947</option>
                                                                            <option value="1948">1948</option>
                                                                            <option value="1949">1949</option>
                                                                            <option value="1950">1950</option>
                                                                            <option value="1951">1951</option>
                                                                            <option value="1952">1952</option>
                                                                            <option value="1953">1953</option>
                                                                            <option value="1954">1954</option>
                                                                            <option value="1955">1955</option>
                                                                            <option value="1956">1956</option>
                                                                            <option value="1957">1957</option>
                                                                            <option value="1958">1958</option>
                                                                            <option value="1959">1959</option>
                                                                            <option value="1960">1960</option>
                                                                            <option value="1961">1961</option>
                                                                            <option value="1962">1962</option>
                                                                            <option value="1963">1963</option>
                                                                            <option value="1964">1964</option>
                                                                            <option value="1965">1965</option>
                                                                            <option value="1966">1966</option>
                                                                            <option value="1967">1967</option>
                                                                            <option value="1968">1968</option>
                                                                            <option value="1969">1969</option>
                                                                            <option value="1970">1970</option>
                                                                            <option value="1971">1971</option>
                                                                            <option value="1972">1972</option>
                                                                            <option value="1973">1973</option>
                                                                            <option value="1974">1974</option>
                                                                            <option value="1975">1975</option>
                                                                            <option value="1976">1976</option>
                                                                            <option value="1977">1977</option>
                                                                            <option value="1978">1978</option>
                                                                            <option value="1979">1979</option>
                                                                            <option value="1980">1980</option>
                                                                            <option value="1981">1981</option>
                                                                            <option value="1982">1982</option>
                                                                            <option value="1983">1983</option>
                                                                            <option value="1984">1984</option>
                                                                            <option value="1985">1985</option>
                                                                            <option value="1986">1986</option>
                                                                            <option value="1987">1987</option>
                                                                            <option value="1988">1988</option>
                                                                            <option value="1989">1989</option>
                                                                            <option value="1990">1990</option>
                                                                            <option value="1991">1991</option>
                                                                            <option value="1992">1992</option>
                                                                            <option value="1993">1993</option>
                                                                            <option value="1994">1994</option>
                                                                            <option value="1995">1995</option>
                                                                            <option value="1996">1996</option>
                                                                            <option value="1997">1997</option>
                                                                            <option value="1998">1998</option>
                                                                            <option value="1999">1999</option>
                                                                            <option value="2000">2000</option>
                                                                            <option value="2001">2001</option>
                                                                            <option value="2002">2002</option>
                                                                            <option value="2003">2003</option>
                                                                            <option value="2004">2004</option>
                                                                            <option value="2005">2005</option>
                                                                            <option value="2006">2006</option>
                                                                            <option value="2007">2007</option>
                                                                            <option value="2008">2008</option>
                                                                            <option value="2009">2009</option>
                                                                            <option value="2010">2010</option>
                                                                            <option value="2011">2011</option>
                                                                            <option value="2012">2012</option>
                                                                            <option value="2013">2013</option>
                                                                            <option value="2014">2014</option>
                                                                            <option value="2015">2015</option>
                                                                            <option value="2016">2016</option>
                                                                            <option value="2017">2017</option>
                                                                            <option value="2018">2018</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="text" class="hidden" id="totalDOB" name="dob">
                                                            <script>
                                                                $(document).ready(function () {
                                                                    $('#totalDOB').val($('#UserProfileDobDay').val()+'/'+$('#UserProfileDobMonth').val()+'/'+$('#UserProfileDobYear').val());
                                                                    $('.js-cake-date').change(function() {
                                                                        $('#totalDOB').val($('#UserProfileDobDay').val()+'/'+$('#UserProfileDobMonth').val()+'/'+$('#UserProfileDobYear').val());
                                                                    });
                                                                })
                                                            </script>
                                                            <div class="input text  required  bot-space">
                                                                <div class="input textarea required validation:{'rule1':{'rule':'notempty','allowEmpty':false,'message':'Required'}}">
                                                                    <textarea name="about_me" placeholder="About Me/Profile" cols="30" rows="6" id="UserProfileAboutMe"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="input text  bot-space">
                                                                <div class="input select"><label for="UserProfileAvailability">Availability</label><select
                                                                            name="availability"
                                                                            id="UserProfileAvailability">
                                                                        <option value="">(choose one)</option>
                                                                        <option value="1 day a week">1 day a week</option>
                                                                        <option value="1 night a week">1 night a week</option>
                                                                        <option value="2-3 days a week">2-3 days a week</option>
                                                                        <option value="2-3 nights a week">2-3 nights a week
                                                                        </option>
                                                                        <option value="3-5 days a week">3-5 days a week</option>
                                                                        <option value="3-5 nights a week">3-5 nights a week
                                                                        </option>
                                                                        <option value="Days only">Days only</option>
                                                                        <option value="Nights only">Nights only</option>
                                                                        <option value="Weekends only">Weekends only</option>
                                                                    </select></div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="form-content-blocks">
                                                <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Location:</h3>
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="mapblock-info inputtypebox0">
                                                            <div class="clearfix address-input-block ver-space">
                                                                <div class="input text required validation:{'allowEmpty':false,'message':'Required','rule':'notempty'}">
                                                                    <label for="UserProfileAddress">Location</label>
                                                                    <input name="location" class="js-offline address-error-message xltriggered" type="text" id="UserProfileAddress">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hidden-xs">

                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>

                                            <div class="bot-space inputtypebox01">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="input text"><label for="UserProfileZipCode">Post
                                                                Code</label>
                                                            <input name="post_code" type="text" id="UserProfileZipCode">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 hidden-xs">

                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="clearfix"></div>
                                            <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Education:</h3>

                                            <!-- <div class="bot-space" >                            </div> -->

                                            <div class="bot-space">
                                                <div class="education_fields_wrap">
                                                    <button class="add_education btn btn-success">Add More</button>
                                                </div>
                                            </div>

                                            <!-- <h3 class="tab-head text-16 textn img-rounded blue-head-bg"></h3>
                                            <div class="bot-space"></div> -->
                                            <div class="clearfix"></div>
                                            <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Work History:</h3>

                                            <div class="bot-space">
                                                <div class="experience_fields_wrap">
                                                    <button class="add_experience btn btn-success">Add More</button>
                                                </div>
                                            </div>

                                            <div class="clearfix">
                                                <div class="form-content-blocks">
                                                    <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Contacts:</h3>
                                                    <div class="ver-space inputtypebox">
                                                        <div class="input text">
                                                            <label for="UserProfileFacebookUrl">Facebook</label>
                                                            <input name="fb_url" class="js-remove-error" maxlength="255" type="text" value="" id="UserProfileFacebookUrl">
                                                            <span class="info">Your Facebook URL start with http://</span>
                                                        </div>
                                                    </div>
                                                    <div class="bot-space inputtypebox">
                                                        <div class="input text">
                                                            <label for="UserProfileWebsite">Web URL</label>
                                                            <input name="web_url" class="js-remove-error" maxlength="255" type="text" value="" id="UserProfileWebsite">
                                                            <span class="info">Your website URL start with http://</span>
                                                        </div>
                                                    </div>
                                                    <div class="bot-space inputtypebox">
                                                        <div class="input text">
                                                            <label for="UserProfileLinkedinUrl">LinkedIn URL</label>
                                                            <input name="linkedin_url" class="js-remove-error" maxlength="255" type="text" value="" id="UserProfileLinkedinUrl">
                                                            <span class="info">Your Linkedin URL start with http://</span>
                                                        </div>
                                                    </div>
                                                    <div class="bot-space inputtypebox">
                                                        <div class="input text">
                                                            <label for="UserProfileYahoo">Yahoo</label>
                                                            <input name="yahoo_url" class="js-remove-error" maxlength="255" type="text" id="UserProfileYahoo">
                                                            <span class="info">Your Yahoo email</span>
                                                        </div>
                                                    </div>
                                                    <div class="bot-space inputtypebox">
                                                        <div class="input text">
                                                            <label for="UserProfileMsn">MSN</label>
                                                            <input name="msn_url" class="js-remove-error" maxlength="255" type="text" id="UserProfileMsn">
                                                            <span class="info">Your MSN ID</span>
                                                        </div>
                                                    </div>
                                                    <div class="bot-space inputtypebox">
                                                        <div class="input text">
                                                            <label for="UserProfileAim">Oovoo URL</label>
                                                            <input name="oovoo_url" class="js-remove-error" maxlength="255" type="text" id="UserProfileAim">
                                                            <span class="info">Your Oovoo ID</span>
                                                        </div>
                                                    </div>
                                                    <div class="bot-space inputtypebox">
                                                        <div class="input text">
                                                            <label for="UserProfileSkype">Skype</label>
                                                            <input name="skype_url" class="js-remove-error" maxlength="255" type="text" id="UserProfileSkype">
                                                            <span class="info">Your Skype ID</span>
                                                        </div>
                                                    </div>
                                                    <div class="bot-space inputtypebox">
                                                        <div class="input text">
                                                            <label for="UserProfileGoogleTalk">Gruveo</label>
                                                            <input name="gruveo_url" class="js-remove-error" maxlength="255" type="text" id="UserProfileGoogleTalk">
                                                            <span class="info">Your Gruveo email</span>
                                                        </div>
                                                    </div>

                                                    <div class="bot-space inputtypebox">
                                                        <div class="input text">
                                                            <label for="UserProfilePhone">Phone</label>
                                                            <input name="phone" placeholder="Phone" class="js-remove-error" maxlength="255" type="text" id="UserProfilePhone">
                                                            <span class="info">Your Phone Number</span>
                                                        </div>
                                                    </div>
                                                    <div class="bot-space inputtypebox">
                                                        <div class="input text">
                                                            <label for="UserProfileMobile">Mobile</label>
                                                            <input name="mobile" placeholder="Mobile" class="js-remove-error" maxlength="255" type="text" id="UserProfileMobile">
                                                            <span class="info">Your Mobile Number</span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                            <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Registration Nos:</h3>
                                            <div class="ver-space inputtypebox">
                                                <div class="input text"><label for="UserProfileCrbNo">CRB/DBS No.</label><input
                                                            name="crb_dbs_no" class="js-remove-error"
                                                            maxlength="255" type="text" value="" id="UserProfileCrbNo"></div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text"><label for="UserProfileCompanyNo">Company
                                                        No.</label><input name="company_no"
                                                                          class="js-remove-error" maxlength="255" type="text"
                                                                          value="333333333333333333" id="UserProfileCompanyNo">
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text"><label for="UserProfileNmcPin">NMC PIN</label><input
                                                            name="nmc_pin" class="js-remove-error"
                                                            maxlength="255" type="text" value="" id="UserProfileNmcPin"></div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text"><label for="UserProfileOther">other</label><input
                                                            name="other" class="js-remove-error"
                                                            maxlength="255" type="text" value="" id="UserProfileOther"></div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text"><label for="UserProfileNino">NINO</label><input
                                                            name="nino_no" class="js-remove-error"
                                                            maxlength="255" type="text" value="111111111" id="UserProfileNino">
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text"><label for="UserProfileTaxCode">Tax Code:</label><input
                                                            name="tax_code" class="js-remove-error"
                                                            maxlength="255" type="text" value="" id="UserProfileTaxCode"></div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text"><label for="UserProfileGmcNo">GMC no</label><input
                                                            name="gmc_no" class="js-remove-error"
                                                            maxlength="255" type="text" value="" id="UserProfileGmcNo"></div>
                                            </div>

                                            <div class="bot-space inputtypebox">
                                                <div class="input text"><label for="UserProfileUtr">UTR</label><input
                                                            name="utr_no" class="js-remove-error"
                                                            maxlength="255" type="text" value="3333333333" id="UserProfileUtr">
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text"><label for="UserProfilePassportNo">Passport
                                                        No:</label><input name="passport_no"
                                                                          class="js-remove-error" maxlength="255" type="text"
                                                                          value="" id="UserProfilePassportNo"></div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text"><label for="UserProfileGphc">GPhc</label><input
                                                            name="gphc" class="js-remove-error"
                                                            maxlength="255" type="text" value="" id="UserProfileGphc"></div>
                                            </div>
                                            <!-- <div class="bot-space inputtypebox" ><div class="input text"><label for="UserProfileMis">Mis</label><input name="data[UserProfile][mis]" class="js-remove-error" maxlength="255" type="text" value="" id="UserProfileMis"/></div></div> -->

                                            <div class="clearfix"></div>
                                            <h4 class="tab-head text-16 textn img-rounded blue-head-bg">Declaration:</h4>
                                            <!--<h4 class="text-16 textn"> I declare that the information provided on this e-form is correct to the best of my knowledge and ability.</h4>
                              <h4 class="text-16 textn">I grant the administrator permission to check the information provided. </h4>
                              <h4 class="text-16 textn dl">I fully understand that I could be prosecuted for providing false or misleading information in order to deceive or decipher details from potential staff. </h4>!-->
                                            <div class="input checkbox no-mar required validation:{'message':'You must accept the declaration','required':true,'rule':['comparison','!=',0]}">
                                                <input type="checkbox" value="1" id="UserProfileDeclaration">
                                                <label for="UserProfileDeclaration checkbox">
                                                    <label class="text-16 textn no-mar dl"><input type="checkbox" value="">
                                                        I hereby declare that the information/data
                                                        provided above is true and accurate to my best knowledge. I fully understand,
                                                        that I will be held liable and/or accountable for the accuracy of the
                                                        information supplied above and I could be legally prosecuted for providing false
                                                        data
                                                    </label>
                                                    <style>
                                                        #submitBtn {
                                                            position: absolute;
                                                            right: -120px;
                                                            top: 55px;
                                                        }
                                                    </style>
                                                    <input type="submit" class="btn no-mar blue-head-bg" id="submitBtn" value="Next">
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                </div>


                            </section>

                            <div class="row">
                                <div class="col-xs-12 col-md-offset-1" style="margin-top: 90px;">
                                    <style>
                                        .circle-bar .circles{
                                            border-radius: 50%;
                                            width: 40px;
                                            height: 40px;
                                            display: inline-block;
                                            background-color: #fff;
                                            margin-top: -17px;
                                        }
                                        .blankcircles{
                                            border-color: green;
                                            border: 1px solid green;
                                            border-radius: 50%;
                                            width: 40px;
                                            height: 40px;
                                            display: inline-block;
                                            background-color: #fff;
                                            margin-top: -17px;
                                        }
                                        .lgcircles{
                                            border-radius: 50%;
                                            width: 60px;
                                            height: 60px;
                                            border-color: green;
                                            border: 1px solid green;
                                            display: inline-block;
                                            background-color: #fff;
                                            margin-top: -23px;
                                        }
                                    </style>
                                    <div class="circle-bar" style=" overflow: visible;position: relative;width: 80%;margin: 0 auto; height: 5px;background-color: green;">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <div class="circles">
                                                    <i style="font-size: 37px; height: 40px;width: 40px; border-radius: 50%;color: white;background-color: green; " class="fa fa-check" aria-hidden="true"></i>
                                                </div>
                                                <p>Compliance<br> Upload Documents</p>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="circles">
                                                    <i style="font-size: 37px; height: 40px;width: 40px; border-radius: 50%;color: white;background-color: green; " class="fa fa-check" aria-hidden="true"></i>
                                                </div>
                                                <p>Tax Compliance <br> &amp; Self-Employment</p>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="circles">
                                                    <i style="font-size: 37px; height: 40px;width: 40px; border-radius: 50%;color: white;background-color: green; " class="fa fa-check" aria-hidden="true"></i>
                                                </div>
                                                <p>Create CV <br> Profile</p>
                                            </div>
                                            <div class="col-xs-3">
                                                <div style="width: 100%;background-color: #fff;height: 5px;"></div>
                                                <div class=" lgcircles">
                                                    <i style="font-size: 56px; height: 40px;width: 40px; border-radius: 50%;color: green; " class="fa fa-check" aria-hidden="true"></i>
                                                </div>
                                                <p> Delivered</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                (function (a) {
                                    a.fn.duplicateElement = function (b) {
                                        b = a.extend(a.fn.duplicateElement.defaults, b);
                                        return this.each(function () {
                                            var e = a(this);
                                            var c = document.createElement(b.tag_name);
                                            c.setAttribute("id", b.tag_id);
                                            e[0].parentNode.appendChild(c);
                                            var d = 0;
                                            a(e, a("#" + b.tag_id)).on("click", b.class_create, function (f) {
                                                e.clone().addClass("dinamic-field").appendTo("#" + b.tag_id);
                                                a(b.class_remove).show();
                                                a(b.class_remove).first().hide();
                                                a(b.class_create).hide();
                                                a(b.class_create).first().show();
                                                f.preventDefault();
                                                d++;
                                                return false
                                            });
                                            e.find(b.class_remove).first().hide();
                                            a("#" + b.tag_id).on("click", b.class_remove, function (f) {
                                                a(this).parents(".dinamic-field").remove();
                                                f.preventDefault();
                                                return false
                                            })
                                        })
                                    };
                                    a.fn.duplicateElement.defaults = {
                                        tag_name: "div",
                                        tag_id: "dinamic-fields",
                                        clone_model: "#clone-field-model",
                                        class_remove: ".remove-this-fields",
                                        class_create: ".create-new-fields"
                                    }
                                })(jQuery);
                            </script>
                            <script type="text/javascript">

                                $(document).ready(function () {

                                    $('#submitBtn').click(function (e) {
                                        if(!$('#UserProfileDeclaration').is(':checked')) {
                                            e.preventDefault();
                                        }
                                    });

                                    $('#Education-model').duplicateElement({
                                        tag_name: 'div',
                                        tag_id: "education-fields",
                                        clone_model: "#clone-education-model",
                                        class_remove: ".remove-this-fields-education",
                                        class_create: ".create-new-fields-education",
                                        onCreate: "",
                                        onRemove: ""
                                    });

                                    $('#Education-model').duplicateElement({
                                        "class_remove": ".remove-this-field-education",
                                        "class_create": ".create-new-field-education"
                                    });


                                    $('#Experience-model').duplicateElement({
                                        tag_name: 'div',
                                        tag_id: "experience-fields",
                                        clone_model: "#clone-experience-model",
                                        class_remove: ".remove-this-fields-experience",
                                        class_create: ".create-new-fields-experience",
                                        onCreate: "",
                                        onRemove: ""
                                    });

                                    $('#Experience-model').duplicateElement({
                                        "class_remove": ".remove-this-field-experience",
                                        "class_create": ".create-new-field-experience"
                                    });

                                });


                            </script>

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    var max_fields = 10; //maximum input boxes allowed
                                    //var x = 1; //initial text box count
                                    var x = '-1';
                                    $('.add_education').click(function (e) { //on add input button click
                                        e.preventDefault();
                                        if (x < max_fields) { //max input box allowed
                                            var a = parseInt(x) + 1;
                                            $('.education_fields_wrap ').append('<div class=edu' + a + '><table id="Education-model"><tbody><tr style="border:0px"><td class="col-md-2"><input style="width: 100%" id=UserEducation' + a + 'School name=data[UserEducation][' + a + '][school] type="text" placeholder="School" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserEducation' + a + 'qualification name=data[UserEducation][' + a + '][qualification] type="text" placeholder="Qualification" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserEducation' + a + 'start name=data[UserEducation][' + a + '][start] type="text" placeholder="Start Year" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserEducation' + a + 'end name=data[UserEducation][' + a + '][end] type="text" placeholder="End Year" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserEducation' + a + 'notes name=data[UserEducation][' + a + '][notes] type="text" placeholder="Notes" class="form-control input-md" value="" required></td><td><a href="javascript:void(0);" class="remove_education" data-id=' + a + '>remove</a></td></tr></tbody></table></div>'); //add input box
                                            x++; //text box increment
                                        }
                                    });

                                    $('.education_fields_wrap').on("click", ".remove_education", function (e) { //user click on remove text
                                        if (x > 0) {
                                            var num = $(this).attr('data-id');
                                            e.preventDefault();
                                            $('.edu' + num).remove();
                                            x--;
                                        }
                                    });

                                    var y = '-1'; //initial text box count
                                    $('.add_experience').click(function (e) { //on add input button click
                                        e.preventDefault();
                                        if (y < max_fields) { //max input box allowed
                                            var b = parseInt(y) + 1;
                                            $('.experience_fields_wrap').append('<div class=exp' + b + '><table id="Experience-model"><tbody><tr style="border:0px"><td class="col-md-2"><input style="width: 100%" id=UserExperience' + b + 'Employer name=data[UserExperience][' + b + '][employer] type="text" placeholder="Employer" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserExperience' + b + 'Title name=data[UserExperience][' + b + '][title] type="text" placeholder="Job Title" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserExperience' + b + 'Start name=data[UserExperience][' + b + '][start] type="text" placeholder="Start Year" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserExperience' + b + 'End name=data[UserExperience][' + b + '][end] type="text" placeholder="End Year" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserExperience' + b + 'Notes name=data[UserExperience][' + b + '][notes] type="text" placeholder="Notes" class="form-control input-md" value="" required><td><a href="javascript:void(0);" class="remove_experience" data-id=' + b + '>remove</a></td></td></tr></tbody></table></div>');
                                        }
                                        y++; //text box increment
                                    });

                                    $('.experience_fields_wrap').on("click", ".remove_experience", function (e) { //user click on remove text
                                        if (y > 0) {
                                            var num = $(this).attr('data-id');
                                            e.preventDefault();
                                            $('.exp' + num).remove();
                                            y--;
                                        }
                                    });
                                });

                            </script>
                        </div>
                    </div>
                </div>
                <!-- for modal -->
                <div class="modal hide fade" id="js-ajax-modal">
                    <div class="modal-body"></div>
                    <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a></div>
                </div>
                <!-- for modal -->
                <!-- for modal -->
                <div class="modal hide fade" id="js-ajax-modal-child">
                    <div class="modal-body"></div>
                    <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a></div>
                </div>
                <!-- for modal -->

                <div class="clearfix"></div>
                <!-- for modal
                <div class="footer-push"></div> -->
            </div>
            <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
        </div>
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

</body>
</html>






