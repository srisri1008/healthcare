<?php
//Check if init.php exists
if (!file_exists('../core/init.php')) {
    header('Location: ../install/');
    exit;
} else {
    require_once '../core/init.php';
}

//Start new Admin Object
$admin = new Admin();

//Check if Admin is logged in
if (!$admin->isLoggedIn()) {
    Redirect::to('index.php');
}
function objectToArray($d) {
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $d = get_object_vars($d);
    }

    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    }
    else {
        // Return array
        return $d;
    }
}
//Only true if freelancer data is retrieved
$flag = false;
//Getting freelancer Data
$freelancerid = Input::get('id');
$row = objectToArray(DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancerid, "LIMIT" => 1])->results()[0]);
if (count($row)) {
        $nid = $row['id'];
        $freelancerid = $row['freelancerid'];
    $userMain = $row;
    $userInfo = objectToArray(DB::getInstance()->get("freelancer_info", "*", ["freelancerid" => $nid, "LIMIT" => 1])->results()[0]);
    $userDocuments = objectToArray(DB::getInstance()->get("documentupload", "*", ["user_id" => $nid, "LIMIT" => 1])->results()[0]);
} else {
    Redirect::to('freelancerlist.php');
}

if(isset($_POST['update_btn'])) {
    if(isset($_FILES)) {
        $file = $_FILES['profile_pic'];
        $errorsCount = 0;
        $name = $file['name'];
        $size = $file['size'];
        $path = "uploads/";
        $valid_formats = array("jpg", "png", "gif", "bmp", "jpeg");
        if (strlen($name)) {
            list($txt, $ext) = explode(".", $name);
            if (in_array($ext, $valid_formats)) {
                if ($size < (1024 * 1024)) // Image size max 1 MB
                {
                    $actual_image_name = time() . 'user' . $_GET['id'] . "." . $ext;
                    $tmp = $file['tmp_name'];

                    if (move_uploaded_file($tmp, $path . $actual_image_name)) {
                        $sta = DB::getInstance()->update('freelancer', ['imagelocation' => $actual_image_name], ['id' => $nid]);
                    }
                } else {
//                            echo 'Image file size max 1 MB';
                }
            } else {

//                        echo 'Invalid file format.. and format must be jpg, png, gif, bmp,jpeg,pdf, doc, docx';
            }
        } else {
//                    echo 'Please select image..!';
        }
    }
    if(count($_POST['userInfo']) && count($_POST['userDocuments'])) {
        //Document Verification
        $documentArr = $_POST['userDocuments'];
        $arrToPassDoc = array('cbr_dbs_status' => 0, 'proof_status' => 0, 'certificates_status' => 0, 'proofaddress_status' => 0, 'hepatitis_status' => 0, 'prof_status' => 0, 'registration_status' => 0, 'utr_status' => 0, 'NHSSmart_status' => 0, 'wp_status' => 0, 'ref1_status' => 0, 'ref2_status' => 0);
        foreach ($documentArr AS $k => $v) {
            $arrToPassDoc[$k] = 1;
        }
        //Update Freelancer Documents
        $freelancerDocumentsUpdate = DB::getInstance()->update('documentupload', $arrToPassDoc, [
            'user_id' => $nid
        ]);

        //Info update
        $arrToPassInfo = [];
        $infoArr = $_POST['userInfo'];
        foreach ($infoArr AS $k => $v) {
            $arrToPassInfo[$k] = $v;
        }
        //Update Freelancer Info
        $freelancerInfoUpdate = DB::getInstance()->update('freelancer_info', $arrToPassInfo, [
            'freelancerid' => $nid
        ]);
        if(count($freelancerDocumentsUpdate) && count($freelancerInfoUpdate)) {
//            header('Location: ' . $_SERVER['PHP_SELF'] . '?id=' . $_GET['id']);
            $updatedError = true;
        } else {
            $hasError = true;
        }

    }
    header('Location: ' . $_SERVER['PHP_SELF'] . '?id=' . $_GET['id']);
}


?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include('template/header.php'); ?>

<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include('template/sidenav.php'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $lang['freelancer']; ?>
                <small><?php echo $lang['section']; ?></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active"><?php echo $lang['edit']; ?><?php echo $lang['freelancer']; ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <style>
                .update-main-heading {
                    margin: 20px 0px;
                    border:1px solid grey;
                    background-color: #f5f5f5 !important;
                    padding-left: 10px;
                    border-radius: 10px;
                }
            </style>
            <div class="container" style="padding: 50px;">
                <form action="" method="post" class="update_form" enctype="multipart/form-data">
                    <div class="main-info">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group"><label>First Name</label><input type="text" name="userInfo[first_name]" class="form-control" value="<?=$userInfo['first_name']?>"></div>
                                <div class="form-group"><label>Last Name</label><input type="text" name="userInfo[last_name]" class="form-control" value="<?=$userInfo['last_name']?>"></div>
                                <div class="form-group"><label>UTR Code</label><input type="text" name="userInfo[utr_no]" class="form-control" value="<?=$userInfo['utr_no']?>"></div>
                                <div class="form-group"><label>Hourly Rate</label><input type="text" name="userInfo[hourly_rate]" class="form-control" value="<?=$userInfo['hourly_rate']?>"></div>
                                <div class="form-group"><label>Care Settings</label><input type="text" name="care_settings" class="form-control"></div>
                                <div class="form-group"><label>Service Users</label><input type="text" name="service_users" class="form-control"></div>
                                <div class="form-group"><label>No. Of Employees</label><input type="text" name="no_of_emp" class="form-control"></div>
                                <div class="form-group"><label>Salaries Range</label><input type="text" name="salaries_range" class="form-control"></div>
                                <div class="form-group"><label>About me</label><textarea rows="5" name="userInfo[about_me]" class="form-control"><?=$userInfo['about_me']?></textarea></div>
                            </div>
                            <div class="col-lg-3 col-lg-offset-1">
                                <div class="form-group">
                                    <label for="changeImage">Change Image</label>
                                    <input type="file" name="profile_pic" id="changeImage">
                                </div>
                                <img src="uploads/<?=$userMain['imagelocation']?>" alt="User image">
                            </div>
                        </div>
                    </div>
                    <div class="location-info">
                        <div class="row update-main-heading">
                            <h4>Location:</h4>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-lg-offset-1">
                                <div class="form-group">
                                    <label>Location</label>
                                    <input type="text" name="userInfo[location]" class="form-control" value="<?=$userInfo['location']?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-lg-offset-1">
                                <div class="form-group">
                                    <label>Postal Code</label>
                                    <input type="text" name="userInfo[post_code]" class="form-control" value="<?=$userInfo['post_code']?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="contact-info">
                        <div class="row update-main-heading">
                            <h4>Contacts:</h4>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-4">
                                <div class="form-group">
                                    <label>Facebook</label>
                                    <input type="text" name="userInfo[fb_url]" class="form-control" value="<?=$userInfo['fb_url']?>">
                                </div>
                            </div>
                            <div class="col-lg-offset-2 col-lg-4">
                                <div class="form-group">
                                    <label>Web URL</label>
                                    <input type="text" name="userInfo[web_url]" class="form-control" value="<?=$userInfo['web_url']?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-4">
                                <div class="form-group">
                                    <label>LinkedIn URL</label>
                                    <input type="text" name="userInfo[linkedin_url]" class="form-control" value="<?=$userInfo['linkedin_url']?>">
                                </div>
                            </div>
                            <div class="col-lg-offset-2 col-lg-4">
                                <div class="form-group">
                                    <label>Yahoo</label>
                                    <input type="text" name="userInfo[yahoo_url]" class="form-control" value="<?=$userInfo['yahoo_url']?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-4">
                                <div class="form-group">
                                    <label>MSN</label>
                                    <input type="text" name="userInfo[msn_url]" class="form-control" value="<?=$userInfo['msn_url']?>">
                                </div>
                            </div>
                            <div class="col-lg-offset-2 col-lg-4">
                                <div class="form-group">
                                    <label>OOvoo URL</label>
                                    <input type="text" name="userInfo[oovoo_url]" class="form-control" value="<?=$userInfo['oovoo_url']?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-4">
                                <div class="form-group">
                                    <label>Skype</label>
                                    <input type="text" name="userInfo[skype_url]" class="form-control" value="<?=$userInfo['skype_url']?>">
                                </div>
                            </div>
                            <div class="col-lg-offset-2 col-lg-4">
                                <div class="form-group">
                                    <label>Gruveo</label>
                                    <input type="text" name="userInfo[gruveo_url]" class="form-control" value="<?=$userInfo['gruveo_url']?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-4">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" name="userInfo[phone]" class="form-control" value="<?=$userInfo['phone']?>">
                                </div>
                            </div>
                            <div class="col-lg-offset-2 col-lg-4">
                                <div class="form-group">
                                    <label>Mobile</label>
                                    <input type="text" name="userInfo[mobile]" class="form-control" value="<?=$userInfo['mobile']?>">
                                </div>
                            </div>
                        </div>
<!--                        <div class="row">-->
<!--                            <div class="col-lg-offset-1 col-lg-4">-->
<!--                                <div class="checkbox">-->
<!--                                    <label><input type="checkbox">Active</label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-lg-offset-2 col-lg-4">-->
<!--                                <div class="checkbox">-->
<!--                                    <label><input type="checkbox">Email Confirmed</label>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                    <div class="registration-info">
                        <div class="row update-main-heading">
                            <h4>Registration Nos:</h4>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-4">
                                <div class="form-group">
                                    <label>CRB/DBS No.</label>
                                    <input type="text" name="userInfo[cbr_dbs_no]" class="form-control" value="<?=$userInfo['cbr_dbs_no']?>">
                                </div>
                            </div>
                            <div class="col-lg-offset-2 col-lg-4">
                                <div class="form-group">
                                    <label>Company No.</label>
                                    <input type="text" name="userInfo[company_no]" class="form-control" value="<?=$userInfo['company_no']?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-4">
                                <div class="form-group">
                                    <label>NMC PIN</label>
                                    <input type="text" name="userInfo[nmc_pin]" class="form-control" value="<?=$userInfo['nmc_pin']?>">
                                </div>
                            </div>
                            <div class="col-lg-offset-2 col-lg-4">
                                <div class="form-group">
                                    <label>Other</label>
                                    <input type="text" name="userInfo[other]" class="form-control" value="<?=$userInfo['other']?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-4">
                                <div class="form-group">
                                    <label>NINO</label>
                                    <input type="text" name="userInfo[nino_no]" class="form-control" value="<?=$userInfo['nino_no']?>">
                                </div>
                            </div>
                            <div class="col-lg-offset-2 col-lg-4">
                                <div class="form-group">
                                    <label>Tax Code</label>
                                    <input type="text" name="userInfo[tax_code]" class="form-control" value="<?=$userInfo['tax_code']?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-4">
                                <div class="form-group">
                                    <label>GMC No.</label>
                                    <input type="text" name="userInfo[gmc_no]" class="form-control" value="<?=$userInfo['gmc_no']?>">
                                </div>
                            </div>
                            <div class="col-lg-offset-2 col-lg-4">
                                <div class="form-group">
                                    <label>UTR</label>
                                    <input type="text" name="userInfo[utr_no]" class="form-control" value="<?=$userInfo['utr_no']?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-1 col-lg-4">
                                <div class="form-group">
                                    <label>Passport No.</label>
                                    <input type="text" name="userInfo[passport_no]" class="form-control" value="<?=$userInfo['passport_no']?>">
                                </div>
                            </div>
                            <div class="col-lg-offset-2 col-lg-4">
                                <div class="form-group">
                                    <label>GPhc</label>
                                    <input type="text" name="userInfo[gphc]" class="form-control" value="<?=$userInfo['gphc']?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="verification-info">
                        <div class="row update-main-heading">
                            <h4>Document Verification:</h4>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[cbr_dbs_status]" <?=$userDocuments['cbr_dbs_status'] ? 'checked' : ''?>>DBS/CRB</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[proof_status]" <?=$userDocuments['proof_status'] ? 'checked' : ''?>>Photo ID</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[certificates_status]" <?=$userDocuments['certificates_status'] ? 'checked' : ''?>>Training Certificates</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[proofaddress_status]" <?=$userDocuments['proofaddress_status'] ? 'checked' : ''?>>Proof Of Address</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[hepatitis_status]" <?=$userDocuments['hepatitis_status'] ? 'checked' : ''?>>Flu Jab Help B/Medical</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[prof_status]" <?=$userDocuments['prof_status'] ? 'checked' : ''?>>Professional Indeminity</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[registration_status]" <?=$userDocuments['registration_status'] ? 'checked' : ''?>>Registration Numbers</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[utr_status]" <?=$userDocuments['utr_status'] ? 'checked' : ''?>>NINO</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[NHSSmart_status]" <?=$userDocuments['NHSSmart_status'] ? 'checked' : ''?>>NHS Smart Card</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[wp_status]" <?=$userDocuments['wp_status'] ? 'checked' : ''?>>Work Permit</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[ref1_status]" <?=$userDocuments['ref1_status'] ? 'checked' : ''?>>Reference X1</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="userDocuments[ref2_status]" <?=$userDocuments['ref2_status'] ? 'checked' : ''?>>Reference X2</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="declaration-info">
                        <div class="row update-main-heading">
                            <h4>Declaration:</h4>
                        </div>
                        <div class="row">
                            <div class="checkbox">
                                <label><input type="checkbox" required>
                                    I hereby declare that the information/data provided above is true and accurate to my best knowledge. I fully understand, that I will be held liable and/or accountable for the accuracy of the information supplied above and I could be legally prosecuted for providing false data
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-success" type="submit" name="update_btn">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>
</div><!-- /.wrapper -->
<!-- ==============================================
 Scripts
 =============================================== -->
<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
</body>
</html>
