<?php
//Get Site Settings Data
$query = DB::getInstance()->get("settings", "*", ["id" => 1]);
if ($query->count()) {
 foreach($query->results() as $row) {
 	$sid = $row->id;
 	$title = $row->title;
 	$use_icon = $row->use_icon;
 	$site_icon = $row->site_icon;
 	$tagline = $row->tagline;
 	$url = $row->url;
 	$description = $row->description;
 	$keywords = $row->keywords;
 	$author = $row->author;
 	$mail = $row->mail;
 	$mailpass = $row->mailpass;
 	$job_limit = $row->job_limit;
 	$service_limit = $row->service_limit;
 	$proposal_limit = $row->proposal_limit;
 	$top_title = $row->top_title;
 	$show_downtitle = $row->show_downtitle;
 	$down_title = $row->down_title;
 	$searchterm = $row->searchterm;
 	$header_img = $row->header_img;
 	$cattagline = $row->cattagline;
 	$testtagline = $row->testtagline;
 	$statstagline = $row->statstagline;
 	$about_top_title = $row->about_top_title;
 	$about_header_img = $row->about_header_img;
 	$about_hello = $row->about_hello;
 	$about_whitebg = $row->about_whitebg;
 	$teamtagline = $row->teamtagline;
 	$timelinetagline = $row->timelinetagline;
 	$how_top_title = $row->how_top_title;
 	$how_header_img = $row->how_header_img;
 	$faq_top_title = $row->faq_top_title;
 	$faq_header_img = $row->faq_header_img;
 	$faq_hello = $row->faq_hello;
 	$contact_top_title = $row->contact_top_title;
 	$contact_header_img = $row->contact_header_img;
 	$contact_location = $row->contact_location;
 	$contact_phone = $row->contact_phone;
 	$contact_email = $row->contact_email;
 	$contact_map = $row->contact_map;
 	$footer_about = $row->footer_about;
 	$facebook = $row->facebook;
 	$twitter = $row->twitter;
 	$google = $row->google;
 	$instagram = $row->instagram;
 	$linkedin = $row->linkedin;
 	$pricing_top_title = $row->pricing_top_title;
 	$pricing_header_img = $row->pricing_header_img;
 	$services_header_img = $row->services_header_img;
 	$jobs_header_img = $row->jobs_header_img;
 	$google_analytics = $row->google_analytics;
 }			
}

//Get Payments Settings Data
$q1 = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($q1->count()) {
 foreach($q1->results() as $r1) {
 	$currency = $r1->currency;
 	$membershipid = $r1->membershipid;
 }			
}

//Get Payments Settings Data
$q2 = DB::getInstance()->get("currency", "*", ["id" => $currency]);
if ($q2->count()) {
 foreach($q2->results() as $r2) {
 	$currency_symbol = $r2->currency_symbol;
 }
}

?>
	<head>
	    <!-- ==============================================
		Title and Meta Tags
		=============================================== -->
		<meta charset="utf-8">
		<title><?php echo escape($title) .' - '. escape($tagline) ; ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="<?php echo escape($description); ?>">
        <meta name="keywords" content="<?php echo escape($keywords); ?>">
        <meta name="author" content="<?php echo escape($author); ?>">
		
		<!-- ==============================================
		Favicons
		=============================================== --> 
		<link rel="shortcut icon" href="img/favicons/favicon.ico">
		<link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-touch-icon-114x114.png">
		
	    <!-- ==============================================
		CSS
		=============================================== -->
        <!-- Style-->
        <link href="assets/css/hopler.css" rel="stylesheet" type="text/css" />
				
		<!-- ==============================================
		Feauture Detection
		=============================================== -->
		<script src="/js/modernizr-custom.js"></script>
		
		<!--[if lt IE 9]>
		 <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>healthcare-temps.co.uk | Home</title>
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
        <!--[if IE 11]>
        <script> var isIE = true;</script>
        <![endif]-->
        <!--[if gt IE 9]>
        <script> var isIE = true;</script>
        <![endif]-->
        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script>
        <script> var isIE = true;</script>
        <![endif]-->
        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <link href="https://healthcare-temps.co.uk/favicon.ico" type="image/x-icon" rel="icon"><link href="https://healthcare-temps.co.uk/favicon.ico" type="image/x-icon" rel="shortcut icon">
        <link rel="stylesheet" type="text/css" href="myassets/css/bootstrap.min.css">
        <!--link href="css/bootstrap.min.css" rel="stylesheet"-->

        <!-- Website CSS -->
        <link rel="stylesheet" type="text/css" href="myassets/css/style.css">        <link rel="stylesheet" type="text/css" href="myassets/css/responsive.css">		<meta name="keywords" content="locum, temps-on-demand, temps recruitment">
        <meta name="description" content="Medical temp recruitment">
        <link rel="apple-touch-icon" href="https://healthcare-temps.co.uk/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="https://healthcare-temps.co.uk/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="https://healthcare-temps.co.uk/apple-touch-icon-114x114.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="myassets/css/css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="myassets/css/default.cache.v2.0b7.css">		<!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="/css/font-awesome-ie7.css" />		<![endif]-->

        <script src="/assets/js/jQuery-2.1.4.min.js"></script>
        <script src="/assets/js/jquery.knob.min.js"></script>
        <script src="/assets/js/typed.min.js"></script>

        <script type="text/javascript" async="" src="myassets/js/tracking.js.download"></script>
        <script type="text/javascript">
            //<![CDATA[
            var cfg = {"basePath":"\/","params":{"controller":"users","action":"home","named":[]},"cfg":{"path_relative":"\/","path_absolute":"https:\/\/healthcare-temps.co.uk\/","site_name":"healthcare-temps.co.uk","timezone":1,"date_format":"M d, Y","today_date":"2018-01-02","lang_code":"en","currency_code":"GBP"}};document.documentElement.className = 'js';(function() {var js = document.createElement('script'); js.type = 'text/javascript'; js.async = true;js.src = "/js/default.cache.v2.0b7.en.js";var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(js, s);})();
            //]]>
        </script><!--
		<meta content="" property="og:app_id" />
		<meta content="" property="fb:app_id" />
							<meta property="og:description" content="Medical temp recruitment" />
							<meta property="og:image" content="https://healthcare-temps.co.uk/img/logo.png"/>
								-->
        <!-- Deep Links Start -->
        <link rel="alternate" media="screen and (os: android)" href="android-app://com.getlancer.getlancer/getlancerprojects.com/users/home">
        <link rel="stylesheet" href="myassets/css/font-awesome.min.css">
        <link rel="alternate" media="screen and (os: ios)" href="ios-app://321456/getlancerprojects.com/users/home">
        <link rel="alternate" media="screen and (os: ios) and (min-device-width : 768px) and (max-device-width : 1024px)" href="ios-app://45464245/getlancerprojects.com/users/home">
        <link rel="alternate" media="screen and (os: windows)" href="windows-phone-app://34536235522/getlancerprojects.com/users/home">
        <!-- Twitter tags start -->
        <meta name="twitter:app:id:iphone" content="321456">
        <meta name="twitter:app:id:ipad" content="45464245">
        <meta name="twitter:app:url:iphone" content="getlancerprojects.com://users/home">
        <meta name="twitter:app:url:ipad" content="getlancerprojects.com://users/home">
        <meta name="twitter:app:id:googleplay" content="com.getlancer.getlancer">
        <meta name="twitter:app:url:googleplay" content="getlancerprojects.com://users/home">
        <!-- Twitter tags end -->
        <!-- Facebook Applinks tags  start -->
        <meta property="al:ios:app_store_id" content="321456">
        <meta property="al:ios:url" content="getlancerprojects.com://users/home">
        <meta property="al:ipad:app_store_id" content="45464245">
        <meta property="al:ipad:url" content="getlancerprojects.com://users/home">
        <meta property="al:android:package" content="com.getlancer.getlancer">
        <meta property="al:android:url" content="getlancerprojects.com://users/home">
        <meta property="al:windows_phone:url" content="getlancerprojects.com://users/home">
        <meta property="al:windows_phone:app_id" content="34536235522">
        <link rel="stylesheet" type="text/css" href="myassets/css/demo.css">		<link rel="stylesheet" type="text/css" href="myassets/css/style(1).css">		<!--<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow&v1' rel='stylesheet' type='text/css' />
		<link href='https://fonts.googleapis.com/css?family=Coustard:900' rel='stylesheet' type='text/css' />
		<link href='https://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css' />-->

        <!-- Facebook Applinks tags  end -->
        <!-- Google tags start -->
        <link rel="alternate" href="android-app://com.getlancer.getlancer/getlancerprojects.com/users/home">
        <!-- Google tags end -->
        <!-- Deep Links end -->
        <link rel="logo" type="img/svg" href="https://healthcare-temps.co.uk/img/logo.svg">
        <!--[if IE]><script type="text/javascript" src="/js/libs/excanvas.js"></script><![endif]-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <script type="text/javascript">
            /*window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set._.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");$.src="//v2.zopim.com/?3sIGJoRCIXaqWO7AcGpv4F3qJ2BIftD6";z.t=+new Date;$.type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");*/

            window.__lc = window.__lc || {};
            window.__lc.license = 9352770;
            (function() {
                var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
            })();
        </script>
        <style>
            .logo_img {
                width: 22%;
            }
            .logo_img {
                width: 211px;
                margin-left: 33px;
            }
            .navbar.navbar-inverse.navigation_bar {
                height: 78px;
                background-color: #132943;
                border-radius: 0px;
            }
            .nav.navbar-nav.navbar-right.nav_right_side {
                margin: 13px 0px;
            }
            .nav.navbar-nav.navbar-right.nav_right_side li a:hover {
                color: #1FAC56;
                transition: 0.3s;
            }
            .nav.navbar-nav.navbar-right.nav_right_side li a {
                font-size: 16px;
            }
         
            .healthcare_temps h6 {
                color: #1FAC54;
                font-size: 29px;
                margin-bottom: 11px;
                margin-top: 48px;
            }
            .healthcare_temps hr {
                height: 3px;
                background-color: #1FAC54;
                width: 77px;
                margin-top: 1px;
            }
           
            .urgent_hjobs {
                background-color: #007FFF;
                color: white;
                text-align: center;
                margin-bottom: 27px;
            }
            .urgent_hjobs h6 {
                font-size: 23px;
                padding: 13px 0px;
                margin-top: 0px;
            }
         
            .hire_textbox {
                height: 39px;
                border-radius: 0px;
                margin-bottom: 21px;
            }
            .hire-job {
                border: 1px solid #007FFF;
                text-align: center;
            }
            .hire-job button {
                background-color: #007FFF;
                color: white;
                border: 1px solid #056cd4;
                padding: 6px 28px;
                margin-bottom: 26px;
                border-radius: 40px;
            }
            .hire-job button:hover {
                background-color: #0772de;
                transition: 0.3s;
            }
            .job_button {
                padding: 0px 19px !important;
                margin-bottom: 0px !important;
            }
            .table.urjent_job tr td {
                border: 0px;
            }
            .benifits_sec {
                background-color: #1FAC56;
                padding-bottom: 44px;
            }
            .container-fluid.main_container {
                padding: 0px 0px;
            }
           
            .benfit_hw p {
                line-height: 21px;
                color: white;
                margin-left: 137px;
            }
            .benfit_hw h6 {
                text-align: center;
                color: white;
                font-size: 23px;
                margin-bottom: 23px
            }
            .benfit_hw button {
                background-color: #3D91C5;
                color: white;
                border: 1px solid #1680c1;
                padding: 8px 17px;
                border-radius: 37px;
                display: table;
                margin: 0 auto !important;
                margin-bottom: 13px;
                margin-top: 39px !important;
            }
            .benfit_hw button:hover {
                background-color: #248bcc;
                transition: 0.3s;
            }

            .healthcare_temp_sec {
                margin-bottom: 79px;
            }
            .suitcase {
                margin-bottom: 19px !important;
            }
            
            .solution_content img {
                width: 100%;
            }
            .solution_content button {
                background-color: #23B3F7;
                border: 1px solid #0c9bde;
                color: white;
                padding: 2px 30px;
                position: relative;
                top: -87px;
                left: 46px;
            }
            .solution_content button:hover {
                background-color: #15a6ea;
                transition: 0.3s;
            }
            .solution_cnt_container {
                margin-bottom: 20px;
            }
            
            .col-lg-4.user_para_text {
                margin-bottom: 41px;
            }
           
           
            .col-lg-12.virtual_care img {
                width: 100%;
            }
            .footer_head h6 {
                font-size: 25px;
                margin-top: 35px;
            }
            ul.footer_links {
                list-style-type: none;
                padding: 0px;
            }
            ul.footer_links li a {
                font-size: 15px;
                color: #7f7f7f;
                text-decoration: none;
            }
            ul.footer_links li a:hover {
                color: #525151;
            }
            .bottom_links {
                background-color: #132943;
            }
            .bottom_links_ul {
                list-style-type: none;
            }
            ul.bottom_links_ul li {
                float: left;
            }
            ul.bottom_links_ul li a {
                color: white;
                padding: 0px 15px;
            }
            .bottom_links {
                background-color: #132943;
                padding: 15px 0px;
                margin-top: 33px;
            }
            .bottom_links_ul {
                list-style-type: none;
                margin: 10px 269px;
            }
            .col-lg-12.bottom_links h6 {
                color: white;
                font-size: 15px;
                text-align: right;
                margin-right: 57px;
            }
            .footer {
                padding: 0px 0px !important;
                background: #FFFFFF !important;
            }


            /* New css */


        </style>
        <style type="text/css">
            :root #header + #content > #left > #rlblock_left
            { display: none !important; }</style><link href="myassets/css/urlfull.css" rel="stylesheet"><style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style>
        <link rel="stylesheet" href="../../myassets/css/main.css">
        <link rel="stylesheet" href="../../myassets/css/res.css">

</head>