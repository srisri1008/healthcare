<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg" style="background-color: white;">
<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->
<style>
    #pages-bonus {
        margin-top: 104px;
    }
    .grid-div {
        display: grid;
        grid-template-columns: repeat(3, 300px);
        grid-gap: 25px;
        grid-auto-rows: minmax(100px, auto);
        grid-auto-columns: minmax(10px, auto);
        align-content: center;
        justify-content: center;
        margin-top: 70px;
        margin-bottom: 40px;
    }
    .grid-div > div {
        border: 1px solid lightgrey;
        padding: 11px;
        box-shadow: 0px 0px 7px;
    }
    .single-div > h4 {
        cursor: pointer;
        color: #1aa933;
    }
</style>
<div id="pages-bonus" class="content user">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
            </ul>
        </div>

        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container-fluid">
                    <div class="row" style="text-align: center">
                        <h1 style="margin-bottom: 40px;">Stories</h1>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <img src="assets/img/user1.png" alt="User">
                            <h3>Trek bikes - Jeremy Ryder</h3>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <img src="assets/img/user2.png" alt="User">
                            <h3>Wayfair - Bill Gerke</h3>
                        </div>
                    </div>
                    <div class="row" style=" width: 90%;margin: 50px auto;margin-top: 80px;">
                        <div class="col-xs-12 col-sm-6" style="position: relative;">
                            <img style="position: absolute;left: 44%;top:-75px;" src="myassets/img/drimg.gif">
                            <div class="" style="width: 90%;margin: 0 auto;border:1px solid #cec6c6;">
                                <h4 style="text-align: center; color:#377454; ">Proprietor</h4>
                                <h4 style="border-bottom:1px solid #cec6c6;text-align: center;color:#377454;">Ashton House 200+ Beds</h4>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <img style="padding-left: 18px" src="myassets/img/manimg1.gif">
                                    </div>
                                    <div class="col-lg-9">
                                        <p class="mc1"style="padding-right: 20px;text-align: justify;">i am amazed by the number of  experience and quality healthcare  professionals we found on temps-direct,  many time at short notice  I love the fact, i am given the  opportunity to select and interview  the temp i want before the acyually  turn up for work.  This site has definitely becomed mygo to place for sourcing nursing homes. No more let downs.</p>
                                        <div class="ptagemagrin" style="margin-top: 20px">
                                            <p>Gaj Ragunathan</p>
                                            <p>Director, Ashton Care Homes Ltd.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6"  style="position: relative; ">
                            <img style="position: absolute;left: 44%;top:-75px;" src="myassets/img/nuersdrimg.gif">
                            <div class="" style="width: 90%;margin: 0 auto;border:1px solid #cec6c6;">
                                <h4 style="text-align: center;color: #2fadc4">Dr.Chee Cheung</h4>
                                <h4 style="border-bottom:1px solid #cec6c6;text-align: center;color: #2fadc4">General Practitioner</h4>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <img style="padding-left: 18px" src="myassets/img/manimg2.gif">
                                    </div>
                                    <div class="col-lg-9">
                                        <p class="mc1"style="padding-right: 20px;text-align: justify;">This Temp-Direct platform offers  GPs, like me, the opportunity to  find extra work, as and when, I  want. There is no pressure not  stress and i do not recieve 10s of  phones calls from people begging  me to cover shifts.  Actually, i am in control of my life,  time, income and salary.</p>
                                        <div class="ptagemagrin" style="margin-top: 20px;">
                                            <p>Soho</p>
                                            <p>London</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style=" width: 90%;margin: 50px auto;">
                        <div class="col-xs-12 col-sm-6" >
                            <div class="" style="width: 90%;margin: 0 auto;border:1px solid #cec6c6;">
                                <h4 style="text-align: center;color:#377454;">Operations Manager</h4>
                                <h4 style="border-bottom:1px solid #cec6c6;text-align: center;color:#377454;">Minister Care Group 1,100+ Beds</h4>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <img style="padding-left: 18px" src="myassets/img/manimg3.gif">
                                    </div>
                                    <div class="col-lg-9">
                                        <p class="mc"style="padding-right: 20px;text-align: justify;">Temps_Direct is a fantastic  platform for sourcing healthcare  temps. It has help us cut staff  shortage by up to 85% and reduce  our spending on tempstaff in the  last 3 months  Definitely recommend it.</p>
                                        <div class="ptagemagrin"style="margin-top: 20px;">
                                            <p>Warren Canicon</p>
                                            <p>Regional Operations Manager</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6"  style="">
                            <div class="" style="width: 90%;margin: 0 auto;border:1px solid #cec6c6;">
                                <h4 style="text-align: center; color: #2fadc4">Julie Vause</h4>
                                <h4 style="border-bottom:1px solid #cec6c6;text-align: center;color: #2fadc4">Senior Practice Nurse</h4>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <img style="padding-left: 18px" src="myassets/img/manimg4.png">
                                    </div>
                                    <div class="col-lg-9">
                                        <p class="mc" style="padding-right: 20px;text-align: justify;">I recently signed up to temps-  Directand it has proven to be very  worth while so far. I really like that i  can choose when to work. I get  paid weekly and I am not hassled  when I am spending time with my  friendsor family. next, I am looking  forward to redeeming my loyalty  bonus. Bu, so far, so good.  </p>
                                        <div class="ptagemagrin" style="margin-top: 20px;">
                                            <p>RGN</p>
                                            <p>Doncaster</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>
<script>
    $(document).ready(function(){
        var mc1 = 0;
        var mc = 0;
        $('.mc1').each(function(){
            var h = $(this).height();
            if(h > mc1)
                mc1 = h;
        });
        $('.mc').each(function(){
            var h = $(this).height();
            if(h > mc)
                mc = h;
        });
        $('.mc').height(mc);
        $('.mc1').height(mc1);
    });
</script>
</body>
</html>
