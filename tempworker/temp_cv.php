<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
    Redirect::to('../index.php');
}

//Get Freelancer's Data
$query = DB::getInstance()->get("profile", "*", ["userid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
if ($query->count()) {
    foreach($query->results() as $row) {
        $nid = $row->id;
        $location = $row->location;
        $city = $row->city;
        $country = $row->country;
        $rate = $row->rate;
        $phone = $row->phone;
        $website = $row->website;
        $about = $row->about;
        $education_profile = $row->education;
        $work_profile = $row->work;
        $awards_profile = $row->awards;
    }
}
$userDocuments = DB::getInstance()->get("documentupload", "*", ["user_id" => $freelancer->data()->id, "LIMIT" => 1]);
if ($userDocuments->count()) {
    echo $userDocuments->results()[0]->id;die;
//    echo '<pre>';print_r($freelancer->data()); print_r($query->results());print_r($userDocuments->results());die;
}
//Edit Profile Data
if(isset($_POST['form_profile'])){
    if (Input::exists()) {
        if(Token::check(Input::get('token'))){

            $errorHandler = new ErrorHandler;

            $validator = new Validator($errorHandler);

            $validation = $validator->check($_POST, [
                'location' => [
                    'required' => true,
                    'maxlength' => 200,
                    'minlength' => 2
                ],
                'city' => [
                    'required' => true,
                    'maxlength' => 200,
                    'minlength' => 2
                ],
                'country' => [
                    'required' => true,
                    'maxlength' => 200,
                    'minlength' => 2
                ],
                'rate' => [
                    'required' => true,
                    'maxlength' => 200,
                    'minlength' => 2
                ],
                'website' => [
                    'required' => true,
                    'maxlength' => 200,
                    'minlength' => 2
                ]
            ]);

            if (!$validation->fails()) {

                $query = DB::getInstance()->get("profile", "*", ["userid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
                if ($query->count() === 1) {

                    //Update Profile
                    $profileUpdate = DB::getInstance()->update('profile',[
                        'location' => Input::get('location'),
                        'city' => Input::get('city'),
                        'country' => Input::get('country'),
                        'rate' => Input::get('rate'),
                        'website' => Input::get('website')
                    ],[
                        'userid' => $freelancer->data()->freelancerid
                    ]);

                    if (count($profileUpdate) > 0) {
                        $updatedError = true;
                    } else {
                        $hasError = true;
                    }

                } else {

                    try{
                        $profileid = uniqueid();
                        $profileInsert = DB::getInstance()->insert('profile', array(
                            'profileid' => $profileid,
                            'userid' => $freelancer->data()->freelancerid,
                            'location' => Input::get('location'),
                            'city' => Input::get('city'),
                            'country' => Input::get('country'),
                            'rate' => Input::get('rate'),
                            'website' => Input::get('website'),
                            'active' => 1,
                            'delete_remove' => 0,
                            'date_added' => date('Y-m-d H:i:s')
                        ));

                        if (count($profileInsert) > 0) {
                            $noError = true;
                        } else {
                            $hasError = true;
                        }


                    }catch(Exception $e){
                        die($e->getMessage());
                    }

                }


            } else {
                $error = '';
                foreach ($validation->errors()->all() as $err) {
                    $str = implode(" ",$err);
                    $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
                }
            }

        }
    }
}


?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>
<!-- Bootstrap Select CSS-->
<link  href="../assets/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $lang['overview']; ?><small><?php echo $lang['section']; ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active"><?php echo $lang['oveview']; ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <style>
                .apnasystem .info span {
                    color: #00a0ff;
                    font-size: 16px;
                    font-weight: 100;
                }
                .apnasystem .info {
                    text-align: left;
                    padding-left:20px;
                }
            </style>
            <div class="container apnasystem" style="text-align: center;">
                <div class="row" style="text-align: left;">
                    <h3 style="background-color: #00a0ff; padding-left: 20px;">Temp CV Profile</h3>
                </div>
                <div class="row">
                    <div class="col-lg-4 sidebar" style="background-color: #F2F2F2;">
                        <img src="https://healthcare-temps.co.uk/img/large_thumb/UserAvatar/7.ebbedb70e502d4110b266f1ffef782f3.jpg" alt="">
                        <h3>Andrea</h3>
                        <div class="row"><h3 style="margin: 0px 10px; background-color: #00a0ff;">Summary</h3></div>
                        <h4>Rating *****(0 Reviews)</h4>
                        <p>Average Pricing: $12.00</p>
                        <p>Flag United Kingdom</p>
                        <p style="border: solid 1px black;">
                            Member Since:Feb 09, 2016<br />
                            Last Activity: jan 23
                        </p>
                        <div class="row"><h3 style="margin: 0px 10px; background-color: #00a0ff;">Verification</h3></div>
                        <ul>
                            <li>CBR/DBS</li><span class="fa fa-check"></span>
                            <li>CBR/DBS</li><span class="fa fa-check"></span>
                            <li>CBR/DBS</li><span class="fa fa-check"></span>
                            <li>CBR/DBS</li><span class="fa fa-check"></span>
                            <li>CBR/DBS</li><span class="fa fa-check"></span>
                            <li>CBR/DBS</li><span class="fa fa-check"></span>
                            <li>CBR/DBS</li><span class="fa fa-check"></span>
                        </ul>
                    </div>
                    <div class="col-lg-8 contentsection">
                        <div class="info">
                            <div class="row"><span>Temp Name:</span> Andrea Martins</div>
                            <div class="row"><span>Speciality:</span> Andrea Martins</div>
                            <div class="row"><span>Location:</span> Andrea Martins</div>
                            <div class="row"><span>Avg. Pay Rate:</span> Andrea Martins</div>
                            <div class="row"><span>Availability:</span> Andrea Martins</div>
                            <div class="row"><span>Verified:</span> Andrea Martins</div>
                        </div>

                        <h3>Skills & Expertise</h3>
                        <p>Care Plan writing  Care Support  Challenging behaviour</p>

                        <div class="row" style="background-color: #00a0ff"><h3>About Me</h3></div>
                        <p>Hard working and dedicated care assistant with over 5 years experience in both residential and nursing care settings.</p>
                        <div class="row" style="background-color: #00a0ff"><h3>Professional Registration</h3></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>CRB</th>
                                <th>NINO</th>
                                <th>UTR</th>
                                <th>NMC PIN</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="row" style="background-color: #00a0ff"><h2>As Temp</h2></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Months</th>
                                <th>Rating</th>
                                <th>Count</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Last 3 months</td>
                                <td>*****</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>Last 6 months</td>
                                <td>*****</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>*****</td>
                                <td>0</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Project</th>
                                <th>Bid</th>
                                <th>Rater</th>
                                <th>Review Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr><td>No User Reviews available</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->


<!-- ==============================================
 Scripts
 =============================================== -->

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
<!-- Summernote WYSIWYG-->
<script src="../assets/js/summernote.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 300,                 // set editor height

            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor

            focus: false,                 // set focus to editable area after initializing summernote
        });
    });
</script>
<!-- Bootstrap Select JS-->
<script src="../assets/js/bootstrap-select.js"></script>

</body>
</html>
