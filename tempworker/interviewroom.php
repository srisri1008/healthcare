<?php 
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
  Redirect::to('../index.php');	
}


?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('template/header.php'); ?> 
    <!-- Theme style -->
    <link href="../assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet" type="text/css" />

 <body class="skin-green sidebar-mini">
     
     <!-- ==============================================
     Wrapper Section
     =============================================== -->
	 <div class="wrapper">
	 	
        <!-- Include navigation.php. Contains navigation content. -->
	 	<?php include ('template/navigation.php'); ?> 
        <!-- Include sidenav.php. Contains sidebar content. -->
	 	<?php include ('template/sidenav.php'); ?> 
	 	
	 	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Interview<small><?php echo $lang['section']; ?></small></h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
            <li class="active"><?=$lang['interviewroom']?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">	 	
		    <!-- Include currency.php. Contains header content. -->
		    <?php include ('template/currency.php'); ?>  
		 <div class="row">
             <div class="col-md-12">
                 <div class="box box-info">
                     <div class="box-header">
                         <h3 class="box-title"><?=$lang['interviewroom']?></h3>
                     </div><!-- /.box-header -->
                     <div class="box-body">
                         <iframe src="" id="myembed" frameborder="0"></iframe>
                     </div><!-- /.box-body -->
                 </div><!-- /.box -->
             </div><!-- /.col-lg-12 -->
         </div><!-- /.row -->
	   </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
         <script>
             var clientId = "demo";

             var tag = document.createElement("script");
             tag.src = "https://www.gruveo.com/embed-api/";
             var firstScriptTag = document.getElementsByTagName("script")[0];
             firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

             var embed;
             function onGruveoEmbedAPIReady() {
                 embed = new Gruveo.Embed("myembed", {
                     width: 680,
                     height: 465,
                     embedParams: {
                         clientid: clientId,
                         color: "63b2de",
                         branding: false
                     }
                 });

                 embed
                     .on("error", onEmbedError)
                     .on("requestToSignApiAuthToken", onEmbedRequestToSignApiAuthToken)
                     .on("ready", onEmbedReady)
                     .on("stateChange", onEmbedStateChange);
             }

             function onEmbedError(e) {
                 console.error("Received error " + e.error + ".");
             }

             function onEmbedRequestToSignApiAuthToken(e) {
                 console.log('OnEmbed Reuqest');
                 var tokenHmac;
                 // ...
                 // Compute the HMAC of e.token. Do it server-side only so you don't
                 // expose your API secret in the client code!
                 // ...
                 embed.authorize(tokenHmac);
             }

             function onEmbedReady(e) {
//                 document.getElementById("call-btn").addEventListener("click", function() {
//                     embed.call("mycode123", true);
//                 });
             }

             function onEmbedStateChange(e) {
                 if (e.state == "call") {
                     setTimeout(function() {
                         embed.end();
                     }, 10000);
                 }
             }
         </script>
      <!-- Include footer.php. Contains footer content. -->	
	  <?php include 'template/footer.php'; ?>	
	 	
     </div><!-- /.wrapper -->   

	
	<!-- ==============================================
	 Scripts
	 =============================================== -->
	 
    <!-- jQuery 2.1.4 -->
    <script src="../assets/js/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.6 JS -->
    <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../assets/js/app.min.js" type="text/javascript"></script>
    <!-- page script -->
    <script type="text/javascript">
      $(function () {
        $("#example1").dataTable({
        "pagingType": "full_numbers",
        "order": []
        });
      });
    </script>
    <script type="text/javascript">
	function doStar(id, userid, state) {
		// id = unique id of the message
		// type = 1 do the like, 2 do the dislike
		$('#like_btn'+id).html('<div class="privacy_loader"></div>');
		$('#doStar'+id).removeAttr('onclick');
		$.ajax({
			type: "POST",
			url: "template/requests/star.php",
			data: "id="+id+"&userid="+userid+"&state="+state, 
			cache: false,
			success: function(html) {
				$('#message-action'+id).empty();
				$('#message-action'+id).html(html);
			}
		});
	}	
	function delete_not(id, freelancerid) {
		// id = unique id of the message/comment/chat
		// type = type of post: message/comment/chat
		$('#del_comment_'+id).html('<div class="preloader-retina"></div>');
		
		$.ajax({
			type: "POST",
			url: "template/requests/delete_not.php",
			data: "id="+id+"&freelancerid="+freelancerid, 
			cache: false,
			success: function(html) {
				if(html == '1') {
				   $('#comment'+id).fadeOut(500, function() { $('#comment'+id).remove(); });
				} else {
				   $('#comment'+id).html($('#del_comment_'+id).html('Sorry, the message could not be removed, please refresh the page and try again.'));
				}
			}
		});
	}			
	</script>    

</body>
</html>
