<?php 
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
  Redirect::to('../index.php');	
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

    <!-- Include header.php. Contains header content. -->
    <?php include ('template/header.php'); ?>

<body>
<div class="container main">
    <style>
        .main {
            text-align: center;
            margin: 91px;
            padding: 20px;
        }
        .goto {
            margin: 30px 0px;
            padding: 12px;
        }
        .texts {
            margin-bottom: 20px;
        }
        .texts>h2, .texts>h5, .mainname {
            color: #22B14C !important;
        }
        .profile-div > div {
            border: 1px solid lightgrey;
            padding: 10px 50px;
            margin: 0px 47px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }
        .view-profile {
            margin: 10px 0px;
        }
        .branch {
            color: #C3CEE1 !important;
        }
        button {
            border-radius: 0px !important;
            background-color: #37A000 !important;
            color: #fff;
        }
    </style>
    <div class="texts">
        <h2>Welcome to healthcare-temps UK</h2>
        <h5>Start hiring now! Temps on demand 24/7</h5>
    </div>
    <div class="row profile-div">
        <div class="col-lg-3">
            <img src="../myassets/img/thopda.png" alt="">
            <h4 class="mainname">Ms Lulia Oprea</h4>
            <h6 class="branch">RGN/RMN</h6>
            <button class="btn view-profile">View Profile</button>
        </div>
        <div class="col-lg-3">
            <img src="../myassets/img/thopda.png" alt="">
            <h4 class="mainname">Ms Lulia Oprea</h4>
            <h6 class="branch">RGN/RMN</h6>
            <button class="btn view-profile">View Profile</button>
        </div>
        <div class="col-lg-3">
            <img src="../myassets/img/thopda.png" alt="">
            <h4 class="mainname">Ms Lulia Oprea</h4>
            <h6 class="branch">RGN/RMN</h6>
            <button class="btn view-profile">View Profile</button>
        </div>
    </div>
    <a href="/tempworker/index.php"><button class="btn goto">Go to dashboard</button></a>
</div>
</body>
</html>
