<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
    Redirect::to('../index.php');
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>

<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Terms of Use<small><?php echo $lang['section']; ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active">Terms of Use</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Include currency.php. Contains header content. -->
            <?php include ('template/currency.php'); ?>
            <div class="container">
            <div class="row">
                <div class="col-lg-11">
                <p>
                    Effective Date: February 2, 2018

                    This User Agreement (this “Agreement”) is a contract between Employer (“you” or “User”)
                    and Healthcare-Temp UK. (“Healthcare-Temps”, “we,” or “us”) and, our affiliate Shocas,
                    Caremart and Yummy-Raw.You must read, agree to, and accept all of the terms and
                    conditions contained in this Agreement in order to use our website located
                    atwww.healthcare-temps.co.uk and our mobile applications, owned and operated by us, our
                    predecessors or successors in interest, or our Affiliates (collectively, the “Site”), all services
                    (except the Healthcare Temps Supply Services), applications and products that are
                    accessible through the Site.

                    This Agreement includes and hereby incorporates by reference the following: Healthcare-
                    Temps Fee Agreement; Terms of Use; Cookie Policy; Privacy Policy; Escrow Policy; Payroll
                    Agreement; Proprietary Rights Infringement Reporting Procedures; “Software License
                    Agreement; API Terms of Use; Widget Terms of Use; Fixed Hourly Payment, Enhanced
                    Bonus Agreement, as such agreements may be in effect and modified by Healthcare-Temps
                    from time to time (collectively, with this Agreement, the “Terms of Service”).

                    Subject to the conditions set forth herein, Healthcare-Temps UK may, in its sole discretion,
                    amend this Agreement and the Terms of Use Policy at any time by posting a revised version
                    on the Site and will provide reasonable advance notice of any amendment that includes a
                    Substantial Change. If the Substantial Change includes an increase to Fees charged by
                    Healthcare-Temps, Healthcare-Temps Inc will provide at least 30 days’ advance notice of the
                    change, but may not provide any advance notice for changes resulting in a reduction in Fees
                    or any temporary or promotional Fee change. Any revisions to the Terms of Service will take
                    effect on the noted effective date or when posted if there is no noted effective date (each, as
                    applicable, the “Effective Date”).

                    Your continued use of the Site or the Site Services after the Effective Date of a revised
                    version of this Agreement or of any other Terms of Service constitutes your acceptance of
                    and agreement to be bound by the Terms of Service as revised. In the event of a conflict

                    between this Agreement and the other Terms of Service, this Agreement will control unless
                    the other Agreement explicitly states that it controls. Capitalized terms are defined throughout
                    this Agreement (Definitions).

                    YOU UNDERSTAND THAT BY USING THE SITE OR SITE SERVICES AFTER THE
                    EFFECTIVE DATE, YOU AGREE TO BE BOUND BY THE TERMS OF SERVICE,
                    INCLUDING THE MANDATORY BINDING ARBITRATION AND CLASS ACTION/JURY
                    TRIAL WAIVER PROVISION IN SECTION 21.4 OF THIS AGREEMENT. IF YOU DO NOT
                    ACCEPT THE TERMS OF SERVICE IN ITS ENTIRETY, YOU MUST NOT ACCESS OR USE
                    THE SITE OR THE SITE SERVICES AFTER THE EFFECTIVE DATE. IF YOU AGREE TO
                    THE TERMS OF SERVICE ON BEHALF OF AN ENTITY, OR IN CONNECTION WITH
                    PROVIDING OR RECEIVING SERVICES ON BEHALF OF AN ENTITY OR AGENCY, YOU
                    REPRESENT AND WARRANT THAT YOU HAVE THE AUTHORITY TO BIND THAT
                    ENTITY OR AGENCY TO THE TERMS OF SERVICE. IN THAT EVENT, “YOU” AND
                    “YOUR” WILL REFER AND APPLY TO THAT ENTITY OR AGENCY.
                </p>

                <h3>1. DIGITAL SIGNATURE</h3>
                <p>
                    By registering for an Upwork account on the Site (an “Account”), or by clicking to accept the
                    Terms of Service when prompted on the Site, you are deemed to have executed this
                    Agreement and the other Terms of Service electronically, effective on the date you register
                    your Account or click to accept the Terms of Service, pursuant to the U.S. Electronic
                    Signatures in Global and National Commerce Act (the E-Sign Act) (15 U.S.C. § 7001, et
                    seq.). Your Account registration constitutes an acknowledgement that you are able to
                    electronically receive, download, and print this Agreement, the other Terms of Service, and
                    any amendments.
                </p>

                <h3>2. CONSENT TO USE ELECTRONIC RECORDS</h3>

                <p>
                    In connection with the Terms of Use, you may be entitled to receive certain records from
                    Healthcare-Temps or our Affiliates, such as contracts, notices, and communications, in
                    writing. To facilitate your use of the Site and the Site Services, you give us permission to
                    provide these records to you electronically instead of in paper form.
                </p>

                <h3>2.1 YOUR CONSENT AND YOUR RIGHT TO WITHDRAW CONSENT</h3>

                <p>
                    By registering for an Account, you consent to electronically receive and access, via email or
                    the Site, all records and notices for the services provided to you under the Terms of Service
                    that we or our Affiliates would otherwise be required to provide to you in paper form.<br />

                    However, we reserve the right, in our sole discretion, to communicate with you via the U.K.
                    Postal Service and other third-party mail services using the address under which your
                    account is registered. Your consent to receive records and notices electronically will remain
                    in effect until you withdraw it. You may withdraw your consent to receive further records and
                    notices electronically at any time by contacting Customer Support. If you withdraw your
                    consent to receive such records and notices electronically, we will revoke your access to the
                    Site and the Site Services, and you will no longer be permitted to use the Site or the Site
                    Services. Any withdrawal of your consent to receive records and notices electronically will be
                    effective only after we have a reasonable period of time to process your request for
                    withdrawal. Please note that your withdrawal of consent to receive records and notices
                    electronically will not apply to records and notices electronically provided by us to you before
                    the withdrawal of your consent becomes effective.
                </p>

                <h3>2.2KEEPING YOUR ADDRESS AND EMAIL ADDRESS CURRENT WITH US</h3>
                <p>
                    In order to ensure that we are able to provide records and notices to you electronically, you
                    agree to notify us immediately of any change in your email address by updating your Account
                    information on the Site or by contacting Customer Support
                </p>

                <h3>2.3HARDWARE AND SOFTWARE YOU WILL NEED TO USE THE SITE SERVICES FOR
                    YOUR BUSINESS</h3>

                <p>
                    To access and retain the records and notices we provide to you electronically, you will need:
                    (a) a valid email address; (b) a computer system that operates on a platform like Windows or
                    Mac; (c) a connection to the Internet; (d) Current Versions of the software, browsers, plug-ins,
                    or other computer applications and programs identified on the Site (Users utilizing other
                    browsers may experience compatibility difficulties); (e) a Current Version of a program that
                    accurately reads and displays PDF files, such as the Current Version of Adobe Acrobat
                    Reader; (f) a computer or device and an operating system capable of supporting all of the
                    above; and (g) a printer to print out and retain records and notices in paper form or electronic
                    storage to retain records and notices in an electronic form
                </p>

                <h3>3. HEALTHCARE-TEMPS USER ACCOUNTS:</h3>
                <h3>3.1 ACCOUNT ELIGIBILITY</h3>

                <p>
                    To use the Site and certain Site Services, you must register for an Account.Healthcare-
                    Temps UK offers the Site and Site Services for your business purposes only, and not for
                    personal, household, or consumer use. To use the Site and Site Services, you must have,
                    and hereby represent that you have, an independent business (whether it be as a self-
                    employed individual/sole proprietor or as a corporation or other entity) and further represent
                    that you intend to use the Site and Site Services for your business purposes only. You
                    understand that you must comply with any licensing or professional registration requirements
                    with respect to your business, and you represent that you comply with all such requirements.
                    To register for an Account, you must be, and hereby represent that you are, a legal entity or
                    an individual 17 years or older who can form legally binding contracts.You must have a
                    clean, valid CRB (dated within nine months of registration), rights to work in the U.K., relevant
                    flu jab and minimum professional qualification. By registering for an Account, by using the Site
                    or Site Services after the Effective Date if you had an account on the Effective Date, or by
                    clicking to accept the Terms of Service when prompted on the Site, you agree to: (a) abide by
                    this Agreement and the other Terms of Service; (b) be financially responsible for your use of
                    the Site and the purchase or delivery of Temp worker Services; and (c) perform your
                    obligations as specified by any Service Contract that you enter into, unless such obligations
                    are prohibited by applicable law or the Terms of Service. Healthcare-Temps reserves the
                    right, in our sole discretion, to refuse, suspend, or revoke your access to the Site and Site
                    Services upon discovery that any information you provided on any form or posted on the Site
                    is not true, accurate, or complete, or such information or other conduct otherwise violates the
                    Terms of Service, or for any other reason or no reason in Healthcare-Temps’s sole discretion.<br />

                    You represent that you are not: (x) a citizen or resident of a geographic area in which access
                    to or use of the Site or Site Services is prohibited by applicable law, decree, regulation, treaty,
                    or administrative act; (y) a citizen or resident of, or located in, a geographic area that is
                    subject to U.K. or other sovereign country sanctions or embargoes; or (z) an individual, or an
                    individual
                </p>

                <h3>3.2 ACCOUNT REGISTRATION; PROFILE</h3>

                <p>
                    By registering for an account, you must complete a User profile (“Profile”), which you consent
                    to be shown to other Users and, unless you change your privacy settings, the public. If you
                    are a Temp Worker, you will use the Site solely as a self-employed worker. Use your Profile
                    to market your skills to Hirers solely for the purpose of entering into an independent
                    contractual relationship with Hirers/Employers. You agree to provide true, accurate, and
                    complete information on your Profile and all registration and other forms you access on the
                    Site or provide to us and to update your information to maintain its truthfulness, accuracy, and
                    completeness. You agree not to provide and to correct any information about your location,
                    your business, your skills, your professional registration, qualification or the services your
                    business provides that is or becomes false or misleading. You agree not to register for more
                    than one Hirer Account or one Temp worker Account without express written permission from
                    us. You agree not to ask or allow another person to create an Account on your behalf, for
                    your use, or for your benefit.
                </p>

                <h3>3.3 IDENTITY VERIFICATION</h3>
                <p>
                    When you register for an Account and from time to time thereafter, your Account will be
                    subject to verification, including, but not limited to, validation against third-party databases or
                    the verification of one or more official government or legal documents that confirm your
                    identity and your ability to work in the U.K. and within the relevant profession. You authorise
                    Healthcare-Temps directly or through third parties, to make any enquiries necessary to
                    validate your identity and confirm your ownership of your email address or financial accounts,
                    subject to applicable law. When requested, you must provide us with information about you
                    and your business.
                </p>

                <h3>3.4 AGENCY ACCOUNTS</h3>

                <p>
                    Agencies are not allowed to use Healthcare-Temps web application and attempting to
                    become a member violate our Terms of Use and Terms of Service
                </p>

                <h3>3.5 USERNAMES AND PASSWORDS</h3>

                <p>
                    When you register for an Account, you will be asked to choose a username and password for
                    the Account. Each User and any Agency Account Administrator will also be asked to choose
                    the initial username and password for any Agency Account that is added to the Account (and
                    can change the password for the Agency Account at any time).<br />

                    You are entirely responsible for safeguarding and maintaining the confidentiality of your
                    Account or Agency Account username and password. You authorise Healthcare-Temps to
                    assume that any person using the Site with your username and password, either is you or is
                    authorised to act for you.
                </p>

                <h3>3.6MARKETPLACE FEEDBACK</h3>

                <p>
                    You acknowledge and agree that feedback benefits the marketplace, all Users, and the
                    efficiency of the Site and you specifically request that Healthcare-Temps post composite or
                    compiled feedback about Users, including yourself, on User Profiles and elsewhere on the
                    Site. You acknowledge and agree that feedback results for you, including your Job Success
                    Score (“JSS”), if any, will consist of comments, ratings, indicators of User satisfaction, and
                    other feedback left by other Users. You further acknowledge and agree that Healthcare-
                    Temps will make feedback results available to other marketplace Users, including composite
                    or compiled feedback
                </p>

                <h3>4.PURPOSE OF THE SITE AND SITE SERVICES</h3>

                <p>
                    The Site is a marketplace where Hirers and Temp Workers can identify each other and
                    advertise, buy, and sell Healthcare skills online. Subject to the Terms of Use Policy,
                    Healthcare-Temps provides the Site Services to Users, including hosting and maintaining the
                    Site, enabling the formation of Service Contracts, and coordinating disputes related to those
                    Service Contracts. If Users agree on terms for Temp worker Services, a Service Contract is
                    formed directly between such Users, subject to the provisions set forth in Section 5
                    (Contractual Relationship Between Hirer and Temp Worker), unless other wised express.
                </p>

                <h3>4.1 ESCROW ACCOUNTS</h3>

                <p>
                    EEC provides escrow services to Users to deliver, hold, or receive payment for an
                    Engagement, and to pay service, membership and payment processing and administration
                    fees to Healthcare Temps (“Escrow Services”). EEC is a Delaware corporation and a
                    licensed Internet escrow agent and holds California Department of Business Oversight
                    License No. 963 5086. The Escrow Services are intended for business use, so you agree to
                    use the Escrow Services only for business purposes and not for consumer, personal, family,
                    or household purposes.<br />

                    (a)Hirers’ Escrow Account. After entering into a Service Contract, the first time a Client
                    makes a payment for an Engagement, EEC will establish and maintain a “Client Escrow
                    Account” to hold funds for theClient to use to make payments for Engagements, to receive
                    refunds in connection with Engagements, and to make payments to Upwork for payment
                    processing and administration fees.<br />

                    (b)Temp workers’ Escrow Account. After entering into a Service Contract, the first time a
                    Freelancer uses the Site to receive payment for an Engagement, EEC will establish and
                    maintain a “Freelancer Escrow Account” for the Freelancer to receive payments for
                    Engagements, withdraw payments, make monthly membership payments and service fees to
                    Healthcare Temps, and issue refunds to Hirers.
                </p>

                <h3>5. CONTRACTUAL RELATIONSHIP BETWEEN CLIENT AND FREELANCER</h3>
                <h3>5.1 SERVICE CONTRACTS</h3>
                <p>
                    You acknowledge and agree that a Service Contract is comprised of the following agreements
                    (as applicable): (a) The Fixed-Price Escrow Instructions; (b) the Hourly, Bonus and Expense
                    Payment Agreement with Escrow Instructions; (c) the Engagement terms awarded and
                    accepted on the Site to the extent that the terms do not, and do not purport to, expand
                    Healthcare-Temps’ obligations or restrict Healthcare-Temps’ rights under the Terms of Use;
                </p>

                <h3>5.2 PAYMENT AGREEMENTS AND ESCROW INSTRUCTIONS</h3>

                <p>
                    Fixed-Price Engagements. If Users choose fixed-price compensation, then the Users agree
                    that they will be bound by, and EEC will follow, the Fixed-Price Escrow Instructions.<br />

                    Hourly Engagements, Bonus Payments, or Expense Payments. If Users choose hourly
                    compensation, and/or if the Hirer makes bonus or expense payments, then the Users agree
                    that they will be bound by, and EEC will follow, the Hourly, Bonus and Expense Payment
                    Agreement with Escrow Instructions.
                </p>

                <h3>6.PAYMENT TERMS</h3>
                <h3>6.1SERVICE FEE; PAYMENT PROCESSING FEES</h3>

                <p>
                    When a Hirer pays a Temp worker, or when funds related to an Engagement are otherwise
                    released to a Temp worker as required by the applicable Escrow Instructions, EEC will credit
                    the Temp Worker Escrow Account for the full amount paid or released, and then subtract and
                    disburse to Healthcare-Temps a service fee in the amount specified in theHealthcare-
                    Temps (the “Service Fee”).Temp worker agrees to pay Healthcare-Temps the Service Fee
                    for using the Site’s communication, invoicing, dispute resolution and payment services,
                    including Payment Protection
                </p>

                <h3>6.2 MEMBERSHIP FEES</h3>

                <p>
                    Temp workers will subscribe to different levels of membership and privileges on the Site.
                </p>

                <h3>6.3NO FEE FOR INTRODUCING OR FOR FINDING ENGAGEMENTS</h3>

                <p>
                    Healthcare-Temps does not introduce Hirers to Temp Workers and does not help Temp
                    Workers secure Engagements. Healthcare Temps UK merely makes the Site Services
                    available to enable Temp workers to do so themselves.
                </p>

                <h3>6.4 NON-PAYMENT</h3>

                <p>
                    If Hirer fails to pay the Temp Worker Fees or any other amounts due under the Terms of
                    Service, whether by canceling Hirer’s credit or debit card, initiating an improper chargeback,
                    or any other means, Healthcare-Temps Inc may suspend or close Hirer’s Account and revoke
                    Hirer’s access to the Site, including Client’s authority to use the Site to process any additional
                    payments, enter into Service Contracts, or obtain any additional Temp worker Services.
                    Without limiting other available remedies, Hirer must pay Healthcare-Temps upon demand
                    for amounts owed under the Terms of Service, plus interest on the outstanding amount at the
                    lesser of one and one-half percent (1.5%) per month or the maximum interest allowed by
                    applicable law, plus attorneys’ fees and other costs of collection to the extent permitted by
                    applicable law. To the extent permitted by applicable law, Healthcare-Temps Inc or EEC, at
                    our discretion, may set off amounts due against other amounts received from or held by
                    Healthcare-Temps or EEC for Hirer, make appropriate reports to credit reporting agencies
                    and law enforcement authorities, and cooperate with credit reporting agencies and law
                    enforcement authorities in any resulting investigation or prosecution.
                </p>

                <h3>6.5 NO RETURN OF FUNDS</h3>

                <p>
                    Client acknowledges and agrees that EEC will charge Client’s designated Payment Method
                    for the Freelancer Fees: (a) for Fixed-Price Contracts, upon Client’s acceptance and approval
                    of the Freelancer Services, and (b) for Hourly Contracts, on the Monday after the week in
                    which work was performed. Therefore, and in consideration of the Site Services provided by
                    Upwork and the Escrow Services provided by EEC, Client agrees that once EEC charges the
                    Client’s designated Payment Method for the Freelancer Fees as provided in this Agreement
                    or the other Terms of Service, the charge is non-refundable, except as otherwise required by
                    applicable law. Client also acknowledges and agrees that the Terms of Service provide a
                    dispute
                </p>

                <h3>6.6 PAYMENT METHODS</h3>

                <p>
                    In order to use certain Site Services, Client must provide account information for at least one
                    valid Payment Method.<br />

                    Client hereby authorizes Upwork, EEC, and Elance Limited to run credit card authorizations
                    on all credit cards provided by Client, to store credit card and banking or other financial details
                    as Client’s method of payment for Services, and to charge Client’s credit card (or any other
                    Payment Method). Credit cards and PayPal accounts and, if applicable, bank accounts in
                    most countries will be charged by EEC. Notwithstanding the foregoing, credit cards and
                    PayPal accounts in Australia, Canada, the Eurozone, and the United Kingdom will be charged
                    by Elance Limited, an Ireland registered company which is an Affiliate of Upwork.<br />

                    When Client authorizes the payment of the Freelancer Fees for a Fixed-Price Contract on the
                    Site, Client automatically and irrevocably authorizes and instructs EEC or Elance Limited to
                    charge Client’s Payment Method for the Freelancer Fees. When Client approves or is
                    deemed to have approved a Time Log for an Hourly Contract, Client automatically and
                    irrevocably authorizes and instructs EEC or Elance Limited to charge Client’s Payment
                    Method for the Freelancer Fees.<br />

                    By providing Payment Method information through the Site, Client represents, warrants, and
                    covenants that: (a) Client is legally authorized to provide such information; (b) Client is legally
                    authorized to perform payments using the Payment Method(s); and (c) such action does not
                    violate the terms and conditions applicable to Client’s use of such Payment Method(s) or
                    applicable law. When Client authorizes a payment using a Payment Method via the Site,
                    Client represents, warrants, and covenants that there are sufficient funds or credit available to
                    complete the payment using the designated Payment Method. To the extent that any
                    amounts owed under this Agreement or the other Terms of Service cannot be collected from
                    Client’s Payment Method(s), Client is solely responsible for paying such amounts by other
                    means.
                </p>

                <h3>6.9 US DOLLARS AND FOREIGN CURRENCY CONVERSION</h3>

                <p>
                    The Site and the Site Services operate in U.S. Dollars. If Client’s Payment Method is
                    denominated in a currency other than U.S. Dollars and requires currency conversion to make
                    payments in U.S. Dollars, the Site may display foreign currency conversion rates that Upwork,
                    EEC or other Affiliates currently make available to convert supported foreign currencies to
                    U.S. Dollars. These foreign currency conversion rates adjust regularly based on market
                    conditions. Please note that the wholesale currency conversion rates at which we or our
                    Affiliates obtain foreign currency will usually be different than the foreign currency conversion
                    rates offered on the Site. Client, at its sole discretion and risk, may authorize the charge of its
                    Payment Method in a supported foreign currency and the conversion of the payment to U.S.
                    Dollars at the foreign currency conversion rate displayed on the Site. A list of supported
                    foreign currencies is available on the Site. If foreign currency conversion is required to make
                    a payment in U.S. Dollars and either Upwork, EEC, or another Affiliate does not support the
                    foreign currency or Client does not authorize the conversion of such payment at the foreign
                    currency conversion rate displayed on the Site, EEC or an Affiliate will charge Client’s
                    Payment Method in U.S. Dollars and Client’s Payment Method provider will convert the
                    payment at a foreign currency conversion rate selected by Client’s Payment Method provider.
                    Client’s Payment Method provider may also charge fees directly to the Payment Method
                    used to fund a cross-border payment even when no currency conversion is involved. Client’s
                    authorization of a payment using a foreign currency conversion rate displayed on the Site is at
                    Client’s sole risk. Upwork, EEC, and other Affiliates are not responsible for currency
                    fluctuations that occur when billing or crediting a Payment Method denominated in a currency
                    other than U.S. Dollars. Upwork, EEC, and other Affiliates are not responsible for currency
                    fluctuations that occur when receiving or sending payments via wire transfer, check or
                    automated clearinghouse to and from the Escrow Account.
                </p>

                <h3>6.10 LIMITED PAYMENT PROTECTION</h3>

                <p>
                    In the rare event that a Client does not make payment for legitimate services performed by a
                    Freelancer, Upwork will provide limited payment protection to the Freelancer as detailed in
                    this Section 6.10 (“Payment Protection”) as a membership benefit to foster fairness, reward
                    loyalty, and encourage the Freelancer to continue to use the Site and Site Services for their
                    business needs.<br />

                    Upwork only provides Payment Protection to Freelancers working on Hourly Contracts and
                    only if all of requirements of the Hourly, Bonus, and Expense Payment Agreement with
                    Escrow Instructions and the following criteria are met: (a) Client has a verified Payment
                    Method; (b) the time represented is captured online using the Upwork Team Software; (c) the
                    Freelancer Services performed and recorded in the Time Log pertain directly to the Service
                    Contract; and (d) each Time Log is annotated with descriptions of the Freelancer Services
                    performed, demonstrating Service Contract compliance. Upwork determines whether the
                    foregoing criteria have been met in our sole discretion. Without limiting the foregoing,
                    Payment Protection does not apply to: (w) Freelancers, Freelancer Services, or Service
                    Contracts violating this Agreement or the other Terms of Service; (x) Freelancers that are
                    aware of or complicit in another User’s violation of this Agreement or the other Terms of
                    Service; (y) Freelancers enrolled in Upwork Payroll; or (z) Freelancers that are suspected (in
                    Upwork’s sole discretion) of actual fraudulent activities or abuse of this Payment Protection.<br />

                    Freelancer hereby irrevocably assigns to Upwork the right to recover from the Client any
                    amounts that Upwork or our Affiliates provide to the Freelancer in connection with the
                    Payment Protection membership benefit.
                </p>

                <h3>7. NON-CIRCUMVENTION</h3>

                <p>
                    You acknowledge and agree that a substantial portion of the compensation Upwork receives
                    for making the Site available to you is collected through the Service Fee described in Section
                    6.1 (“Service Fee”). Upwork only receives this Service Fee when a Client and a Freelancer
                    pay and receive payment through the Site. Therefore, for 24 months from the time you
                    identify or are identified by any party through the Site (the “Non-Circumvention Period”), you
                    must use the Site as your exclusive method to request, make, and receive all payments for
                    work directly or indirectly with that party or arising out of your relationship with that party (the
                    “Upwork Relationship”). You may opt-out of this obligation with respect to each Client-
                    Freelancer relationship only if Client or prospective Client or Freelancer pays Upwork for each
                    such relationship:

                    (a) an “Opt-Out Fee” computed to be the greater of the following amounts:

                    (i)$2,500; or

                    (ii)15% of the cost to the Client of the services to be performed in the Upwork Relationship
                    during the Non-Circumvention Period, as estimated in good faith by the prospective Client; or

                    (iii)all Service Fees that would be earned by Upwork from the Upwork Relationship during the
                    Non-Circumvention Period, computed based on the annualized amount earned by Freelancer
                    from Client during the most recent normalized 8-week period, or during such shorter period as
                    data is available to Upwork; and

                    (b) interest at the rate of 18% per annum or the maximum rate permitted by applicable law,
                    whichever is less, calculated from the date Client first makes payment to the subject
                    Freelancer until the date the Opt-Out Fee is paid.

                    To pay the Opt-Out Fee, you must request instructions by sending an email message to
                    optoutfee@upwork.com.

                    Except if you pay the Opt-Out Fee, you agree not to circumvent the Payment Methods offered
                    by the Site. By way of illustration and not in limitation of the foregoing, you must not:

                    <ul>
                        <li>
                            Submit proposals or solicit parties identified through the Site to contact, hire, work
                            with, or pay outside the Site.
                        </li>
                        <li>
                            Accept proposals or solicit parties identified through the Site to contact, deliver
                            services, invoice, or receive payment outside the Site.
                        </li>
                        <li>
                            Invoice or report on the Site or in an Opt-Out Fee request an invoice or payment
                            amount lower than that actually agreed between Users.
                        </li>
                    </ul>
                    You agree to notify Upwork immediately if another person improperly contacts you or
                    suggests making or receiving payments outside of the Site. If you are aware of a breach or
                    potential breach of this non-circumvention agreement, please submit a confidential report to
                    Upwork by sending an email message to:policy-reports@upwork.com.
                    If you refuse to accept any new version of the Terms of Service or elect not to comply with
                    certain conditions of using the Site, such as minimum rates supported on the Site, and
                    therefore choose to cease using the Site, you may pay the Opt-Out Fee for each other User
                    you wish to continue working with on whatever terms you agree after you cease using the
                    Site.
                </p>

                <h3>8.SERVICE CONTRACT TERMS</h3>

                <p>
                    Unless otherwise expressly agreed to in writing by both Users, and except for Freelancer
                    Services using Upwork Payroll, the default terms and conditions of the Service Contract that a
                    Freelancer enters directly with a Client when the Freelancer agrees to provide Freelancer
                    Services to the Client are as set forth in this Section 8. If Upwork Payroll is used for the
                    Engagement, then only Sections 8.7 and 8.8 of this Section 8 apply. Users may agree
                    between them on any additional or different terms for their Service Contract as long as such
                    terms do not and do not purport to affect the rights or responsibilities of Upwork or violate the
                    Terms of Service. Upwork is not a party to any Service Contract by or between Users.<br />

                    Users agree that the terms concerning the Service Contract described on the Site, including
                    Freelancer Fees, rates, hours, and milestones, form part of the Service Contract. Users
                    agree to obtain the consent of the other before making changes to the Service Contract by
                    adding additional or different milestones or making other changes to the Service Contract on
                    the Site. If consent of the other party is not first obtained, the other party may reject such
                    changes by terminating the Service Contract (see Section 8.5) or accept such changes by
                    continuing to work on the Service Contract.
                </p>

                <h3>8.1 SERVICES</h3>

                <p>
                    Freelancer will perform the Freelancer Services in a professional and workmanlike manner
                    and will timely deliver any agreed upon Work Product. The manner and means of performing
                    the Freelancer Services will be determined and controlled solely by Freelancer, which is
                    engaged by Client as an independent contractor.
                </p>

                <h3>8.2 RESPONSIBILITY FOR EMPLOYEES AND SUBCONTRACTORS, INCLUDING
                    AGENCY MEMBERS</h3>

                <p>
                    To ensure accurate billing, work billed for Hourly Contracts under a Freelancer’s Account
                    must be performed by the Freelancer that has the Account. With an Agency Account, the
                    Agency may bill for hourly work done by Agency Members.<br />

                    If a User subcontracts with or employs third parties to perform Freelancer Services on behalf
                    of the User for any Engagement, the User represents and warrants that it does so as a legally
                    recognized entity or person and in compliance with all applicable laws and regulations. As
                    used in this Agreement, the term “Delegee” refers to any employee, independent contractor,
                    or agent of a User, including an Agency Member, that the User engages to perform any work
                    on its behalf under a Service Contract. Regardless of whether a User has Delegees, the User
                    remains responsible for all services performed under the User’s Service Contracts, including
                    ensuring that the services comply with the Terms of Service (including confidentiality and
                    intellectual property obligations). Each Agency specifically acknowledges and agrees that
                    Agency Members have the authority to bind the Agency to Service Contracts entered into by
                    Agency Members on behalf of the Agency.<br />

                    Freelancer, Agency, Delegee, and Client acknowledge and agree that Delegees are not
                    employees, independent contractors or agents of Upwork or Client. Agency, Delegee, and
                    Freelancer represent, warrant, and covenant that: (a) Agency and, if applicable, each other
                    User is solely responsible for all payments, obligations, wages, costs, unemployment
                    insurance, workers’ compensation insurance, contributions, and expenses of Delegees; (b)
                    neither Upwork nor Client has the right or power to supervise or control Delegees; and (c) no
                    Delegees of any Agency or other User will have any claim under this Agreement or the other
                    Terms of Service for overtime pay, sick leave, holiday or vacation pay, retirement benefits,
                    worker’s compensation benefits, unemployment benefits, contributions, or any other
                    employee benefits of any kind from Upwork or Client.<br />

                    With respect to Delegees, Upwork merely provides the platform for Agency or Freelancer to
                    communicate and share information with Clients and, if they are Users, with Delegees.
                    Agency, Delegee, and Freelancer understand and agree that the contract terms, pay rate,
                    work hours, service dates and working conditions will be established by the Agency,
                    Freelancer, and/or Client and not by Upwork. Agency, Delegee, and Freelancer acknowledge
                    and agree that Delegees are not employees or independent contractors of Upwork, and
                    further acknowledge and agree that they will not be providing any services to Upwork (directly
                    or indirectly) while employed or engaged by the Agency or another User.<br />

                    Agency, Delegee, and Freelancer acknowledge and agree that Upwork does not, in any way,
                    supervise, direct, or control Delegees; Upwork does not set Delegees’ contract terms
                    (including determining whether the contract will be set at an hourly or fixed fee), fees, rates,
                    work hours, work schedules, or location of work; Upwork will not provide Delegees with
                    training or any equipment, labor, or materials needed for a particular Service Contract; and
                    Upwork does not provide the premises at which the Delegees will perform the work.
                </p>

                <h3>8.3 CLIENT PAYMENTS AND BILLING</h3>

                <p>
                    For Hourly Contracts, Client is billed for Freelancer Fees on a weekly basis. For Fixed-Price
                    Contracts, Client is billed immediately upon sending a Fixed-Price Contract offer or upon
                    activating any additional milestone.
                </p>

                <h3>8.4 DISPUTE RESOLUTION</h3>

                <p>
                    With respect to disputes arising between Clients and Freelancers, you agree to abide by the
                    dispute resolution provisions set forth in the Escrow Instructions that apply to your particular
                    Service Contract.
                </p>

                <h3>8.5 TERMINATION OF A SERVICE CONTRACT</h3>

                <p>
                    Under Hourly Contracts, either Client or Freelancer has the right to terminate the Service
                    Contract after providing any required notice, or immediately on the end date specified in the
                    Service Contract terms and/or upon completion of the Freelancer Services, in the event of a
                    material breach, or with the consent of the other party. Except as required by law, Client
                    remains obligated to pay the Freelancer Fees for any Freelancer Services provided prior to
                    termination of the Hourly Contract.<br />

                    Under Fixed-Price Contracts, once a Client’s Payment Method has been charged to fund the
                    escrow account for the Engagement, absent a full refund to Client by Freelancer, the Service
                    Contract does not terminate until the Freelancer Services are completed. However, either
                    Client or Freelancer has the right to terminate a Fixed-Price Contract at any time with the
                    consent of the other party or in the event of a material breach. If a Fixed-Price Contract is
                    terminated, Client does not have the right to recover any payments already released to
                    Freelancer from the escrow account for the Engagement.
                </p>

                <h3>8.6 INTELLECTUAL PROPERTY RIGHTS</h3>
                <h3>CERTAIN DEFINED TERMS</h3>

                <p>
                    The following capitalized terms have the following meanings:

                    “Background Technology” means all Inventions developed by Freelancer other than in the
                    course of providing Freelancer Services to Client under the Service Contract and all
                    Inventions that Freelancer incorporates into Work Product.<br />

                    “Client Materials”means requests, intellectual property, and any other information or
                    materials that Client provides to Freelancer for Freelancer to perform Freelancer Services.<br />

                    “Invention” means any ideas, concepts, information, materials, processes, data, programs,
                    know-how, improvements, discoveries, developments, designs, artwork, formulae, other
                    copyrightable works, and techniques and all Intellectual Property Rights therein.
                </p>

                <h3>BACKGROUND TECHNOLOGY</h3>

                <p>
                    Freelancer will disclose in the Engagement Terms any Background Technology which
                    Freelancer proposes to incorporate into Work Product or upon which use or distribution of the
                    Work Product will depend. If Freelancer discloses no Background Technology, Freelancer
                    warrants that it will not incorporate any Background Technology into Work Product provided
                    pursuant thereto. Freelancer will separately provide, with each delivery of Work Product to
                    Client, a third-party bill of materials that identifies all Background Technology and other third-
                    party materials that have been incorporated into the Work Product and provides, for each item
                    of Background Technology identified, (a) the name and any associated version number, (b)
                    the applicable license or licensing terms, (c) whether the item has been modified by
                    Freelancer, and (d) how the item has been incorporated into, is used by, or is relied upon by
                    the Work Product. Notwithstanding the foregoing, unless otherwise agreed in the
                    Engagement Terms, Freelancer agrees that it will not incorporate into Work Product or
                    otherwise deliver to Client any software code for which the use or distribution of the code will
                    create (or purport to create) obligations for Client to grant any rights or immunities under
                    Client intellectual property to a third-party, including without limitation any obligation that the
                    Work Product or Client software combined with, derived from, or distributed with such Work
                    Product (x) be disclosed or distributed in source code form, (y) be licensed for the purpose of
                    making derivative works, or (z) be redistributable at no charge.
                </p>

                <h3>LICENSE TO BACKGROUND TECHNOLOGY</h3>

                <p>
                    Upon Freelancer’s receipt of full payment from Client for delivery of Work Product, Freelancer
                    hereby automatically grants to Client a non-exclusive, perpetual, fully-paid and royalty-free,
                    irrevocable and worldwide right, with rights to sublicense through multiple levels of
                    sublicensees, to reproduce, make derivative works of, distribute, publicly perform, and
                    publicly display in any form or medium, whether now known or later developed, make, have
                    made, use, sell, import, offer for sale, and exercise any and all present or future rights in the
                    Background Technology incorporated or used in Work Product delivered for that payment. If
                    payment is made only for partial delivery of Work Product, the grant described herein applies
                    only to the portion of Work Product delivered.
                </p>

                <h3>CLIENT MATERIALS</h3>

                <p>
                    Client grants Freelancer a limited, non-exclusive, revocable (at any time, at Client’s sole
                    discretion) right to use the Client Materials as necessary solely for the performance of the
                    Freelancer Services under the applicable Service Contract. Client reserves all other rights
                    and interest, including, without limitation, all Intellectual Property Rights, in and to the Client
                    Materials. Upon completion or termination of the Service Contract, or upon Client’s written
                    request, Freelancer will immediately return all Client Materials to Client and further agrees to
                    destroy all copies of Client Materials and Deliverables (except for Background Technology as
                    permitted by the Service Contract) contained in or on Freelancer’s premises, systems, or any
                    other equipment or location otherwise under Freelancer’s control. Within ten days of such
                    request from Client, Freelancer agrees to provide written certification to Client that Freelancer
                    has returned or destroyed all Client Materials and Work Product as provided in this
                    subsection.
                </p>

                <h3>OWNERSHIP OF WORK PRODUCT AND INTELLECTUAL PROPERTY</h3>

                <p>
                    Upon Freelancer’s receipt of full payment from Client, the Work Product, including without
                    limitation all Intellectual Property Rights in the Work Product, will be the sole and exclusive
                    property of Client, and Client will be deemed to be the author thereof. If Freelancer has any
                    Intellectual Property Rights to the Work Product that are not owned by Client upon
                    Freelancer’s receipt of payment from Client, Freelancer hereby automatically irrevocably
                    assigns to Client all right, title and interest worldwide in and to such Intellectual Property
                    Rights. Except as set forth above, Freelancer retains no rights to use, and will not challenge
                    the validity of Client’s ownership in, such Intellectual Property Rights. Freelancer hereby
                    waives any moral rights, rights of paternity, integrity, disclosure and withdrawal or inalienable
                    rights under applicable law in and to the Work Product. If payment is made only for partial
                    delivery of Work Product, the assignment described herein applies only to the portion of Work
                    Product delivered.
                </p>

                <h3>LICENSE TO OR WAIVER OF OTHER RIGHTS</h3>

                <p>
                    If Freelancer has any right to the Work Product, including without limitation any Intellectual
                    Property Right, that cannot be assigned to Client by Freelancer, Freelancer hereby
                    automatically, upon Freelancer’s receipt of full payment from Client, unconditionally and
                    irrevocably grants to Client during the term of such rights, an exclusive, even as to
                    Freelancer, irrevocable, perpetual, worldwide, fully-paid and royalty-free license to such
                    rights, with rights to sublicense through multiple levels of sublicensees, to reproduce, make
                    derivative works of, distribute, publicly perform and publicly display in any form or medium,
                    whether now known or later developed, make, use, sell, import, offer for sale and exercise
                    any and all such rights. If Freelancer has any rights to such Work Product that cannot be
                    assigned or licensed, Freelancer hereby automatically, upon Freelancer’s receipt of payment
                    from Client, unconditionally and irrevocably waives the enforcement of such rights, and all
                    claims and causes of action of any kind against Client or related to Client’s customers, with
                    respect to such rights, and will, at Client’s request and expense, consent to and join in any
                    action to enforce such rights. If payment is made only for partial delivery of Work Product, the
                    grant described herein applies only to the portion of Work Product delivered.
                </p>

                <h3>ASSISTANCE</h3>

                <p>
                    Freelancer will assist Client in every way, including by signing any documents or instruments
                    reasonably required, both during and after the term of the Service Contract, to obtain and
                    enforce Intellectual Property Rights relating to Work Product in all countries. In the event
                    Client is unable, after reasonable effort, to secure Freelancer’s signature on any document
                    needed in connection with the foregoing, Freelancer hereby designates and appoints Client
                    and its duly authorized officers and agents as its agent and attorney in fact to act on its behalf
                    to further the purposes of this Section with the same legal force and effect as if executed by
                    Freelancer.
                </p>

                <h3>IMMUNITY</h3>

                <p>
                    A disclosure of information will be immune from prosecution or civil action under the Defend
                    Trade Secrets Act, 18 U.S.C. section 1832, if it: (A) is made (i) in confidence to a Federal,
                    State, or local government official, either directly or indirectly, or to an attorney; and (ii) solely
                    for the purpose of reporting or investigating a suspected violation of law; or (B) is made in a
                    complaint or other document filed in a lawsuit or other proceeding, if such filing is made under
                    seal.
                </p>

                <h3>8.7 WORKER CLASSIFICATION</h3>

                <p>
                    Client is responsible and assumes all liability for determining whether Freelancers are
                    independent contractors or employees and engaging them accordingly; Upwork disclaims any
                    liability for such determination or the related Engagement. The Terms of Service do not
                    create a partnership or agency relationship between Users. Freelancer does not have
                    authority to enter into written or oral (whether implied or express) contracts on behalf of
                    Upwork. For Service Contracts classified as independent contractor relationships, Client may
                    not require an exclusive relationship. A Freelancer classified as an independent contractor is
                    free at all times to provide Freelancer Services to persons or businesses other than Client,
                    including any competitor of Client.

                    For Freelancer Services classified as employment, Client must sign up for and participate in
                    the <a href="https://upwork.com/info/payroll-client" target="_blank">Upwork Payroll</a> program made available on the Site, with employment services provided
                    as discussed in Section 8.8 (Employment Services), unless Client pays the Opt-Out Fee. For
                    all purposes with Upwork Payroll, the employer of the Freelancer will be the third-party
                    Staffing Provider and not Upwork under any circumstances.
                </p>

                <h3>8.8 EMPLOYMENT SERVICES</h3>

                <p>
                    If a Client will receive services from a Freelancer it has classified as an employee, then the
                    Client agrees that the Upwork Payroll Agreement applies, and Client agrees to enroll in
                    Upwork Payroll for each such relationship. In this case, Upwork’s third-party staffing vendor
                    (the “Staffing Provider”) will hire the Freelancer at the request of Client and assign the
                    Freelancer to work for Client, as described in the Upwork Payroll Agreement.<br />

                    If a Client and a Freelancer decide to use Upwork Payroll, then Freelancer acknowledges that
                    Freelancer has agreed to become a Payroll Employee, employed by the Staffing Provider and
                    assigned to Client, based on Freelancer’s own determination that Client offers work
                    acceptable and appropriate for Freelancer on terms that are acceptable to Freelancer.
                    Freelancer agrees to complete such documents as the Staffing Provider may legally and
                    reasonably require in connection with the employer-employee relationship, including, without
                    limitation, an authorization for a background check, an employment agreement or offer letter,
                    applicable tax forms, and an I-9 or other documentation to establish authorization for
                    employment in the applicable country (collectively, the “Employment Paperwork”).
                    Freelancer will not become an employee of the Staffing Provider and will not start work until
                    all Employment Paperwork has been completed and returned to the Staffing Provider, and the
                    Staffing Provider has accepted Freelancer as an employee. Once Freelancer becomes an
                    employee of the Staffing Provider, unlike independent contractor engagements, Freelancer
                    understands that he/she will not have the right to control the manner and means by which
                    he/she works and will be subject to the Staffing Provider’s policies and requirements.<br />

                    With respect to Payroll Employees, Upwork merely provides the platform for Freelancer to
                    communicate and share information with the Staffing Provider and Client. Freelancer
                    understands and agrees that the pay rate, work hours, employment dates and working
                    conditions will be established or confirmed by the Staffing Provider and/or Client and not by
                    Upwork, and that Freelancer will not have any contract on the Upwork Site or with Upwork
                    regarding such employment terms; any job posting, proposal, pay rate or other information in
                    the platform is solely for the purpose of enabling the Payroll Employee to communicate with
                    the Staffing Provider and the Client. Freelancer acknowledges and agrees that he/she is not
                    an employee of Upwork, and that he/she will not be providing any services to Upwork (directly
                    or indirectly) while employed by the Staffing Provider unless Upwork is expressly designated
                    in writing as the Client.<br />

                    Freelancer understands and agrees that Freelancer is responsible for accurately recording
                    Freelancer’s work hours and reporting all hours worked to the Staffing Provider. By
                    completing and/or submitting a Time Log, Freelancer verifies that the hours worked reflected
                    in the Time Log are accurately recorded and include all hours worked, including but not
                    limited to any overtime hours and any paid rest breaks that Freelancer is authorized to take
                    by the Staffing Provider and/or by applicable law. Any provision of this Agreement that would
                    conflict with applicable law regarding the timing or amount of wage payments shall not apply
                    to any Payroll Employee.<br />

                    Freelancer agrees to personally provide all services for any contract under which the
                    Freelancer is working as a Payroll Employee. Freelancer acknowledges that Freelancer, and
                    not Upwork or the Staffing Provider, will be responsible for timely and professionally
                    completing all work that he or she is assigned while working as a Payroll Employee.
                </p>

                <h3>9.RECORDS OF COMPLIANCE</h3>

                <p>
                    Users will each (1) create and maintain records to document satisfaction of their respective
                    obligations under this Agreement; any Service Contract, including, without limitation, their
                    respective payment obligations and compliance with tax and employment laws; and any
                    Upwork Payroll Agreement, and (2) provide copies of such records to Upwork upon request.
                    Nothing in this subsection requires or will be construed as requiring Upwork to supervise or
                    monitor a User’s compliance with this Agreement, the other Terms of Service, or a Service
                    Contract.
                </p>

                <h3>10.RELATIONSHIP WITH UPWORK</h3>

                <p>
                    Upwork does not deduct any amount for withholding, unemployment, Social Security, or other
                    taxes for Client or Freelancer, each of which is solely responsible for all tax returns and
                    payments required to be filed with or made to any federal, state, or local tax authority in any
                    nation with respect to Freelancer’s performance, and Client’s acceptance, of Freelancer
                    Services.
                </p>

                <h3>13.1 SERVICE CONTRACTS</h3>

                <p>
                    You expressly acknowledge, agree, and understand that: (a) the Site is merely a venue where
                    Users may act as Clients and/or Freelancers; (b) Upwork is not a party to any Service
                    Contracts between Clients and Freelancers; (c) you are not an employee of Upwork, and
                    Upwork does not, in any way, supervise, direct, or control the Freelancer or Freelancer
                    Services; (d) Upwork will not have any liability or obligations under or related to Service
                    Contracts for any acts or omissions by you or other Users; (e) Upwork has no control over
                    Freelancers or the Freelancer Services offered or rendered by Freelancers; and (f) Upwork
                    makes no representations as to the reliability, capability, or qualifications of any Freelancer or
                    the quality, security, or legality of any Freelancer Services, and Upwork disclaims any and all
                    liability relating thereto.
                </p>

                <h3>14.3UNAUTHORIZED ACCESS AND USE; SITE INTERFERENCE; MALICIOUS
                    SOFTWARE</h3>

                <p>
                    The Site contains robot exclusion headers. You agree that you will not use any robot, spider,
                    scraper, or other automated means to access the Site for any purpose without our express
                    written permission. You will not access the audiovisual content available on the Site for any
                    purpose or in any manner other than streaming. You agree that you will not: (a) take any
                    action that imposes or we believe may impose (in our sole discretion) an unreasonable or
                    disproportionately large load on the Site’s infrastructure; (b) copy, reproduce, modify, create
                    derivative works from, distribute, or publicly display any content (other than content you have
                    submitted to the Site) from the Site, any software code that is part of the Site, or any services
                    that are offered on the Site without the prior express written permission of Upwork and the
                    appropriate third party, as applicable; (c) interfere or attempt to interfere with the proper
                    operation of the Site or any activities conducted on the Site; (d) bypass any measures we
                    may use to prevent or restrict access to the Site or any subparts of the Site, including, without
                    limitation, features that prevent or restrict use or copying of any content or enforce limitations
                    on use of the Site or the content therein; (e) transmit spam, chain letters, or other unsolicited
                    communications; (f) attempt to interfere with or compromise the system integrity or security or
                    decipher any transmissions to or from the servers running the Site; (g) collect or harvest any
                    personally identifiable information, including Account names, from the Site; (h) access any
                    content on the Site through any technology or means other than those provided or authorized
                    by the Site; or (i) directly or indirectly, advertise or promote another website, product, or
                    service or solicit other Users for other websites, products, or services.<br />

                    Additionally, you agree that you will not post or introduce any invalid data, virus, worm, or
                    other harmful or malicious software code, agent, hidden procedure, routine, or mechanism
                    through or to the Site or the Site software that is designed to cause to cease functioning,
                    disrupt, disable, harm, or otherwise impair in any manner, including aesthetic disruptions or
                    distortions, the operation of (or to allow you or any other person to access or damage or
                    corrupt data, storage media, programs, equipment, or communications or otherwise interfere
                    with operations of or on) the Site or any other software, firmware, hardware, computer
                    system, or network of Upwork or any third party
                </p>

                <h3>14.4THIRD-PARTY VERIFICATION</h3>

                <p>
                    UThe Site makes available various services provided by third parties to verify a User’s
                    credentials and provide other information. Any information or content expressed or made
                    available by these third parties or any other Users is that of the respective author(s) or
                    distributor(s) and not of Upwork. Upwork neither endorses nor is responsible for the accuracy
                    or reliability of any opinion, advice, information, or statement made on the Site by anyone
                    other than Upwork’s authorized employees acting in their official capacities.
                </p>

                <h3>14.6 MOBILE AND OTHER DEVICES</h3>

                <p>
                    When using our mobile applications, please be aware that your carrier’s normal rates and
                    fees, such as text messaging and data charges, will still apply. Our mobile applications may
                    not contain the same functionality available on the Site.
                </p>

                <h3>16.WARRANTY DISCLAIMER</h3>

                <p>
                    YOU AGREE NOT TO RELY ON THE SITE, THE SITE SERVICES, ANY INFORMATION ON
                    THE SITE OR THE CONTINUATION OF THE SITE. THE SITE AND THE SITE SERVICES
                    ARE PROVIDED “AS IS” AND ON AN “AS AVAILABLE” BASIS. UPWORK MAKES NO
                    EXPRESS REPRESENTATIONS OR WARRANTIES WITH REGARD TO THE SITE, THE
                    SITE SERVICES, WORK PRODUCT, OR ANY ACTIVITIES OR ITEMS RELATED TO THIS
                    AGREEMENT OR THE OTHE TERMS OF SERVICE. TO THE MAXIMUM EXTENT
                    PERMITTED BY APPLICABLE LAW, UPWORK DISCLAIMS ALL EXPRESS AND IMPLIED
                    CONDITIONS, REPRESENTATIONS, AND WARRANTIES INCLUDING, BUT NOT LIMITED
                    TO, THE WARRANTIES OF MERCHANTABILITY, ACCURACY, FITNESS FOR A
                    PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT. SOME JURISDICTIONS
                    MAY NOT ALLOW FOR ALL OF THE FOREGOING LIMITATIONS ON WARRANTIES, SO
                    TO THAT EXTENT, SOME OR ALL OF THE ABOVE LIMITATIONS MAY NOT APPLY TO
                    YOU. SECTION 20 (TERM AND TERMINATION) STATES USER’S SOLE AND EXCLUSIVE
                    REMEDY AGAINST UPWORK WITH RESPECT TO ANY DEFECTS, NON-
                    CONFORMANCES, OR DISSATISFACTION.
                </p>

                <h3>17.LIMITATION OF LIABILITY</h3>

                <p>
                    Upwork is not liable, and you agree not to hold us responsible, for any damages or losses
                    arising out of or in connection with the Terms of Service, including, but not limited to:
                </p>
                    <ul>
                        <li>your use of or your inability to use our Site or Site Services;</li>
                        <li>delays or disruptions in our Site or Site Services;</li>
                        <li>viruses or other malicious software obtained by accessing, or linking to, our Site or
                            Site Services;</li>
                        <li>glitches, bugs, errors, or inaccuracies of any kind in our Site or Site Services;</li>
                        <li>damage to your hardware device from the use of the Site or Site Services;</li>
                        <li>the content, actions, or inactions of third parties’ use of the Site or Site Services;</li>
                        <li>a suspension or other action taken with respect to your account;</li>
                        <li>your reliance on the quality, accuracy, or reliability of job postings, Profiles, ratings,
                            recommendations, and feedback (including their content, order, and display), or
                            metrics found on, used on, or made available through the Site; and</li>
                        <li>your need to modify practices, content, or behavior or your loss of or inability to do
                            business, as a result of changes to the Terms of Service.</li>
                    </ul>
                <p>
                    ADDITIONALLY, IN NO EVENT WILL UPWORK, OUR AFFILIATES, OUR LICENSORS, OR
                    OUR THIRD-PARTY SERVICE PROVIDERS BE LIABLE FOR ANY SPECIAL,
                    CONSEQUENTIAL, INCIDENTAL, PUNITIVE, EXEMPLARY, OR INDIRECT COSTS OR
                    DAMAGES, INCLUDING, BUT NOT LIMITED TO, LITIGATION COSTS, INSTALLATION
                    AND REMOVAL COSTS, OR LOSS OF DATA, PRODUCTION, PROFIT, OR BUSINESS
                    OPPORTUNITIES. THE LIABILITY OF UPWORK, OUR AFFILIATES, OUR LICENSORS,
                    AND OUR THIRD-PARTY SERVICE PROVIDERS TO ANY USER FOR ANY CLAIM
                    ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT OR THE OTHER
                    TERMS OF SERVICE WILL NOT EXCEED THE LESSER OF: (A) $2,500; OR (B) ANY FEES
                    RETAINED BY UPWORK WITH RESPECT TO SERVICE CONTRACTS ON WHICH USER
                    WAS INVOLVED AS CLIENT OR FREELANCER DURING THE SIX-MONTH PERIOD
                    PRECEDING THE DATE OF THE CLAIM. THESE LIMITATIONS WILL APPLY TO ANY
                    LIABILITY, ARISING FROM ANY CAUSE OF ACTION WHATSOEVER ARISING OUT OF
                    OR IN CONNECTION WITH THIS AGREEMENT OR THE OTHER TERMS OF SERVICE,
                    WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY, OR
                    OTHERWISE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH COSTS OR DAMAGES
                    AND EVEN IF THE LIMITED REMEDIES PROVIDED HEREIN FAIL OF THEIR ESSENTIAL
                    PURPOSE. SOME STATES AND JURISDICTIONS DO NOT ALLOW FOR ALL OF THE
                    FOREGOING EXCLUSIONS AND LIMITATIONS, SO TO THAT EXTENT, SOME OR ALL
                    OF THESE LIMITATIONS AND EXCLUSIONS MAY NOT APPLY TO YOU.
                </p>

                <h3>20.AGREEMENT TERM AND TERMINATION</h3>

                <p>
                    The Terms of Service as amended from time to time, will become effective on the later of the
                    Effective Date or your first visit to the Site and will remain in effect for the duration of your use
                    of the Site or Site Services. Unless both you and Upwork expressly agree otherwise in
                    writing, either of us may terminate this Agreement in our sole discretion, at any time, without
                    explanation, upon written notice to the other, which will result in the termination of the other
                    Terms of Service as well, except as otherwise provided herein. You may provide written
                    notice tolegalnotices@upwork.com. If you are using Upwork Payroll, you must legally
                    terminate your relationship with Client or Freelancer, as applicable, before terminating this
                    Agreement. In the event you properly terminate this Agreement, your right to use the Site is
                    automatically revoked, and your Account will be closed; however, (a) if you have any open
                    Engagements when you terminate this Agreement, you will continue to be bound by this
                    Agreement and the other Terms of Service until all such Engagements have closed on the
                    Site; (b) Upwork will continue to perform those Site Services necessary to complete any open
                    Engagement or related transaction between you and another User; and (c) you will continue
                    to be obligated to pay any amounts accrued but unpaid as of the date of termination or as of
                    the completion of any open Engagements, whichever is later, to Upwork for any Site Services
                    and to any Freelancers for any Freelancer Services. Without limiting any other provisions of
                    the Terms of Service, the termination of this Agreement for any reason will not release you,
                    any User with whom you have entered into a Service Contract, or Upwork from any
                    obligations incurred prior to termination of this Agreement or that thereafter may accrue in
                    respect of any act or omission prior to such termination. Those portions of the Terms of
                    Service necessary to implement the foregoing survive termination of this Agreement for any
                    reason.

                    Without limiting Upwork’s other rights or remedies, we may temporarily suspend, indefinitely
                    suspend, or permanently revoke your access to the Site and refuse to provide any or all Site
                    Services to you if: (i) you breach the letter or spirit of any terms and conditions of this
                    Agreement or other parts of the Terms of Service; (ii) we suspect or become aware that you
                    have provided false or misleading information to us; or (iii) we believe, in our sole discretion,
                    that your actions may cause legal liability for you, our Users, or Upwork or our Affiliates; may
                    be contrary to the interests of the Site or the User community; or may involve illicit activity. If
                    your Account is suspended or closed, you may not use the Site under the same Account or a
                    different Account or reregister under a new Account without Upwork’s prior written consent. If
                    you attempt to use the Site under a different Account, we reserve the right to reclaim available
                    funds in that Account and/or use an available Payment Method to pay for any amounts owed
                    by you to the extent permitted by applicable law.
                </p>

                <h3>20.1ENFORCEMENT OF AGREEMENT</h3>

                <p>
                    Upwork has the right, but not the obligation, to suspend or revoke your access to the Site and
                    Site Services if we believe that you have violated or acted inconsistently with the letter or
                    spirit of this Agreement or the Terms of Service or violated our rights or those of another
                    party. Without limiting Upwork’s other rights or remedies, we may suspend or close your
                    Account, use self-help in connection with our rights to reclaim funds, and refuse to provide
                    any further access to the Site or the Services to you if (a) you breach any terms and
                    conditions of this Agreement or other Terms of Service; (b) we are unable to verify or
                    authenticate any information you provide to us; or (c) we believe that your actions may cause
                    legal liability for you, other Users, or Upwork.
                </p>

                <h3>21.CANCELLATIONS, REFUNDS, AND DISPUTES</h3>
                <h3>21.1 DISPUTE PROCESS AND SCOPE</h3>
                <p>
                    For disputes arising between Clients and Freelancers, you agree to abide by the dispute
                    process that is explained in the Escrow Instructions that apply to your particular Service
                    Contract.<br />

                    If a dispute arises between you and Upwork or our Affiliates, our goal is to resolve the dispute
                    quickly and cost-effectively. Accordingly, you, Upwork, and our Affiliates agree to resolve any
                    claim, dispute, or controversy that arises out of or relates to this Agreement, the other Terms
                    of Service, your relationship with Upwork (including any claimed employment with Upwork or
                    one of its Affiliates or successors), the termination of your relationship with Upwork, or the
                    Site Services (each, a “Claim”) in accordance with this Section. For the avoidance of doubt,
                    Claims include, but are not limited to, all claims, disputes, or controversies arising out of or
                    relating to the Terms of Service, any Service Contract, escrow payments or agreements, any
                    payments or monies you claim are due to you from Upwork or its Affiliates or successors,
                    trade secrets, unfair competition, false advertising, consumer protection, privacy,
                    compensation, classification, minimum wage, seating, expense reimbursement, overtime,
                    breaks and rest periods, termination, discrimination or harassment and claims arising under
                    the Uniform Trade Secrets Act as enacted in any state, Civil Rights Act of 1964, Americans
                    With Disabilities Act, Age Discrimination in Employment Act, Family Medical Leave Act, Fair
                    Labor Standards Act, Employee Retirement Income Security Act (except for claims for
                    employee benefits under any benefit plan sponsored by the Company and (a) covered by the
                    Employee Retirement Income Security Act of 1974 or (b) funded by insurance), Affordable
                    Care Act, Genetic Information Non-Discrimination Act, state statutes or regulations
                    addressing the same or similar subject matters, and all other federal or state legal claims
                    arising out of or relating to your relationship with Upwork or the termination of that
                    relationship. Only with respect to the Arbitration Provision, Claims do not include disputes
                    that may not be subject to a pre-dispute arbitration agreement as provided by the Dodd-Frank
                    Wall Street Reform and Consumer Protection Act (Public Law 111-203) and are excluded
                    from the coverage of the Arbitration Provision.<br />

                    You agree that any Claim must be resolved as described in the subsections below titled
                    “Informal Dispute Resolution” and “Mandatory Binding Arbitration and Class Action/Jury Trial
                    Waiver.”
                </p>

                <h3>21.2 CHOICE OF LAW</h3>

                <p>
                    This Agreement, the other Terms of Service, and any Claim will be governed by and
                    construed in accordance with the laws.
                </p>

                <h3>22. GENERAL</h3>
                <h3>22.1 ENTIRE AGREEMENT</h3>

                <p>
                    This Agreement, together with the other Terms of Service, sets forth the entire agreement and
                    understanding between you and Upwork relating to the subject matter hereof and thereof and
                    cancels and supersedes any prior or contemporaneous discussions, agreements,
                    representations, warranties, and other communications between you and us, written or oral,
                    to the extent they relate in any way to the subject matter hereof and thereof. The section
                    headings in the Terms of Service are included for ease of reference only and have no binding
                    effect. Even though Upwork drafted the Terms of Service, you represent that you had ample
                    time to review and decide whether to agree to the Terms of Service. If an ambiguity or
                    question of intent or interpretation of the Terms of Service arises, no presumption or burden
                    of proof will arise favoring or disfavoring you or Upwork because of the authorship of any
                    provision of the Terms of Service.
                </p>

                <h3>22.5 NO WAIVER</h3>

                <p>
                    The failure or delay of either party to exercise or enforce any right or claim does not constitute
                    a waiver of such right or claim and will in no way affect that party’s right to later enforce or
                    exercise it, unless such party issues an express written waiver, signed by a duly authorized
                    representative of such party.
                </p>

                <h3>22.8 FORCE MAJEURE</h3>

                <p>
                    The parties to this Agreement will not be responsible for the failure to perform or any delay in
                    performance of any obligation hereunder due to labor disturbances, accidents, fires, floods,
                    telecommunications or Internet failures, strikes, wars, riots, rebellions, blockades, acts of
                    government, governmental requirements and regulations or restrictions imposed by law or
                    any other similar conditions beyond the reasonable control of such party. The time for
                    performance of such party will be extended by the period of such delay. Irrespective of any
                    extension of time, if an event of Force Majeure occurs and its effect continues for a period of
                    60 days, either the party may give to the other a 30-day notice of termination. If, at the end of
                    the 30 day period, the effect of the Force Majeure continues, the Agreement and the other
                    Terms of Service will terminate, except as provided in Section 20.3.<br />

                    person or entity blocked or denied by the United States government. Unless otherwise
                    explicitly stated, all materials found on the Site are solely directed to individuals, companies,
                    or other entities located in the United States.
                </p>

                </div>
            </div><!-- /.row -->
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->


<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
    $(function () {
        $("#example1").dataTable();
    });
</script>
<script type="text/javascript">
    $(function() {


        $(".btn-danger").click(function(){

            //Save the link in a variable called element
            var element = $(this);

            //Find the id of the link that was clicked
            var id = element.attr("id");

            //Built a url to send
            var info = 'id=' + id;
            if(confirm("<?php echo $lang['delete_portfolio']; ?>"))
            {
                var parent = $(this).parent().parent();
                $.ajax({
                    type: "GET",
                    url: "template/delete/deleteportfolio.php",
                    data: info,
                    success: function()
                    {
                        parent.fadeOut('slow', function() {$(this).remove();});
                    }
                });


            }
            return false;

        });

    });
</script>

<script type="text/javascript">
    $(function() {

        $(".btn-info").click(function(){

            //Save the link in a variable called element
            var element = $(this);

            //Find the id of the link that was clicked
            var id = element.attr("id");

            //Built a url to send
            var info = 'id=' + id;
            if(confirm("<?php echo $lang['activate_portfolio']; ?>"))
            {
                var parent = $(this).parent().parent();
                $.ajax({
                    type: "GET",
                    url: "template/actions/activateportfolio.php",
                    data: info,
                    success: function()
                    {
                        window.location.reload();
                    }
                });


            }
            return false;

        });

    });
</script>

<script type="text/javascript">
    $(function() {

        $(".btn-default").click(function(){

            //Save the link in a variable called element
            var element = $(this);

            //Find the id of the link that was clicked
            var id = element.attr("id");

            //Built a url to send
            var info = 'id=' + id;
            if(confirm("<?php echo $lang['deactivate_portfolio']; ?>"))
            {
                var parent = $(this).parent().parent();
                $.ajax({
                    type: "GET",
                    url: "template/actions/deactivateportfolio.php",
                    data: info,
                    success: function()
                    {
                        window.location.reload();
                    }
                });


            }
            return false;

        });

    });
</script>

</body>
</html>
