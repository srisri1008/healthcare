<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
    Redirect::to('../index.php');
}

?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>

<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $lang['payment']; ?><small><?php echo $lang['section']; ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active">Add Payment Method</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Include currency.php. Contains header content. -->
            <?php include ('template/currency.php'); ?>
            <style>
                .maindiv {
                    margin-top: 35px;
                    margin-bottom: 46px;
                }
                .btndiv {
                    margin-top: 30px;
                    margin-bottom: 30px;
                    border: 1px solid lightgrey;
                    background-color: #f0f0f0;
                    padding: 20px 10px;
                    border-radius: 5px;
                }
                .actiondiv li {
                    margin-left: 10px;
                }
                .maindiv img {
                    width: 200px;
                    height: 150px;
                }
                .btndiv img {
                    width: 50px;
                    height: 50px;
                }
            </style>
            <div class="container" style="text-align: center;">
                <h1 style="font-weight: bold">Add a verified Payment method</h1>
                <div class="row maindiv">
                    <div class="col-lg-2 col-lg-offset-2">
                        <img src="uploads/images/paypal_logo.jpg" alt="Paypal logo">
                    </div>
                    <div class="col-lg-2">
                        <img src="uploads/images/stripe_logo.png" alt="Stripe logo">
                    </div>
                    <div class="col-lg-2">
                        <img src="uploads/images/braintree_logo.png" alt="Braintree logo">
                    </div>
                    <div class="col-lg-2">
                        <img src="uploads/images/coingate_logo.png" alt="Coingate logo">
                    </div>
                </div>
                <h4 style="font-weight: bold">Add a verified payment method</h4>
                <p>Get paid faster, weekly or monthly & direct to your account.</p>
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 btndiv">
                        <img src="uploads/images/mastercard_logo.png" alt="Mastercard logo">
                        <img src="uploads/images/americanexpress_logo.jpg" alt="Amercian Express logo">
                        <img src="uploads/images/visa_logo.png" alt="Visa logo">
                        <img src="uploads/images/jcb_logo.png" alt="JCB logo">
                        <button class="btn btn-success" style="margin-left: 1%; margin-top: 17px;">Add Debit or Credit Card</button>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->


<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
</body>
</html>
