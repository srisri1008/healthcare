<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
    Redirect::to('../index.php');
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>

<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="background-color: white !important;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Timesheet<small><?php echo $lang['section']; ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active">Timesheet</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Include currency.php. Contains header content. -->
            <?php include ('template/currency.php'); ?>
            <style>
                img {
                    width: 100%;
                }
                .aasmani {
                    background-color: #18a5d0;
                    color: white;
                }
                .single_item {
                    margin-bottom: 20px;
                    background-color: #fff0d1;
                    padding: 15px 10px;
                }

            </style>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="row single_item">
                            <div class="col-lg-5">
                                <img src="uploads/timesheet.png" alt="Timesheet">
                            </div>
                            <div class="col-lg-7">
                                <h2 style="font-weight: bold">Timesheet Download - Word</h2>
                                <h3>Download a printable editable timesheet<br />in word format.</h3>
                                <a href="uploads/timesheet-template.docx">
                                    <button class="btn aasmani">Download Timesheet</button>
                                </a>
                            </div>
                        </div>

                        <div class="row single_item" style="position: relative;">
                            <div class="col-lg-5">
                                <img src="uploads/timesheet.png" alt="Timesheet">
                            </div>
                            <div class="col-lg-7">
                                <h2 style="font-weight: bold">Timesheet Download - HTML</h2>
                                <h3>Download a printable editable timesheet.</h3>
                                <a href="uploads/timesheet-template.docx">
                                    <button class="btn aasmani">Download Timesheet</button>
                                </a>
                            </div>
                            <div style="position: absolute; text-align: center; right: -200px">
                                <img src="uploads/timesheet_lock.png" alt="Timesheet Lock">
                                <h4><a href="http://www.docusign.co.uk" target="_blank">E-signature</a></h4>
                            </div>
                        </div>
                        <div class="row" style="position: relative;">
                            <img src="uploads/invoice_banner.jpg" alt="Invoice Banner">
                            <button type="button" style="position: absolute;right: 3%;bottom: 10%;padding: 2% 13%;opacity: 0;">Invoice button</button>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->


<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
    $(function () {
        $("#example1").dataTable();
    });
</script>

</body>
</html>
