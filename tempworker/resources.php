<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
    Redirect::to('../index.php');
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>

<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $lang['resources']; ?><small><?php echo $lang['section']; ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active"><?php echo $lang['resources']; ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Include currency.php. Contains header content. -->
            <?php include ('template/currency.php'); ?>
            <style>
                .row>div {
                    border: 1px solid lightblue;
                    width: 250px;
                    margin: 10px;
                    padding: 20px;
                    height: 200px;
                }
            </style>
            <div class="container" style="text-align: center">
                <div class="row">
                    <div class="col-lg-3">
                        <a href="https://www.gmc-uk.org/registration-and-licensing/the-medical-register" target="_blank">
                            <img src="uploads/images/general-medical-counsil.png" alt="">
                            <h4>Doctor Registration</h4>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="https://www.nmc.org.uk/registration/search-the-register/" target="_blank">
                            <img src="../myassets/images/nursing_council.png" alt="">
                            <h4>Nurse Registration</h4>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="https://www.pharmacyregulation.org/registers" target="_blank">
                            <img src="../myassets/images/general_council.png" alt="">
                            <h4>Pharmacist Registration</h4>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/nvq.png" alt="">
                        <h4>Carrer Qualification</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <img src="../myassets/images/dbs.png" alt="">
                        <h4>DBS Application</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/company.png" alt="">
                        <h4>Form A Company</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/hm.png" alt="">
                        <h4>Tax Code Application</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/noddle.png" alt="">
                        <h4>Credit Check Application</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <img src="../myassets/images/banda.png" alt="">
                        <h4>Online Interview</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/gola.png" alt="">
                        <h4>Online Training</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/paypal.png" alt="">
                        <h4>Online Payment</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/computer.png" alt="">
                        <h4>Online Timesheet</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <img src="../myassets/images/private.png" alt="">
                        <h4>Private Pension</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/pension.png" alt="">
                        <h4>Pension Healthcare</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/dental.png" alt="">
                        <h4>Private Dental Care</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/mortgages.png" alt="">
                        <h4>Mortgages</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <img src="../myassets/images/accountant.png" alt="">
                        <h4>Accountant</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/tax_advisor.png" alt="">
                        <h4>Tax Advisor</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/bank_account.png" alt="">
                        <h4>Bank Account</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/salary_calculator.png" alt="">
                        <h4>Salary Calculator</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <img src="../myassets/images/online_gp.png" alt="">
                        <h4>Online GP</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/now_pharmacy.png" alt="">
                        <h4>Online Pharmacy</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/dentist.png" alt="">
                        <h4>Online Dentist</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/housekeeper.png" alt="">
                        <h4>Housekeeper</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <img src="../myassets/images/airbnb.png" alt="">
                        <h4>Book Accommodation</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/rent_a_car.png" alt="">
                        <h4>Rent a Car near you</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/order_fast.png" alt="">
                        <h4>Order Fast food</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/groceries.png" alt="">
                        <h4>Order Groceries</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <img src="../myassets/images/rent_a_room.png" alt="">
                        <h4>Rent a Room</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/book_a_taxi.png" alt="">
                        <h4>Book a Taxi</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/dry_clearners.png" alt="">
                        <h4>Book Dry cleaners</h4>
                    </div>
                    <div class="col-lg-3">
                        <img src="../myassets/images/fresh_vegs.png" alt="">
                        <h4>Order Fresh vegs</h4>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->


<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
    $(function () {
        $("#example1").dataTable();
    });
</script>

</body>
</html>
