<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
    Redirect::to('../index.php');
}


//Get Freelancer's Data
$query = DB::getInstance()->get("profile", "*", ["userid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
if ($query->count()) {
    foreach($query->results() as $row) {
        $nid = $row->id;
        $location = $row->location;
        $city = $row->city;
        $country = $row->country;
        $rate = $row->rate;
        $phone = $row->phone;
        $website = $row->website;
        $about = $row->about;
        $education_profile = $row->education;
        $work_profile = $row->work;
        $awards_profile = $row->awards;
        $type_id = $row->type_id;
        $rate_id = $row->rate_id;
        $category_id = $row->category_id;
        $availability_id = $row->availability_id;
    }
}


//Get reference of temp worker.
$query_temp_freelancer = DB::getInstance()->get("temp_references", "*", ["freelancer_id" => $freelancer->data()->id]);
$temp_ref = array();
$temp_ref1= array();
$temp_ref2 = array();
$temp_ref3 = array();
$temp_ref4 = array();


if ($query_temp_freelancer->count()) {
    foreach($query_temp_freelancer->results() as $key=>$row) {
        $temp_ref[$key]['name']= $row->name;
        $temp_ref[$key]['organization']= $row->organization;
        $temp_ref[$key]['occupation']= $row->occupation;
        $temp_ref[$key]['email']= $row->email;
        $temp_ref[$key]['id']= $row->id;
        $temp_ref[$key]['freelancer_id']= $row->freelancer_id;

    }
}

//Create different array for each reference record (row).
//Every tem work has at most 4 references.
$temp_ref1 = $temp_ref[0];
$temp_ref2 = $temp_ref[1];
$temp_ref3 = $temp_ref[2];
$temp_ref4 = $temp_ref[3];

//=========================Get references END=================================//

//Get multiple rows of freelancer
$queryAll = DB::getInstance()->get("temp_education", "*", ["user_id" => $freelancer->data()->id]);
$allDataRows = array();
if ($queryAll->count()) {
    foreach($queryAll->results() as $row) {
        $allDataRows[] = $row;
    }
}




//Edit Profile Data
if(isset($_POST['form_profile'])){
    if (Input::exists()) {
        if(Token::check(Input::get('token'))){

            $errorHandler = new ErrorHandler;

            $validator = new Validator($errorHandler);

            $validation = $validator->check($_POST, [
                'type_id' => [
                    'required' => true,
                ],
                'rate_id' => [
                    'required' => true,
                ],
                'city' => [
                    'required' => true,
                    'maxlength' => 200,
                    'minlength' => 2
                ],
                'country' => [
                    'required' => true,
                    'maxlength' => 200,
                    'minlength' => 2
                ],
                'linkedin' => [
                    'required' => true,
                    'maxlength' => 200,
                    'minlength' => 2
                ]
            ]);

            if (!$validation->fails()) {

                $query = DB::getInstance()->get("profile", "*", ["userid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
                if ($query->count() === 1) {

                    //Update Profile
                    $profileUpdate = DB::getInstance()->update('profile',[
                        'rate_id' => Input::get('rate_id'),
                        'type_id' => Input::get('type_id'),
                        'category_id' => Input::get('category_id'),
                        'availability_id' => Input::get('availability_id'),
                        'city' => Input::get('city'),
                        'country' => Input::get('country'),
                        'website' => Input::get('website')
                    ],[
                        'userid' => $freelancer->data()->freelancerid
                    ]);

                    if (count($profileUpdate) > 0) {
                        header('Location: '. $_SERVER['PHP_SELF'] . '?a='. $_GET['a']);
                        $updatedError = true;
                    } else {
                        $hasError = true;
                    }

                } else {

                    try{
                        $profileid = uniqueid();
                        $profileInsert = DB::getInstance()->insert('profile', array(
                            'profileid' => $profileid,
                            'userid' => $freelancer->data()->freelancerid,
                            'location' => Input::get('location'),
                            'city' => Input::get('city'),
                            'country' => Input::get('country'),
                            'rate' => Input::get('rate'),
                            'website' => Input::get('website'),
                            'active' => 1,
                            'delete_remove' => 0,
                            'rate_id' => Input::get('rate_id'),
                            'type_id' => Input::get('type_id'),
                            'category_id' => Input::get('category_id'),
                            'availability_id' => Input::get('availability_id'),
                            'date_added' => date('Y-m-d H:i:s')
                        ));

                        if (count($profileInsert) > 0) {
                            header('Location: '. $_SERVER['PHP_SELF'] . '?a='. $_GET['a']);
                            $noError = true;
                        } else {
                            $hasError = true;
                        }


                    }catch(Exception $e){
                        die($e->getMessage());
                    }

                }


            } else {
                $error = '';
                foreach ($validation->errors()->all() as $err) {
                    $str = implode(" ",$err);
                    $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
                }
            }

        }
    }
}



////Edit Profile Data
if(isset($_POST['form_references1'])){



    if (Input::exists()) {

        //if(Token::check(Input::get('token'))){

        $errorHandler = new ErrorHandler;

        $validator = new Validator($errorHandler);

        $validation = $validator->check($_POST, [
            'name1' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'occupation1' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'organization1' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'email1' => [
                'required' => true,
                'maxlength' => 250,
                'email' => true
            ],

        ]);


        if (!$validation->fails()) {


            if (!empty($temp_ref1)) {

                //Update Profile
                $temp_ref1_update = DB::getInstance()->update('temp_references',[
                    'name' => Input::get('name1'),
                    'occupation' => Input::get('occupation1'),
                    'organization' => Input::get('organization1'),
                    'email' => Input::get('email1'),

                ],[
                    'id' => $temp_ref1['id']
                ]);

                if (count($temp_ref1_update) > 0) {
                    header('Location: '. $_SERVER['PHP_SELF'] . '?a='. $_GET['a']);
                    $updatedError = true;
                } else {
                    $hasError = true;
                }

            } else {


                try{

                    $temp_ref1_insert = DB::getInstance()->insert('temp_references', array(
                        'name' => "'".Input::get('name1')."''",
                        'occupation' => "'".Input::get('occupation1')."''",
                        'organization' => "'".Input::get('organization1')."''",
                        'email' => "'".Input::get('email1')."''",
                        'freelancer_id' => $freelancer->data()->id,
                    ));

                    if (count($temp_ref1_insert) > 0) {
                        header('Location: '. $_SERVER['PHP_SELF'] . '?a='. $_GET['a']);
                        $noError = true;
                    } else {
                        $hasError = true;
                    }


                }catch(Exception $e){
                    die($e->getMessage());
                }

            }


        } else {
            $error = '';
            foreach ($validation->errors()->all() as $err) {
                $str = implode(" ",$err);
                $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
            }
        }

        //  }
    }
}

////Edit Profile Data
if(isset($_POST['form_references2'])){


    echo"hellooo1";
    if (Input::exists()) {
        echo"hellooo";

        //if(Token::check(Input::get('token'))){

        $errorHandler = new ErrorHandler;

        $validator = new Validator($errorHandler);

        $validation = $validator->check($_POST, [
            'name2' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'occupation2' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'organization2' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'email2' => [
                'required' => true,
                'maxlength' => 250,
                'email' => true
            ],

        ]);


        if (!$validation->fails()) {


            if ($temp_ref2) {

                //Update Profile
                $temp_ref2_update = DB::getInstance()->update('temp_references',[
                    'name' => Input::get('name2'),
                    'occupation' => Input::get('occupation2'),
                    'organization' => Input::get('organization2'),
                    'email' => Input::get('email2'),


                ],[
                    'id' => $temp_ref2['id']
                ]);

                if (count($temp_ref2_update) > 0) {
                    header('Location: '. $_SERVER['PHP_SELF'] . '?a='. $_GET['a']);
                    $updatedError = true;
                } else {
                    $hasError = true;
                }

            } else {

                try{

                    $temp_ref2_insert = DB::getInstance()->insert('temp_references', array(
                        'name' => Input::get('name2'),
                        'occupation' => Input::get('occupation2'),
                        'organization' => Input::get('organization2'),
                        'email' => Input::get('email2'),
                        'freelancer_id' => $freelancer->data()->id,
                    ));

                    if (count($temp_ref2_insert) > 0) {
                        header('Location: '. $_SERVER['PHP_SELF'] . '?a='. $_GET['a']);
                        $noError = true;
                    } else {
                        $hasError = true;
                    }


                }catch(Exception $e){
                    die($e->getMessage());
                }

            }


        } else {
            $error = '';
            foreach ($validation->errors()->all() as $err) {
                $str = implode(" ",$err);
                $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
            }
        }

        //  }
    }
}



////Edit Reference3 Data
if(isset($_POST['form_references3'])){


    echo "j1";

    if (Input::exists()) {


        //if(Token::check(Input::get('token'))){

        $errorHandler = new ErrorHandler;

        $validator = new Validator($errorHandler);

        $validation = $validator->check($_POST, [
            'name3' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'occupation3' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'organization3' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'email3' => [
                'required' => true,
                'maxlength' => 250,
                'email' => true
            ],

        ]);


        if (!$validation->fails()) {


            if ($temp_ref3) {

                //Update Profile
                $temp_ref3_update = DB::getInstance()->update('temp_references',[
                    'name' => Input::get('name3'),
                    'occupation' => Input::get('occupation3'),
                    'organization' => Input::get('organization3'),
                    'email' => Input::get('email3'),

                ],[
                    'id' => $temp_ref3['id']
                ]);

                if (count($temp_ref3_update) > 0) {
                    header('Location: '. $_SERVER['PHP_SELF'] . '?a='. $_GET['a']);
                    $updatedError = true;
                } else {
                    $hasError = true;
                }

            } else {



                try{

                    $temp_ref3_insert = DB::getInstance()->insert('temp_references', array(
                        'name' => Input::get('name3'),
                        'occupation' =>  Input::get('occupation3'),
                        'organization' =>  Input::get('organization3'),
                        'email' =>  Input::get('email3'),
                        'freelancer_id' => $freelancer->data()->id,
                    ));

                    if (count($temp_ref3_insert) > 0) {
                        header('Location: '. $_SERVER['PHP_SELF'] . '?a='. $_GET['a']);
                        $noError = true;
                    } else {
                        $hasError = true;
                    }
                    die;

                }catch(Exception $e){
                    die($e->getMessage());
                }

            }


        } else {
            $error = '';
            foreach ($validation->errors()->all() as $err) {
                $str = implode(" ",$err);
                $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
            }
        }

        //  }
    }
}


////Edit Reference4 Data
if(isset($_POST['form_references4'])){


    echo "j1";

    if (Input::exists()) {


        //if(Token::check(Input::get('token'))){

        $errorHandler = new ErrorHandler;

        $validator = new Validator($errorHandler);

        $validation = $validator->check($_POST, [
            'name4' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'occupation4' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'organization4' => [
                'required' => true,
                'maxlength' => 250,
                'minlength' => 2
            ],
            'email4' => [
                'required' => true,
                'maxlength' => 250,
                'email' => true
            ],

        ]);


        if (!$validation->fails()) {


            if ($temp_ref4) {

                //Update Profile
                $temp_ref4_update = DB::getInstance()->update('temp_references',[
                    'name' => Input::get('name4'),
                    'occupation' => Input::get('occupation4'),
                    'organization' => Input::get('organization4'),
                    'email' => Input::get('email4'),

                ],[
                    'id' => $temp_ref3['id']
                ]);

                if (count($temp_ref3_update) > 0) {
                    header('Location: '. $_SERVER['PHP_SELF'] . '?a='. $_GET['a']);
                    $updatedError = true;
                } else {
                    $hasError = true;
                }

            } else {



                try{

                    $temp_ref4_insert = DB::getInstance()->insert('temp_references', array(
                        'name' => Input::get('name4'),
                        'occupation' =>  Input::get('occupation4'),
                        'organization' => Input::get('organization4'),
                        'email' => Input::get('email4'),
                        'freelancer_id' => $freelancer->data()->id,
                    ));

                    if (count($temp_ref4_insert) > 0) {
                        header('Location: '. $_SERVER['PHP_SELF'] . '?a='. $_GET['a']);
                        $noError = true;
                    } else {
                        $hasError = true;
                    }
                    die;

                }catch(Exception $e){
                    die($e->getMessage());
                }

            }


        } else {
            $error = '';
            foreach ($validation->errors()->all() as $err) {
                $str = implode(" ",$err);
                $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
            }
        }

        //  }
    }
}


//Edit About Data
if(isset($_POST['form_about'])){
    if (Input::exists()) {
        if(Token::check(Input::get('token'))){

            $errorHandler = new ErrorHandler;

            $validator = new Validator($errorHandler);

            $validation = $validator->check($_POST, [
                'about_desc' => [
                    'required' => true,
                    'minlength' => 2
                ]
            ]);

            if (!$validation->fails()) {

                $query = DB::getInstance()->get("profile", "*", ["userid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
                if ($query->count() === 1) {

                    //Update About
                    $aboutUpdate = DB::getInstance()->update('profile',[
                        'about' => Input::get('about_desc')
                    ],[
                        'userid' => $freelancer->data()->freelancerid
                    ]);

                    if (count($aboutUpdate) > 0) {
                        $updatedError = true;
                    } else {
                        $hasError = true;
                    }

                } else {

                    try{
                        $profileid = uniqueid();
                        $profileInsert = DB::getInstance()->insert('profile', array(
                            'profileid' => $profileid,
                            'userid' => $freelancer->data()->freelancerid,
                            'about' => Input::get('about_desc'),
                            'active' => 1,
                            'delete_remove' => 0,
                            'date_added' => date('Y-m-d H:i:s')
                        ));

                        if (count($profileInsert) > 0) {
                            $noError = true;
                        } else {
                            $hasError = true;
                        }


                    }catch(Exception $e){
                        die($e->getMessage());
                    }

                }

            } else {
                $error = '';
                foreach ($validation->errors()->all() as $err) {
                    $str = implode(" ",$err);
                    $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
                }
            }

        }
    }
}

//Edit Education Data
if(isset($_POST['form_education'])){
    if (Input::exists()) {
        if(Token::check(Input::get('token'))){
            $errorHandler = new ErrorHandler;

            $validator = new Validator($errorHandler);

            $validation =
                $validator->check(
                    $_POST[0], [
                        'school' => [
                            'required' => true,
                            'minlength' => 2
                        ],

                        'qualification' => [
                            'required' => true,
                            'minlength' => 2
                        ],

                        'startyear' => [
                            'required' => true,
                            'minlength' => 2
                        ],

                        'endyear' => [
                            'required' => true,
                            'minlength' => 2
                        ],

                        'notes' => [
                            'required' => true,
                            'minlength' => 2
                        ],

                    ]
                );
//     echo '<pre>';print_r($_POST);die;
            if (!$validation->fails()) {
                try{
                    $profileid = uniqueid();
                    $count = 0;
                    $results = array();
                    foreach($_POST as $post) {
                        $results[] = $post;
                    }
                    if(count($results)) {
//                   echo '<pre>'; print_r($results);die;
                        foreach ($results as $result) {
                            if(isset($result['school'])) {
                                $profileInsert = DB::getInstance()->insert('temp_education', array(
                                    'user_id' => $freelancer->data()->id,
                                    'school' => $result['school'],
                                    'qualification' => $result['qualification'],
                                    'startyear' => $result['startyear'],
                                    'endyear' => $result['endyear'],
                                    'notes' => $result['notes'],
                                    'created_at' => time(),
                                    'updated_at' => time()
                                ));
                            }
                        }
                    }
                    if (count($profileInsert) > 0) {
                        $noError = true;
                    } else {
                        $hasError = true;
                    }


                }catch(Exception $e){
                    die($e->getMessage());
                }

            } else {
                $error = '';
                foreach ($validation->errors()->all() as $err) {
                    $str = implode(" ",$err);
                    $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
                }
            }

        }
    }
}

//Edit Work Data
if(isset($_POST['form_work'])){
    if (Input::exists()) {
        if(Token::check(Input::get('token'))){
            $errorHandler = new ErrorHandler;

            $validator = new Validator($errorHandler);

            $validation =
                $validator->check(
                    $_POST[0], [
                        'employer' => [
                            'required' => true,
                            'minlength' => 2
                        ],

                        'jobtitle' => [
                            'required' => true,
                            'minlength' => 2
                        ],

                        'startyear' => [
                            'required' => true,
                            'minlength' => 2
                        ],

                        'endyear' => [
                            'required' => true,
                            'minlength' => 2
                        ],

                        'notes' => [
                            'required' => true,
                            'minlength' => 2
                        ],

                    ]
                );
//     echo '<pre>';print_r($_POST);die;
            if (!$validation->fails()) {
                try{
                    $profileid = uniqueid();
                    $count = 0;
                    $results = array();
                    foreach($_POST as $post) {
                        $results[] = $post;
                    }
                    if(count($results)) {
//                   echo '<pre>'; print_r($results);die;
                        foreach ($results as $result) {
                            if(isset($result['employer'])) {
                                $profileInsert = DB::getInstance()->insert('temp_work', array(
                                    'user_id' => $freelancer->data()->id,
                                    'employer' => $result['employer'],
                                    'jobtitle' => $result['jobtitle'],
                                    'startyear' => $result['startyear'],
                                    'endyear' => $result['endyear'],
                                    'notes' => $result['notes'],
                                    'created_at' => time(),
                                    'updated_at' => time()
                                ));
                            }
                        }
                    }
                    if (count($profileInsert) > 0) {
                        $noError = true;
                    } else {
                        $hasError = true;
                    }


                }catch(Exception $e){
                    die($e->getMessage());
                }

            } else {
                $error = '';
                foreach ($validation->errors()->all() as $err) {
                    $str = implode(" ",$err);
                    $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
                }
            }

        }
    }
}

//Edit Awards Data
if(isset($_POST['form_awards'])){
    if (Input::exists()) {
        if(Token::check(Input::get('token'))){

            $errorHandler = new ErrorHandler;

            $validator = new Validator($errorHandler);

            $validation = $validator->check($_POST, [
                'awards_desc' => [
                    'required' => true,
                    'minlength' => 2
                ]
            ]);

            if (!$validation->fails()) {

                $query = DB::getInstance()->get("profile", "*", ["userid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
                if ($query->count() === 1) {

                    //Update Awards
                    $awardsUpdate = DB::getInstance()->update('profile',[
                        'awards' => Input::get('awards_desc')
                    ],[
                        'userid' => $freelancer->data()->freelancerid
                    ]);

                    if (count($awardsUpdate) > 0) {
                        $updatedError = true;
                    } else {
                        $hasError = true;
                    }

                } else {

                    try{
                        $profileid = uniqueid();
                        $profileInsert = DB::getInstance()->insert('profile', array(
                            'profileid' => $profileid,
                            'userid' => $freelancer->data()->freelancerid,
                            'awards' => Input::get('awards_desc'),
                            'active' => 1,
                            'delete_remove' => 0,
                            'date_added' => date('Y-m-d H:i:s')
                        ));

                        if (count($profileInsert) > 0) {
                            $noError = true;
                        } else {
                            $hasError = true;
                        }


                    }catch(Exception $e){
                        die($e->getMessage());
                    }

                }

            } else {
                $error = '';
                foreach ($validation->errors()->all() as $err) {
                    $str = implode(" ",$err);
                    $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
                }
            }

        }
    }
}

//Edit Skills
if(isset($_POST['form_skills'])){
    if (Input::exists()) {
        if(Token::check(Input::get('token'))){

            $errorHandler = new ErrorHandler;

            $validator = new Validator($errorHandler);

            $validation = $validator->check($_POST, [
                'skills_name[]' => [
                    'required' => true,
                    'minlength' => 2
                ]
            ]);

            if (!$validation->fails()) {

                $query = DB::getInstance()->get("profile", "*", ["userid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
                if ($query->count() === 1) {

                    //Update Skills
                    $skills = Input::get('skills_name');
                    $choice1=implode(',',$skills);
                    $skillsUpdate = DB::getInstance()->update('profile',[
                        'skills' => $choice1
                    ],[
                        'userid' => $freelancer->data()->freelancerid
                    ]);

                    if (count($skillsUpdate) > 0) {
                        $updatedError = true;
                    } else {
                        $hasError = true;
                    }

                } else {

                    try{
                        $profileid = uniqueid();
                        $skills = Input::get('skills_name');
                        $choice1=implode(',',$skills);
                        $profileInsert = DB::getInstance()->insert('profile', array(
                            'profileid' => $profileid,
                            'userid' => $freelancer->data()->freelancerid,
                            'skills' => $choice1,
                            'active' => 1,
                            'delete_remove' => 0,
                            'date_added' => date('Y-m-d H:i:s')
                        ));

                        if (count($profileInsert) > 0) {
                            $noError = true;
                        } else {
                            $hasError = true;
                        }


                    }catch(Exception $e){
                        die($e->getMessage());
                    }

                }

            } else {
                $error = '';
                foreach ($validation->errors()->all() as $err) {
                    $str = implode(" ",$err);
                    $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
                }
            }

        }
    }
}

?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>
<!-- Bootstrap Select CSS-->
<link  href="../assets/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
<style>
    .row.singleinfo, .row.singleinfowork {
        border: 1px solid lightgrey;
        padding: 8px 0px;
        width: 100%;
        margin-left: 3px;
        margin-bottom: 10px;
    }
    #addMoreBtn, #addMoreWorkBtn {
        background-image: linear-gradient(to right,#31C427 0%, #2EB25E 51%, #217DCB 100%);
    }
    }
</style>

<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $lang['overview']; ?><small><?php echo $lang['section']; ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active"><?php echo $lang['oveview']; ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Include currency.php. Contains header content. -->
            <?php include ('template/currency.php'); ?>
            <div class="row">

                <div class="col-lg-12">
                    <?php if (isset($error)) {
                        echo $error;
                    } ?>

                    <?php if(isset($updatedError) && $updatedError == true) { //If email is sent ?>
                        <div class="alert alert-success fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong><?php echo $lang['noError']; ?></strong> <?php echo $lang['updated_success']; ?></strong>
                        </div>
                    <?php } ?>

                    <?php if(isset($hasError)) { //If errors are found ?>
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong><?php echo $lang['hasError']; ?></strong> <?php echo $lang['has_Error']; ?>
                        </div>
                    <?php } ?>

                    <?php if(isset($noError) && $noError == true) { //If email is sent ?>
                        <div class="alert alert-success fade in">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong><?php echo $lang['noError']; ?></strong> <?php echo $lang['saved_success']; ?></strong>.
                        </div>
                    <?php } ?>

                </div>

                <div class="col-lg-4">
                    <?php $profile = (Input::get('a') == 'profile') ? ' active' : ''; ?>
                    <?php $selected = (Input::get('a') == 'about') ? ' active' : ''; ?>
                    <?php $education = (Input::get('a') == 'education') ? ' active' : ''; ?>
                    <?php $work = (Input::get('a') == 'work') ? ' active' : ''; ?>
                    <?php $awards = (Input::get('a') == 'awards') ? ' active' : ''; ?>
                    <?php $skills = (Input::get('a') == 'skills') ? ' active' : ''; ?>
                    <?php $references = (Input::get('a') == 'references') ? ' active' : ''; ?>
                    <div class="list-group">
                        <a href="overview.php?a=profile" class="list-group-item<?php echo $profile; ?>">
                            <em class="fa fa-fw fa-info text-white"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['profile']; ?>
                        </a>
                        <a href="overview.php?a=about" class="list-group-item<?php echo $selected; ?>">
                            <em class="fa fa-fw fa-user-md text-white"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['about']; ?>
                        </a>
                        <a href="overview.php?a=education" class="list-group-item<?php echo $education; ?>">
                            <em class="fa fa-fw fa-text-width text-white"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['education']; ?>
                        </a>
                        <a href="overview.php?a=skills" class="list-group-item<?php echo $skills; ?>">
                            <em class="fa fa-fw fa-cogs text-white"></em>&nbsp;&nbsp;&nbsp;Temp <?php echo $lang['skills']; ?>
                        </a>
                        <a href="overview.php?a=work" class="list-group-item<?php echo $work; ?>">
                            <em class="fa fa-fw  fa-tasks text-white"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['work']; ?> History
                        </a>
                        <a href="overview.php?a=awards" class="list-group-item<?php echo $awards; ?>">
                            <em class="fa fa-fw fa-trophy text-white"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['awards']; ?>
                        </a>

                        <a href="overview.php?a=references" class="list-group-item<?php echo $references; ?>">
                            <em class="fa fa-fw fa-tasks text-white"></em>&nbsp;&nbsp;&nbsp;<?php echo $lang['references']; ?>
                        </a>

                    </div>
                </div>

                <div class="col-lg-8">
                    <?php if (Input::get('a') == 'profile') : ?>
                        <!-- Input addon -->
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><?php echo $lang['edit']; ?> <?php echo $lang['profile']; ?></h3>
                            </div>
                            <div class="box-body">
                                <form role="form" method="post" id="addstudentform">
                                    <div class="form-group">
                                        <label>Temp Type</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                            <select class="form-control" name="type_id" required>
                                                <option value="">Select Type</option>
                                                <?php
                                                foreach($temp_type AS $ttype) {
                                                    ?>
                                                    <option value="<?=$ttype->id?>" <?=($ttype->id == $type_id ? 'selected' : '')?>><?=$ttype->title?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group hidden">
                                        <label>Job Catagory</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                            <select class="form-control" name="category_id" required>
                                                <option value="">Select Type</option>
                                                <?php
                                                foreach($categories AS $category) {
                                                    ?>
                                                    <option value="<?=$category->id?>" <?=($category->id == $category_id ? 'selected' : '')?>><?=$category->name?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label><?php echo $lang['city']; ?>/Town</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                            <input type="text" name="city" class="form-control" value="<?php
                                            if (isset($_POST['form_profile'])) {
                                                echo escape(Input::get('city'));
                                            } else {
                                                echo escape($city);
                                            }
                                            ?>"/>
                                        </div>
                                    </div>
                                    <div class="form-group hidden">
                                        <label><?php echo $lang['country']; ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                                            <input type="text" name="country" class="form-control" value="<?php
                                            if (isset($_POST['form_profile'])) {
                                                echo escape(Input::get('country'));
                                            } else {
                                                echo escape($country);
                                            }
                                            ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Hourly <?php echo $lang['rate']; ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                            <select class="form-control" name="rate_id" required>
                                                <option value="">Select Rate</option>
                                                <?php
                                                foreach($temp_rate AS $trate) {
                                                    ?>
                                                    <option value="<?=$trate->id?>" <?=($trate->id == $rate_id) ? 'selected' : ''?>><?=$trate->title?>: <?=$currency_symbol?><?=$trate->rangemin?> <?=$trate->rangemax ? '-' : '+'?> <?=$trate->rangemax?> p/hr</option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Hourly <?php echo $lang['availability']; ?></label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                            <select class="form-control" name="availability_id" required>
                                                <option value="">Select Rate</option>
                                                <?php
                                                foreach($availability_all AS $availability) {
                                                    ?>
                                                    <option value="<?=$availability->id?>" <?=($availability->id == $availability_id) ? 'selected' : ''?>><?=$availability->title?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Linkedin URL</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                            <input type="text" name="website" class="form-control" value="<?php
                                            if (isset($_POST['form_profile'])) {
                                                echo escape(Input::get('website'));
                                            } else {
                                                echo escape($website);
                                            }
                                            ?>">
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                                        <button type="submit" name="form_profile" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                                    </div>
                                </form>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->


                    <?php elseif (Input::get('a') == 'references') : ?>
                        <!-- Input addon -->



                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><?php echo $lang['edit']; ?> <?php echo $lang['edit_cv_reference']; ?></h3>
                            </div>
                            <div class="col-lg-6" style="background-color: white">
                                <div class="box-body">
                                    <form role="form" method="post" id="addstudentform">
                                        <div class="form-group">
                                            <label>Name of Referee</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                                <input type="text" name="name1" class="form-control" value="<?php
                                                if (isset($_POST['form_references1'])) {
                                                    echo escape(Input::get('name1'));
                                                } else {
                                                    echo escape($temp_ref1['name']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo $lang['occupation']; ?>/Position</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                                <input type="text" name="occupation1" class="form-control" value="<?php
                                                if (isset($_POST['form_references1'])) {
                                                    echo escape(Input::get('occupation1'));
                                                } else {
                                                    echo escape($temp_ref1['occupation']);
                                                }
                                                ?>" required/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo $lang['organization']; ?></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                                                <input type="text" name="organization1" class="form-control" value="<?php
                                                if (isset($_POST['form_references1'])) {
                                                    echo escape(Input::get('organization1'));
                                                } else {
                                                    echo escape($temp_ref1['organization']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo $lang['email']; ?></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input type="text" name="email1" class="form-control" value="<?php
                                                if (isset($_POST['form_references1'])) {
                                                    echo escape(Input::get('email1'));
                                                } else {
                                                    echo escape($temp_ref1['email']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>

                                        <div class="box-footer">
                                            <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                                            <button type="submit" name="form_references1" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                                        </div>
                                    </form>
                                </div><!-- /.box-body -->
                            </div>


                            <!-- second Referee -->
                            <div class="col-lg-6" style="background-color: white">
                                <div class="box-body">
                                    <form role="form" method="post" id="addstudentform">
                                        <div class="form-group">
                                            <label>Name of Referee</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                                <input type="text" name="name2" class="form-control" value="<?php
                                                if (isset($_POST['form_references2'])) {
                                                    echo escape(Input::get('name2'));
                                                } else {
                                                    echo escape($temp_ref2['name']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo $lang['occupation']; ?>/Position</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                                <input type="text" name="occupation2" class="form-control" value="<?php
                                                if (isset($_POST['form_references2'])) {
                                                    echo escape(Input::get('occupation2'));
                                                } else {
                                                    echo escape($temp_ref2['occupation']);
                                                }
                                                ?>" required/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo $lang['organization']; ?></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                                                <input type="text" name="organization2" class="form-control" value="<?php
                                                if (isset($_POST['form_references2'])) {
                                                    echo escape(Input::get('organization2'));
                                                } else {
                                                    echo escape($temp_ref2['organization']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo $lang['email']; ?></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input type="text" name="email2" class="form-control" value="<?php
                                                if (isset($_POST['form_references2'])) {
                                                    echo escape(Input::get('email2'));
                                                } else {
                                                    echo escape($temp_ref2['email']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>

                                        <div class="box-footer">
                                            <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                                            <button type="submit" name="form_references2" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                                        </div>
                                    </form>
                                </div><!-- /.box-body -->
                            </div>


                            <!-- Third Referee -->

                            <div class="col-lg-6" style="background-color: white">
                                <div class="box-body">
                                    <form role="form" method="post" id="addstudentform">
                                        <div class="form-group">
                                            <label>Name of Referee</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                                <input type="text" name="name3" class="form-control" value="<?php
                                                if (isset($_POST['form_references3'])) {
                                                    echo escape(Input::get('name3'));
                                                } else {
                                                    echo escape($temp_ref3['name']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo $lang['occupation']; ?>/Position</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                                <input type="text" name="occupation3" class="form-control" value="<?php
                                                if (isset($_POST['form_references3'])) {
                                                    echo escape(Input::get('occupation3'));
                                                } else {
                                                    echo escape($temp_ref3['occupation']);
                                                }
                                                ?>" required/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo $lang['organization']; ?></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                                                <input type="text" name="organization3" class="form-control" value="<?php
                                                if (isset($_POST['form_references3'])) {
                                                    echo escape(Input::get('organization3'));
                                                } else {
                                                    echo escape($temp_ref3['organization']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo $lang['email']; ?></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input type="text" name="email3" class="form-control" value="<?php
                                                if (isset($_POST['form_references3'])) {
                                                    echo escape(Input::get('email3'));
                                                } else {
                                                    echo escape($temp_ref3['email']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>

                                        <div class="box-footer">
                                            <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                                            <button type="submit" name="form_references3" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                                        </div>
                                    </form>
                                </div><!-- /.box-body -->
                            </div>

                            <!-- Fourth Referee -->

                            <div class="col-lg-6" style="background-color: white">
                                <div class="box-body">
                                    <form role="form" method="post" id="addstudentform">
                                        <div class="form-group">
                                            <label>Name of Referee</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                                <input type="text" name="name4" class="form-control" value="<?php
                                                if (isset($_POST['form_references4'])) {
                                                    echo escape(Input::get('name4'));
                                                } else {
                                                    echo escape($temp_ref4['name']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo $lang['occupation']; ?>/Position</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-info"></i></span>
                                                <input type="text" name="occupation4" class="form-control" value="<?php
                                                if (isset($_POST['form_references4'])) {
                                                    echo escape(Input::get('occupation4'));
                                                } else {
                                                    echo escape($temp_ref4['occupation']);
                                                }
                                                ?>" required/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo $lang['organization']; ?></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-flag"></i></span>
                                                <input type="text" name="organization4" class="form-control" value="<?php
                                                if (isset($_POST['form_references4'])) {
                                                    echo escape(Input::get('organization4'));
                                                } else {
                                                    echo escape($temp_ref4['organization']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label><?php echo $lang['email']; ?></label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                <input type="text" name="email4" class="form-control" value="<?php
                                                if (isset($_POST['form_references4'])) {
                                                    echo escape(Input::get('email4'));
                                                } else {
                                                    echo escape($temp_ref4['email']);
                                                }
                                                ?>" required>
                                            </div>
                                        </div>

                                        <div class="box-footer">
                                            <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                                            <button type="submit" name="form_references4" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                                        </div>
                                    </form>
                                </div><!-- /.box-body -->
                            </div>

                            <!--  Fourth referee Ends -->


                        </div><!-- /.box -->








                    <?php elseif (Input::get('a') == 'about') : ?>
                        <!-- Input addon -->
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><?php echo $lang['edit']; ?> <?php echo $lang['about']; ?> <?php echo $lang['details']; ?></h3>
                            </div>
                            <div class="box-body">
                                <form role="form" method="post" id="editform">

                                    <div class="form-group">
                                        <label><?php echo $lang['about']; ?> <?php echo $lang['description']; ?></label>
                                        <textarea type="text" id="summernote" name="about_desc" class="form-control">
                      	<?php
                        if (isset($_POST['form_about'])) {
                            echo escape(Input::get('about_desc'));
                        } else {
                            echo escape($about);
                        }
                        ?></textarea>
                                    </div>

                                    <div class="box-footer">
                                        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                                        <button type="submit" name="form_about" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                                    </div>
                                </form>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                        <?php

                    elseif (Input::get('a') == 'education') : ?>
                        <!-- Input addon -->
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><?php echo $lang['edit']; ?> <?php echo $lang['education']; ?> <?php echo $lang['details']; ?></h3>
                            </div>
                            <div class="box-body">
                                <form role="form" method="post">

                                    <div class="form-group">
                                        <label><?php echo $lang['education']; ?> <?php echo $lang['description']; ?></label>
                                        <div id="infoforms">
                                            <div class="row singleinfo">
                                                <span class="col-lg-2"><input type="text" name="userinfo_0[school]" placeholder="School" required></span>
                                                <span class="col-lg-2"><input type="text" name="userinfo_0[qualification]" placeholder="Qualification" required></span>
                                                <span class="col-lg-2"><input type="text" name="userinfo_0[startyear]" placeholder="Start Year" required></span>
                                                <span class="col-lg-2"><input type="text" name="userinfo_0[endyear]" placeholder="End Year" required></span>
                                                <span class="col-lg-2"><input type="text" name="userinfo_0[notes]" placeholder="Notes"></span>
                                                <span class="col-lg-2"><input type="button" value="Remove" onclick="removeSingle(this)"></span>
                                            </div>
                                        </div>
                                        <input type="button" id="addMoreBtn" value="Add More">
                                        <script>
                                            var count = 1;
                                            var formstr = `<div class="row singleinfo">
                                  <span class="col-lg-2"><input type="text" name="userinfo_${count}[school]" placeholder="School" required></span>
                                  <span class="col-lg-2"><input type="text" name="userinfo_${count}[qualification]" placeholder="Qualification" required></span>
                                  <span class="col-lg-2"><input type="text" name="userinfo_${count}[startyear]" placeholder="Start Year" required></span>
                                  <span class="col-lg-2"><input type="text" name="userinfo_${count}[endyear]" placeholder="End Year" required></span>
                                  <span class="col-lg-2"><input type="text" name="userinfo_${count}[notes]" placeholder="Notes"></span>
                                  <span class="col-lg-2"><input type="button" value="Remove"onclick="removeSingle(this)"></span>
                              </div>`;
                                            $(function () {
                                                $('#addMoreBtn').click(function (e) {
                                                    e.preventDefault;
                                                    $('#infoforms').append(formstr);
                                                })
                                            })
                                            function removeSingle(thisRef) {
                                                $(thisRef).closest('div.row.singleinfo').remove();
                                            }
                                        </script>
                                        <?php /*
                      <table class="table">
                          <thead>
                              <tr>
                                  <td>School</td>
                                  <td>Qualification</td>
                                  <td>Start Year</td>
                                  <td>End Year</td>
                                  <td>Notes</td>
                                  <td></td>
                              </tr>
                          </thead>
                          <tbody>
                      	<?php
                        if(isset($allDataRows) && count($allDataRows)) {
                            foreach($allDataRows as $row) {
                                ?>
                                <tr>
                                    <td><?=$row->school ?></td>
                                    <td><?=$row->qualification ?></td>
                                    <td><?=$row->startyear ?></td>
                                    <td><?=$row->endyear ?></td>
                                    <td><?=$row->notes ?></td>
                                    <td><span class="fa fa-edit"></span><span class="fa fa-remove"></span></td>
                                </tr>
                                <?php
                            }
                        }
//                         if (isset($_POST['form_education'])) {
//							 echo escape(Input::get('education_desc'));
//						  } else {
//						  echo escape($education_profile);
//						  }
					   */ ?>
                                        </tbody>
                                        </table>
                                    </div>

                                    <div class="box-footer">
                                        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                                        <button type="submit" name="form_education" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                                    </div>
                                </form>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->

                    <?php elseif (Input::get('a') == 'work') : ?>
                        <!-- Input addon -->
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><?php echo $lang['edit']; ?> <?php echo $lang['work']; ?> <?php echo $lang['details']; ?></h3>
                            </div>
                            <div class="box-body">
                                <form role="form" method="post">

                                    <div class="form-group">
                                        <label><?php echo $lang['work']; ?> <?php echo $lang['description']; ?></label>
                                        <div id="infoformswork">
                                            <div class="row singleinfowork">
                                                <span class="col-lg-2"><input type="text" name="userinfo_0[employer]" placeholder="Employer" required></span>
                                                <span class="col-lg-2"><input type="text" name="userinfo_0[jobtitle]" placeholder="Job Title" required></span>
                                                <span class="col-lg-2"><input type="text" name="userinfo_0[startyear]" placeholder="Start Year" required></span>
                                                <span class="col-lg-2"><input type="text" name="userinfo_0[endyear]" placeholder="End Year" required></span>
                                                <span class="col-lg-2"><input type="text" name="userinfo_0[notes]" placeholder="Notes"></span>
                                                <span class="col-lg-2"><input type="button" value="Remove" onclick="removeSingleWork(this)"></span>
                                            </div>
                                        </div>
                                        <input type="button" id="addMoreWorkBtn" value="Add More">
                                        <script>
                                            var count = 1;
                                            var formstr = `<div class="row singleinfowork">
                                                              <span class="col-lg-2"><input type="text" name="userinfo_${count}[employer]" placeholder="Employer" required></span>
                                                              <span class="col-lg-2"><input type="text" name="userinfo_${count}[jobtitle]" placeholder="Job Title" required></span>
                                                              <span class="col-lg-2"><input type="text" name="userinfo_${count}[startyear]" placeholder="Start Year" required></span>
                                                              <span class="col-lg-2"><input type="text" name="userinfo_${count}[endyear]" placeholder="End Year" required></span>
                                                              <span class="col-lg-2"><input type="text" name="userinfo_${count}[notes]" placeholder="Notes"></span>
                                                              <span class="col-lg-2"><input type="button" value="Remove"onclick="removeSingleWork(this)"></span>
                                                          </div>`;
                                            $(function () {
                                                $('#addMoreWorkBtn').click(function (e) {
                                                    e.preventDefault;
                                                    $('#infoformswork').append(formstr);
                                                })
                                            })
                                            function removeSingleWork(thisRef) {
                                                $(thisRef).closest('div.row.singleinfowork').remove();
                                            }
                                        </script>
                                        <?php /*
                      <table class="table">
                          <thead>
                              <tr>
                                  <td>School</td>
                                  <td>Qualification</td>
                                  <td>Start Year</td>
                                  <td>End Year</td>
                                  <td>Notes</td>
                                  <td></td>
                              </tr>
                          </thead>
                          <tbody>
                      	<?php
                        if(isset($allDataRows) && count($allDataRows)) {
                            foreach($allDataRows as $row) {
                                ?>
                                <tr>
                                    <td><?=$row->school ?></td>
                                    <td><?=$row->qualification ?></td>
                                    <td><?=$row->startyear ?></td>
                                    <td><?=$row->endyear ?></td>
                                    <td><?=$row->notes ?></td>
                                    <td><span class="fa fa-edit"></span><span class="fa fa-remove"></span></td>
                                </tr>
                                <?php
                            }
                        }
//                         if (isset($_POST['form_education'])) {
//							 echo escape(Input::get('education_desc'));
//						  } else {
//						  echo escape($education_profile);
//						  }
					   */ ?>
                                        </tbody>
                                        </table>
                                    </div>

                                    <div class="box-footer">
                                        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                                        <button type="submit" name="form_work" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                                    </div>
                                </form>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->

                        <?php

                    elseif (Input::get('a') == 'awards') : ?>
                        <!-- Input addon -->
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><?php echo $lang['edit']; ?> <?php echo $lang['awards']; ?> <?php echo $lang['details']; ?></h3>
                            </div>
                            <div class="box-body">
                                <form role="form" method="post" id="editform">

                                    <div class="form-group">
                                        <label><?php echo $lang['awards']; ?> <?php echo $lang['and']; ?> <?php echo $lang['achievements']; ?></label>
                                        <textarea type="text" id="summernote" name="awards_desc" class="form-control">
                      	<?php
                        if (isset($_POST['form_awards'])) {
                            echo escape(Input::get('awards_desc'));
                        } else {
                            echo escape($awards_profile);
                        }
                        ?></textarea>
                                    </div>

                                    <div class="box-footer">
                                        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                                        <button type="submit" name="form_awards" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                                    </div>
                                </form>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->

                        <?php

                    elseif (Input::get('a') == 'skills') : ?>

                        <!-- Input addon -->
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><?php echo $lang['edit']; ?> <?php echo $lang['skills']; ?> <?php echo $lang['details']; ?></h3>
                            </div>
                            <div class="box-body">
                                <form role="form" method="post" id="editform">

                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-pencil-square"></i></span>
                                            <select class="selectpicker form-control" name="skills_name[]" type="text" title="Edit Skills List" data-live-search="true" data-width="30%" data-selected-text-format="count > 3" multiple="multiple">
                                                <?php

                                                $query = DB::getInstance()->get("profile", "*", ["userid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
                                                if ($query->count()) {
                                                    foreach($query->results() as $row) {
                                                        $skills = $row->skills;
                                                        $arr=explode(',',$skills);
                                                    }
                                                }

                                                $query = DB::getInstance()->get("temp_skill", "*", ["ORDER" => "name ASC"]);
                                                if ($query->count()) {
                                                    foreach($query->results() as $row) {
                                                        $names[] = $row->title;
                                                    }
                                                }

                                                foreach($names as $key=>$name){
                                                    if(in_array($name,$arr)){
                                                        echo $skills .= '<option value = "'.$name.'" data-tokens="'.$name.'" selected="selected">'.$name.'</option>';
                                                        unset($skills);
                                                    }else{
                                                        echo $skills .= '<option value = "'.$name.'" data-tokens="'.$name.'" >'.$name.'</option>';
                                                        unset($skills);
                                                    }
                                                    unset($name);
                                                }

                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="box-footer">
                                        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                                        <button type="submit" name="form_skills" class="btn btn-primary full-width"><?php echo $lang['submit']; ?></button>
                                    </div>
                                </form>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->


                              <!-- Input addon -->
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title"><?php echo $lang['your']; ?> <?php echo $lang['skills']; ?></h3>
                            </div>
                            <div class="box-body">

                                <?php
                                $query = DB::getInstance()->get("profile", "*", ["userid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
                                if ($query->count()) {
                                    foreach($query->results() as $row) {
                                        $skills = $row->skills;
                                        $arr=explode(',',$skills);
                                    }
                                }

                                foreach ($arr as $key => $value) {
                                    echo '<label class="label label-success">'. $value .'</label> &nbsp;';
                                }
                                ?>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->


                    <?php endif; ?>



                </div><!-- /.col -->



            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->


<!-- ==============================================
 Scripts
 =============================================== -->

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
<!-- Summernote WYSIWYG-->
<script src="../assets/js/summernote.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 300,                 // set editor height

            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor

            focus: false,                 // set focus to editable area after initializing summernote
        });
    });
</script>
<!-- Bootstrap Select JS-->
<script src="../assets/js/bootstrap-select.js"></script>
