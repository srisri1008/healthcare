<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
    Redirect::to('../index.php');
}


//Add Instructor Function
if (Input::exists()) {
    if(Token::check(Input::get('token'))){

        $errorHandler = new ErrorHandler;

        $validator = new Validator($errorHandler);

        $validation = $validator->check($_POST, [
            'name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 200
            ],
            'title' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 200
            ],
            'team_desc' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 500
            ],
            'facebook' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 200
            ],
            'twitter' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 200
            ],
            'linkedin' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 200
            ]
        ]);

        if (!$validation->fails()) {

            $path = "uploads/team/";
            $valid_formats = array("jpg", "png", "gif", "bmp");

            $name = $_FILES['photoimg']['name'];
            $size = $_FILES['photoimg']['size'];

            if(strlen($name))
            {
                list($txt, $ext) = explode(".", $name);
                if(in_array($ext,$valid_formats) && $size<(700*400))
                {
                    $actual_image_name = uniqid().time().substr($txt, 5).".".$ext;
                    $image_name = $actual_image_name;
                    $newname=$path.$image_name;
                    $tmp = $_FILES['photoimg']['tmp_name'];
                    if(move_uploaded_file($tmp, $path.$actual_image_name))
                    {

                        try{
                            $teamid = uniqueid();
                            $teamInsert = DB::getInstance()->insert('team', array(
                                'description' => Input::get('team_desc'),
                                'teamid' => $teamid,
                                'userid' => $freelancer->data()->freelancerid,
                                'name' => Input::get('name'),
                                'title' => Input::get('title'),
                                'facebook' => Input::get('facebook'),
                                'twitter' => Input::get('twitter'),
                                'linkedin' => Input::get('linkedin'),
                                'imagelocation' => $newname,
                                'active' => 1,
                                'delete_remove' => 0,
                                'date_added' => date('Y-m-d H:i:s')
                            ));

                            if (count($teamInsert) > 0) {
                                $noError = true;
                            } else {
                                $hasError = true;
                            }


                        }catch(Exception $e){
                            die($e->getMessage());
                        }

                    }else{
                        $imageError = true;
                    }
                }else{
                    $formatError = true;
                }
            }else{
                $selectError = true;
            }

        } else {
            $error = '';
            foreach ($validation->errors()->all() as $err) {
                $str = implode(" ",$err);
                $error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
            }
        }

    }
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>

<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Withdrawal<small><?php echo $lang['section']; ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active">Withdrawal Method</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Include currency.php. Contains header content. -->
            <?php include ('template/currency.php'); ?>
            <style>
                .maindiv {
                    text-align: center;
                }
                .maindiv button {
                    margin: 10px;
                    padding: 10px 40px;
                    width: 250px;
                }
                .btn-success {
                    color: #fff !important;
                    background-color: #5cb85c !important;
                    border-color: #4cae4c !important;
                }
                .btn-success:hover {
                    color: #fff;
                    background-color: #449d44 !important;
                    border-color: #398439 !important;
                }
            </style>
            <div class="container">
                <h2 style="margin-bottom: 80px;">Choose Withdrawal Method</h2>
                <div class="maindiv">
                    <div class="row">
                        <button class="btn btn-success">Paypal</button>
                        <button class="btn btn-success">Crypto Currency</button>
                    </div>
                    <div class="row">
                        <button class="btn btn-success">Stripe</button>
                        <button class="btn btn-success">Crypto Currency</button>
                    </div>
                    <button class="btn btn-success">Skrill</button>
                </div>
            </div>
</body>
</html>

</section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->


<!-- ==============================================
 Scripts
 =============================================== -->

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
</body>
</html>
