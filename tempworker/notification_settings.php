<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Freelancer object
$freelancer = new Freelancer();
$dbObj = new DB();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
    Redirect::to('../index.php');
}

function checked($item) {
    $status = '';
    if(!empty($item) && ($item == 1)) {
        $status = 'checked';
    }
    return $status;
}

if(isset($_POST['submit'])) {
    //Check if record exists for the user
    $isExists = $dbObj->getData("SELECT id FROM notification_settings WHERE freelancer_id = ". $freelancer->data()->freelancerid);
    if(count($isExists)) {
        $sqlFront = "UPDATE notification_settings SET";
        $sqlMiddle = "";
        foreach ($_POST as $k => $v) {
            if($k == 'submit') {
                continue;
            }
            $sqlMiddle .= "$k = $v,";
        }
        $sqlMiddle .= "updated_at = ". time();
        $sqlEnd = "WHERE freelancer_id = ". $freelancer->data()->freelancerid;
        $sqlTotal = $sqlFront .' '. $sqlMiddle .' '. $sqlEnd;
        $dbObj->query($sqlTotal);
        Redirect::to($_SERVER['PHP_SELF']);
    }
    else {
        $sqlFront = "INSERT INTO notification_settings";
        $sqlKeys = "";
        $sqlValues = "";
        foreach ($_POST as $k => $v) {
            if($k == 'submit') {
                continue;
            }
            $sqlKeys .= "$k,";
            $sqlValues .= "$v,";
        }
        $sqlKeys = "(" .$sqlKeys. "freelancer_id, updated_at)";
        $sqlValues = "(" . $sqlValues . $freelancer->data()->freelancerid .", ".time().")";
        $sqlTotal = "$sqlFront $sqlKeys VALUES $sqlValues";
        $dbObj->query($sqlTotal);
        Redirect::to($_SERVER['PHP_SELF']);
    }
}
$checkExist = false;
$checkData = $dbObj->getData("SELECT * FROM notification_settings WHERE freelancer_id = ". $freelancer->data()->freelancerid);
if(count($checkData)) {
    $checkData = $checkData[0];
    $checkExist = true;
}

?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>
<!-- Bootstrap Select CSS-->
<link  href="../assets/css/bootstrap-select.css" rel="stylesheet" type="text/css" />

<body class="skin-green sidebar-mini">
<style>
    .notification {
        font-size: 25px;
        background-color: #f0f0f0;
        padding: 6px;
    }
    .checkbox {
        margin-top: 20px !important;
    }

    .checkboxes input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #ffffff;
        border: 1px solid lightgrey;
    }

    .checkboxes input:checked ~ .checkmark {
        background-color: #3ac01e;
    }

    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    .checkboxes input:checked ~ .checkmark:after {
        display: block;
    }

    .checkboxes .checkmark:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
    .checkbox_heading {
        margin-left: 26px;
        font-size: 16px;
    }
    .savebtn {
        margin-top: 15px;
        margin-left: 10px;
        background-color: #3ac01e !important;
        padding: 10px 30px;
    }
</style>
<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Notification Settings<small><?php echo $lang['section']; ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active">Notification settings</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" style="padding-left: 40px">
            <div class="container">
                <div class="row checkboxes">
                    <span class="notification">Notification</span>&emsp;Send me push notifications via email or sms relating to:
                    <form action="" method="post">
                        <div class="checkbox">
                            <label>
                                <span class="checkbox_heading">New temp jobs posted</span>
                                <input type="hidden" value="0" name="job_posted">
                                <input type="checkbox" value="1" name="job_posted" <?=$checkExist ? checked($checkData['job_posted']) : ''?>>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <span class="checkbox_heading">An interview is initiated</span>
                                <input type="hidden" value="0" name="interview_initiated">
                                <input type="checkbox" value="1" name="interview_initiated" <?=$checkExist ? checked($checkData['interview_initiated']) : ''?>>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <span class="checkbox_heading">An offer or interview invitation is received</span>
                                <input type="hidden" value="0" name="offer_received">
                                <input type="checkbox" name="offer_received" value="1" <?=$checkExist ? checked($checkData['offer_received']) : ''?>>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <span class="checkbox_heading">An offer is rejected</span>
                                <input type="hidden" value="0" name="offer_rejected">
                                <input type="checkbox" value="1" name="offer_rejected" <?=$checkExist ? checked($checkData['offer_rejected']) : ''?>>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <span class="checkbox_heading">An offer or interview invitation is withdrawn</span>
                                <input type="hidden" value="0" name="offer_withdrawn">
                                <input type="checkbox" value="1" name="offer_withdrawn" <?=$checkExist ? checked($checkData['offer_withdrawn']) : ''?>>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <span class="checkbox_heading">A proposal is rejected</span>
                                <input type="hidden" value="0" name="proposal_rejected">
                                <input type="checkbox" value="1" name="proposal_rejected" <?=$checkExist ? checked($checkData['proposal_rejected']) : ''?>>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <span class="checkbox_heading">A job I applied is to modified or cancelled</span>
                                <input type="hidden" value="0" name="job_modified">
                                <input type="checkbox" value="1" name="job_modified" <?=$checkExist ? checked($checkData['job_modified']) : ''?>>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <span class="checkbox_heading">New bonuses or freebies on offer</span>
                                <input type="hidden" value="0" name="offer_bonuses">
                                <input type="checkbox" value="1" name="offer_bonuses" <?=$checkExist ? checked($checkData['offer_bonuses']) : ''?>>
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <button class="btn btn-success savebtn" type="submit" name="submit">Save</button>
                    </form>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->


<!-- ==============================================
 Scripts
 =============================================== -->

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
<!-- Summernote WYSIWYG-->
<script src="../assets/js/summernote.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 300,                 // set editor height

            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor

            focus: false,                 // set focus to editable area after initializing summernote
        });
    });
</script>
<!-- Bootstrap Select JS-->
<script src="../assets/js/bootstrap-select.js"></script>

</body>
</html>
<!--Done-->
