<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
  Redirect::to('../index.php');	
}

//Get Instructor's Data
$query = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
if ($query->count()) {
 foreach($query->results() as $row) {
 	$nid = $row->id;
 	$name = $row->name;
 	$username = $row->username;
 	$email = $row->email;
 	$phone = $row->phone;
 	$bgimage = $row->bgimage;
 }			
}	

//Get Social Data
$querySocial = DB::getInstance()->get("freelancer_info", "*", ["freelancerid" => $freelancer->data()->id, "LIMIT" => 1]);
if ($querySocial->count()) {
    $flag = 1;
    $socialArr = array();
    foreach($querySocial->results() as $row) {
        $socialArr = $row;
    }
}

//Get Payments Settings Data
$query = DB::getInstance()->get("payments_settings", "*", ["id" => 1]);
if ($query->count()) {
    foreach ($query->results() as $row) {
        $pid = $row->id;
        $currencyid = $row->currency;
        $paypal_client_id = $row->paypal_client_id;
        $paypal_secret = $row->paypal_secret;
        $stripe_secret_key = $row->stripe_secret_key;
        $stripe_publishable_key = $row->stripe_publishable_key;
        $membershipid = $row->membershipid;
        $jobs_cost = $row->jobs_cost;
        $jobs_pay_limit = $row->jobs_pay_limit;
        $bids_cost = $row->bids_cost;
        $bids_limit = $row->bids_limit;
        $jobs_percentage = $row->jobs_percentage;
        $employer_name = $row->employer_name;
        $job_description = $row->job_description;
        $location = $row->location;
        $t_type = $row->temp_type;
        $salary_rate = $row->salary_rate;
        $date_posted = $row->date_posted;
    }
}

//Update Social Data
if(isset($_POST['updatebtn'])){
if (Input::exists()) {
 if(Token::check(Input::get('token'))){

	$errorHandler = new ErrorHandler;

	$validator = new Validator($errorHandler);

	$validation = $validator->check($_POST, [
	  'phone' => [
	     'required' => true,
	  ]
	]);

    if (!$validation->fails()) {
		//Info update
        $arrToPassInfo = [];
        $infoArr = $_POST['userInfo'];
        foreach ($infoArr AS $k => $v) {
            $arrToPassInfo[$k] = $v;
        }
        //Check if already exists or not
        if(isset($flag)) {
            //Update Freelancer Info
            $freelancerInfoUpdate = DB::getInstance()->update('freelancer_info', $arrToPassInfo, [
                'freelancerid' => $nid
            ]);
            if($freelancerInfoUpdate) {
            header('Location: ' . $_SERVER['PHP_SELF']);
                $updatedError = true;
            } else {
                $hasError = true;
            }
        } else {
            //Insert Freelancer Info
            $arrToPassInfo['freelancerid'] = $nid;
            $freelancerInfoInsert = DB::getInstance()->insert('freelancer_info', $arrToPassInfo);
            if($freelancerInfoInsert) {
            header('Location: ' . $_SERVER['PHP_SELF']);
                $updatedError = true;
            } else {
                $hasError = true;
            }
        }



	 } else {
     $error = '';
     foreach ($validation->errors()->all() as $err) {
     	$str = implode(" ",$err);
     	$error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
     }
   }

  }
 }
}

?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('template/header.php'); ?> 

 <body class="skin-green sidebar-mini">
     
     <!-- ==============================================
     Wrapper Section
     =============================================== -->
	 <div class="wrapper">
	 	
        <!-- Include navigation.php. Contains navigation content. -->
	 	<?php include ('template/navigation.php'); ?> 
        <!-- Include sidenav.php. Contains sidebar content. -->
	 	<?php include ('template/sidenav.php'); ?> 
	 	
	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Social Media<small class="hidden"><?php echo $lang['section']; ?></small></h1>
          <ol class="breadcrumb">
            <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Social Media</li>
          </ol>
        </section>

        <!-- Main content -->
          <div class="container" style="margin-top: 20px;">
            <form method="post">
              <div class="row">
                  <div class="col-lg-offset-1 col-lg-10">
                      <h4 style="padding:15px 20px; background-color: #1B97E0;">Social Media Links</h4>
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-5 col-lg-offset-1">
                      <div class="form-group">
                          <label>Facebook</label>
                          <input type="url" name="userInfo[fb_url]" value="<?=isset($flag) ? $socialArr->fb_url : ''?>" placeholder="Your Facebook URL starts with http://" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>LinkedIn URL</label>
                          <input type="url" name="userInfo[linkedin_url]" value="<?=$flag ? $socialArr->linkedin_url : ''?>" placeholder="Your LinkedIn URL starts with http://" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>MSN</label>
                          <input type="text" name="userInfo[msn_url]" value="<?=$flag ? $socialArr->msn_url : ''?>" placeholder="Your MSN ID" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>Skype</label>
                          <input type="text" name="userInfo[skype_url]" value="<?=$flag ? $socialArr->skype_url : ''?>" placeholder="Your Skype ID" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>Phone</label>
                          <input type="tel" name="userInfo[phone]" value="<?=$flag ? $socialArr->phone : ''?>" placeholder="Your Phone No" class="form-control" required>
                      </div>
                  </div>
                  <div class="col-lg-5">
                      <div class="form-group">
                          <label>Website</label>
                          <input type="url" name="userInfo[web_url]" value="<?=$flag ? $socialArr->web_url : ''?>" placeholder="Your Websites URL starts with http://" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>Yahoo</label>
                          <input type="url" name="userInfo[yahoo_url]" value="<?=$flag ? $socialArr->yahoo_url : ''?>" placeholder="Your Yahoo URL starts with http://" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>Oovoo</label>
                          <input type="text" name="userInfo[oovoo_url]" value="<?=$flag ? $socialArr->oovoo_url : ''?>" placeholder="Your Oovoo ID" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>Gruveo</label>
                          <input type="email" name="userInfo[gruveo_url]" value="<?=$flag ? $socialArr->gruveo_url : ''?>" placeholder="Your Gruveo email" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>Mobile</label>
                          <input type="tel" name="userInfo[mobile]" value="<?=$flag ? $socialArr->mobile : ''?>" placeholder="Your Mobile Number" class="form-control">
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-offset-1 col-lg-10">
                    <h4 style="padding:15px 20px; background-color: #1B97E0;">Declaration</h4>
                  </div>
                  <div class="col-lg-offset-1 col-lg-10">
                      <div class="checkbox">
                          <label><input type="checkbox" class="checkbox" required>I hereby declare that the information/data provided above is true and accurate to my best knowledge. I fully understand, that I will be held liable and/or accountable for the accuracy of the information supplied above and I could be legally prosecuted for providing false data</label>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-offset-1 col-lg-10">
                      <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
                    <input class="btn" type="submit" name="updatebtn" value="Update" style="background-color: #1B97E0;">
                  </div>
              </div>
          </form>
          </div>

<!--          Single featured jobs box-->
          <?php
          if(isset($t_type)) {
              ?>
              <div class="container">
                  <div class="row col-lg-offset-1">
                      <h3 style="color: #d62134">Featured Jobs</h3>
                      <div class="col-sm-3 col-md-3 col-lg-2" style="border: 5px solid #d62134">
                          <h4>Job Title: </h4>
                          <h4>Salary/Rate: </h4>
                          <h4>Location: </h4>
                          <h4>Job Posted: </h4>
                          <h4>Employer: </h4>
                      </div>
                      <div class="col-sm-3 col-md-3 col-lg-3" style="border: 5px solid #d62134">
                          <h4><?=ucfirst($t_type)?></h4>
                          <h4><?=$salary_rate?></h4>
                          <h4><?=$location?></h4>
                          <h4><?=$date_posted?></h4>
                          <h4><?=ucfirst($employer_name)?></h4>
                      </div>
                  </div>
                  <div class="row col-lg-offset-1">
                      <div class="col-sm-6 col-md-6 col-lg-5" style="text-align: center; border: 5px solid #d62134">
                          <p><?=$job_description?></p>
                      </div>
                  </div>
              </div>
              <?php
          }
          ?>
<!--          single featured jobs box end-->
      </div><!-- /.content-wrapper -->
	 	
      <!-- Include footer.php. Contains footer content. -->	
	  <?php include 'template/footer.php'; ?>	
	 	
     </div><!-- /.wrapper -->   

	
	<!-- ==============================================
	 Scripts
	 =============================================== -->
	 
    <!-- jQuery 2.1.4 -->
    <script src="../assets/js/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.6 JS -->
    <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../assets/js/app.min.js" type="text/javascript"></script>
    
</body>
</html>
