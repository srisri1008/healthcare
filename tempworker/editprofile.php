<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
  Redirect::to('../index.php');	
}

//Get Instructor's Data
$query = DB::getInstance()->get("freelancer", "*", ["freelancerid" => $freelancer->data()->freelancerid, "LIMIT" => 1]);
if ($query->count()) {
 foreach($query->results() as $row) {
 	$nid = $row->id;
 	$name = $row->name;
 	$username = $row->username;
 	$email = $row->email;
 	$phone = $row->phone;
 	$bgimage = $row->bgimage;
 }			
}

$conn = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'));
$conn->exec("SET CHARACTER SET utf8");
$sql = "SELECT * FROM freelancer WHERE freelancerid = " . $freelancer->data()->freelancerid;
$result = $conn->query($sql);
$row = $result->fetch(PDO::FETCH_NUM);
$msg = '';
$row[0] = $freelancer->data()->freelancerid;

if (isset($row[0]) && $row[0] != '') {
    if (isset($_POST['_method'])) {
        $appendTo = "";
        $sql = "SELECT * FROM freelancer_info WHERE freelancerid = ". $row[0];
        $res = $conn->query($sql)->fetch(PDO::FETCH_ASSOC);
        if(count($res) > 1) {
            foreach ($_POST as $k => $v) {
                if($k != '_method' && $k != 'data') {
                    $appendTo .= $k .'= "'. $v . '",';
                }
            }
            $appendTo = rtrim($appendTo, ',');
            $sql = "UPDATE freelancer_info SET ". $appendTo . " WHERE freelancerid = ". $row[0];
            $conn->query($sql);
        } else {
            $keys = "freelancerid,";
            $values = "$row[0],";
            foreach ($_POST as $k => $v) {
                if($k != '_method' && $k != 'data' && !empty($v)) {
                    $keys .= "$k,";
                    $values .= "'$v',";
                }
            }
            $keys = rtrim($keys, ',');
            $values = rtrim($values, ',');

            $sql = "INSERT INTO freelancer_info (" .$keys. ") VALUES (" .$values. ")";
            $conn->query($sql);
        }
        die('68');
        Redirect::to('temp_upload_5.php?id='. $_GET['id']);
    }
} else {
    die('72');
    Redirect::to('register.php');
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>

    <!-- ==============================================
    Title and Meta Tags
    =============================================== -->
    <meta charset="utf-8">
    <title><?php echo escape($title) . ' - ' . escape($tagline); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo escape($description); ?>">
    <meta name="keywords" content="<?php echo escape($keywords); ?>">
    <meta name="author" content="<?php echo escape($author); ?>">

    <!-- ==============================================
    Favicons
    =============================================== -->
    <link rel="shortcut icon" href="img/favicons/favicon.ico">
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-touch-icon-114x114.png">

    <!-- ==============================================
    CSS
    =============================================== -->
    <!-- Style-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="assets/css/login.css" rel="stylesheet" type="text/css">

    <!-- ==============================================
    Feauture Detection
    =============================================== -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="assets/js/modernizr-custom.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>

<!-- Paste this code after body tag -->
<div class="loader">
    <div class="se-pre-con"></div>
</div>


<?
$basename = basename($_SERVER["REQUEST_URI"], ".php");
$editname = basename($_SERVER["REQUEST_URI"]);
$test = $_SERVER["REQUEST_URI"];
?>
<!-- ==============================================
Navigation Section
=============================================== -->
<header id="header" headroom="" role="banner" tolerance="5" offset="700"
        class="navbar navbar-fixed-top navbar--white ng-isolate-scope headroom headroom--top">
    <nav role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle header-nav__button" data-toggle="collapse" data-target=".navbar-main">
                <span class="icon-bar header-nav__button-line"></span>
                <span class="icon-bar header-nav__button-line"></span>
                <span class="icon-bar header-nav__button-line"></span>
            </button>
            <div class="header-nav__logo">
                <a class="header-nav__logo-link navbar-brand" href="index.php">
                    <?php if ($use_icon === '1'): ?>
                        <i class="fa <?php echo $site_icon; ?>"></i>
                    <?php endif; ?>  <?php echo escape($title); ?></a>
            </div>
        </div>
        <div class="collapse navbar-collapse navbar-main navbar-right">
            <ul class="nav navbar-nav header-nav__navigation">
                <li class="header-nav__navigation-item
	         <?php echo $active = ($_SERVER['HTTP_HOST'] && $test === '') ? ' active' : '';
                echo $active = ($basename == 'index') ? ' active' : ''; ?>">
                    <a href="index.php" class="header-nav__navigation-link">
                        <?php echo $lang['home']; ?>
                    </a>
                </li>
                <li class="header-nav__navigation-item <?php echo $active = ($basename == 'jobs') ? ' active' : '';
                echo $active = ($editname == 'jobpost.php?title=' . Input::get('title') . '') ? ' active' : ''; ?>">
                    <a href="newjobs.php" class="header-nav__navigation-link ">
                        <?php echo $lang['jobs']; ?>
                    </a>
                </li>
                <li class="header-nav__navigation-item <?php echo $active = ($basename == 'services') ? ' active' : '';
                echo $active = ($editname == 'tempworker.php?a=' . Input::get('a') . '&id=' . Input::get('id') . '') ? ' active' : ''; ?>">
                    <a href="services.php" class="header-nav__navigation-link ">
                        <?php echo $lang['services']; ?>
                    </a>
                </li>
                <li class="header-nav__navigation-item <?php echo $active = ($basename == 'about') ? ' active' : ''; ?>">
                    <a href="about.php" class="header-nav__navigation-link ">
                        <?php echo $lang['about']; ?>
                    </a>
                </li>
                <li class="header-nav__navigation-item <?php echo $active = ($basename == 'how') ? ' active' : ''; ?>">
                    <a href="how.php" class="header-nav__navigation-link ">
                        <?php echo $lang['how']; ?><?php echo $lang['it']; ?><?php echo $lang['works']; ?>
                    </a>
                </li>
                <li class="header-nav__navigation-item <?php echo $active = ($basename == 'faq') ? ' active' : ''; ?>">
                    <a href="faq.php" class="header-nav__navigation-link ">
                        <?php echo $lang['faq']; ?>
                    </a>
                </li>
                <li class="header-nav__navigation-item <?php echo $active = ($basename == 'contact') ? ' active' : ''; ?>">
                    <a href="contact.php" class="header-nav__navigation-link ">
                        <?php echo $lang['contact']; ?>
                    </a>
                </li>

                <?php
                //Start new Admin object
                $admin = new Admin();
                //Start new Client object
                $client = new Client();
                //Start new Freelancer object
                $freelancer = new Freelancer();

                if ($admin->isLoggedIn()) { ?>
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <?php // echo $profileimg; ?>
                            <img src="Admin/<?php echo escape($admin->data()->imagelocation); ?>" class="user-image"
                                 alt="User Image"/>

                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">
                  	<?php echo escape($admin->data()->name); ?>
                  </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="m_2"><a href="Admin/dashboard.php"><i
                                            class="fa fa-dashboard"></i><?php echo $lang['dashboard']; ?></a></li>
                            <li class="m_2"><a href="Admin/profile.php?a=profile"><i
                                            class="fa fa-user"></i><?php echo $lang['view']; ?> <?php echo $lang['profile']; ?>
                                </a></li>
                            <li class="m_2"><a href="Admin/logout.php"><i
                                            class="fa fa-lock"></i> <?php echo $lang['logout']; ?></a></li>
                        </ul>
                    </li>
                <?php } elseif ($client->isLoggedIn()) { ?>
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <?php // echo $profileimg; ?>
                            <img src="hirer/<?php echo escape($client->data()->imagelocation); ?>" class="user-image"
                                 alt="User Image"/>

                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">
                  	<?php echo escape($client->data()->name); ?>
                  </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="m_2"><a href="hirer/dashboard.php"><i
                                            class="fa fa-dashboard"></i><?php echo $lang['dashboard']; ?></a></li>
                            <li class="m_2"><a href="hirer/profile.php?a=profile"><i
                                            class="fa fa-user"></i><?php echo $lang['view']; ?> <?php echo $lang['profile']; ?>
                                </a></li>
                            <li class="m_2"><a href="hirer/logout.php"><i
                                            class="fa fa-lock"></i> <?php echo $lang['logout']; ?></a></li>
                        </ul>
                    </li>
                <?php } elseif ($freelancer->isLoggedIn()) { ?>
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <?php // echo $profileimg; ?>
                            <img src="tempworker/<?php echo escape($freelancer->data()->imagelocation); ?>"
                                 class="user-image" alt="User Image"/>

                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">
                  	<?php echo escape($freelancer->data()->name); ?>
                  </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="m_2"><a href="tempworker/index.php"><i
                                            class="fa fa-dashboard"></i><?php echo $lang['dashboard']; ?></a></li>
                            <li class="m_2"><a href="tempworker/profile.php?a=profile"><i
                                            class="fa fa-user"></i><?php echo $lang['view']; ?> <?php echo $lang['profile']; ?>
                                </a></li>
                            <li class="m_2"><a href="tempworker/logout.php"><i
                                            class="fa fa-lock"></i> <?php echo $lang['logout']; ?></a></li>
                        </ul>
                    </li>
                <?php } else { ?>
                    <li class="header-nav__navigation-item <?php echo $active = ($basename == 'login') ? ' active' : '';
                    echo $active = ($basename == 'forgot') ? ' active' : '';
                    echo $active = ($basename == 'reset') ? ' active' : ''; ?>">
                        <a class="header-nav__navigation-link" href="login.php"><?php echo $lang['login']; ?></a>
                    </li>
                    <li class="header-nav__navigation-item <?php echo $active = ($basename == 'register') ? ' active' : ''; ?>">
                        <a class="header-nav__navigation-link header-nav__navigation-link--outline"
                           href="register.php"><?php echo $lang['signup']; ?><?php echo $lang['for']; ?><?php echo $lang['free']; ?></a>
                    </li>
                <?php } ?>

                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php echo $lang['languages']; ?>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="m_2"><a href="<?php echo $test; ?>?lang=english">English</a></li>
                        <li class="m_2"><a href="<?php echo $test; ?>?lang=french">French</a></li>
                        <li class="m_2"><a href="<?php echo $test; ?>?lang=german">German</a></li>
                        <li class="m_2"><a href="<?php echo $test; ?>?lang=portuguese">Portuguese</a></li>
                        <li class="m_2"><a href="<?php echo $test; ?>?lang=spanish">Spanish</a></li>
                        <li class="m_2"><a href="<?php echo $test; ?>?lang=russian">Russian</a></li>
                        <li class="m_2"><a href="<?php echo $test; ?>?lang=chinese">Chinese</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<!-- ==============================================
Header
=============================================== -->
<style>
    .img-thumbnail inner_img img {
        width: 50px;
        height: 50px;
    }

    .img-thumbnail.inner_img {
        margin-bottom: 33px;
    }

    .compliance-content .img-thumbnail inner_img {
        display: block;
        max-width: 100%;
        height: auto;
        padding: 4px;
        line-height: 1.42857143;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 4px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
        margin: 2px auto;
    }

    .wrapper {
        display: grid;
        grid-gap: 10px;
        color: #444;
        margin-bottom: 23px;
    }

    img-thumbnail.inner_img {
        margin-bottom: 33px;
        display: block;
    }

    .img-responsive01 {
        width: 50%;
    }

    .img-thumbnail.inner_img.bottom_img {
        padding: 15px 46px;
    }

    .img-responsive01.bottom_img_01 {
        width: 89%;
    }

    .responsive01.bottom_img_01.bottom_img_02 {
        width: 75%;
    }
</style>
<header class="header-login" style="
        background-size: cover;
        background-position: center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        color: #fff;
        height: 55vh;
        width: 100%;

        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center; ">
    <div class="container">
        <div class="content">
            <div class="row">
                <!--<h1 class="revealOnScroll" data-animation="fadeInDown">
	       	<?php if ($use_icon === '1'): ?>
	       		<i class="fa <?php //echo $site_icon; ?>"></i>
	       	<?php endif; ?>  <?php //echo escape($title); ?></h1>
		 <div id="typed-strings">
		  <span><?php //echo escape($tagline); ?></span>
		 </div>-->
                <!-- <p id="typed"></p>-->
            </div><!-- /.row -->
        </div><!-- /.content -->
    </div><!-- /.container -->
</header><!-- /header -->

<!-- ==============================================
Banner Login Section
=============================================== -->
<div id="pjax-body">
    <div id="main" class="main">
        <div class="container">
            <div class="js-validation-part">
                <div class="userProfiles form js-responses">
                    <section class="row ver-space ver-smspace">
                        <div class="js-corner round-5">
                            <div class="tab-head bot-mspace text-13 clearfix blue-head-bg">
                                <h2 class="pull-left text-16 no-mar">Edit Profile : sefafa </h2>
                            </div>


                            <p class="dc bluec">Your Digital CV Profile is your opportunity to advertise your skills to
                                employers and other recruiters. By marketing your skills, qualification, experience and
                                writing a brief description about yourself, you are 5 times likely to get more better
                                paying jobs from top medical and healthcare employers. Your profile must be approved and
                                vetted, so take time to insert accurate information..</p>


                            <div class="form-blocks js-corner">
                                <form action="" class="form-horizontal js-geo-form" id="UserProfileEditForm" method="post" accept-charset="utf-8">
                                    <div style="display:none;"><input type="hidden" name="_method" value="POST"><input
                                                type="hidden" name="data[_Token][key]"
                                                value="2c72f57adc85a840e7c26b00fbff40cd753a42eb" id="Token677384830">
                                    </div>

                                    <div class="clearfix">
                                        <div class="form-content-blocks clearfix show">
                                            <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Personal
                                                Info:</h3>
                                            <div class="row">
                                                <div class="pull-right col-md-2">
                                                    <div>
                                                        <a href="/user_profiles/profile_image/546" class="">Change
                                                            Image</a>
                                                        <span class="">
                                                            <span class="avtar-box">
                                                                <a href="/user/sefafa" title="sefafa" class="js-tooltip  show no-pad ">
                                                                    <img src="/img/big_thumb/UserAvatar/1.22bb67ea7586f030ead30b33adaf7520.jpg" width="100" height="100" class="js-tooltip  img-thumbnail " alt="[Image: sefafa]" title="">
                                                                </a>
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">

                                                    <div class="input text  bot-space">
                                                        <div class="input text required validation:{'rule1':{'rule':'notempty','allowEmpty':false,'message':'Required'}} error">
                                                            <input name="first_name" placeholder="First Name" maxlength="100" type="text" value="" id="UserProfileFirstName">
                                                            <div class="error-message">Required</div>
                                                        </div>
                                                    </div>
                                                    <div class="input text  bot-space">
                                                        <div class="input text">
                                                            <input name="last_name" placeholder="Last Name" maxlength="100" type="text" value="" id="UserProfileLastName">
                                                        </div>
                                                    </div>
                                                    <div class="input text  bot-space">
                                                        <div class="input text">
                                                            <label for="UserProfileMiddleName">Middle Name</label>
                                                            <input name="middle_name" placeholder="Temp Name" maxlength="100" type="text" value="" id="UserProfileMiddleName">
                                                        </div>
                                                    </div>
                                                    <div class="bot-space">
                                                        <div class="input text">
                                                            <label for="UserProfileUtrCode">UTR Code</label><input name="utr_no" type="text" value="0" id="UserProfileUtrCode">
                                                        </div>
                                                    </div>
                                                    <div class="ver-space">
                                                        <div class="input text">
                                                            <input name="hourly_rate" placeholder="Hourly Rate (£)" maxlength="8" type="text" id="UserProfileHourRate">
                                                        </div>
                                                    </div>

                                                    <div class="input select  required  bot-space">
                                                        <div class="input select required validation:{'rule1':{'rule':'notempty','allowEmpty':false,'message':'Required'}}">
                                                            <label for="UserProfileGenderId">Gender</label>
                                                            <select name="gender" placeholder="Gender" id="UserProfileGenderId">
                                                                <option value="">Please Select</option>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="input text date-time select">
                                                        <div class="js-datetime  required  xltriggered"
                                                             data-displayed="true">
                                                            <div class="js-cake-date">
                                                                <select placeholder="DOB" id="UserProfileDobMonth">
                                                                    <option value="">Please Select</option>
                                                                    <option value="01">January</option>
                                                                    <option value="02">February</option>
                                                                    <option value="03">March</option>
                                                                    <option value="04">April</option>
                                                                    <option value="05">May</option>
                                                                    <option value="06">June</option>
                                                                    <option value="07">July</option>
                                                                    <option value="08">August</option>
                                                                    <option value="09">September</option>
                                                                    <option value="10">October</option>
                                                                    <option value="11">November</option>
                                                                    <option value="12">December</option>
                                                                </select>-
                                                                <select placeholder="DOB" id="UserProfileDobDay">
                                                                    <option value="">Please Select</option>
                                                                    <option value="01">1</option>
                                                                    <option value="02">2</option>
                                                                    <option value="03">3</option>
                                                                    <option value="04">4</option>
                                                                    <option value="05">5</option>
                                                                    <option value="06">6</option>
                                                                    <option value="07">7</option>
                                                                    <option value="08">8</option>
                                                                    <option value="09">9</option>
                                                                    <option value="10">10</option>
                                                                    <option value="11">11</option>
                                                                    <option value="12">12</option>
                                                                    <option value="13">13</option>
                                                                    <option value="14">14</option>
                                                                    <option value="15">15</option>
                                                                    <option value="16">16</option>
                                                                    <option value="17">17</option>
                                                                    <option value="18">18</option>
                                                                    <option value="19">19</option>
                                                                    <option value="20">20</option>
                                                                    <option value="21">21</option>
                                                                    <option value="22">22</option>
                                                                    <option value="23">23</option>
                                                                    <option value="24">24</option>
                                                                    <option value="25">25</option>
                                                                    <option value="26">26</option>
                                                                    <option value="27">27</option>
                                                                    <option value="28">28</option>
                                                                    <option value="29">29</option>
                                                                    <option value="30">30</option>
                                                                    <option value="31">31</option>
                                                                </select>-
                                                                <select placeholder="DOB" id="UserProfileDobYear">
                                                                    <option value="">Please Select</option>
                                                                    <option value="1918">1918</option>
                                                                    <option value="1919">1919</option>
                                                                    <option value="1920">1920</option>
                                                                    <option value="1921">1921</option>
                                                                    <option value="1922">1922</option>
                                                                    <option value="1923">1923</option>
                                                                    <option value="1924">1924</option>
                                                                    <option value="1925">1925</option>
                                                                    <option value="1926">1926</option>
                                                                    <option value="1927">1927</option>
                                                                    <option value="1928">1928</option>
                                                                    <option value="1929">1929</option>
                                                                    <option value="1930">1930</option>
                                                                    <option value="1931">1931</option>
                                                                    <option value="1932">1932</option>
                                                                    <option value="1933">1933</option>
                                                                    <option value="1934">1934</option>
                                                                    <option value="1935">1935</option>
                                                                    <option value="1936">1936</option>
                                                                    <option value="1937">1937</option>
                                                                    <option value="1938">1938</option>
                                                                    <option value="1939">1939</option>
                                                                    <option value="1940">1940</option>
                                                                    <option value="1941">1941</option>
                                                                    <option value="1942">1942</option>
                                                                    <option value="1943">1943</option>
                                                                    <option value="1944">1944</option>
                                                                    <option value="1945">1945</option>
                                                                    <option value="1946">1946</option>
                                                                    <option value="1947">1947</option>
                                                                    <option value="1948">1948</option>
                                                                    <option value="1949">1949</option>
                                                                    <option value="1950">1950</option>
                                                                    <option value="1951">1951</option>
                                                                    <option value="1952">1952</option>
                                                                    <option value="1953">1953</option>
                                                                    <option value="1954">1954</option>
                                                                    <option value="1955">1955</option>
                                                                    <option value="1956">1956</option>
                                                                    <option value="1957">1957</option>
                                                                    <option value="1958">1958</option>
                                                                    <option value="1959">1959</option>
                                                                    <option value="1960">1960</option>
                                                                    <option value="1961">1961</option>
                                                                    <option value="1962">1962</option>
                                                                    <option value="1963">1963</option>
                                                                    <option value="1964">1964</option>
                                                                    <option value="1965">1965</option>
                                                                    <option value="1966">1966</option>
                                                                    <option value="1967">1967</option>
                                                                    <option value="1968">1968</option>
                                                                    <option value="1969">1969</option>
                                                                    <option value="1970">1970</option>
                                                                    <option value="1971">1971</option>
                                                                    <option value="1972">1972</option>
                                                                    <option value="1973">1973</option>
                                                                    <option value="1974">1974</option>
                                                                    <option value="1975">1975</option>
                                                                    <option value="1976">1976</option>
                                                                    <option value="1977">1977</option>
                                                                    <option value="1978">1978</option>
                                                                    <option value="1979">1979</option>
                                                                    <option value="1980">1980</option>
                                                                    <option value="1981">1981</option>
                                                                    <option value="1982">1982</option>
                                                                    <option value="1983">1983</option>
                                                                    <option value="1984">1984</option>
                                                                    <option value="1985">1985</option>
                                                                    <option value="1986">1986</option>
                                                                    <option value="1987">1987</option>
                                                                    <option value="1988">1988</option>
                                                                    <option value="1989">1989</option>
                                                                    <option value="1990">1990</option>
                                                                    <option value="1991">1991</option>
                                                                    <option value="1992">1992</option>
                                                                    <option value="1993">1993</option>
                                                                    <option value="1994">1994</option>
                                                                    <option value="1995">1995</option>
                                                                    <option value="1996">1996</option>
                                                                    <option value="1997">1997</option>
                                                                    <option value="1998">1998</option>
                                                                    <option value="1999">1999</option>
                                                                    <option value="2000">2000</option>
                                                                    <option value="2001">2001</option>
                                                                    <option value="2002">2002</option>
                                                                    <option value="2003">2003</option>
                                                                    <option value="2004">2004</option>
                                                                    <option value="2005">2005</option>
                                                                    <option value="2006">2006</option>
                                                                    <option value="2007">2007</option>
                                                                    <option value="2008">2008</option>
                                                                    <option value="2009">2009</option>
                                                                    <option value="2010">2010</option>
                                                                    <option value="2011">2011</option>
                                                                    <option value="2012">2012</option>
                                                                    <option value="2013">2013</option>
                                                                    <option value="2014">2014</option>
                                                                    <option value="2015">2015</option>
                                                                    <option value="2016">2016</option>
                                                                    <option value="2017">2017</option>
                                                                    <option value="2018">2018</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="hidden" id="totalDOB" name="dob">
                                                    <script>
                                                        $(document).ready(function () {
                                                            $('#totalDOB').val($('#UserProfileDobDay').val()+'/'+$('#UserProfileDobMonth').val()+'/'+$('#UserProfileDobYear').val());
                                                            $('.js-cake-date').change(function() {
                                                                $('#totalDOB').val($('#UserProfileDobDay').val()+'/'+$('#UserProfileDobMonth').val()+'/'+$('#UserProfileDobYear').val());
                                                            });
                                                        })
                                                    </script>
                                                    <div class="input text  required  bot-space">
                                                        <div class="input textarea required validation:{'rule1':{'rule':'notempty','allowEmpty':false,'message':'Required'}}">
                                                            <textarea name="about_me" placeholder="About Me/Profile" cols="30" rows="6" id="UserProfileAboutMe"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="input text  bot-space">
                                                        <div class="input select"><label for="UserProfileAvailability">Availability</label><select
                                                                    name="availability"
                                                                    id="UserProfileAvailability">
                                                                <option value="">(choose one)</option>
                                                                <option value="1 day a week">1 day a week</option>
                                                                <option value="1 night a week">1 night a week</option>
                                                                <option value="2-3 days a week">2-3 days a week</option>
                                                                <option value="2-3 nights a week">2-3 nights a week
                                                                </option>
                                                                <option value="3-5 days a week">3-5 days a week</option>
                                                                <option value="3-5 nights a week">3-5 nights a week
                                                                </option>
                                                                <option value="Days only">Days only</option>
                                                                <option value="Nights only">Nights only</option>
                                                                <option value="Weekends only">Weekends only</option>
                                                            </select></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-content-blocks">
                                        <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Location:</h3>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="mapblock-info inputtypebox0">
                                                    <div class="clearfix address-input-block ver-space">
                                                        <div class="input text required validation:{'allowEmpty':false,'message':'Required','rule':'notempty'}">
                                                            <label for="UserProfileAddress">Location</label>
                                                            <input name="location" class="js-offline address-error-message xltriggered" type="text" id="UserProfileAddress">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 hidden-xs">

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="bot-space inputtypebox01">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="input text"><label for="UserProfileZipCode">Post
                                                        Code</label>
                                                    <input name="post_code" type="text" id="UserProfileZipCode">
                                                </div>
                                            </div>
                                            <div class="col-md-4 hidden-xs">

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>
                                    <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Education:</h3>

                                    <!-- <div class="bot-space" >                            </div> -->

                                    <div class="bot-space">
                                        <div class="education_fields_wrap">
                                            <button class="add_education btn btn-success">Add More</button>
                                        </div>
                                    </div>

                                    <!-- <h3 class="tab-head text-16 textn img-rounded blue-head-bg"></h3>
                                    <div class="bot-space"></div> -->
                                    <div class="clearfix"></div>
                                    <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Work History:</h3>

                                    <div class="bot-space">
                                        <div class="experience_fields_wrap">
                                            <button class="add_experience btn btn-success">Add More</button>
                                        </div>
                                    </div>

                                    <div class="clearfix">
                                        <div class="form-content-blocks">
                                            <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Contacts:</h3>
                                            <div class="ver-space inputtypebox">
                                                <div class="input text">
                                                    <label for="UserProfileFacebookUrl">Facebook</label>
                                                    <input name="fb_url" class="js-remove-error" maxlength="255" type="text" value="" id="UserProfileFacebookUrl">
                                                    <span class="info">Your Facebook URL start with http://</span>
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text">
                                                    <label for="UserProfileWebsite">Web URL</label>
                                                    <input name="web_url" class="js-remove-error" maxlength="255" type="text" value="" id="UserProfileWebsite">
                                                    <span class="info">Your website URL start with http://</span>
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text">
                                                    <label for="UserProfileLinkedinUrl">LinkedIn URL</label>
                                                    <input name="linkedin_url" class="js-remove-error" maxlength="255" type="text" value="" id="UserProfileLinkedinUrl">
                                                    <span class="info">Your Linkedin URL start with http://</span>
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text">
                                                    <label for="UserProfileYahoo">Yahoo</label>
                                                    <input name="yahoo_url" class="js-remove-error" maxlength="255" type="text" id="UserProfileYahoo">
                                                    <span class="info">Your Yahoo email</span>
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text">
                                                    <label for="UserProfileMsn">MSN</label>
                                                    <input name="msn_url" class="js-remove-error" maxlength="255" type="text" id="UserProfileMsn">
                                                    <span class="info">Your MSN ID</span>
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text">
                                                    <label for="UserProfileAim">Oovoo URL</label>
                                                    <input name="oovoo_url" class="js-remove-error" maxlength="255" type="text" id="UserProfileAim">
                                                    <span class="info">Your Oovoo ID</span>
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text">
                                                    <label for="UserProfileSkype">Skype</label>
                                                    <input name="skype_url" class="js-remove-error" maxlength="255" type="text" id="UserProfileSkype">
                                                    <span class="info">Your Skype ID</span>
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text">
                                                    <label for="UserProfileGoogleTalk">Gruveo</label>
                                                    <input name="gruveo_url" class="js-remove-error" maxlength="255" type="text" id="UserProfileGoogleTalk">
                                                    <span class="info">Your Gruveo email</span>
                                                </div>
                                            </div>

                                            <div class="bot-space inputtypebox">
                                                <div class="input text">
                                                    <label for="UserProfilePhone">Phone</label>
                                                    <input name="phone" placeholder="Phone" class="js-remove-error" maxlength="255" type="text" id="UserProfilePhone">
                                                    <span class="info">Your Phone Number</span>
                                                </div>
                                            </div>
                                            <div class="bot-space inputtypebox">
                                                <div class="input text">
                                                    <label for="UserProfileMobile">Mobile</label>
                                                    <input name="mobile" placeholder="Mobile" class="js-remove-error" maxlength="255" type="text" id="UserProfileMobile">
                                                    <span class="info">Your Mobile Number</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                    <h3 class="tab-head text-16 textn img-rounded blue-head-bg">Registration Nos:</h3>
                                    <div class="ver-space inputtypebox">
                                        <div class="input text"><label for="UserProfileCrbNo">CRB/DBS No.</label><input
                                                    name="crb_dbs_no" class="js-remove-error"
                                                    maxlength="255" type="text" value="" id="UserProfileCrbNo"></div>
                                    </div>
                                    <div class="bot-space inputtypebox">
                                        <div class="input text"><label for="UserProfileCompanyNo">Company
                                                No.</label><input name="company_no"
                                                                  class="js-remove-error" maxlength="255" type="text"
                                                                  value="333333333333333333" id="UserProfileCompanyNo">
                                        </div>
                                    </div>
                                    <div class="bot-space inputtypebox">
                                        <div class="input text"><label for="UserProfileNmcPin">NMC PIN</label><input
                                                    name="nmc_pin" class="js-remove-error"
                                                    maxlength="255" type="text" value="" id="UserProfileNmcPin"></div>
                                    </div>
                                    <div class="bot-space inputtypebox">
                                        <div class="input text"><label for="UserProfileOther">other</label><input
                                                    name="other" class="js-remove-error"
                                                    maxlength="255" type="text" value="" id="UserProfileOther"></div>
                                    </div>
                                    <div class="bot-space inputtypebox">
                                        <div class="input text"><label for="UserProfileNino">NINO</label><input
                                                    name="nino_no" class="js-remove-error"
                                                    maxlength="255" type="text" value="111111111" id="UserProfileNino">
                                        </div>
                                    </div>
                                    <div class="bot-space inputtypebox">
                                        <div class="input text"><label for="UserProfileTaxCode">Tax Code:</label><input
                                                    name="tax_code" class="js-remove-error"
                                                    maxlength="255" type="text" value="" id="UserProfileTaxCode"></div>
                                    </div>
                                    <div class="bot-space inputtypebox">
                                        <div class="input text"><label for="UserProfileGmcNo">GMC no</label><input
                                                    name="gmc_no" class="js-remove-error"
                                                    maxlength="255" type="text" value="" id="UserProfileGmcNo"></div>
                                    </div>

                                    <div class="bot-space inputtypebox">
                                        <div class="input text"><label for="UserProfileUtr">UTR</label><input
                                                    name="utr_no" class="js-remove-error"
                                                    maxlength="255" type="text" value="3333333333" id="UserProfileUtr">
                                        </div>
                                    </div>
                                    <div class="bot-space inputtypebox">
                                        <div class="input text"><label for="UserProfilePassportNo">Passport
                                                No:</label><input name="passport_no"
                                                                  class="js-remove-error" maxlength="255" type="text"
                                                                  value="" id="UserProfilePassportNo"></div>
                                    </div>
                                    <div class="bot-space inputtypebox">
                                        <div class="input text"><label for="UserProfileGphc">GPhc</label><input
                                                    name="gphc" class="js-remove-error"
                                                    maxlength="255" type="text" value="" id="UserProfileGphc"></div>
                                    </div>
                                    <!-- <div class="bot-space inputtypebox" ><div class="input text"><label for="UserProfileMis">Mis</label><input name="data[UserProfile][mis]" class="js-remove-error" maxlength="255" type="text" value="" id="UserProfileMis"/></div></div> -->

                                    <div class="clearfix"></div>
                                    <h4 class="tab-head text-16 textn img-rounded blue-head-bg">Declaration:</h4>
                                    <!--<h4 class="text-16 textn"> I declare that the information provided on this e-form is correct to the best of my knowledge and ability.</h4>
                      <h4 class="text-16 textn">I grant the administrator permission to check the information provided. </h4>
                      <h4 class="text-16 textn dl">I fully understand that I could be prosecuted for providing false or misleading information in order to deceive or decipher details from potential staff. </h4>!-->
                                    <div class="input checkbox no-mar required validation:{'message':'You must accept the declaration','required':true,'rule':['comparison','!=',0]}">
                                        <input type="checkbox" value="1" id="UserProfileDeclaration">
                                        <label for="UserProfileDeclaration">
                                            <p class="text-16 textn no-mar dl">I hereby declare that the information/data
                                                provided above is true and accurate to my best knowledge. I fully understand,
                                                that I will be held liable and/or accountable for the accuracy of the
                                                information supplied above and I could be legally prosecuted for providing false
                                                data </p>
                                        </label>
                                    </div>

                                    <div class="clearfix"></div>
                                    <br>
                                    <div class="well no-bor no-shad clearfix">
                                        <div class="submit no-mar">
                                            <input type="submit" class="btn no-mar blue-head-bg" id="submitBtn" value="Update">

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>


                    </section>


                    <script type="text/javascript">
                        (function (a) {
                            a.fn.duplicateElement = function (b) {
                                b = a.extend(a.fn.duplicateElement.defaults, b);
                                return this.each(function () {
                                    var e = a(this);
                                    var c = document.createElement(b.tag_name);
                                    c.setAttribute("id", b.tag_id);
                                    e[0].parentNode.appendChild(c);
                                    var d = 0;
                                    a(e, a("#" + b.tag_id)).on("click", b.class_create, function (f) {
                                        e.clone().addClass("dinamic-field").appendTo("#" + b.tag_id);
                                        a(b.class_remove).show();
                                        a(b.class_remove).first().hide();
                                        a(b.class_create).hide();
                                        a(b.class_create).first().show();
                                        f.preventDefault();
                                        d++;
                                        return false
                                    });
                                    e.find(b.class_remove).first().hide();
                                    a("#" + b.tag_id).on("click", b.class_remove, function (f) {
                                        a(this).parents(".dinamic-field").remove();
                                        f.preventDefault();
                                        return false
                                    })
                                })
                            };
                            a.fn.duplicateElement.defaults = {
                                tag_name: "div",
                                tag_id: "dinamic-fields",
                                clone_model: "#clone-field-model",
                                class_remove: ".remove-this-fields",
                                class_create: ".create-new-fields"
                            }
                        })(jQuery);
                    </script>
                    <script type="text/javascript">

                        $(document).ready(function () {

                            $('#submitBtn').click(function (e) {
                                if(!$('#UserProfileDeclaration').is(':checked')) {
                                    e.preventDefault();
                                }
                            });

                            $('#Education-model').duplicateElement({
                                tag_name: 'div',
                                tag_id: "education-fields",
                                clone_model: "#clone-education-model",
                                class_remove: ".remove-this-fields-education",
                                class_create: ".create-new-fields-education",
                                onCreate: "",
                                onRemove: ""
                            });

                            $('#Education-model').duplicateElement({
                                "class_remove": ".remove-this-field-education",
                                "class_create": ".create-new-field-education"
                            });


                            $('#Experience-model').duplicateElement({
                                tag_name: 'div',
                                tag_id: "experience-fields",
                                clone_model: "#clone-experience-model",
                                class_remove: ".remove-this-fields-experience",
                                class_create: ".create-new-fields-experience",
                                onCreate: "",
                                onRemove: ""
                            });

                            $('#Experience-model').duplicateElement({
                                "class_remove": ".remove-this-field-experience",
                                "class_create": ".create-new-field-experience"
                            });

                        });


                    </script>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            var max_fields = 10; //maximum input boxes allowed
                            //var x = 1; //initial text box count
                            var x = '-1';
                            $('.add_education').click(function (e) { //on add input button click
                                e.preventDefault();
                                if (x < max_fields) { //max input box allowed
                                    var a = parseInt(x) + 1;
                                    $('.education_fields_wrap ').append('<div class=edu' + a + '><table id="Education-model"><tbody><tr style="border:0px"><td class="col-md-2"><input style="width: 100%" id=UserEducation' + a + 'School name=data[UserEducation][' + a + '][school] type="text" placeholder="School" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserEducation' + a + 'qualification name=data[UserEducation][' + a + '][qualification] type="text" placeholder="Qualification" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserEducation' + a + 'start name=data[UserEducation][' + a + '][start] type="text" placeholder="Start Year" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserEducation' + a + 'end name=data[UserEducation][' + a + '][end] type="text" placeholder="End Year" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserEducation' + a + 'notes name=data[UserEducation][' + a + '][notes] type="text" placeholder="Notes" class="form-control input-md" value="" required></td><td><a href="javascript:void(0);" class="remove_education" data-id=' + a + '>remove</a></td></tr></tbody></table></div>'); //add input box
                                    x++; //text box increment
                                }
                            });

                            $('.education_fields_wrap').on("click", ".remove_education", function (e) { //user click on remove text
                                if (x > 0) {
                                    var num = $(this).attr('data-id');
                                    e.preventDefault();
                                    $('.edu' + num).remove();
                                    x--;
                                }
                            });

                            var y = '-1'; //initial text box count
                            $('.add_experience').click(function (e) { //on add input button click
                                e.preventDefault();
                                if (y < max_fields) { //max input box allowed
                                    var b = parseInt(y) + 1;
                                    $('.experience_fields_wrap').append('<div class=exp' + b + '><table id="Experience-model"><tbody><tr style="border:0px"><td class="col-md-2"><input style="width: 100%" id=UserExperience' + b + 'Employer name=data[UserExperience][' + b + '][employer] type="text" placeholder="Employer" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserExperience' + b + 'Title name=data[UserExperience][' + b + '][title] type="text" placeholder="Job Title" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserExperience' + b + 'Start name=data[UserExperience][' + b + '][start] type="text" placeholder="Start Year" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserExperience' + b + 'End name=data[UserExperience][' + b + '][end] type="text" placeholder="End Year" class="form-control input-md" value="" required></td><td><input style="width: 100%" id=UserExperience' + b + 'Notes name=data[UserExperience][' + b + '][notes] type="text" placeholder="Notes" class="form-control input-md" value="" required><td><a href="javascript:void(0);" class="remove_experience" data-id=' + b + '>remove</a></td></td></tr></tbody></table></div>');
                                }
                                y++; //text box increment
                            });

                            $('.experience_fields_wrap').on("click", ".remove_experience", function (e) { //user click on remove text
                                if (y > 0) {
                                    var num = $(this).attr('data-id');
                                    e.preventDefault();
                                    $('.exp' + num).remove();
                                    y--;
                                }
                            });
                        });

                    </script>
                </div>
            </div>
        </div>
        <!-- for modal -->
        <div class="modal hide fade" id="js-ajax-modal">
            <div class="modal-body"></div>
            <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a></div>
        </div>
        <!-- for modal -->
        <!-- for modal -->
        <div class="modal hide fade" id="js-ajax-modal-child">
            <div class="modal-body"></div>
            <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a></div>
        </div>
        <!-- for modal -->

        <div class="clearfix"></div>
        <!-- for modal
        <div class="footer-push"></div> -->
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>
<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Typed JS -->
<script src="assets/js/typed.min.js" type="text/javascript"></script>
<!-- Kafe JS -->
<script src="assets/js/kafe.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.photoimg').on('change', function () {
            $("#imageform").ajaxForm({
                success: processJson,
            }).submit();
        });
    });

    function processJson(data) {
        alert(data);
        window.location.reload();
    }

    console.log(<?=$numberOfUploads?>);
</script>

</body>
</html>
