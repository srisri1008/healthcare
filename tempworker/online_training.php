<?php 
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
  Redirect::to('../index.php');	
}

?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('template/header.php'); ?> 
    <!-- AdminLTE CSS -->
    <link href="../assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- Datetime Picker -->
    <link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Jquery UI CSS -->
    <link href="../assets/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
    <!-- Progress CSS -->
    <link href="../assets/css/progress.css" rel="stylesheet" type="text/css" />

    <!-- jQuery 2.1.4 -->
    <script src="../assets/js/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.6 JS -->
    <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../assets/js/app.min.js" type="text/javascript"></script>

    <script src="../assets/js/jquery.knob.min.js"></script>


    <body class="skin-green sidebar-mini">
     
     <!-- ==============================================
     Wrapper Section
     =============================================== -->
	 <div class="wrapper">
	 	
        <!-- Include navigation.php. Contains navigation content. -->
	 	<?php include ('template/navigation.php'); ?> 
        <!-- Include sidenav.php. Contains sidebar content. -->
	 	<?php include ('template/sidenav.php'); ?> 
	 	
	 	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
          <style>
              .training_logo {
                  background-color: blueviolet;
                  color: white;
                  width: 103px;
                  height: 26px;
                  padding-left: 8px;
              }
          </style>
          <section class="content-header">
              <h1>Online Training<small>Section</small></h1>
              <ol class="breadcrumb">
                  <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                  <li class="active">Online Training</li>
              </ol>
          </section>

        <!-- Main content -->
        <section class="content">	
		    <!-- Include currency.php. Contains header content. -->
		    <?php include ('template/currency.php'); ?>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2">
                    <div class="row" style="border: 2px solid lightgrey;">
                        <div class="training_logo">Video Training</div>
                        <div class="form-group" style="width: 220px;margin-left: 17px;">
                            <label for="video">Select Video</label>
                            <select name="video" id="video" class="form-control">
                                <option value="">-- Select Video --</option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <iframe title="YouTube video player" class="youtube-player" type="text/html"
                                    width="100%" height="390" src="//www.youtube.com/embed/DhVYnVu7NdQ"
                                    frameborder="0" allowFullScreen></iframe>
                            <iframe title="YouTube video player" class="youtube-player" type="text/html"
                                    width="100%" height="390" src="//www.youtube.com/embed/"
                                    frameborder="0" allowFullScreen></iframe>
                            <iframe title="YouTube video player" class="youtube-player" type="text/html"
                                    width="100%" height="390" src="//www.youtube.com/embed/"
                                    frameborder="0" allowFullScreen></iframe>
                        </div>
                        <div class="col-lg-6">
                            <iframe title="YouTube video player" class="youtube-player" type="text/html"
                                    width="100%" height="390" src="//www.youtube.com/embed/"
                                    frameborder="0" allowFullScreen></iframe>
                            <iframe title="YouTube video player" class="youtube-player" type="text/html"
                                    width="100%" height="390" src="//www.youtube.com/embed/"
                                    frameborder="0" allowFullScreen></iframe>
                            <iframe title="YouTube video player" class="youtube-player" type="text/html"
                                    width="100%" height="390" src="//www.youtube.com/embed/"
                                    frameborder="0" allowFullScreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
	   </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	 
      <!-- Include footer.php. Contains footer content. -->	
	  <?php include 'template/footer.php'; ?>	
	 	
     </div><!-- /.wrapper -->   
	<!-- ==============================================
	 Scripts
	 =============================================== -->

</body>
</html>
