<?php
//Check if init.php exists
if(!file_exists('../core/init.php')){
    header('Location: ../install/');
    exit;
}else{
    require_once '../core/init.php';
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
    Redirect::to('../index.php');
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<?php include ('template/header.php'); ?>

<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <?php include ('template/navigation.php'); ?>
    <!-- Include sidenav.php. Contains sidebar content. -->
    <?php include ('template/sidenav.php'); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Privacy Policy<small><?php echo $lang['section']; ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="index.php"><i class="fa fa-dashboard"></i> <?php echo $lang['home']; ?></a></li>
                <li class="active">Privacy Policy</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Include currency.php. Contains header content. -->
            <?php include ('template/currency.php'); ?>
            <div class="container">
                <div class="row">
                    <div class="col-lg-11">
                        <div style="text-align: center; color: #0070c0; margin-bottom: 30px">
                            <h2>ESCROW POLICY</h2>
                        </div>
                        <p>
                            This Agreement was last modified on 1st May 2018.
                            This User Agreement describes the terms and conditions which you accept by using our Web &amp; Mobile application or our
                            Services. We have incorporated by reference some linked information.
                        </p>

                        <h3>1. Negotiating Fixed Pay Rate Agreement.</h3>
                        <p>
                            When a Hirer/Buyer is seeking to hire a temp worker, they can either negotiate Fixed Pay Rate agreement or an
                            Enhance Engagement agreement. The fixed pay rate or pay-rate-per-hour agreement is a flat fee that the employer
                            has agreed to pay the temp worker and covers payment for each hour the temp worker works (nothing more). The
                            total pre-agreed fixed pay rate will appear on the Hirer&#39;s &#39;bid accepted section of their dashboard and will be the
                            total amount required to be transferred from the Hirer’s wallet to their escrow account. The fixed pay rate per hour
                            will also be reflected on the temp worker&#39;s time-sheet and invoice. (Caveat: If the temp worker performs less or
                            more hours, he/she can only invoice the Hirer for the number of extra hours worked).
                            The second type of payment option which can be negotiated between a Hirer and temp worker is called the
                            &#39;Enhanced Engagements agreement. This type of agreement occurs when the Hirer pre-agrees to pay the temp
                            worker a flat fee / pay rate per hour plus, any bonuses or cover miscellaneous expenses (I.e. mileage, transportation
                            cost, accommodation cost, fuel, food etc) NB: Enhance Engagement agreement are non-taxable and are outside
                            Healthcare-Temps&#39; service fee agreement. Thus, Hirers are required to make two types of Escrow payment within
                            one transaction (fixed pay rate + bonus or enhanced payment).
                            Temp workers irrevocably authorise and instruct Healthcare-Temps, as its market place provider, to (i) facilitate an
                            Escrow collection, (automatedly or manually) prior to the generation of time-sheet &amp; invoice) and recorded all
                            negotiations (e-messages, interviews etc) between Hirer &amp; Temp worker. This is necessary to avoid dispute and
                            protect both Temp-workers and Hirers. On this site, Hirers are obligated to pay money into their Escrow account at
                            least four hours before the Temp worker commences work.
                            On the other hand, Temp workers are obligated to record start and end time on a time-sheet, create and submit an
                            invoice for Hirer&#39;s review before funds in escrow is release to the Temp worker. The Temp Worker represents and
                            warrants (i) that the temp worker has completed the applicable Temping Services fully and satisfactorily; and (ii)
                            that the hours the temp worker reports are true, accurate, and complete.
                        </p>
                        <h3>2). Enhanced Engagement Agreement:</h3>
                        <p>
                            To create an enhance engagement agreement invoice and bill a Hirer for miscellaneous expenses or bonuses, (such
                            as, car petrol, taxi, accommodation etc) the Hirer must first agree and authorise such claims by ticking boxes when
                            posting job or adding notes in email when negotiating job. Second, the employer must tick the relevant boxes in the
                            “Bonus section on the post job page on the Site&quot;. Next, the Temp worker must follow the instructions and save or
                            provide supporting documents (such as receipts) when submitting an invoice to the Hirer. In this agreement, the
                            Temp worker (1) irrevocably instructs and authorises Healthcare-Temps to facilitate transfer of a digital invoice on
                            the Temp worker’s behalf to the Hirer for payment; and (2) represents and warrants that such expenses were
                            actually incurred by the Temp-worker and were pre-authorised by the Hirer.
                            To pay the Temp-worker&#39;s enhanced invoice, the Hirer must follow the instructions and links on the Site and provide
                            the information requested. If the Employer approves an Enhanced invoice submitted on behalf of Temp-worker,
                            Healthcare-Temps will make the payment as described in the Payment Agreement in the subsection titled “Paying a
                            Time-sheet, Enhanced Invoice, or Bonus.”
                        </p>
                        <h3>3). Time-sheet Review</h3>
                        <p>
                            The liability is on the Hirer to review and approve time sheets weekly (no later than 23:00 pm GMT on each Saturday
                            of each week). After 23:01pm GMT of each Saturday, if no dispute has arisen, this will be deemed as evidence of
                            Hirer&#39;s approval of the Temp Worker’s Time-sheet and Invoice and Escrow payment will be released to Temp Worker
                            within the following 2 Days.

                            As a Temp Worker, your time-sheets must be submitted no later than 17:00 on the Monday of the following week in
                            which the work was done by the Temp worker. Following submission of the time-sheet (the “Time-sheet Review
                            Period”) is between Monday 17:01 GMT and Saturday 23:00 GMT. Thus, Hirers have 5 days in which to review and
                            approve the temp worker&#39;s time sheet. By not raising a dispute towards the time-sheet implies that the Hirer has
                            unequivocally approved the Temp Worker&#39;s time sheet (and invoice).
                            NB: During the Time-sheet Review Period, the Employer may approve time, reject time, or file a dispute as to some
                            or all of the time (hours) recorded on the time-sheet. If there is an enhance arrangement&#39; the Hirer cannot reject
                            time or file a Dispute with regards to the enhancement / bonus. All bonuses must be pre-agreed between Hirer and
                            Temp Worker before the exact amount is paid into Escrow account.
                            On the Saturday of the week following submission of the time-sheet, Hirers will be deemed to have approved all
                            non-rejected or undisputed time, and Healthcare-Temps will make payment as described in the Payment Agreement
                            subsection of the. Escrow Account, supported by the Hirer&#39;s signature.
                        </p>
                        <h3>4). Digital Signature:</h3>
                        <p>
                            By electronically awarding or accepting a Fixed Pay Rate or Enhance Engagement, submitting or approving an
                            invoice, or authorising a bonus payment on the Site, the Hirer and Temp-worker is deemed to have executed an
                            Agreement electronically, effective on the date the Hirer transfer funds to their Escrow Account via the Site,
                            pursuant to the EU Electronic Signatures Directives (1999/93/ec)). Electronically agreeing to do so constitutes your
                            acknowledgement that you are able to electronically receive, download, and print this Agreement, including its
                            Escrow Instructions.
                        </p>

                        <h3>5). Bonus:</h3>
                        <p>
                            Hirers may also pay Temp workers a bonus via the Enhanced Engagement agreement, thus submitting two escrow
                            payments simultaneously and at the Hirer&#39;s discretion when using the Site. To pay a bonus to a Temp-worker, the
                            Employer must follow the instructions and links on the Site and tick the relevant box(es) provide in the post job
                            section of the employer&#39;s dashboard, &#39;Post Temp Job&#39;. Once Healthcare-Temps.uk has published confirmation of
                            type of bonuses to be paid&#39; this represents the Hirer&#39;s authorisation to make the payment of a bonus to the Temp
                            worker via the Site. Healthcare-Temps.uk will make the payment in accordance with Hirer/s instructions and
                            payment will be displayed in the subsection of Hirers’ dashboard area titled “My Transaction or Bonuses.”
                        </p>
                        <h3>6). Instructions to Pay / Release Escrow</h3>
                        <p>
                            Hirers&#39; instruction to Healthcare-Temps.co.uk and its wholly owned subsidiaries to release escrow and/or pay a
                            Temp-worker is irrevocable. Uploading funds to Escrow account represent an instruction by a Hirer, authorising
                            subsequent transfer of funds to the Temp worker from the Hirer&#39;s Escrow Account (including authorisation to
                            charge Hirer&#39;s Payment Method). Such instruction is also a representation by the Employer&#39;s that the Employer has
                            received, inspected, and accepted the subject quality of work and/or expense. Hirers also acknowledge and agree
                            that upon receipt &amp; review of Temp workers’ invoice, Hirers give instruction to pay the Temp worker, Healthcare-
                            Temps.uk will transfer funds to Temp workers. Note importantly, subsequent to such instructions by Hirers,
                            Healthcare-Temps.co.uk and its Affiliates have no responsibility to and may not be able to recover such funds
                            transferred to Temp Workers. Therefore, and in consideration of services described in this Policy, Hirers agree that
                            once Healthcare-Temps or its subsidiary has charged the Hirers’ Payment Method, the charge is non-refundable.
                        </p>
                        <h3>7). Disputes between Employer and Temp worker</h3>
                        <p>
                            By using the services provided in this Agreement, both parties agree to follow the Refund and Cancellation Policy
                            and the Enhanced Payment Dispute Resolution Policy referenced therein, in connection with any Disputes about
                            temp workers’ service or payments by Hirers. You both further acknowledge and agree that Healthcare-
                            Temps.co.uk and its Affiliates are not and will not be a party to any such Dispute. Healthcare-Temps may, at its sole
                            discretion, withhold or delay payment in the event of Dispute between Hirer and Temp Worker.
                        </p>
                        <h3>8. Release and Delivery of Amounts in Escrow</h3>
                        <p>
                            In addition, Healthcare-Temps.co.uk is authorised to and will release applicable portions of the Hirer&#39;s Escrow
                            Account fund (each portion, a “Release”) to the Temp Worker’s Escrow Account in accordance with one or more
                            Release Conditions provided below or as otherwise permitted by applicable law. The amount of the Release will be

                            delivered to the Temp worker’s Escrow Account, in accordance with Temp worker’s and Hirer’s instructions, as
                            applicable in their Agreement, the Account Agreement, and the other Terms of Service will be judge amicably.
                        </p>
                        <h3>9. Release Conditions</h3>
                        <p>
                            As used in this Agreement, “Release Condition” means any of the following:
                            1. Hirers and Temp Workers have submitted joint instructions for a Release of funds.
                            2. Hirers have approved all or a portion of the Temp Worker&#39;s weekly timesheet. This Release Condition will
                            apply to and only for time recorded by Temp Workers which Hirers have approved.
                            3. Temp Workers have submitted a time-sheet under the User Agreement that includes Payment Services.
                            4. Hirers have not rejected or filed a Dispute Notice Form for time recorded on Temp Worker’s weekly time-
                            sheet during the Time-sheet Review Period pursuant to the Hourly Dispute Resolution Policy. This Release Condition
                            will apply to and only for time recorded by Temp Workers that were not rejected by a Hirer.
                            5. Temp Workers files a Dispute Notice Form for Employer’s rejection of time recorded on weekly time-sheet for an
                            Engagement within Work Review pursuant to the Hourly Dispute Resolution Policy, and Healthcare-Temps.co.uk
                            determines that the time is related to the Engagement’s requirements or Client’s instructions documented in the
                            Work review room.
                        </p>
                        <h3>10. Client files a Dispute Notice Form</h3>
                        <p>
                            With respect to Temp Worker’s weekly time-sheet for an Engagement without the use of Work Review, pursuant to
                            the Hourly Dispute Resolution Policy, and Hirer and Temp post a resolution of the Dispute to the Work review room
                            within two Business Days of Healthcare-Temps.co.uk notifying the Temp worker of the Dispute.
                            Temp Workers may file a Dispute Notice Form because the Hirer did not pay for time which qualifies as &#39;Work&#39;, for
                            example; induction time, handover, medication check or dealing with an emergency.
                            The final order of a court of competence with relevant jurisdiction from which an appeal is not taken.
                            Where applicable, if we believe, in our discretion, that a site user or member has committed or is attempting to
                            commit fraud, supply false information/documentation or other illicit acts contrary to the Site Services, Healthcare-
                            Temps.co.uk will take the necessary actions as we deem appropriate within our sole discretion and in accordance
                            with applicable law. Notwithstanding any of the foregoing occurrences and to the extent permitted by applicable
                            law, this Release Condition will be upheld.
                        </p>
                        <h3>11.. Work Review and Payment Protection for Temp workers:</h3>
                        <p>
                            Temp-workers will be paid per Hour and in accordance to job duties performed and at the amount properly
                            authorised by Hirers. The Work Review Room is provided for Temp Worker to electronically record what tasks they
                            have performed whilst engaged by Hirers. The work review room is provided to cover the following obligations, as
                            well as protect Temp Workers against dispute and/or non-payment.
                            a. Both Hirers and Temp Workers must have agreed to use Work Review Room upon acceptance of an Engagement,
                            as part of the Engagement terms.
                            b. Employers must clearly outline key duties which Temp Workers must perform. Failure to do this waivers
                            Employers’ right to raise a dispute.
                            c. Temp Workers must confirm date and time key tasked were performed as noted by Employers as a condition of
                            effectively executing the engagement.
                            d. Employers must have their Account in good standing, a valid and authenticated default Payment Method, and
                            Employers must agree for Escrow funds to be automatically paid to Temp Worker before the job commences.
                            e. Temp Workers Account must be in good standing.
                            f. Temp Workers must write at least one comment (per hour) documented in Work Review Room or on message
                            board prior to submitting their invoice.
                            g. The number of hours billed by Temp Workers must be within the hours authorised by the Hirer.
                            h. Within five days after notification of rejected or unpaid time, the Temp Worker must submit a Dispute Notice
                            Form clearly stating whether they accept or reject the dispute, and where necessary, state their counter proposal.
                            (1) New hours to be authorised by Hirer (2) changes to bonus payments; (3) refunds; (4) time to be added to time-
                            sheet and invoice (5) time added after Hirer has filed a Dispute Notice Form for an invoice and before the resolution
                            of that Dispute; (6) Engagements prohibited by the Terms of Service.<br /><br />
                            Visit our FAQ if you have any question that you are unsure about. The End
                        </p>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Include footer.php. Contains footer content. -->
    <?php include 'template/footer.php'; ?>

</div><!-- /.wrapper -->


<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="../assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="../assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="../assets/plugins/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../assets/js/app.min.js" type="text/javascript"></script>
<!-- page script -->
<script type="text/javascript">
    $(function () {
        $("#example1").dataTable();
    });
</script>
<script type="text/javascript">
    $(function() {


        $(".btn-danger").click(function(){

            //Save the link in a variable called element
            var element = $(this);

            //Find the id of the link that was clicked
            var id = element.attr("id");

            //Built a url to send
            var info = 'id=' + id;
            if(confirm("<?php echo $lang['delete_portfolio']; ?>"))
            {
                var parent = $(this).parent().parent();
                $.ajax({
                    type: "GET",
                    url: "template/delete/deleteportfolio.php",
                    data: info,
                    success: function()
                    {
                        parent.fadeOut('slow', function() {$(this).remove();});
                    }
                });


            }
            return false;

        });

    });
</script>

<script type="text/javascript">
    $(function() {

        $(".btn-info").click(function(){

            //Save the link in a variable called element
            var element = $(this);

            //Find the id of the link that was clicked
            var id = element.attr("id");

            //Built a url to send
            var info = 'id=' + id;
            if(confirm("<?php echo $lang['activate_portfolio']; ?>"))
            {
                var parent = $(this).parent().parent();
                $.ajax({
                    type: "GET",
                    url: "template/actions/activateportfolio.php",
                    data: info,
                    success: function()
                    {
                        window.location.reload();
                    }
                });


            }
            return false;

        });

    });
</script>

<script type="text/javascript">
    $(function() {

        $(".btn-default").click(function(){

            //Save the link in a variable called element
            var element = $(this);

            //Find the id of the link that was clicked
            var id = element.attr("id");

            //Built a url to send
            var info = 'id=' + id;
            if(confirm("<?php echo $lang['deactivate_portfolio']; ?>"))
            {
                var parent = $(this).parent().parent();
                $.ajax({
                    type: "GET",
                    url: "template/actions/deactivateportfolio.php",
                    data: info,
                    success: function()
                    {
                        window.location.reload();
                    }
                });


            }
            return false;

        });

    });
</script>

</body>
</html>
