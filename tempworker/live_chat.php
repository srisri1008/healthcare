<?php 
//Check if init.php exists
if(!file_exists('../core/init.php')){
	header('Location: ../install/');        
    exit;
}else{
 require_once '../core/init.php';	
}

//Start new Freelancer object
$freelancer = new Freelancer();

//Check if Freelancer is logged in
if (!$freelancer->isLoggedIn()) {
  Redirect::to('../index.php');	
}

//Get clients for chat
//$allClients = DB::getInstance()->get('client', '*', ['LIMIT' => 10]);
//if($allClients->count()) {
//    $allClientsData = $allClients->results();
//}
function arrayToObject($d) {
    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return (object) array_map(__FUNCTION__, $d);
    }
    else {
        // Return object
        return $d;
    }
}
$ob = new DB();
$result = $ob->getData("SELECT client.* FROM job LEFT JOIN client ON client.clientid = job.clientid WHERE job.freelancerid = ".$freelancer->data()->freelancerid." AND client.clientid <> '' GROUP BY client.id");
if(count($result)) {
//    echo '<pre>';
//    print_r($result);
    $allClientsData = arrayToObject($result);
}
?>
<!DOCTYPE html>
<html lang="en-US" class="no-js">
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('template/header.php'); ?> 
    <!-- AdminLTE CSS -->
    <link href="../assets/css/AdminLTE/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- Datetime Picker -->
    <link href="../assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Jquery UI CSS -->
    <link href="../assets/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
    <!-- Progress CSS -->
    <link href="../assets/css/progress.css" rel="stylesheet" type="text/css" />

    <!-- jQuery 2.1.4 -->
    <script src="../assets/js/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.6 JS -->
    <script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../assets/js/app.min.js" type="text/javascript"></script>

    <script src="../assets/js/jquery.knob.min.js"></script>


    <body class="skin-green sidebar-mini">
     
     <!-- ==============================================
     Wrapper Section
     =============================================== -->
	 <div class="wrapper">
	 	
        <!-- Include navigation.php. Contains navigation content. -->
	 	<?php include ('template/navigation.php'); ?> 
        <!-- Include sidenav.php. Contains sidebar content. -->
	 	<?php include ('template/sidenav.php'); ?> 
	 	
	 	  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

          <section class="content-header">
              <h1>Live Chat<small>Section</small></h1>
              <ol class="breadcrumb">
                  <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
                  <li class="active">Live Chat</li>
              </ol>
          </section>

        <!-- Main content -->
        <section class="content">	
		    <!-- Include currency.php. Contains header content. -->
		    <?php include ('template/currency.php'); ?>
            <style>
                .single_client {
                    cursor: pointer;
                    border: 1px solid black;
                    margin: 22px 3px;
                    padding: 16px;
                }
                .single_client > img {
                    width: 100px;
                    height:100px;
                }
                .chat-section > div {
                    border: 1px solid lightgrey;
                }
                .message_client {
                    background-color: #00acd6;
                    display: inline-block;
                    padding: 5px 40px;
                    border-radius: 15px;
                    float: left;
                    clear: both;
                }
                .message_temp {
                    background-color: #00b300;
                    display: inline-block;
                    padding: 5px 40px;
                    border-radius: 15px;
                    float: right;
                    clear: both;
                }
            </style>
            <div id="sendtoid" style="display: none"></div>
            <div class="row chat-section">
                <div class="col-lg-2" style="text-align: center">
                    <script>
                        function getMessages(thisRef) {
                            userid = $(thisRef).data('id');
                            $.ajax({
                                url: "getmessages.php",
                                type: 'POST',
                                data: {userid},
                                success: function (data) {
                                    $('#sendtoid').html(userid);
                                    $('#message_form').css('display', '');
                                    var parsed = JSON.parse(data);
                                    parsed.sort(function (a, b) {
                                        return a.sent_at > b.sent_at;
                                    });
                                    var toAppend = "";
                                    var current_messagetype = "";
                                    var last_messagetype = "";
                                    parsed.forEach(function (value, index) {
                                        current_messagetype = value.user_type;
                                        if(current_messagetype == last_messagetype) {
                                            toAppend += '<br />'+value.message;
                                        } else {
                                            if(value.user_type == 2) {
                                                toAppend += '<h4 class="message_temp" data-type="2">'+value.message;
                                            } else {
                                                toAppend += '<h4 class="message_client" data-type="1">'+value.message;
                                            }
                                        }
                                        last_messagetype = value.user_type;
                                    });
                                    $('.single_message').html(toAppend);
                                }
                            })
                        }
                    </script>
                    <!-- foreach get all chatted clients -->
                    <?php
                    if(isset($allClientsData)) {
                        foreach ($allClientsData AS $singleClient) {
                            ?>
                            <div class="single_client" onclick="getMessages(this)" data-id="<?=$singleClient->id?>" data-name="<?=$singleClient->name?>">
                                <img src="https://<?=$_SERVER['HTTP_HOST']?>/hirer/<?=$singleClient->imagelocation?>" alt="">
                                <h4><?=$singleClient->name?></h4>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div class="col-lg-8">
                    <!-- Chat with Client section -->
                    <div class="single_message">
                        <!-- if client message left css class otherwise message to right -->
                        <h2>Please select user to chat</h2>
                    </div><br />
                    <form style="display: none" id="message_form">
                        <div class="form-group">
                            <input type="text" class="form-control" id="typehere"><br />
                            <button class="btn btn-primary" id="btnsend">Send Message</button>
                        </div>
                    </form>
                    <div id="results"></div>
                    <iframe id="ifrm" style="display: none" src="//healthcare-temps.co.uk:3000"></iframe>
                </div>
            </div>
	   </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	 
      <!-- Include footer.php. Contains footer content. -->	
	  <?php include 'template/footer.php'; ?>	
	 	
     </div><!-- /.wrapper -->   
	<!-- ==============================================
	 Scripts
	 =============================================== -->
     <script>
         function getMessage (e) {
             data = JSON.parse(e.data);
             if(data.send_to_id == <?=$freelancer->data()->id?>) {
                 $('.single_message').append('<h4 class="message_client" data-type="1">'+data.message+'</h4>');
             }
         }
         if (window.addEventListener) {
             // For standards-compliant web browsers
             window.addEventListener("message", getMessage, false);
         }
         else {
             window.attachEvent("onmessage", getMessage);
         }
         $(function () {
             $('#btnsend').click(function (e) {
                 e.preventDefault();
                 var dataToSend = JSON.stringify({"user_type": "2", "send_from_id": "<?=$freelancer->data()->id?>", "send_to_id": $('#sendtoid').html(),"message": $('#typehere').val()});
                 $.ajax({
                     url: 'savemessages.php',
                     type: 'POST',
                     data: {"user_type": "2", "send_from_id": "<?=$freelancer->data()->id?>", "send_to_id": 3,"message": $('#typehere').val()},
                     success: function (data) {
                         console.log(data);
                     }
                 });
                 document.getElementById('ifrm').contentWindow.postMessage(dataToSend, '*');
                 $('.single_message').append('<h4 class="message_temp" data-type="2">'+$('#typehere').val()+'</h4>');
                 $('#typehere').val('');
             });

         });
     </script>

</body>
</html>
