<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>
<style>
    .virtue  {
        text-align: center;
        font-size: 36px;
        color: #15aa44;
    }
    .healthcare-hirers h5 {
        text-align: right;
        color: #15aa44;
        font-size: 21px;
    }
</style>
<body class="greybg" style="background-color: #fff;">

<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<style>
    p {
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        line-height: 22px;
        letter-spacing: 0px !important;
    }
</style>
<!-- ==============================================
Hello Section
=============================================== -->
<div id="pages-features" class="content user">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489" title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter" target="_blank"></a> </li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113" title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw" title="Follow me on youtube" target="_blank"> </a> </li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts" title="Follow me on google plus" target="_blank"></a> </li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/" title="Follow me on blogger" target="_blank"></a> </li>
            </ul>
        </div>
        <div id="js-head-menu" class="">
            <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <a class="show hor-space" title="Temps-direct.uk" href="/">
                                <img alt="" src="/img/logo.png">
                            </a>
                        </div>
                        <div class="cantent">
                            <p>
                                Your log-out has been successful.</p>
                            <p>
                                Thank you for using Temps-Direct temp recruiting platform.</p>
                            <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                            <input name="feedback" type="commant">
                            <p>Thank you!</p>
                        </div>
                        <div class="modal-footer hide">
                            <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default">  Close</a>
                        </div>
                    </div>

                </div>
            </div>
            <header id="header">
                <div class="navbar no-mar pr z-top">
                    <div class="navigation-wrapper">
                        <div class="navbar-inner no-pad no-round">
                            <div class="container clearfix">
                                <h1 class="brand top-smspace"> <a href="/" title="healthcare-temps.co.uk" class="show hor-space"><img src="/img/logo.png" alt=""></a></h1>
                                <a class="btn btn-navbar mspace js-no-pjax" data-toggle="collapse" data-target=".nav-collapse"> <i class="icon-align-justify icon-24 whitec"></i></a>
                                <div class="nav-collapse">
                                    <ul class="nav dc">
                                        <!--li class=" divider-left divider-right"> <a href="/page/employer" class="js-no-pjax bootstro" data-bootstro-step="1" data-bootstro-title="Employer?" data-bootstro-placement="bottom"> <i class="icon-briefcase text-20 no-pad"></i> <span class="show top-mspace">Employer?</span></a>
                        </li>
                                        <li class=" divider-left divider-right">
                          <a href="/page/freelancer" class="bootstro" data-bootstro-step="2" data-bootstro-title="Freelancer?" data-bootstro-placement="bottom" title="Freelancer?"><i class="icon-male text-20 no-pad"></i> <span class="show top-mspace">Freelancer?</span></a>				</li-->

                                    </ul>
                                    <div class="pull-right mob-clr tab-clr dc">
                                        <ul class="nav nav-right dc">
                                            <li class="divider-left divider-right tab-sep-none "><a href="/users/register/manual" title="Register">Register</a></li>

                                            <li class="divider-left tab-sep-none divider-right "><a href="/users/login" title="Login">Login</a></li>
                                            <li class=" divider-left tab-sep-none divider-right">
                                                <a href="/page/how-it-works" title="How it Works">How it Works</a>
                                            </li>
                                            <li class="divider-right tab-sep-none">
                                                <form class="form-search bot-mspace mob-searchform">
                                                    <div class="input select">
                                                        <select id="search_type_id" name="search_type_id">
                                                            <option value="0">Portfolios</option>
                                                            <option value="1">Jobs</option>
                                                            <option value="2">Freelance Projects</option>
                                                            <option value="3">Freelancers</option>
                                                            <option value="4">Services</option>
                                                        </select>
                                                    </div>
                                                    <div class="input text">
                                                        <label class="hide" for="search">Search</label>
                                                        <input id="search" placeholder="Search" name="q" class="span18" type="search">
                                                        <label class="hide" for="Search">Search</label>
                                                        <input value="Search" title="Search" class="btn" id="Search" type="submit">
                                                    </div>
                                                </form>
                                                <a id="trigger-search" href="#" class="js-no-pjax" title="Search"><i class="icon-search text-20"></i></a>
                                                <!--?php// } ?-->
                                            </li>
                                            <li class="divider-left tab-sep-none ">
                                                <a href="/" title="Home">Home</a>
                                            </li>
                                            <li class="divider-left tab-sep-none ">
                                                <a href="#"><span style="display: block;position: absolute;margin-left: 22px;">0800-368-8769</span><i class="fa fa-phone" aria-hidden="true" style="font-size: 30px;"></i></a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="nav-search">
                        <div class="search-input-container">
                            <form action="/users" class="form-search blue-btn-bg bot-mspace home-search-form clearfix {&quot;user_url&quot;:&quot;/users&quot;}" id="UserViewForm" method="get" accept-charset="utf-8"><div style="display:none;"><input name="_Token" value="fc5d2a3185fd52dcb51a66a30a60f343b05d4c72" id="Token653252732" type="hidden"></div><div class="input select"><select name="search_type_id" id="search_type_id">
                                        <option value="3">Temps</option>
                                        <option value="2">Temp Jobs</option>
                                    </select></div><div class="input text right-space"><label for="UserQ">Location</label><input name="q" id="UserQ" type="text"></div><div class="submit"><input value="Go" type="submit"></div><div style="display:none;"><input name="_Token" value="2b7195b6dfe8c13a4d606cf90df5de63477dad34%3A" id="TokenFields1498596274" type="hidden"><input name="_Token" value="Page.Add%7CPage.Preview%7CPage.Update%7CPage.content%7CPage.description_meta_tag%7CPage.id%7CPage.meta_keywords%7CPage.parent_id%7CPage.slug%7CPage.status_option_id%7CPage.title%7C_wysihtml5_mode" id="TokenUnlocked379987697" type="hidden"></div></form>	  </div>
                    </div>
                </div>
            </header>
            <script type="text/javascript">
                //<![CDATA[
                function getCookie (c_name) {var c_value = document.cookie;var c_start = c_value.indexOf(" " + c_name + "=");if (c_start == -1) {c_start = c_value.indexOf(c_name + "=");}if (c_start == -1) {c_value = null;} else {c_start = c_value.indexOf("=", c_start) + 1;var c_end = c_value.indexOf(";", c_start);if (c_end == -1) {c_end = c_value.length;}c_value = unescape(c_value.substring(c_start,c_end));}return c_value;}if (getCookie('il')) {(function() {var js = document.createElement('script'); js.type = 'text/javascript'; js.async = true;js.src = "https://healthcare-temps.co.uk/users/show_header.js?u=";var s = document.getElementById('js-head-menu'); s.parentNode.insertBefore(js, s);})();} else {document.getElementById('js-head-menu').className = '';}
                //]]>
            </script>
        </div>
        <!-- <div id="pjax-body" class="clearfix mob-space mob-no-mar" >-->
        <div id="pjax-body">
            <div id="main" class="main">
                <div class="container">
                    <title>Demo</title>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <!--Bootstrap Css-->
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                    <!--Jquery-->
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                    <!--Javascript-->
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                    <!--Font awesome-->
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                    <!-- Custom Css-->
                    <style>
                        .main-feat {
                            text-align: center;
                        }
                        .main-feat h5 {
                            color: #15aa44;
                            font-size: 36px;
                        }
                        .main-feat p {
                            font-size: 19px;
                            color: #888888;
                        }
                        .healthcare-hirers h5 {
                            text-align: right;
                            color: #15aa44;
                            font-size: 21px;
                        }

                        .communications p {
                            color: #737272;
                        }
                        .multidevices p {
                            color: #737272;
                        }
                        .vetting p {
                            color: #737272;
                        }
                        .social-media-left p {
                            color: #737272;
                        }
                        .communications h5 {
                            color: #737272;
                        }
                        .multidevices h5 {
                            color: #737272;
                        }
                        .vetting h5 {
                            color: #737272;
                        }
                        .social-media-left h5 {
                            color: #737272;
                        }
                        .mobile-img {
                            text-align: center;
                        }
                        .Healthcare-Temps h5 {
                            color: #15aa44;
                            font-size: 21px;
                        }
                        .connect1 h5 {
                            color: #737272;
                        }
                        .reporting h5 {
                            color: #737272;
                        }
                        .real-benefits h5 {
                            color: #737272;
                        }
                        .social-media-right h5 {
                            color: #737272;
                        }
                        .connect1 p {
                            color: #737272;
                            margin-left: 35px;
                            margin-bottom: 31px;
                        }
                        .reporting p {
                            color: #737272;
                            margin-left: 35px;
                        }
                        .real-benefits p {
                            color: #737272;
                            margin-left: 35px;
                        }
                        .social-media-right p {
                            color: #737272;
                            margin-left: 35px;
                        }
                        .connect1 h5 span {
                            margin-right: 16px;
                        }
                        .reporting h5 span {
                            margin-right: 16px;
                        }
                        .real-benefits h5 span {
                            margin-right: 16px;
                        }
                        .social-media-right h5 span {
                            margin-right: 16px;
                        }
                        .communications h5 span {
                            margin-left: 16px;
                        }
                        .multidevices h5 span {
                            margin-left: 16px;
                        }
                        .vetting h5 span {
                            margin-left: 16px;
                        }
                        .social-media-left h5 span {
                            margin-left: 16px;
                        }
                        .communications {
                            margin-top: 32px;
                        }
                        .connect {
                            margin-top: 32px;
                        }
                        .social-media-right {
                            margin-top: 23px;
                        }
                        .coming-soon {
                            text-align: center;
                        }
                        .coming-soon h5 {
                            font-size: 21px;
                        }
                        .coming-soon a img {
                            width: 96px;
                        }
                        .patient_video {
                            width: 211px;
                            position: relative;
                            left: 463px;
                            top: 330px;
                            z-index: 7;
                        }
                        .doctor_video {
                            width: 209px;
                            position: relative;
                            left: 249px;
                            top: 495px;
                            z-index: 7;
                            height: 166px;
                        }
                    </style>

                    <div class="container">
<!--                        <video class="patient_video" src="https://ak2.picdn.net/shutterstock/videos/6859732/preview/stock-footage-african-man-explaining-neck-pain-to-camera.mp4" autoplay muted></video>-->
<!--                        <video class="doctor_video" src="./myassets/doc_2.mp4" autoplay muted></video>-->
                        <div class="row">
                            <div class="col-lg-12 main-feat">
                                <h5>Main Features</h5>
                                <p>Healthcare-Professionals on-Demand 24/7<br>
                                    Making healthcare accessible to all
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 healthcare-hirers">
                                <h5>Healthcare Hirers:</h5>
                                <div class="communications">
                                    <h5>AUTOMATION<span><img src="./myassets/img/bak-ai.png"></span></h5>
                                    <p> A fresh approach to hiring care professionals<br />
                                        by automating the recruitment process. From<br />
                                        vetting to rota mgnt via cognitive intelligence.<br />
                                    </p>
                                </div>
                                <div class="vetting">
                                    <h5>VETTING<span><img src="../img/calender.jpg"></span></h5>
                                    <p>Conduct online vetting, background checks, DBS <br>
                                        verification, Staff traffing, Dispute management <br>
                                        &amp; Secure online payment.</p>
                                </div>
                                <div class="multidevices">
                                    <h5>REMOTE PATIENT CARE<span><img src="../img/gallery.jpg"></span></h5>
                                    <p>Smart technology is utilised to bring world class<br />
                                        healthcare to homes, clinics and hospitals in<br />
                                        remote areas. Robotics, Tele-medicine etc </p>
                                </div>
                                <div class="social-media-left">
                                    <h5>COMMUNICATIONS<span><img src="../img/sms.jpg"></span></h5>
                                    <p>Communicate with healthcare professionals via<br />
                                        encrypted video, audio and inbound text messages.<br />
                                        Conduct online interview, video consultation and<br />
                                        more. </p>
                                </div>
                            </div>
                            <div class="col-lg-4 mobile-img" style="margin-top: -331px;">
                                <div style="overflow: hidden;width: 208px;top: 381px;border-radius: 5px;left: 74px;position: relative;">
                                    <video autoplay muted loop src="./myassets/stock-footage-african-man-explaining-neck-pain-to-camera.mp4" style="height: 145px;" type="video/mp4"></video>
                                    <video autoplay muted loop src="./myassets/doc_2.mp4" style="width: 100%;" type="video/mp4"></video>
                                </div>
                                <img src="./myassets/img/mobile.jpg">
                            </div>
                            <div class="col-lg-4 Healthcare-Temps">
                                <h5>Healthcare Temps:</h5>
                                <div class="connect1">
                                    <h5><span><img src="./myassets/img/mic.png"></span>PHJA</h5>
                                    <p>Your Personal Healthcare job Assistant that<br />
                                        helps you create Digital CV, paperless time<br />
                                        sheet & invoice. Wages direct to account etc. </p>
                                </div>
                                <div class="reporting">
                                    <h5><span><img src="../img/smobile.jpg"></span>MULTIPLE DEVICES </h5>
                                    <p>Access medical & healthcare jobs directly on<br />
                                        Mobile. Tablet pc, Laptop & more. Online<br />
                                        interviews, live chat & real time notfications.</p>
                                </div>
                                <div class="real-benefits">
                                    <h5><span><img src="../img/suitcase.jpg"></span>REAL BENEFITS</h5>
                                    <p>No overheads, no start-up cost, low risk, free <br>
                                        gifts, unlimited earnings &amp; perks. </p>
                                </div>
                                <div class="social-media-right">
                                    <h5><span><img src="../img/share-l.jpg"></span>SOCIAL MEDIA INTEGRATION</h5>
                                    <p>Post reviews and rate Hirers / Employers. Share <br>
                                        your profile and successes on Facebook, Twitter etc. </p>
                                </div>
                            </div>
                        </div>
                        <div class="coming-soon">
                            <h5>Coming Soon</h5>
                            <a href=""><img src="../img/app-store.png"></a> <a href=""><img src="../img/playstore.png" <="" a="">
                            </a></div><a href="">
                        </a></div><a href="">


                    </a></div><a href="">
                </a></div><a href="">
            </a></div><a href="">
            <!-- for modal -->
        </a><div class="modal hide fade" id="js-ajax-modal"><a href="">
                <div class="modal-body"></div>
            </a><div class="modal-footer"><a href=""></a><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a> </div>
        </div>
        <!-- for modal -->
        <!-- for modal -->
        <div class="modal hide fade" id="js-ajax-modal-child">
            <div class="modal-body"></div>
            <div class="modal-footer"><a href="#" class="btn js-no-pjax" data-dismiss="modal">Close</a> </div>
        </div>
        <!-- for modal -->

        <div class="clearfix"></div>
        <!-- for modal
        <div class="footer-push"></div> -->
    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

<a id="scrollup">Scroll</a>

<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Kafe JS -->
<script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
