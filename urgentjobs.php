<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
    header('Location: install/');
    exit;
}else{
    require_once 'core/frontinit.php';
}

//echo '<pre>';print_r($_POST);echo '</pre>';

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<style>
    footer {
        position: absolute;
        bottom: 0;
    }
</style>
<!-- Include header.php. Contains header content. -->
<?php include ('includes/template/header.php'); ?>

<body class="greybg">

<!-- Include navigation.php. Contains navigation content. -->
<?php include ('includes/template/navigation.php'); ?>

<!-- ==============================================
Header
=============================================== -->

<div class="container">
    <div class="content">
        <div class="row">

            <?php
            //Start new Admin object
            $admin = new Admin();
            //Start new Client object
            $client = new Client();
            //Start new Freelancer object
            $freelancer = new Freelancer();

            if ($admin->isLoggedIn()) { ?>
            <?php } elseif($freelancer->isLoggedIn()) { ?>
            <?php } elseif($client->isLoggedIn()) { ?>


<!--                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">-->
<!--                    <a href="hirer/addjob.php" class="kafe-btn kafe-btn-mint full-width revealOnScroll" data-animation="bounceIn" data-timeout="400">-->
<!--                        <i class="fa fa-tags"></i>-->
<!--                        --><?php //echo $lang['post']; ?><!-- --><?php //echo $lang['a']; ?><!-- --><?php //echo $lang['job']; ?><!--, --><?php //echo $lang['it\'s']; ?><!-- --><?php //echo $lang['free']; ?><!-- !-->
<!--                    </a>-->
<!--                </div><!-- /.col-lg-3 -->-->
<!--            --><?php //} else { ?>
<!--                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">-->
<!--                    <a href="login.php" class="kafe-btn kafe-btn-mint full-width revealOnScroll" data-animation="bounceIn" data-timeout="400">-->
<!--                        <i class="fa fa-tags"></i>-->
<!--                        --><?php //echo $lang['post']; ?>
<!--                        --><?php //echo $lang['a']; ?>
<!--                        --><?php //echo $lang['job']; ?><!--,-->
<!--                        --><?php //echo $lang['it\'s']; ?>
<!--                        --><?php //echo $lang['free']; ?><!-- !-->
<!--                    </a>-->
<!--                </div><!-- /.col-lg-3 -->-->

            <?php } ?>

        </div> <!-- /.row -->
    </div> <!-- /.content -->


</div><!-- /.container -->


<!-- ==============================================
Jobs Section
=============================================== -->
<style>
    div.search-jobs-urgents input,
    div.search-jobs-urgents button,
    div.search-jobs-urgents select
    {
        margin-top: 7px;
    }

    div.job
    {
        /*box-shadow: 12px 12px 2px 1px rgba(0, 0, 255, .2);*/
    }

</style>
<section class="jobslist" style="margin-top: 27px;">

    <div class="container">

        <div class="row">
        
        <form name="jobserach" id="jobsearch" method="post" action="urgentjobs.php">

            <div  style="text-align:center;height:60px;margin-top:10%;background-color:#66BD00 !important;" class="search-jobs-urgents col-lg-12">
                <!--<input placeholder="Care Assisstant" style="height: 38px;width:20%" type="text" class="form-control">-->

                <?php

                           $TempTypeQuery =  DB::getInstance()->get("temp_type", "*");

                     if ($TempTypeQuery->count()) {

                        $temp_type = $TempTypeQuery->results();
                           ?>
                           <select name="temp_type_id" style="width:20%;display: inline-block" class="form-control">
                           <option value="">Temp Type:</option>
                           <?php
                           foreach($temp_type AS $ttype) {
                               ?>
                               <option value="<?=$ttype->id?>" <?=($ttype->id == $type_id ? 'selected' : '')?>><?=$ttype->title?></option>
                               <?php
                           }
                        }
                           ?>
                       </select>


               
                <?php                            

                           $locationQuery =  DB::getInstance()->get("locations", "*",["ORDER" => "location ASC",  "AND" => ["active" => 1]]);

                     if ($locationQuery->count()) {

                        $locations = $locationQuery->results();
                           ?>
                           <select style="width:20%;display: inline-block" class="form-control" id="location" name="country" required>
                           <option value="">Location</option>
                           <?php
                           foreach($locations AS $location) {
                               ?>
                               <option value="<?=$location->location?>" ><?=$location->location?></option>
                               <?php
                           }
                        }
                           ?>
                       </select>

               <!-- <select style="width:20%;display: inline-block" class="form-control">
                    <option value="10 miles">10 miles</option>
                </select>-->
                <!--<button class="btn btn-warning" type="button" style="margin-top:1%;background-color:#E25600;!important;color:#FFE199">Search</button>-->
                <input type="submit" name="search" value="Search" class="
                submit-btn btn btn-warning" style="margin-top:1%;background-color:#E25600;!important;color:#FFE199"/>
               <!-- <input type="reset" name="search" value="Reset" id="resetSearch" class="btn btn-warning" style="margin-top:1%;background-color:#E25600;!important;color:#FFE199"/>-->
            </div>
            </form>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 white padding0">

                <!--		 <form action="searchpage.php" method="get" class="list-search revealOnScroll" data-animation="fadeInDown" data-timeout="200">-->
                <!--		  <button><i class="fa fa-search"></i></button>-->
                <!--		  <input type="text" class="form-control" name="searchterm" style="height: 55px;" placeholder="--><?php //echo $lang['job']; ?><!-- --><?php //echo $lang['title']; ?><!--, --><?php //echo $lang['keywords']; ?><!-- --><?php //echo $lang['or']; ?><!-- --><?php //echo $lang['company']; ?><!-- --><?php //echo $lang['name']; ?><!--" value=""/>-->
                <!--		  <div class="clearfix"></div>-->
                <!--		 </form>-->

                <?php

                //print_r($_POST);

                $conditions = '';

                if(isset($_POST['search']) && $_POST['search'] == 'Search'){

                    if((isset($_POST['temp_type_id']) && !empty($_POST['temp_type_id'])) && (isset($_POST['country']) && !empty($_POST['country']))){

                        $conditions = ' AND temp_type_id = "'.$_POST['temp_type_id'].'" AND `country` = "'.$_POST['country'].'" ';

                    }elseif(empty($_POST['temp_type_id']) && !empty($_POST['country'])){

                        $conditions = ' AND `country` = "'.$_POST['country'].'"';

                    }elseif(!empty($_POST['temp_type_id']) && empty($_POST['country'])){

                        $conditions = ' AND temp_type_id = "'.$_POST['temp_type_id'].'"';
                    }else{

                    }

                    

                }

               // $query = DB::getInstance()->get("job", "*", ["ORDER" => "date_added DESC",  "AND" => ["active" => 1, "delete_remove" => 0, "chk_urgent" => 1]]);



               // $query = DB::getInstance()->query("SELECT  *  FROM `job` where `active` = '1' AND `delete_remove` = '0' AND `chk_urgent` = '1'  ".$conditions."  ORDER BY date_added DESC ");

                $settingsq =  DB::getInstance()->get("settings", "*");
                if ($settingsq->count()) {

                  $settings = $settingsq->results();
                  $urgentHours = $settings[0]->urgent_job_hours;
                  //echo '<pre>';print_r($settings);echo '</pre>';
                }

                 $query = DB::getInstance()->query("SELECT  *  FROM `job` where `active` = '1' AND `delete_remove` = '0' AND `chk_urgent` = '1' AND `date_added` >= (NOW() - INTERVAL ".$urgentHours." DAY) AND `date_added`  < NOW()  ".$conditions."  ORDER BY date_added DESC ");


                  if($query->count() > 0) {

                    $jresults = $query->results();

                    //echo '<pre>';print_r($jresults);echo '</pre>';
                    $serviceList = '';
                    $x = 1;

                foreach($query->results() as $row) 
                {

                    $cenvertedTime = date('Y-m-d H:i:s',strtotime('-48 hour',strtotime($startTime)));

                   $serviceList = '';
                   $date_added = ago(strtotime($row->date_added));


                    $blurb = truncateHtml($row->description, 400);

                    $skills = explode(',',$row->skills );
                     $pskill = '';
                     foreach ($skills as $key => $skill) {
                        $pskill .= '<span class="bg-dynamic">'.$skill.'</span>';
                     }
                    $skills_each =  '<label class="label label-success"><span>'. $row->skills .'</span></label> &nbsp;';

                   $clientquery =  DB::getInstance()->get("client", "*", ["clientid" => $row->clientid]);

                     if ($clientquery->count()) {

                        $clientdetails = $clientquery->results();

                            foreach ($clientquery->results() as $clientrow) {
                                $clientName = $clientrow->name;
                                $clientusername = $clientrow->username;
                                $clientimagelocation = $clientrow->imagelocation;
                            }

                        }


                        $profilequery =  DB::getInstance()->get("profile", "*", ["userid" => $row->clientid]);

                     if ($profilequery->count()) {

                        $profiledetails = $profilequery->results();

                        //echo '<pre>';print_r($profiledetail);echo '</pre>';exit;

                            foreach ($profilequery->results() as $profilerow) {
                                $company_name = $profilerow->company_name;
                                $care_settings = $profilerow->care_settings;
                                $no_patients = $profilerow->no_patients;
                                $about = $profilerow->about;
                            }

                        }


                        $TempTypeQuery =  DB::getInstance()->get("temp_type", "*", ["id" => $row->temp_type_id]);

                     if ($TempTypeQuery->count()) {

                        $tempTypedetails = $TempTypeQuery->results();
                            foreach ($TempTypeQuery->results() as $r2) {
                                $TempTypeName = $r2->title;
                                
                            }
                        }


                         $jobCategoryquery =  DB::getInstance()->get("category", "*", ["id" => $row->temp_category_id]);

                     if ($jobCategoryquery->count()) {

                        $jobCatdetails = $jobCategoryquery->results();
                            foreach ($jobCategoryquery->results() as $r3) {
                                $categoryName = $r3->name;
                                
                            }
                        }

                        

                    $tempRateId =  DB::getInstance()->get("temp_rate", "*", ["id" => $row->temp_rate_id]);

                     if ($tempRateId->count()) {

                        $tempRatedetails = $tempRateId->results();
                            foreach ($tempRateId->results() as $r4) {
                                $rangemin = $r4->rangemin;
                                $rangemax = $r4->rangemax;
                                $title = $r4->title;
                                $temp_rateId = $r4->id;
                                
                            }
                        }


                        $tempshift =  DB::getInstance()->get("temp_shift", "*", ["id" => $row->temp_shift_id]);

                     if ($tempshift->count()) {

                        $tempShiftdetails = $tempshift->results();
                            foreach ($tempshift->results() as $r5) {
                                
                                $shiftTitle = $r5->title;
                                
                                
                            }
                        }


                        //Start new Admin object
                        $admin = new Admin();
                        //Start new Client object
                        $client = new Client();
                        //Start new Freelancer object
                        $freelancer = new Freelancer();

                        
                            $sen ='     
                 <a href="job_details.php?title='.$row->jobid.'" class="kafe-btn kafe-btn-mint-small">
                 View</a>
             ';
                       
                        
                        echo $serviceList .= '

		    
				 <div class="job job-section">	
				 
				  <div class="row top-sec">
				   <div class="col-lg-12">
					<div class="col-lg-2 col-xs-12">
					<div class="profile-icon" style="width:115px;border-right:1px solid black;border-bottom:1px solid black"" class"temp-img">
					 <a href="#">
					  <img style="width:70px; height: 115px;" class="img-responsive" src="hirer/'. escape($clientimagelocation) .'"  alt="">
					  
					 </a>
					 </div>
					</div><!-- /.col-lg-2 -->
					<div  class="col-lg-8 col-xs-12 company-profile"> 
					 <h4><a style="color:#05cb95 !important;"href="#"><i class="fa fa-user-md"></i> '.escape($TempTypeName) .'</a></h4>
					 <p><i class="fa fa-map-marker"></i>'. escape($row->country) .'</p>

                     <p><i>'.$currency_symbol.'</i> '. $rangemin .($rangemax ? ' - '. $rangemax : '+') .'</p>
                    

                     <p><i class="fa fa-building" aria-hidden="true"></i> '.escape($company_name).'</p>

                     <p><i class="fa fa-building" aria-hidden="true"></i> '.escape($shiftTitle).'</p>
                     


					</div><!-- /.col-lg-10 -->
					<div class="col-lg-2">
					<div style="display:inline;white-space:nowrap;position:relative;left: -150px;top: 25px;">
					<span style="color:#05cb95!important;"><i class="fa fa-heart-o"></i> &nbsp;Save</span>
					<span style="margin-left:20px;color:#05cb95!important;"><i class="fa fa-envelope"></i> &nbsp;Email</span></div>
					 '.$sen.'
					</div>
					
				   </div><!-- /.col-lg-12 -->
				  </div><!-- /.row -->
				  
				  <div class="row mid-sec">			 
				   <div class="col-lg-12">
				   <div class="col-lg-2"></div>
               <button style="float:right;margin-right:30px;position:  absolute;bottom: 50px;right: 50px;" type="button" class="btn btn-xs btn-warning">URGENT</button>
				   <div class="col-lg-8 description-section">
                    <p><i class="fa fa-fw fa-bars"></i> <span>'.$lang['jobdescription'].': '.'</span>'.$blurb.'</p>
                    <p class="background-separator">
                    <i></i>
                    <div class="background-none">Skill Required:</div>'.
                      $pskill.'
                      </p>
                       <div class="location-section">
                         <div class="col-lg-3 padding0">
                          <label>'.$lang['location'].':</label>
                          <p><i class="fa fa-map-marker"></i> '.escape($row->country).'</p>
                         </div>
                         <div class="col-lg-3">
                          <label>'.$lang['rate_hour'].'</label>
                           <p>'.$rangemin .($rangemax ? ' + '. $rangemax : '+') .'</p>
                         </div>
                         <div class="col-lg-6">
                          <label>'.$lang['timeposted'].'</label>
                           <p>'.$date_added.'</p>
                         </div>
                       </div>
				   </div><!-- /.col-lg-12 -->

				   </div><!-- /.col-lg-12 -->
				  </div><!-- /.row -->
				  
				  <div class="row bottom-sec">
				   <div class="col-lg-12">
					
					<div class="col-lg-12">
					 <hr class="small-hr">
					</div> 
					
					
                   
					
					
				   </div><!-- /.col-lg-12 -->
				  </div><!-- /.row -->
				 
				 </div><!-- /.job -->
		 			
					 ';

                        unset($serviceList);
                        unset($skills_each);
                         //unset($_POST);
                        //unset($sen);
                        $x++;
            }
                }else 

                {
                    echo $serviceList = '<div class="job">  
          
                    <div class="row top-sec">
                     <div class="col-lg-12">
                    <div class="col-xs-12">
                        <p style="text-align:center;">'.$lang['no_urgent_jobs'].'</p>
                        </div></div></div>';
                }

                //print
                //echo Pagination($total,$limit,$page);
                
                ?>

            </div><!-- /.col-lg-8 -->
        </div><!-- /.row -->
         <div>
            
            
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
          (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-2199989077469674",
            enable_page_level_ads: true
          });
        </script>


       </div>
    </div><!-- /.container-fluid -->
</section><!-- /section -->

<!-- Include footer.php. Contains footer content. -->
<?php include 'includes/template/footer1.php'; ?>




  




<!-- ==============================================
Scripts
=============================================== -->

<script>
$(document).ready(function() {
     $('#resetSearch').click(function(){
        alert('hi');
              <?php 
                unset($_POST);
                
                
              ?>
              location.reload('http://172.10.1.4:8844/urgentjobs.php');
     });
    });
</script>

<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Waypoints JS -->
<script src="assets/js/waypoints.min.js" type="text/javascript"></script>
<!-- Kafe JS -->
<script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
