<?php
//Check if init.php exists
if(!file_exists('core/frontinit.php')){
	header('Location: install/');        
    exit;
}else{
 require_once 'core/frontinit.php';	
}

//Edit Data
if (Input::exists()) {
 if(Token::check(Input::get('token'))){

	$errorHandler = new ErrorHandler;
	
	$validator = new Validator($errorHandler);
	
	$validation = $validator->check($_POST, [
	  'name' => [
	     'required' => true,
	     'minlength' => 2
	  ],
	  'email' => [
	     'required' => true,
	     'minlength' => 2
	  ],
	  'message' => [
	     'required' => true,
	     'minlength' => 2
	  ]
	]);
		 
    if (!$validation->fails()) {
		

		$subject = 'New message to '.$title.' from '.Input::get('name');
		$sendCopy = trim($_POST['sendCopy']);
		$body = "Name: Input::get('name') \n\nEmail: Input::get('email') \n\nSubject: $subject \n\nMessage: Input::get('message')";
		$headers = 'From: ' .' <'.Input::get('email').'>' . "\r\n" . 'Reply-To: ' . $mail;

		mail($mail, $subject, $body, $headers);
        
        $noError = true;
			
	 } else {
     $error = '';
     foreach ($validation->errors()->all() as $err) {
     	$str = implode(" ",$err);
     	$error .= '
	           <div class="alert alert-danger fade in">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <strong>Error!</strong> '.$str.'
		       </div>
		       ';
     }
   }

  }
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->
	
    <!-- Include header.php. Contains header content. -->
    <?php include ('includes/template/header.php'); ?>

<style>
    .side_icons {
        margin-top: -9px;
        padding-left: 97px;
        padding-top: 50px;
        color: #83c5c3;
        font-size: 21px;
        line-height: 2em;
    }
    footer {
        padding-top: 0 !important;
    }
</style>

<body class="greybg">
	
     <!-- Include navigation.php. Contains navigation content. -->
 	 <?php include ('includes/template/navigation.php'); ?>
     <div>
         <img src="myassets/images/contactus_bg.png" alt="Contact us">
     </div>
     <div class="container">
         <div class="row">
             <ul class="nav nav-tabs" style="margin-bottom: 0;">
                 <li class="active"><a data-toggle="tab" href="#contact_sales" style="color: black !important;">Contact Sales</a></li>
                 <li><a data-toggle="tab" href="#contact_sales" style="color: black !important;">Contact Support</a></li>
                 <li><a data-toggle="tab" href="#contact_sales" style="color: black !important;">Contact Account</a></li>
                 <li><a data-toggle="tab" href="#contact_sales" style="color: black !important;">Redeem Loyalty Bonus</a></li>
             </ul>
             <div class="col-sm-12 col-md-6 col-lg-6" style="border: 1px solid black; padding: 40px;margin-bottom: 40px;">
                 <div class="tab-content">
                     <div id="contact_sales" class="tab-pane fade in active">
                         <form>
                             <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-user"></i>
                            </span>
                                 <input id="email" type="text" class="form-control" name="email" placeholder="Email" style="height: 100%">
                             </div>
                             <br />
                             <div class="input-group">
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-lock"></i>
                            </span>
                                 <input id="password" type="password" class="form-control" name="password" placeholder="Password" style="height: 100%">
                             </div>
                             <br />
                             <div class="form-group ">
                                 <label for="message">Message</label>
                                 <textarea name="message" id="message" rows="5" class="form-control"></textarea>
                             </div>
                         </form>
                     </div>
                     <div id="menu1" class="tab-pane fade">
                         <h3>Menu 1</h3>
                         <p>Some content in menu 1.</p>
                     </div>
                     <div id="menu2" class="tab-pane fade">
                         <h3>Menu 2</h3>
                         <p>Some content in menu 2.</p>
                     </div>
                 </div>
             </div>
             <div class="col-sm-12 col-md-6 col-lg-6 side_icons">
                 <div>
                     <i class="fa fa-map-pin"></i>
                     <span>152 City Road, London EC1V 2NX</span>
                 </div>
                 <div>
                     <i class="fa fa-skype"></i>
                     <span>Healthcare.Temps</span>
                 </div>
                 <div>
                     <i class="fa fa-phone"></i>
                     <span>+44 203-325-2591</span>
                 </div>
                 <div>
                     <i class="fa fa-envelope"></i>
                     <span>chat.bot-Healthcare-temps</span>
                 </div>
             </div>
         </div>
     </div>
     <!-- Include footer.php. Contains footer content. -->
 	 <?php include ('includes/template/footer.php'); ?> 
	 
     <a id="scrollup">Scroll</a>
	 
     <!-- ==============================================
	 Scripts
	 =============================================== -->
	 
     <!-- jQuery 2.1.4 -->
     <script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
     <!-- Bootstrap 3.3.6 JS -->
     <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
     <!-- Kafe JS -->
     <script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
