<?php
//Check if init.php exists
if (!file_exists('core/frontinit.php')) {
    header('Location: install/');
    exit;
} else {
    require_once 'core/frontinit.php';
}

?>
<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Include header.php. Contains header content. -->
<?php include('includes/template/header.php'); ?>
<body class="greybg" style="background-color: #fff;">

<!-- Include navigation.php. Contains navigation content. -->
<?php include('includes/template/navigation.php'); ?>

<style>
    .h2des{
        border-bottom: 3px solid #378139;
        display: inline;
        color:#3f9a41;
    }
    .pdes{
        margin-top: 22px;
        font-size: 20px;
        margin-bottom: 35px;
    }
    .container{
        margin-top: 50px;
        margin-bottom: 50px;
    }
    .otherkey{
        text-align: center;
        margin-top: 50px;
        margin-bottom: 50px;
    }
    b{
        line-height: 50px;
    }
    .remote{
        line-height: 50px;
    }
    .imglinehight{
        margin-top: 10px;
    }
    li {
        list-style: none;
        font-size: 16px;
    }
    .dotted_ul li{
        list-style: outside;
    }
</style>
<!-- ==============================================
Hello Section
=============================================== -->
<div id="pages-features" class="content user" style="margin-top: 100px;">
    <div class="wrapper">
        <div class="social_icn">
            <ul class="unstyled clearfix pull-right media-icons">
                <li class="facebook"><a class="grayc" href="https://www.facebook.com/profile.php?id=100012090310489"
                                        title="Follow me on facebook" target="_blank"></a></li>
                <li class="twitter"><a class="grayc" href="http://twitter.com/temps_direct" title="Follow me on twitter"
                                       target="_blank"></a></li>
                <li class="linkedin"><a class="grayc" href="https://uk.linkedin.com/in/temps-direct-b92620113"
                                        title="Follow me on linkedin" target="_blank"></a></li>
                <li class="youtube"><a class="grayc" href="https://www.youtube.com/channel/UCkZtVJJiZYnwduhCjL6EFSw"
                                       title="Follow me on youtube" target="_blank"> </a></li>
                <li class="google-plus"><a class="grayc" href="https://plus.google.com/u/0/103726647534950244788/posts"
                                           title="Follow me on google plus" target="_blank"></a></li>
                <li class="blogger"><a class="grayc" href="http://medicaltempsandlocums.blogspot.co.uk/"
                                       title="Follow me on blogger" target="_blank"></a></li>
            </ul>
        </div>
        <div id="js-head-menu" class="">
            <div id="logoutmodel" class="modal fade" role="dialog" style="display:none;">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <a class="show hor-space" title="Temps-direct.uk" href="/">
                                <img alt="" src="myassets/img/logo.png">
                            </a>
                        </div>
                        <div class="cantent">
                            <p>
                                Your log-out has been successful.</p>
                            <p>
                                Thank you for using Temps-Direct temp recruiting platform.</p>
                            <p>Please feel free to leave your feedback or recommend a friend in the box below.</p>
                            <input name="feedback" type="commant">
                            <p>Thank you!</p>
                        </div>
                        <div class="modal-footer hide">
                            <a href="https://healthcare-temps.co.uk/users/logout" class="btn btn-default"> Close</a>
                        </div>
                    </div>

                </div>
            </div>

            <script type="text/javascript">
                //<![CDATA[
                function getCookie(c_name) {
                    var c_value = document.cookie;
                    var c_start = c_value.indexOf(" " + c_name + "=");
                    if (c_start == -1) {
                        c_start = c_value.indexOf(c_name + "=");
                    }
                    if (c_start == -1) {
                        c_value = null;
                    } else {
                        c_start = c_value.indexOf("=", c_start) + 1;
                        var c_end = c_value.indexOf(";", c_start);
                        if (c_end == -1) {
                            c_end = c_value.length;
                        }
                        c_value = unescape(c_value.substring(c_start, c_end));
                    }
                    return c_value;
                }
                if (getCookie('il')) {
                    (function () {
                        var js = document.createElement('script');
                        js.type = 'text/javascript';
                        js.async = true;
                        js.src = "https://healthcare-temps.co.uk/users/show_header.js?u=";
                        var s = document.getElementById('js-head-menu');
                        s.parentNode.insertBefore(js, s);
                    })();
                } else {
                    document.getElementById('js-head-menu').className = '';
                }
                //]]>
            </script>
        </div>

        <div class="container">
            <div class="row">

            </div>
            <div style="text-align: center; color: #00b300">
                <h1>TERMS’ OF USE POLICY</h1>
            </div>
            <p>
                In order to maintain a safe and trusted marketplace www.healthcare-temps.co.uk and all related and
                affiliated websites (the “Site”) and to avoid use of the Site for unauthorised or unintended purposes,
                we require you and all other users of the Site (each, a “User”) to agree to and comply with these
                Terms of Use (the “Terms”). These Terms set forth the acceptable and prohibited uses of our Site
                and are in addition to the other Terms of Service. By accessing the Site or using any of the Site
                Services after the effective date, you agree to these Terms. You are also independently responsible
                for complying with all applicable laws related to your use of the Site or the Site Services whether or
                not covered by the Terms.
            </p>

            <ol>
                <li>
                    <h3>1. PROHIBITED SITE USE</h3>
                    <p>
                        The uses described in these Terms are prohibited regardless of where on the Site they occur. For
                        example, the activities are prohibited in job posts, job bids, inbox messages, communications with
                        customer service or disputes, the community forum, and Hirer or Temp Worker feedback.
                    </p>
                    <ol>
                        <li>
                            <h3>1.1 Illegal, Fraudulent, Harmful, or Offensive Uses</h3>
                            <p>
                                You may not use, or encourage, promote, facilitate, or instruct or induce others to use, the Site or
                                Services for any activities that violate any law, statute, ordinance or regulation; for any other illegal,
                                fraudulent, harmful, or offensive purpose; or to transmit, store, display, distribute or otherwise make
                                available content that is illegal, harmful, or offensive.<br />

                                Examples of prohibited uses of the Site or Site Services include:
                            </p>
                            <ul class="dotted_ul">
                                <li>Seeking, offering, or endorsing illegal, obscene, or pornographic services or activities,
                                    including services (i) that would violate the intellectual property rights, including copyrights, of
                                    another person, entity, service, product, or website or (ii) that would involve the creation,
                                    review, or editing of pornographic, erotic, obscene, or sexually explicit material.
                                </li>
                                <li>Posting content that is offensive, defamatory, profane, vulgar, obscene, threatening,
                                    discriminatory, illegal, pornographic, obscene or sexually explicit in nature.
                                </li>
                                <li>
                                    Seeking, offering, or endorsing any services that would violate Healthcare-Temps’ Terms of
                                    Service or the terms of service of another website or any other contractual obligations.
                                </li>
                                <li>
                                    Seeking, offering, or endorsing any services that violate the academic policies of any
                                    educational institution.
                                </li>
                                <li>
                                    Fraudulently billing or attempting to fraudulently bill any Hirer, including by (i) falsifying or
                                    manipulating or attempting to falsify or manipulate the hours, keystrokes, or mouse clicks
                                    recorded in the Upwork Team App, (ii) reporting, recording, or otherwise billing clients for time
                                    that was not actually worked, or (iii) reporting, recording, or otherwise billing hours worked by
                                    another person as hours worked by you in a way that is misleading or false.
                                </li>
                                <li>
                                    Expressing a preference in a job post or job bid or otherwise unlawfully discriminating on the
                                    basis of race, religion, colour, national origin, ancestry, physical or mental disability, medical
                                    condition, genetic information, marital status, sex, gender, gender identity, gender expression,
                                    age, sexual orientation, military/veteran status or any basis protected by applicable law.
                                </li>
                                <li>Posting content that is harassing towards another person or violates the rights of a third party.</li>
                                <li>Posting identifying information concerning another person.</li>
                                <li>Making or demanding bribes.</li>
                                <li>Making or demanding payments without the intention of providing or receiving services in
                                    exchange for the payment.
                                </li>
                                <li>Spamming other Users.</li>
                                <li>
                                    Using any robot, spider, scraper, or other automated means to access the Site for any
                                    purpose without our express written permission or collecting or harvesting any personally
                                    identifiable information, including Account names, from the Site.
                                </li>
                                <li>
                                    Engaging in any conduct that is reasonably likely to or that is intended to harm the Site,
                                    including (i) imposing an unreasonable or disproportionately large load (in our sole discretion)
                                    on the Site’s infrastructure; (ii) interfering or attempting to interfere with the proper operation
                                    of the Site or Site Services or any activities conducted on the Site; (iii) bypassing any
                                    measures we may use to prevent or restrict access to the Site or any subparts of the Site,
                                    including, without limitation, features that prevent or restrict use or copying of any content or
                                    enforce limitations on use of the Site or the content therein; or (iv) attempting to interfere with
                                    or compromise the system integrity or security or decipher any transmissions to or from the
                                    servers running the Site.
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h3>1.2 Using the Site Other than For the Intended Purposes</h3>
                            <p>
                                Healthcare-Temps makes the Site and Services available for Hirers and Temp workers to find one
                                another, enter into service relationships, make and receive payments through escrow, and receive
                                and perform Temp Services. Users are expected to use the Site and Services for their intended
                                purposes and Users may not use the Site and Services in contravention of their intended purposes.<br />

                                The following are examples of prohibited use of the Site:
                            </p>
                            <ul class="dotted_ul">
                                <li>Offering services for the sole purpose of obtaining positive feedback of any kind.</li>
                                <li>
                                    Requesting, demanding, or receiving free services, including requesting Temp workers to
                                    submit work as part of the bid process for very little or no money or posting contests in which
                                    Temp workers submit work with no or very little pay, and only the winning submission is paid
                                    the full amount.
                                </li>
                                <li>
                                    Requesting a fee before allowing another User to submit a bid.
                                </li>
                                <li>
                                    Posting the same job multiple times so that more than one version remains active at a given
                                    time.
                                </li>
                                <li>
                                    Withholding payment or Work Product or engaging in any other conduct for the purpose of
                                    obtaining positive feedback from another User.
                                </li>
                                <li>
                                    Attempting to falsify, manipulate, or coerce another User by threatening to give negative
                                    feedback.
                                </li>
                                <li>
                                    Misusing the feedback feature to express views unrelated to the work, such as political,
                                    religious, or social commentary.
                                </li>
                                <li>Duplicating or sharing accounts.</li>
                                <li>
                                    Selling, trading, or giving an account to another person without Upwork’s consent.
                                </li>
                                <li>
                                    Sharing or soliciting contact information such as email, phone number, or Skype ID in a profile
                                    or job post.
                                </li>
                                <li>
                                    Soliciting or processing payment outside of Healthcare-Temps web or mobile application in
                                    violation of the Terms of Service.
                                </li>
                                <li>
                                    Advertising products or services that are outside or beyond the scope of the Healthcare-
                                    Temps marketplace.
                                </li>
                                <li>
                                    Advertising on Healthcare-Temps to recruit Temp workers to join an Agency or another
                                    website or company.
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h3>1.3 Using the Site to Post False or Misleading Content</h3>
                            <p>
                                All profiles, jobs, bids, and other content posted to the Site must be truthful and not misleading. Here
                                are examples of prohibited uses:
                            </p>
                            <ul class="dotted_ul">
                                <li>
                                    Misrepresenting a Temp worker’s registration, qualification, experience, skills, or
                                    information.
                                </li>
                                <li>
                                    Impersonating any person or entity, including, but not limited to, a Healthcare-Temps
                                    representative, forum leader, guide or host, or falsely stating or otherwise misrepresenting
                                    your affiliation with a person or entity.
                                </li>
                                <li>
                                    Falsely attributing statements to any Healthcare-Temp representative, forum leader, guide or
                                    host.
                                </li>
                                <li>
                                    Allowing another person to use your account, which is misleading to other Users.
                                </li>
                                <li>
                                    Falsely stating or implying a relationship with Healthcare-Temps or with another company
                                    with whom you do not have a relationship.
                                </li>
                                <li>
                                    Falsely stating or implying a relationship with another User, including an Agency continuing to
                                    use a Temp worker’s profile or information after the Temp worker no longer works with the
                                    Agency.
                                </li>
                                <li>
                                    Falsely stating that one Temp Worker will perform the work on a job when another will in fact
                                    perform the work, including submitting a proposal on behalf of a Temp Worker that is unable,
                                    unwilling, or unavailable to do the work.
                                </li>
                            </ul>
                        </li>
                    </ol>
                </li>

                <li>
                    <h3>2. ENFORCEMENT</h3>
                    <p>
                        We reserve the right, but do not assume the obligation, to investigate any violation of these Terms.
                        We may investigate violations and may remove, disable access to, or modify any content that violates
                        these Terms.<br />
                        We may report any activity that we suspect violates any law or regulation to appropriate law
                        enforcement officials, regulators, or other appropriate third parties. Our reporting may include
                        disclosing appropriate User information. We also may cooperate with appropriate law enforcement
                        agencies, regulators, or other appropriate third parties to help with the investigation and prosecution
                        of illegal conduct by providing network and systems information related to alleged violations of these
                        Terms.
                    </p>
                </li>

                <li>
                    <h3>REPORTING AND CORRECTING VIOLATIONS</h3>
                    <p>
                        If you become aware of any violation of these Terms, you must immediately report it to the
                        Administrator. You agree to assist us with any investigation we undertake and to take any remedial
                        steps we require in order to correct a violation of these Terms.
                    </p>
                </li>
            </ol>
        </div>

    </div>
    <!--<footer id="footer" class="footer sep-top top-space mob-space footer_2">-->
</div>

<!-- Include footer.php. Contains footer content. -->
<?php include ('includes/template/footer.php'); ?>

<a id="scrollup">Scroll</a>

<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js" type="text/javascript"></script>
<!-- Bootstrap 3.3.6 JS -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Kafe JS -->
<script src="assets/js/kafe.js" type="text/javascript"></script>

</body>
</html>
